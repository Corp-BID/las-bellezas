<?php

$conf_tienda_obj = new Config_Tienda();
$monedas_obj = new Monedas();

$_SESSION['DatosMonedaDestino'] = null;
$_SESSION['TasaDestino'] = null;

//========buscar siempre la moneda por defecto ==========//
$DatosMoneda = $conf_tienda_obj->DefaultCoin($_DB_);
$_SESSION['DatosMonedaDefecto'] = $DatosMoneda;

//========cambio de moneda del carrito ==========//
$moneda = (isset($_GET['moneda']) && $_GET['moneda'])?intval($_GET['moneda']):NULL;

if (!empty($moneda)){

    if ($_SESSION['DatosMonedaDefecto'][0]['id'] != $moneda) {
        $MonedaDestino = $monedas_obj->MonedaDestinoCambio($_DB_, $moneda);
        $_SESSION['DatosMonedaDestino'] = $MonedaDestino;
    }
}

$_SESSION['ValorCalculo'] = 1;

if ($moneda) {
    if ($_SESSION['DatosMonedaDefecto'][0]['id'] == $moneda) {
        $_SESSION['ValorCalculo'] = 1;
    } else {
        if (!empty($_SESSION['DatosMonedaDestino'][0]['id'])) {
            $conf_tienda_obj = new Config_Tienda();
            $MonedaDestino = $conf_tienda_obj->TasaDestino($_DB_, $_SESSION['DatosMonedaDestino'][0]['id']);
            $_SESSION['TasaDestino'] = $MonedaDestino[0]['id'];
            $_SESSION['ValorCalculo'] = $MonedaDestino[0]['valor_calculo'];
        }
    }
}