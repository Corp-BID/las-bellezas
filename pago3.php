<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");
include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");

$pedido = new Pedidos();
$cliente = new Clientes();
$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$datos_pedido = $pedido -> ConsultarPedido($_DB_, $_SESSION['id_pedido']);
$productos = $pedido -> ConsultarItemsPedido($_DB_, $_SESSION['id_pedido']);
$datos_facturacion = $cliente -> obtenerDatosFacturacion($_DB_, $datos_pedido[0]['datos_facturacion']);

$banco = (isset($_POST['banco']) && $_POST['banco'])?substr(strval($_POST['banco']), 0, 300) :NULL; // limitamos a 3 el size del strin
$referencia = (isset($_POST['referencia']) && $_POST['referencia'])?substr(strval($_POST['referencia']), 0, 300) :NULL; // limitamos a 3 el size del string
$monto = (isset($_POST['monto']) && $_POST['monto'])?substr(strval($_POST['monto']), 0, 300) :NULL; // limitamos a 3 el size del string
$fecha = (isset($_POST['fecha']) && $_POST['fecha'])?substr(strval($_POST['fecha']), 0, 300) :NULL; // limitamos a 3 el size del string

?>

<div class="container">
    <div class="row">
        
        
        <div class="col-sm-12"> 
            <div class="table-responsive">
                    
                <h2>Resumen de su Pedido</h2>
                
                    <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td class="text-center"></td>
                        <td class="text-left">Código</td>
                        <td class="text-left">Producto</td>
                        <td class="text-right">Cantidad</td>
                        <td class="text-right">Precio Unitario</td>
                        <td class="text-right">Total</td>
                        <td></td>
                    </tr>
                    </thead>
                       
                    <tbody>
                    <?
                    $total_piezas = 0;
                    $total_bolivares = 0;     
                
                    $produc = new Productos();
                        
                    foreach($productos as $prod)
                    {
                        $producto = $produc -> Consultar($_DB_, $prod['codigo_producto']);
                        ?>
                        
                    
                    <tr>
                        
                        <td class="text-center" width="100">                  
                            <img src="./Productos/<?php echo $producto[0]['foto'];?>"  onerror="this.src='./Productos/no_disponible.jpg';" class="img-responsive" alt="<? echo $prod['descripcion'];?>" title="<? echo $prod['descripcion'];?>">
                        </td>
                        <td class="text-left"><? echo $prod['codigo_producto'];?></td>
                        <td class="text-left"><? echo $prod['descripcion'];?><br><? echo $prod['marca'];?><br></td>
                        <td class="text-right"><? echo $prod['cantidad']; $total_piezas += $prod['cantidad'];?></td>
                        <td class="text-right"><?= $simbolo." ". number_format($prod['precio_unitario'] * $ValorCalculo,2,",",".");?></td>
                        <td class="text-right"><?= $simbolo." ". number_format($prod['precio_total'] * $ValorCalculo,2,",","."); $total_bolivares += $prod['precio_total'] ;?></td>
                        <td></td>
                    </tr>
                    <?
                    }?>    
                    </tbody>
                    </table>
                </div>
   
                <br>
                
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td class="text-right"><strong>Total a pagar:</strong></td>
                            <td class="text-right"><?=$simbolo." ".number_format($total_bolivares* $ValorCalculo,2,",",".");?></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
        
        </div>
        
        
        <div  class="col-sm-6">      
            
            <h2>Datos de Facturación</h2>
           
            <table class="table table-bordered">
            <tr>
                <td><b>Nombre o Razon Social:</b> <? echo $datos_facturacion[0]['nombre'];?></td>    
            </tr>
            <tr>
                <td><b>Cedula o Rif:</b> <? echo $datos_facturacion[0]['cedula'];?></td>    
            </tr>
            <tr>
                <td><b>Teléfono:</b> <? echo $datos_facturacion[0]['telefonos'];?></td>    
            </tr>
            <tr>
                <td><b>Estado:</b> <? echo $datos_facturacion[0]['estado'];?></td>    
            </tr>
            <tr>
                <td><b>Dirección:</b> <? echo $datos_facturacion[0]['direccion'];?></td>    
            </tr>
            <tr>
                <td><b>Dirección de Entrega:</b> <? echo $datos_facturacion[0]['direccion2'];?></td>    
            </tr>
            <tr>
                <td><b>Tipo de Entrega:</b> <? echo $datos_facturacion[0]['tipo_entrega'];?></td>    
            </tr>
            <tr>
                <td><b>Observaciones:</b> <? echo $datos_facturacion[0]['observaciones'];?></td>    
            </tr>
            </table>
          
        </div>
        
        
        <div class="col-sm-6">      
            
            <h2>Reportar Pago</h2>
            
            <table class="table table-bordered">
            <tr>
                <td><b>Banco que Utilizo:</b> <? echo $banco;?></td>    
            </tr>
            <tr>
                <td><b>Número de Referencia o Recibo:</b> <? echo $referencia;?></td>    
            </tr>
            <tr>
                <td><b>Monto:</b> <? echo $monto;?></td>    
            </tr>
            <tr>
                <td><b>Fecha:</b> <? echo $fecha;?></td>    
            </tr>
            </table>

        </div>
        
        
        <div class="col-sm-12">      
        
        <form action="./pago_o.php" method="POST">
        <input type="hidden" name="op" value="pago">
        <input type="hidden" name="banco" value="<? echo $banco;?>">
        <input type="hidden" name="referencia" value="<? echo $referencia;?>">
        <input type="hidden" name="monto" value="<? echo $monto;?>">
        <input type="hidden" name="fecha" value="<? echo $fecha;?>">
            
              
                <div class="text-center"><input type="submit" value="Guardar y Pagar" class="btn btn-primary"></div>
        </form>
    </div>
        
    </div>
</div>


<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");