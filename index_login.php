<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : '';
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? strval($_GET['sc']) : '';
$pag = (isset($_GET['pag']) && $_GET['pag']) ? strval($_GET['pag']) : '';
// Ver Header
?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Ingresar al Sistema</h2>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li> Login </li>
            </ul>
        </div>
    </div>
</div>


<!-- login-area start -->
<div class="register-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12 col-lg-6 col-xl-6 ml-auto mr-auto">
                <div class="login">
                    <div class="login-form-container">
                        <div class="login-form">
                            <form action="./login.php" method="post">
                                <input type="text" name="user-name" placeholder="Correo Electrónico">
                                <input type="password" name="user-password" placeholder="Clave">
                                <div class="button-box">

                                    <div class="login-toggle-btn">
                                        <div class="text-left pt-2">
                                            <a class="text-left " href="./register.php">Registrate...</a>
                                        </div>

                                        <a class="text-right" href="javascripts:void(0);" data-toggle="modal" data-target="#exampleModal">¿Olvido su contraseña?</a>
                                    </div>
                                    <button type="submit" class="default-btn floatright">Ingresar</button>
                                </div>
                                <input type="hidden" name="pag" value="<?=$pag;?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

<div class="modal" id="exampleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Resetear la contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="pl-5">
                <code><strong>Importante</strong></code><p>Para recuperar la contraseña debe proporcionar el Correo Electronico con el que se registró,
                    se le enviará un código de validación, revise su correo </p>
            </div>

            <div id="alertEmail" class="row col-12" style="display: none">
                <div  class="pl-5">
                    <div class="alert alert-danger" role="alert">
                        Correo no se encuentra registrado!
                    </div>
                </div>
            </div>

            <form id="frm_validar_email">
                <div class="modal-body pt-0">

                    <input type="text" name="email" placeholder="Correo Electrónico">
                    <input type="hidden" name="op" value="validarCorreo">
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="enviar_email">Enviar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="codigoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Resetear la contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="pl-5">
                <p>Ingrese el código de validación</p>
            </div>
            <div id="alertCodigo" class="row col-12" style="display: none">
                <div class="pl-5">
                    <div class="alert alert-danger" role="alert">
                        Código de validación incorrecto!
                    </div>
                </div>
            </div>

            <form id="frm_validar_codigo">
                <div class="modal-body pt-0">
                    <input type="text" name="codigo" placeholder="Código de Validación">
                    <input type="hidden" name="op" value="validarCodigo">

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="enviar_codigo">Validar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="ResetModal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Resetear la contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="pl-5">
                <p>Cambio de Contraseña</p>
            </div>

            <div id="alertPassword" class="row col-12" style="display: none">
                <div class="pl-5">
                    <div class="alert alert-danger" role="alert">
                        Contraseñas no coinciden!
                    </div>
                </div>
            </div>

            <form id="frm_reset_password">
                <input type="hidden" name="op" value="reset">
                <input type="hidden" name="validar" name="validar" value="">
                <div class="modal-body pt-0">
                    <input type="password" name="password" id="password" placeholder="Contraseña Nueva">
                </div>

                <div class="modal-body pt-0">
                    <input type="password" name="cpassword" id="cpassword" placeholder="Repita la contraseña">
                </div>

            </form>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="reset">Resetear</button>
            </div>
        </div>
    </div>
</div>


<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#enviar_email',function ()
    {
        $("#alertEmail").hide();
        if ($('#email').val() != '')
        {
            let formData = new FormData(document.getElementById('frm_validar_email'));
            axios.post( './reset_password.php', formData)
                .then(response => {
                    if (response.data.success)
                    {
                        $('#exampleModal').modal('hide');
                        $('#codigoModal').modal('show');
                    }else {
                        $("#alertEmail").show();
                        $('#op').val(response.data.op);
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    });

    $("#exampleModal").on("hidden.bs.modal", function() {
        $(this)
            .find("input")
            .val('')
            .end();
    });

    $(document).on('click', '#enviar_codigo',function ()
    {
        $("#alertCodigo").hide();
        if ($('#codigo').val() != '')
        {
            let formData = new FormData(document.getElementById('frm_validar_codigo'));
            axios.post( './reset_password.php', formData)
                .then(response => {
                    if (response.data.success[0]['id'])
                    {
                        $('input[name="validar"]').val(response.data.success[0]['id']);
                       // $('#id').val(response.data.success);
                        $('#codigoModal').modal('hide');
                        $('#ResetModal').modal('show');
                    }else
                        $("#alertCodigo").show();
                })
                .catch(error => {
                    console.log(error);
                })
        }
    });

    $("#codigoModal").on("hidden.bs.modal", function() {
        $(this)
            .find("input")
            .val('')
            .end();
    });

    $(document).on('click', '#reset',function ()
    {
        if ($('#password').val() == $('#cpassword').val())
        {
            $("#alertPassword").hide();
            let formData = new FormData(document.getElementById('frm_reset_password'));
            axios.post( './reset_password.php', formData)
                .then(response => {
                    if (response.data.success)
                    {
                        Swal.fire(
                            'Información',
                            'Su contraseña ha sido actualizada',
                            'info'
                        );
                        window.location.reload(true);
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }else
            $("#alertPassword").show();
    });

    $("#ResetModal").on("hidden.bs.modal", function() {
        $(this)
            .find("input")
            .val('')
            .end();
    });


});
</script>