<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
?>

<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Bienvenidos</h2>
            <ul>
                <li><a href="#">Inicio</a></li>
                <li> Bienvenidos </li>
            </ul>
        </div>
    </div>
</div>


<div class="about-services pt-20">
    <div class="container">
        <div class="alert alert-success text-center" role="alert">
            <h4>Hola <? echo $_SESSION['cliente']['nombres'].' '.$_SESSION['cliente']['apellidos'];?> (<? echo $_SESSION['cliente']['correo'];?>) bienvenidos a Express Charcuteria</h4>
        </div>

        <center>
        <a href="./listado.php" class="btn btn-outline-danger" role="button">Ver Listado de Productos</a>
        <a href="./historico_pedido.php" class="btn btn-outline-success" role="button">Mis Pedidos</a>
        </center>
       
    </div>
</div>



<div class="about-story ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="about-story">
                    <h3 class="story-title">Sobre Express Charcuteria</h3>
                    <p class="story-subtitle">
                        Nacimos hace 3 años con un solo objetivo llevar a su mesa los mejores productos en charcutería, y es que a quien no le gusta un buen sándwich, un warp o nuestra tradicional arepa rellena con jamón, queso, salchichón mortadela y para usted de contar.
                    </p>
                    <p class="story-subtitle">
                        ¿Se te hizo agua la boca?... En Charcutería Express encontrarás de todo para engalanar tu mesa, productos frescos de altísima calidad que solo tu paladar podrá probar.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="about-story-img">
                    <div class="about-story-img1">
                        <img src="assets/img/team/1.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="brand-logo ptb-100">
        <div id="demo">
            <div class="container">
                <div class="section-title-others text-center mb-55">
                    <h2>Nuestras Marcas</h2>
                </div>
                <div class="row">
                    <div class="col-12">

                        <div id="owl-demo" class="owl-carousel">
                            <div class="item"><img src="./Marca/1.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/2.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/3.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/4.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/5.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/6.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/7.png" alt="Owl Image"></div>
                            <div class="item"><img src="./Marca/8.png" alt="Owl Image"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>
    $(document).ready(function() {

        $("#owl-demo").owlCarousel({
            autoplay: true,
            dots: true,
            loop: true,
            items: 4
        });

    });
</script>
