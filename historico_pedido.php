<?php

include_once(realpath(dirname(__FILE__))."/include/header.php");
//include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");
$ped = new Pedidos();

$pedidos = $ped -> ListadoPedidoClientes($_DB_, $_SESSION['cliente']['correo']);

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

foreach($pedidos as $posi => $pedido)
{

    $validar_monto = $ped -> ConsultarMontoItemPedido($_DB_, $pedido['id_pedido']);

    if($pedido['monto_total'] != $validar_monto)
    {
        $ped -> ModificarMontoTotal($_DB_, $pedido['id_pedido'], $validar_monto);
    }    
}

$pedidos = $ped -> ListadoPedidoClientes($_DB_, $_SESSION['cliente']['correo']);
?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="./index.php">Inicio</a></li>
                <li> Historico de Pedidos </li>
            </ul>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-12"> 
            <div class="table-responsive">
                <h2>Historico de Pedidos</h2>
                    <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Número Pedido</th>
                        <th class="text-left">Fecha</th>
                        <th class="text-right">Monto $</th>
                        <th class="text-right">Monto Bs</th>
                        <th class="text-center">Estatus</th>
                        <th class="text-center">Pago Validado</th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                       
                    <tbody>
                        
                    <?php
                    foreach($pedidos as $posi => $pedido)
                    {
                        $tasaCompra = ($pedido['valor_calculo'] > 0 ) ? $pedido['valor_calculo'] : 1;
                        ?>
                            <tr>
                                <td class="text-center"><?= $pedido['id_pedido'];?></td>
                                <td class="text-left"><?php
                                    $objeto_DateTime = DateTime::createFromFormat('Y-m-d H:m:s', $pedido['fecha']);
                                    $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                                    echo $cadena_nuevo_formato;?>
                                </td>
                                <td class="text-right"><?= "$ ". number_format($pedido['monto_total'] ,2,",",".");?></td>
                                <td class="text-right"><?= "Bs ".number_format($pedido['monto_total'] * $tasaCompra,2,",",".");?></td>
                                <td class="text-center">
                                    <?php
                                    echo $pedido['condicion'];
                                    ?>
                                </td>
                                <td class="text-center"><?php
                                     $confirmada = ($pedido['validado']) ? 'Si' : 'No';
                                     echo $confirmada;
                                    ?>
                                </td>
                                <td>
                                     <div class='btn-group mx-3'>
                                         <a href="./ver_pedido.php?p=<?php echo $pedido['id_pedido'];?>" alt="Ver Pedido" title="Ver Pedido">
                                             <span class='btn btn-info '><i class='ion-search'></i></span>
                                         </a>

                                         <?php
                                         if ($pedido['id_condicion'] == 0){
                                         ?>
                                         <a href="./pago2.php?id=<?php echo $pedido['id_pedido'];?>" alt="Pagar" title="Pagar">
                                             <span class='btn btn-success'><i class='ion-card'></i></span>
                                         </a>
                                         <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        <?php
                    }?>
                    </tbody>
                    </table>
                </div>
   
                <br>
        </div>
    </div>
        
</div>


<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");