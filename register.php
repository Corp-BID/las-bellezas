<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
?>
<style>
	#frmregistro {
		width: 500px;
	}
	#frmregistro label {
		width: 250px;
	}
	#frmregistro label.error, #frmregistro input.submit {
		color: red;
	}
</style>
<?php
$mensaje_cliente_registrado = array();
$mensaje_cliente_nuevo = array();

if (isset($_POST['registro']) == 1) {
    $email = (isset($_POST['email']) && $_POST['email']) ? strval($_POST['email']) : NULL;
    $password = (isset($_POST['password']) && $_POST['password']) ? strval($_POST['password']) : NULL;
    $cedula = (isset($_POST['cedula']) && $_POST['cedula']) ? strval($_POST['cedula']) : NULL;
    $nombre = (isset($_POST['nombre']) && $_POST['nombre']) ? strval($_POST['nombre']) : NULL;
    $apellido = (isset($_POST['apellido']) && $_POST['apellido']) ? strval($_POST['apellido']) : NULL;
    $telefono = (isset($_POST['telefono']) && $_POST['telefono']) ? strval($_POST['telefono']) : NULL;

    $cliente = new Clientes();

    $cliente_registrado = $cliente->ValidarCorreo($_DB_, $email);



    if (!$cliente_registrado['correo']) {
        $cliente->AgregarCliente($_DB_, $email, $password, $cedula, $nombre, $apellido, $telefono);
       // $mensaje_cliente_nuevo = 1;

        $mensaje_cliente_nuevo = [
            'error' =>0,
            'login'=>'<a href="./index_login.php">Iniciar sesión </a>',
            'message'=>'Su Usuario se Registro Correctamente'
        ];
    } else {
        //$mensaje_cliente_registrado = 1;
        $mensaje_cliente_registrado = [
            'error' =>1,
            'email'=>$email,
            'message'=>'<strong>Advertencia:</strong> Disculpe ese Correo Electrónico ya se encuentra registado'
        ];
    }
}

?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Regístrate</h2>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li> Regístrate </li>
            </ul>
        </div>
    </div>
</div>


<!-- register-area start -->
<div class="register-area ptb-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-12 col-lg-6 col-xl-6 ml-auto mr-auto">
                <div class="login">
                    <div class="login-form-container">

                        <?php
                        if (count($mensaje_cliente_nuevo) > 0 ) { ?>
                            <div class="alert alert-success text-center" role="alert">
                                <p><?=$mensaje_cliente_nuevo['message'];?></p>
                                <p><?=$mensaje_cliente_nuevo['login'];?></p>
                            </div>
                        <?php
                        }

                        if (count($mensaje_cliente_registrado) > 0 ) { ?>
                            <div class="alert alert-danger text-center" role="alert">
                                <p><?=$mensaje_cliente_registrado['email'];?></p>
                                <p><?=$mensaje_cliente_registrado['message'];?></p>
                            </div>
                        <?php
                        } ?>


                        <div class="login-form">
                            <form action="register.php" method="POST" name="frmregistro" id="frmregistro" class="form-horizontal">
                                <input type='hidden' name="registro" id="registro" value="1">
                                <input type="email" name="email" placeholder="Correo Electrónico">
                                <input type="password" name="password" placeholder="Clave">
                                <input type="text" name="cedula" placeholder="Cédula">
                                <input type="text" name="nombre" placeholder="Nombres">
                                <input type="text" name="apellido" placeholder="Apellidos">
                                <input type="text" name="telefono" placeholder="Teléfonos">

                                <div class="button-box">
                                    <button type="submit" class="default-btn floatright">Registrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>

<script>
    $("#frmregistro").validate({
        normalizer: function( value ) {
            return $.trim( value );
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            password:{
                required: true,
                minlength: 4,
                maxlength: 8
            },
            cedula:{
                required: true,
                minlength: 4
            },
            nombre:{
                required: true
            },
            apellido:{
                required: true
            },
            telefono:{
                required: true,
                maxlength: 11,
                minlength: 11
            }
        },
        messages: {
            email: "Por favor agregue un correo válido"
        }
    })
</script>