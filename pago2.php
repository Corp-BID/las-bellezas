<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");

$pedido = new Pedidos();
$cliente = new Clientes();
$id_pedido = (isset($_GET['id']) && $_GET['id']) ? substr(strval($_GET['id']), 0, 6) : $_SESSION['id_pedido'];

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$datos_pedido = $pedido->ConsultarPedido($_DB_, $id_pedido);
$datos_facturacion = $cliente->obtenerDatosFacturacion($_DB_, $datos_pedido['data'][0]['datos_facturacion']);
?>
<style>
    .visible {
        visibility: visible;
    }

    .ocultar {
        visibility: hidden;
    }
</style>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Reportar Pago</h2>
            <ul>
                <li><a href="./index.php">Inicio</a></li>
                <li> Reportar Pago </li>
            </ul>
        </div>
    </div>
</div>

<div class="cart-main-area pt-95 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <h2>Resumen de su Pedido</h2>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-left">Producto</td>
                                <td class="text-right">Cantidad</td>
                                <td class="text-right">Precio Unitario</td>
                                <td class="text-right">Total</td>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $total_piezas = 0;
                            $total_bolivares = 0;

                            foreach ($datos_pedido['dataItems'] as $producto) { ?>
                                <tr>
                                    <td class="product-thumbnail text-center">
                                        <a href="#"><img src="./Productos/thumbs/<?php echo $producto['foto']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" class="img-responsive" width="100" alt="<?php echo $producto['descripcion']; ?>" title="<?php echo $producto['descripcion']; ?>"></a>
                                    </td>
                                    <td class="text-left"><?php echo $producto['nombre']; ?><br><?php echo $producto['descripcion_marca']; ?><br></td>
                                    <td class="text-right"><?php echo $producto['cantidad'];?></td>
                                    <td class="text-right"><?= $simbolo . " " . number_format($producto['precio_unitario'] * $ValorCalculo, 2, ",", "."); ?></td>
                                    <td class="text-right"><?= $simbolo . " " . number_format($producto['precio_total'] * $ValorCalculo, 2, ",", ".");?></td>
                                </tr>
                            <?php
                                $total_bolivares += $producto['precio_total'];
                                $total_piezas += $producto['cantidad'];
                            } ?>

                            <tr>
                                <td class="text-right" colspan="4"><strong>Total a pagar:</strong></td>
                                <td class="text-right"><?= $simbolo . " " . number_format($total_bolivares * $ValorCalculo, 2, ",", "."); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>

            <div class="col-sm-12">
                <h2>Datos de Facturación</h2>
                <table class="table table-bordered">
                    <tr>
                        <td><b>Nombre o Razon Social:</b> <?php echo $datos_facturacion[0]['nombre']; ?></td>
                    </tr>
                    <tr>
                        <td><b>Cedula o Rif:</b> <?php echo $datos_facturacion[0]['cedula']; ?></td>
                    </tr>
                    <tr>
                        <td><b>Teléfono:</b> <?php echo $datos_facturacion[0]['telefonos']; ?></td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-8">
                        <table class="table table-bordered">
                            <tbody>
                                <tr style="background-color:#f6e597">
                                    <td colspan="2" align="center">
                                        <p><strong>Seleccion un metodo de entrega</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center opcion_entrega">
                                        <input type="radio" id="opcion_entrega" name="opcion_entrega" value="retirar"
                                               style="width: 15px;height: 10px"> Retirar en Tienda
                                    </td>
                                    <td class="text-center opcion_entrega">
                                        <input type="radio" id="opcion_entrega" name="opcion_entrega" value="enviar"
                                               style="width: 15px;height: 10px"> Envio a Domicilio
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="datos_envio" style="display: none">
                    <div class="col-md-12">
                        <div class="checkout-form-list">
                            <label>Dirección Principal</label>
                            <input type="text" name="direccion" id="direccion" placeholder="">
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="checkout-form-list">
                            <label>Dirección Secundaria</label>
                            <input type="text" name="direccion2" id="direccion2" placeholder="Especificque la Dirección de Entrega en caso de no ser la anterior">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="checkout-form-list">
                            <label>Observaciones</label>
                            <input type="text" name="observaciones" id="observaciones" placeholder="">
                        </div>
                    </div>


                </div>

                <div id="datos_retiro" style="display: none">
                    <div class="col-md-12">
                        <div class="checkout-form-list">
                            <label>Dirección</label>
                            <p><code>Nota:</code>Para realizar el retiro de su pedido debe
                                dirigirse a nuestra tienda ubicada en Santa Mónica</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6" id="reportar_pago" name="reportar_pago">
                <form action="./pago_o.php" method="POST" class="form-horizontal" id="frm_reporte_pago" name="frm_reporte_pago">
                    <input type="hidden" name="op" value="pago">
                    <input type="hidden" id="monto" name="monto" value="<?= $total_bolivares; ?>">
                    <?php
                    $showHideNacional = '';
                    $showHideInternacional = '';
                    if (isset($DatosMoneda[0]['simbolos'])) {
                        $checkedNacional = '';
                        $checkedInternacional = '';
                        if (strtolower($DatosMoneda[0]['simbolos']) == 'bs') {
                            $checkedNacional = 'checked';
                            $showHideInternacional = 'style="display:none"';
                        }

                        if ((strtolower($DatosMoneda[0]['simbolos'])) != 'bs') {
                            $checkedInternacional = 'checked';
                            $showHideNacional = 'style="display:none"';
                        }
                    }

                    ?>
                    <div class="row">
                        <div class="col-sm-12 col-sm-offset-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr style="background-color:#f6e597">
                                        <td colspan="4" align="center">
                                            <p><strong>Forma de Pago</strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center forma_pago"> <input type="radio" name="forma_pago" value="efectivo" style="width: 15px;height: 10px"> Efectivo</td>
                                        <td class="text-center forma_pago"> <input type="radio" name="forma_pago" value="transferencia" style="width: 15px;height: 10px"> Transfrencia</td>
                                        <td class="text-center forma_pago"> <input type="radio" name="forma_pago" value="pago movil" style="width: 15px;height: 10px"> Pago Móvil</td>
                                        <td class="text-center forma_pago"> <input type="radio" name="forma_pago" value="punto de venta" style="width: 15px;height: 10px"> Punto de Venta</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="col-md-12" id="observacionEfectivo" style="display: none">
                                    <label>Observaciones</label>
                                    <textarea cols="3" name="observacion_efectivo" id="observacion_efectivo" placeholder="Coloque alguna observacion referente a pago" maxlength="200"></textarea>
                            </div>

                            <div class="col-sm-6">
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" id="efectivo_reportar_pago_submit" name="efectivo_reportar_pago_submit" class="btn btn-primary reportar_pago_submit">Reportar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="transferencia" name="transferencia" class="" style="display: none">
                        <div class="row">
                            <div class="col-sm-12 col-sm-offset-8">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <p><strong>Seleccion tipo de transferencia</strong></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center opcion_pago"> <input type="radio" name="opcion_pago" value="nacional" <?= $checkedNacional; ?> style="width: 15px;height: 10px">Nacional</td>
                                            <td class="text-center opcion_pago"> <input type="radio" name="opcion_pago" value="internacional" <?= $checkedInternacional; ?> style="width: 15px;height: 10px">Internacional</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div id="ReporteNacional" name="ReporteNacional" <?= $showHideNacional; ?>>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Banco que Utilizó</label>
                                <div class="col-sm-12">
                                    <select name='bancoNacional' id="bancoNacional" class="bancoNacional" data="bancoNacional">
                                        <option value=''>Seleccione un Banco</option>
                                        <option value='Banesco'>Banesco</option>
                                        <option value='Banplus'>Banplus</option>
                                        <option value='Bnc'>BNC</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="referenciaNacional" class="col-sm-12 control-label">Número de Referencia o Recibo</label>

                                <div class="col-sm-12">
                                    <input type="text" name="referenciaNacional" class="form-control" id="referenciaNacional" placeholder="Numero">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="fechaNacional" class="col-sm-5 control-label">Fecha de la Trasferencia</label>
                                <div class="col-sm-6">
                                    <input class="fechaNacional" maxlength="10" type="text" name="fechaNacional" id="fechaNacional" value="<?= date("d-m-Y"); ?>" onkeydown="return false">
                                </div>
                                <div class="col-sm-6">
                                    <code>XX-XX-XXXX</code>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" id="transf_reportar_pago_submit" name="transf_reportar_pago_submit" class="btn btn-primary reportar_pago_submit">Reportar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ReporteInternacional" name="ReporteInternacional" <?= $showHideInternacional; ?>>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Banco que Utilizó</label>
                                <div class="col-sm-12">
                                    <input type="text" id="bancoInternacional" name="bancoInternacional" class="bancoInternacional">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="referenciaInternacional" class="col-sm-12 control-label">Número de Referencia o Recibo</label>

                                <div class="col-sm-12">
                                    <input type="text" name="referenciaInternacional" class="form-control" id="referenciaInternacional" placeholder="Numero">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="fechaInternacional" class="col-sm-5 control-label">Fecha de la Trasferencia</label>
                                <div class="col-sm-6">
                                    <input class="fechaInternacional" maxlength="10" type="text" name="fechaInternacional" id="fechaInternacional" value="<?= date("d-m-Y"); ?>" onkeydown="return false">
                                </div>
                                <div class="col-sm-6">
                                    <code>XX-XX-XXXX</code>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" id="transf_reportar_pago_submit" name="transf_reportar_pago_submit" class="btn btn-primary reportar_pago_submit">Reportar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<?php include_once(realpath(dirname(__FILE__)) . "/include/footer.php"); ?>

<script>
    $(function() {

        $('.fechaNacional').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });

        $('.fechaInternacional').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });

        $('input[type=radio][name=forma_pago]').on('change', function() {
            if ($(this).val() == 'pago movil'){
                const rbs = document.querySelectorAll('input[name="opcion_pago"]');
                for (const rb of rbs) {
                    if (rb.value == 'nacional') {
                        rb.checked = true;
                        const e = new Event("change");
                        rb.dispatchEvent(e);
                        break;
                    }
                }
            }
        });

        $('#frm_reporte_pago').on('click', '.reportar_pago_submit', function(e) {
            e.preventDefault();
            $("#frm_reporte_pago").submit();
        });

        $('#frm_reporte_pago').submit(function(e) {
            e.preventDefault();
            if (document.querySelector("input[name=opcion_entrega]:checked")) {
                var form = document.getElementById('frm_reporte_pago');
                value = document.querySelector("input[name=opcion_entrega]:checked").value;

                var input = document.createElement('input');
                input.setAttribute('name', 'opcion_entrega');
                input.setAttribute('value', value);
                input.setAttribute('type', 'hidden');
                form.appendChild(input);

                if (value == 'enviar') {
                    if (document.getElementById('direccion').value != '') {


                        var direccion = document.createElement('input');
                        direccion.setAttribute('name', 'direccion');
                        direccion.setAttribute('value', document.getElementById('direccion').value);
                        direccion.setAttribute('type', 'hidden');
                        form.appendChild(direccion);

                        var direccion2 = document.createElement('input');
                        direccion2.setAttribute('name', 'direccion2');
                        direccion2.setAttribute('value', document.getElementById('direccion2').value);
                        direccion2.setAttribute('type', 'hidden');
                        form.appendChild(direccion2);

                        var observaciones = document.createElement('input');
                        observaciones.setAttribute('name', 'observaciones');
                        observaciones.setAttribute('value', document.getElementById('observaciones').value);
                        observaciones.setAttribute('type', 'hidden');
                        form.appendChild(observaciones);
                    } else {
                        Swal.fire(
                            'Error en los Datos',
                            'Debe colocar un dirección principal',
                            'info'
                        );
                        return;
                    }
                }

            } else {
                Swal.fire(
                    'Error en los Datos',
                    'Debe seleccionar un metodo de entrega',
                    'info'
                );
                return;
            }

            if (document.querySelector("input[name=forma_pago]:checked")) {

                var forma_pago = document.querySelector("input[name=forma_pago]:checked").value;

                if (forma_pago == 'transferencia') {

                    var opcion_pago = document.querySelector("input[name=opcion_pago]:checked");

                    if (opcion_pago.value == 'nacional') {
                        var box = document.getElementById('bancoNacional');
                        var banco = box.options[box.selectedIndex].value;
                        var ref = document.getElementById('referenciaNacional').value;
                        var fecha = document.getElementById('fechaNacional').value;
                    } else {
                        var banco = document.getElementById('bancoInternacional').value;
                        var ref = document.getElementById('referenciaInternacional').value;
                        var fecha = document.getElementById('fechaInternacional').value;
                    }

                    if (banco == '') {
                        Swal.fire(
                            'Error en los Datos',
                            'Debe seleccionar un Banco',
                            'info'
                        );

                        return;
                    }

                    if (ref == '') {
                        Swal.fire(
                            'Error en los Datos',
                            'Debe indicar el número de transferencia',
                            'info'
                        );

                        return;
                    }

                    if (fecha == '') {
                        Swal.fire(
                            'Error en los Datos',
                            'Debe indicar una fecha',
                            'info'
                        );

                        return;
                    }
                }
            } else {
                Swal.fire(
                    'Error en los Datos',
                    'Debe seleccionar la forma de pago',
                    'info'
                );

                return;
            }

            form.submit();
            return true;

        });

        $('input[type=radio][name=opcion_entrega]').on('change', function() {
            if ($(this).val() == 'enviar') {
                $("#datos_envio").show();
                $("#datos_retiro").hide();
                $("#btnEntrega").hide();
            } else {
                $("#datos_retiro").show();
                $("#btnEntrega").show();
                $("#datos_envio").hide();
            }
        });


        $('input[type=radio][name=opcion_pago]').on('change', function() {
            $("select#bancoNacional").prop('selectedIndex', 0);
            document.getElementById('referenciaNacional').value = '';

            document.getElementById('bancoInternacional').value = '';
            document.getElementById('referenciaInternacional').value = '';


            if ($(this).val() == 'nacional') {
                $("#ReporteNacional").show();
                $("#ReporteInternacional").hide();
            } else {
                $("#ReporteInternacional").show();
                $("#ReporteNacional").hide();
            }
        });

        $('input[type=radio][name=forma_pago]').on('change', function() {
            $("select#bancoNacional").prop('selectedIndex', 0);
            document.getElementById('referenciaNacional').value = '';

            document.getElementById('bancoInternacional').value = '';
            document.getElementById('referenciaInternacional').value = '';

            if ($(this).val() == 'efectivo' || $(this).val() == 'punto de venta') {
                $('#observacionEfectivo').show();
                $('#efectivo_reportar_pago_submit').show();
                $('#transf_reportar_pago_submit').hide();
                $("#transferencia").hide();

            } else {
                $('#transf_reportar_pago_submit').show();
                $('#efectivo_reportar_pago_submit').hide();
                $('#observacionEfectivo').hide();
                $("#transferencia").show();

            }
        });
    });

</script>