<?php
include_once(realpath(dirname(__FILE__)) . "/conexion.php");
?>
<a href="#"><img src=""></a>
<div class="slider-area">
    <div class="slider-active owl-carousel">
        <?php
        $banner_obj = new Banners();
        $banner = $banner_obj->Listado($_DB_);

        /** /
        echo "<pre>";
        print_r($banner);
        echo "</pre>";
        /**/


        if ($banner) {
            $i = 1;

            foreach ($banner as $slide) {
                $boton = (!empty($slide['boton'])) ? $slide['boton'] : null;
                $link = (!empty($slide['link'])) ? $slide['link'] : null;
                $titulo = (!empty($slide['titulo'])) ? $slide['titulo'] : null;
                $descripcion = (!empty($slide['descripcion'])) ? $slide['descripcion'] : null;
                ?>
                <div class="<?php if($i==1){echo "single-slider "; $i++;}?>single-slider-hm1 bg-img ptb-260" style="background-image: url('./Productos/slider/<?=$slide["imagen"];?>')">
                    <div class="container">
                        <div class="slider-content slider-content-style-1 slider-animated-1">
                            <h3 class="animated"><?=$titulo;?></h3>
                            <h2 class="animated"><?=$descripcion;?></h2>
                            <?php if ($boton){ ?>
                                <a class="btn-hover slider-btn-style animated" href="<?=$link;?>"><?=$boton;?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php }  ?>
    </div>
</div>

