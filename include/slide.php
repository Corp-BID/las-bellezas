<?php
include_once(realpath(dirname(__FILE__)) . "/conexion.php");
$banner_obj = new Banners();
$banner = $banner_obj->Listado($_DB_);

?>

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <?php
                if ($banner) {
                    $i=0;
                    foreach ($banner as $slide) {
                        $boton = (!empty($slide['boton'])) ? $slide['boton'] : null;
                        $link = (!empty($slide['link'])) ? $slide['link'] : null;
                        $titulo = (!empty($slide['titulo'])) ? $slide['titulo'] : null;
                        $descripcion = (!empty($slide['descripcion'])) ? $slide['descripcion'] : null;

                        $active = ($i==0) ? 'active' : '';
                        ?>

                        <div class="carousel-item <?=$active;?>"
                             style="background-image: url('./Productos/slider/<?= $slide["imagen"]; ?>');">
                            <div class="carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animate__animated animate__fadeInDown"><?=$titulo;?></h2>
                                    <p class="animate__animated animate__fadeInUp"><?=$descripcion;?></p>
                                    <?php if ($boton){ ?>
                                    <a href="<?=$link;?>>" class="btn-get-started animate__animated animate__fadeInUp scrollto"><?=$boton;?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    $i++;
                    }
                }?>


            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
                <span class="sr-only">Proximo</span>
            </a>

        </div>
    </div>
</section><!-- End Hero -->


