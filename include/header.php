<?php
//Variable para medir el tiempo de ejecucion
$tiempo_inicial = microtime(true);
include_once(realpath(dirname(__FILE__)) . "/conexion.php");
?>
<!DOCTYPE html>
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths" style="" lang="zxx">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><? echo NOMBRE_SITE;?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="./assets/img/logo/logo.png">

    <!-- all css here -->
    <!-- <link rel="stylesheet" href="./assets/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="./assets/css/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="./assets/css/magnific-popup.css">
    <!-- <link rel="stylesheet" href="./assets/css/animate.css">-->
    <link rel="stylesheet" href="./assets/css/animate/animate.css">
    <link href="assets/css/icofont/icofont.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="./assets/css/owl.carousel.min.css">-->
    <link rel="stylesheet" href="./assets/css/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="./assets/css/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="./assets/css/slinky.min.css">
    <link rel="stylesheet" href="./assets/css/slick.css">
    <link rel="stylesheet" href="./assets/css/ionicons.min.css">
    <link rel="stylesheet" href="./assets/css/bundle.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/css/responsive.css">

    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

    <?php
    include_once(realpath(dirname(__FILE__) . "/../") . "/config/Config_Tasa_Moneda.php");

    $monedas_obj = new Monedas();
    $categorias = $categoria_obj->listado($_DB_);

    $DatosMoneda = $_SESSION['DatosMonedaDefecto'];
    $DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
    $ValorCalculo = $_SESSION['ValorCalculo'];

    $moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
    $simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

    $tasa_obj = new TasaDolar();
    $valorMoneda = $monedas_obj->MonedaPorDefectoConf($_DB_);
    $isMonedaDefault = $valorMoneda['id'];
    $tasaDefault = $tasa_obj->TasaConfDefecto($_DB_, $isMonedaDefault);
    $tasaIsDefault =  number_format($tasaDefault['valor_calculo'], 2, ",", ".");


    if (!empty($DatosMoneda[0]['id'])) {
        $monedas = $monedas_obj->Listado($_DB_, 0);
    ?>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- header start -->
        <div class="wrapper">
            <div class="body-overlay"></div>
            <header class="pl-155 pr-155 intelligent-header headroom headroom--not-top headroom--unpinned headroom--bottom">
                <div class="header-area header-area-2">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                            <div>
                                <div class="logo">
                                    <a href="./index.php"><img src="./assets/img/logo/logo.png" alt="" class="img-fluid" width="60%"></a>
                                </div>
                            </div>
                            <div class="col-md-7 menu-none-block menu-center">
                                <div class="main-menu">
                                    <nav>
                                        <ul>
                                            <li><a href="index.php">Inicio</a></li>
                                            <li><a href="./about-us.php">Conócenos</a></li>
                                            <li><a href="./listado.php">Productos</a></li>
                                            <li><a href="./contacto.php">Contáctanos</a></li>

                                            <li class="dropdown"><a href="#"><i class="fa fa-money" aria-hidden="true"></i> <b><?php echo $moneda; ?> (<?php echo $simbolo; ?>)</b> <i class="fa fa-angle-down"></i></a>
                                                <ul role="menu" class="sub-menu">
                                                    <?php

                                                    foreach ($monedas as $moneda) {
                                                    ?>
                                                        <li><a href="<?php echo "?" . $_SERVER['QUERY_STRING'] . "&moneda=" . $moneda['id']; ?>"><?php echo $moneda['descripcion']; ?> (<?php echo $moneda['simbolos']; ?>)</a></li>
                                                    <?php
                                                    } ?>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="header-search-cart">
                                    <div class="header-search common-style">
                                        <button class="sidebar-trigger-search">
                                            <span class="ion-ios-search-strong"></span>
                                        </button>
                                    </div>
                                    <div class="header-cart common-style">
                                        <button class="sidebar-trigger">
                                            <span class="ion-bag"></span>
                                        </button>
                                    </div>
                                    <div class="header-cart common-style">
                                        <button class="header-navbar-active">
                                            <span class="ion-navicon"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="header-space"></div>
            <!-- header end -->

            <?php
            $presupuesto = isset($_SESSION['presupuesto']);

            $productos_carrito = isset($presupuesto['productos']);
            $total_carrito = isset($presupuesto['total']);

            if (isset($_SESSION['presupuesto'])) {
                $presupuesto = $_SESSION['presupuesto'];
                $productos_carrito = $presupuesto['productos'];
                $total_carrito = $presupuesto['total'];
            }
            ?>

            <!-- sidebar-cart start -->
            <div class="sidebar-cart onepage-sidebar-area">
                <div class="wrap-sidebar">
                    <div class="sidebar-cart-all">
                        <div class="sidebar-cart-icon">
                            <button class="op-sidebar-close"><span class="ion-android-close"></span></button>
                        </div>
                        <div class="cart-content">
                            <h3>Mi Carrito</h3>
                            <ul>
                                <?php
                                if ($productos_carrito) {
                                    foreach ($productos_carrito as $clave => $prod) { ?>
                                        <li class="single-product-cart">
                                            <div class="cart-img">
                                                <a href="#"><img src="./Productos/thumbs/<?php echo $prod['foto']; ?>" width="50" alt=""></a>
                                            </div>
                                            <div class="cart-title">
                                                <h3><a href="#"> <?php echo $prod['nombre'] ?></a></h3>
                                                <span><?php echo  $prod['cantidad'] . ' x ' . number_format($prod['precio'] * $ValorCalculo, 2, ",", "."); ?></span>
                                            </div>
                                            <div class="cart-delete">
                                                <a href="./carrito_o.php?op=del&pos=<?php echo $clave; ?>"><i class="ion-ios-trash-outline"></i></a>
                                            </div>
                                        </li>
                                <?php
                                    }
                                }
                                ?>
                                <li class="single-product-cart">
                                    <div class="cart-total">
                                        <h4>Total <?= $simbolo; ?>: <span><?php echo  number_format($total_carrito * $ValorCalculo, 2, ",", "."); ?></span></h4>
                                    </div>
                                </li>
                                <li class="single-product-cart">
                                    <div class="cart-checkout-btn">
                                        <a class="btn-hover cart-btn-style" href="./carrito.php">Ver Carrito</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main-search start -->
            <div class="main-search-active">
                <div class="sidebar-search-icon">
                    <button class="search-close"><span class="ion-android-close"></span></button>
                </div>
                <div class="sidebar-search-input">
                    <form>
                        <div class="form-search">
                            <input name="search" id="search" class="input-text" placeholder="Buscar..." type="search">
                            <button>
                                <i class="ion-ios-search-strong"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- main-search start -->
            <div class="cur-lang-acc-active">
                <div class="wrap-sidebar">
                    <div class="sidebar-nav-icon">
                        <button class="op-sidebar-close"><span class="ion-android-close"></span></button>
                    </div>
                    <div class="cur-lang-acc-all">
                        <?php
                        if (isset($_SESSION['cliente']['correo'])) { ?>

                            <div class="col-12 ">
                                <ul>
                                    <li><a href="index.php">Inicio</a></li>
                                    <li><a href="./about-us.php">Conócenos</a></li>
                                    <li><a href="./listado.php">Productos</a></li>
                                    <li><a href="./contacto.php">Contáctanos</a></li>

                                    <li><br></li>

                                    <li class="dropdown"><i class="fa fa-money" aria-hidden="true"></i> <b>Precios en:</b>
                                        <ul role="menu" class="sub-menu">
                                            <?php

                                            foreach ($monedas as $moneda) {
                                            ?>
                                                <li><a href="<?php echo "?" . $_SERVER['QUERY_STRING'] . "&moneda=" . $moneda['id']; ?>"><?php echo $moneda['descripcion']; ?> (<?php echo $moneda['simbolos']; ?>)</a></li>
                                            <?php
                                            } ?>
                                        </ul>
                                    </li>

                                    <li><br></li>
                                </ul>

                                <b>Usuario: <span><?php echo $_SESSION['cliente']['correo']; ?></span></b>

                                <ul>
                                    <li><a href="./historico_pedido.php"> Mis Pedidos </li>

                                    <li><a href="./logout.php"> Salir </a></li>
                                </ul>
                                 <br>
                                <label><b>Tasa de cambio:</b></label>
                                <p><code><?=$tasaIsDefault;?></code></p>
                            </div>


                        <?php
                        } else { ?>
                            <div class="single-currency-language-account">

                                <div class="col-12 ">
                                    <ul>
                                        <li><a href="index.php">Inicio</a></li>
                                        <li><a href="./about-us.php">Conócenos</a></li>
                                        <li><a href="./listado.php">Productos</a></li>
                                        <li><a href="./contacto.php">Contáctanos</a></li>

                                        <li><br></li>

                                        <li class="dropdown"><i class="fa fa-money" aria-hidden="true"></i> <b>Precios en:</b>
                                            <ul role="menu" class="sub-menu">
                                                <?php

                                                foreach ($monedas as $moneda) {
                                                ?>
                                                    <li><a href="<?php echo "?" . $_SERVER['QUERY_STRING'] . "&moneda=" . $moneda['id']; ?>"><?php echo $moneda['descripcion']; ?> (<?php echo $moneda['simbolos']; ?>)</a></li>
                                                <?php
                                                } ?>
                                            </ul>
                                        </li>

                                        <li><br></li>
                                    </ul>

                                    <label><b>Mi Cuenta:</b></label>

                                    <ul>
                                        <li><a href="./index_login.php">Iniciar sesión </a></li>

                                        <li><a href="./register.php">Registrate</a></li>
                                    </ul>

                                    <br>

                                    <label><b>Tasa de cambio:</b></label>
                                    <p><code><?=$tasaIsDefault;?></code></p>

                                </div>
                            </div>
                        <?php
                        } ?>
                    </div>
                </div>
            </div>

        <?php } else { ?>
            <h1>Debe configurar la moneda por defecto y su tasa de cambio</h1>
        <?php }
