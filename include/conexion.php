<?php
$timeout_duration = 3600;

include_once(realpath(dirname(__FILE__))."/sessions.php");

include_once(realpath(dirname(__FILE__) . "/../") . "/config/Config_Conexion.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Sql.php");

include_once(realpath(dirname(__FILE__)."/../")."/clases/Logs.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Usuario.php");

//include_once(realpath(dirname(__FILE__)."/../")."/clases/Contacto.php");

include_once(realpath(dirname(__FILE__)."/../")."/clases/ConsultaComun.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Config_Tienda.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/VigenciaPedido.php");

include_once(realpath(dirname(__FILE__)."/../")."/clases/Categorias.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Productos.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/TasaDolar.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Marcas.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Monedas.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Paginator.php");

include_once(realpath(dirname(__FILE__)."/../")."/clases/Clientes.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Pedidos.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Pago.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/TasaDolar.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Banners.php");

include_once(realpath(dirname(__FILE__)."/../")."/clases/Usuario.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/class.upload.php");
include_once(realpath(dirname(__FILE__)."/../")."/clases/Testimonios.php");


//include_once(realpath(dirname(__FILE__)."/../")."/clases/xls/PHPExcel/IOFactory.php");

//chequear el login
$_DB_= new Sql(DBHOST,DBUSER,DBPASSWORD,DBDATABASE);
$productos_obj = new Productos();
$categoria_obj = new Categorias();
$marcas_obj = new Marcas();


// Ruta para colocar en el menu
$ruta_index= "";
if(basename($_SERVER['PHP_SELF']) != "index.php")
{$ruta_index = "index.php";}

$_SESSION['categorias'] = $categoria_obj->listado($_DB_);