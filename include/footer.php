<footer class="footer-area gray-bg pt-100 pb-95">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5 col-12">
                <div class="footer-widget">
                    <div class="footer-widget-l-content">
                        <ul>
                            <li><a href="https://www.facebook.com/Charcuteria-express-628106464504431"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/express_charcuteria"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 col-12">
                <div class="footer-widget">
                    <div class="footer-widget-m-content text-center">
                        <div class="footer-logo">
                            <a href="#"><img src="./assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <div class="footer-nav">
                            <nav>
                                <ul>
                                    <li><a href="index.php">Inicio</a></li>
                                    <li><a href="./about-us.php">Conócenos</a></li>
                                    <li><a href="./listado.php">Productos</a></li>
                                    <li><a href="./contacto.php">Contáctanos</a></li>
                                </ul>
                            </nav>
                        </div>
                        <p>Copyright © Express Charcuteria . Todo los derechos reservados.</p>
                        <p>Desarrollado por:<a href="https://corp-bid.com/">Corp-Bid</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-12">
                <div class="footer-widget f-right">
                    <div class="footer-widget-r-content">
                        <ul>
                            <li><span>Teléfono :</span> +58(424)-155-1992 </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- all js here -->

<div class="btn-whatsapp">
    <a href="https://api.whatsapp.com/send?phone=584241551992&text=Hola!%20Necesito%20una%20ayuda." target="_blank">
        <img src="http://s2.accesoperu.com/logos/btn_whatsapp.png" alt="">
    </a>
</div>

<script src="./assets/js/vendor/jquery-1.12.0.min.js"></script>
<script src="./assets/js/popper.js"></script>
<script src="./assets/css/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="./assets/js/bootstrap.min.js"></script>-->
<script src="./assets/js/jquery.magnific-popup.min.js"></script>
<script src="./assets/js/isotope.pkgd.min.js"></script>
<script src="./assets/js/imagesloaded.pkgd.min.js"></script>
<script src="./assets/js/jquery.counterup.min.js"></script>
<script src="./assets/js/waypoints.min.js"></script>
<script src="./assets/js/slinky.min.js"></script>
<script src="./assets/js/ajax-mail.js"></script>
<!--<script src="./assets/js/owl.carousel.min.js"></script>-->
<script src="./assets/css/owl.carousel/owl.carousel.min.js"></script>
<script src="./assets/js/plugins.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>

<!-- Validate -->
<script src="../admin/js/plugins/validate/jquery.validate.min.js"></script>

<script src="../node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="../node_modules/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    // grab an element
    var myElement = document.querySelector(".intelligent-header");
    // construct an instance of Headroom, passing the element
    var headroom = new Headroom(myElement);
    // initialise
    headroom.init();
</script>

<script src="./assets/js/main.js"></script>
<script src="../admin/js/app.js"></script>

</body>

</html>

<?
//Variable para medir el tiempo de ejecucion
/**/
$tiempo_final = microtime(true);
$tiempo = $tiempo_final - $tiempo_inicial;

$tiempo = number_format($tiempo, 2, ",", ".");

echo "<b>El tiempo de ejecución fue de " . $tiempo . " segundos</b>";
/**/