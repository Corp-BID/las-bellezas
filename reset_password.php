<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$op = (isset($_POST['op'])) ? $_POST['op'] : NULL;

$cliente = new Clientes();

if ($op == 'validarCorreo'){

    $email = (isset($_POST['email'])) ? $_POST['email'] : NULL;
    $CodigoCliente = $cliente -> ValidarCorreo($_DB_, $email);

    if (!empty($CodigoCliente['id']))
    {
        $setDatos = $cliente -> setCodigoValidacion($_DB_, $CodigoCliente['id']);


        $datos = $cliente -> getCodigoValidacionId($_DB_, $CodigoCliente['id']);

        $nombres_apellidos = $datos[0]['nombres']." ".$datos[0]['apellidos'];

        $enviarEmail = EnviarEmail($email, $datos[0]['codigo_validacion'], $nombres_apellidos);

        $info = [
            'success' => $enviarEmail,
        ];

    }else{
        $info = [
            'success' => false,
            'op' =>'validarCorreo'
        ];
    }

    echo json_encode($info);

} elseif ($op == 'validarCodigo'){
    $codigo = (isset($_POST['codigo'])) ? $_POST['codigo'] : NULL;
    $CodigoCliente = $cliente -> getCodigoValidacion($_DB_, $codigo);

    $info = [
        'success' => $CodigoCliente,
        'op' =>'validarCodigo'
    ];

    echo json_encode($info);

} elseif ($op == 'reset'){

    $clave = (isset($_POST['password'])) ? $_POST['password'] : NULL;
    $id = (isset($_POST['validar'])) ? $_POST['validar'] : NULL;

    $CodigoCliente = $cliente -> setResetPassword($_DB_, $id, $clave);

    $info = [
        'success' => $CodigoCliente
    ];
    echo json_encode($info);
}

function EnviarEmail($email, $codigo, $datos){

    $para = $email;
    $titulo = 'Recuperación de Contraseña - express@gmail.com';

    $mensaje  = "<html>
                    <head>
                    
                    </head>
                    <body>
                    
                    <h1>Recuperación de Contraseña - Código de Validación</h1>
                    <br>
                    Hola " .  $datos . " gracias por escribirnos.
                    
                    <br><br>
                    
                    A continuación tus código de validación para resetear tu contraseña:
                    
                    Código: <strong><?=$codigo;?></strong>
                    <br><br>
                    
                    Estamos complacidos en servirte, por ello, en caso de alguna duda, inquietud o dificultad, estamos a la orden. <br><br>
                    Saludos Cordiales,<br><br>
                    <strong>Xpress</strong><br><br>
                    <br>
                    Teléfono:+58 424-155.19.92<br>
                    
                    </body>
                    </html>";

    $cabeceras = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $cabeceras .= 'From: info@expresscharcuteria.com';

    return mail($para, $titulo, $mensaje, $cabeceras);

}
