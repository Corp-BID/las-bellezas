<?php

include_once(realpath(dirname(__FILE__)) . "/include/header.php");
//include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");

$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : '';
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? strval($_GET['sc']) : '';

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];



?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Mi Carrito</h2>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li> Mi Pedido </li>
            </ul>
        </div>
    </div>
</div>
<!-- shopping-cart-area start -->
<div class="cart-main-area pt-95 pb-100">
    <div class="container">
        <form action="./carrito_o.php" method="POST">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1 class="cart-heading">Mi Carrito</h1>

                    <input type="hidden" name="op" value="modlis">

                    <div class="table-content table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-name" colspan="3">Producto</th>
                                    <th class="product-price">Precio</th>
                                    <th class="product-price">Presentación</th>
                                    <th class="product-quantity">Cantidad</th>
                                    <th class="product-subtotal">Total a Pagar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                if (isset($productos_carrito)) {
                                    foreach ($productos_carrito as $clave => $prod) { ?>
                                        <input type="hidden" name="posicion[]" value="<?php echo $clave; ?>">
                                        <tr>
                                            <td class="product-remove"><a href="./carrito_o.php?op=del&pos=<?php echo $clave; ?>"><i class="ion-android-close"></i></a></td>
                                            <td class="product-thumbnail">
                                                <a href="#"><img src="./Productos/thumbs/<?php echo $prod['foto']; ?>"  onerror="this.src='./Productos/no_disponible.jpg';" width="80" alt=""></a>
                                            </td>
                                            <td class="product-name text-left"><?php echo $prod['nombre'] ?><br><?php $prod['descripcion_marca'];?></td>
                                            <td class="product-price"><span class="amount"><?= $simbolo." ".  number_format($prod['precio'] * $ValorCalculo, 2, ",", "."); ?></span></td>
                                            <td><?=$prod['presentacion'];?></td>
                                            <td class="product-quantity">
                                                <input type="number" name="cantidades[]" value="<?php echo $prod['cantidad']; ?>" type="number">
                                            </td>
                                            <td class="product-subtotal"><span><?= $simbolo." ".  number_format($prod['total'] * $ValorCalculo, 2, ",", ".") ?></span></td>

                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="coupon-all">
                                <div class="coupon2">
                                    <input class="button" name="update_cart" value="Actualizar Pedido" type="submit">
                                    <div class="mt-3">
                                        <a href="./listado.php" class="button">Seguir Comprando</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form action="./pago_o.php" method="POST">
            <div class="row">
                <div class="col-md-6 ml-auto mt-5">
                    <div class="form-group mt-2">
                        <h3>Observación del pedido</h3>
                        <textarea maxlength="200" class="form-control" name="observacion" id="observacion" cols="30" rows="3" placeholder="Escriba una observación referente a su pedido en caso de tenerla"></textarea>
                    </div>
                </div>

                <div class="col-md-6 ml-auto">
                    <div class="cart-page-total">
                        <h2>Carrito Total</h2>
                        <ul>
                            <li>Subtotal <?= $simbolo;?><span><?php echo  number_format($total_carrito * $ValorCalculo , 2, ",", "."); ?></span></li>
                            <li>Total <?= $simbolo;?><span><?=number_format($total_carrito * $ValorCalculo , 2, ",", "."); ?></span></li>
                        </ul>
                    </div>

                </div>
            </div>

            <input type="submit" value="Guardar y Pagar" class="btn btn-primary btn-block" style="cursor: pointer;">
            <input type="hidden" name="op" value="new">
            <input type="hidden" name="pag" value="pago">
        </form>
        </div>
    </div>
</div>
    <?php
    include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
