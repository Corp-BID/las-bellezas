<?php

include_once(realpath(dirname(__FILE__)) . "/include/header.php");
include_once(realpath(dirname(__FILE__)) . "/include/slide.php");
?>
<style>
    .product-cart {
        height: 50px !important;
    }
</style>



<?php
$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$productos_destacados = $productos_obj->ListadoAleatorio($_DB_, 0, 0, 1);
$num_productos_destacados = sizeof($productos_destacados);

if ($num_productos_destacados == 0) {
    $productos_destacados = $productos_obj->ListadoAleatorio($_DB_, 0, 0, 0, 8);
} elseif ($num_productos_destacados < 8) {
    $num_productos_fantantes = 8 - $num_productos_destacados;
    $productos_destacados_complemento = $productos_obj->ListadoAleatorio($_DB_, 0, 0, 0, $num_productos_fantantes);

    $productos_destacados = array_merge($productos_destacados, $productos_destacados_complemento);
}

$_SESSION['productos_destacados'] = $productos_destacados;
$num_productos_destacados = sizeof($productos_destacados);

$productos_nuevos = $productos_obj->ListadoAleatorio($_DB_, 1, 0, 0);
$num_productos_nuevos = sizeof($productos_nuevos);



$productos_ofertas = $productos_obj->ListadoAleatorio($_DB_, 0, 1, 0);
$num_productos_ofertas = sizeof($productos_ofertas);



if ($num_productos_nuevos >= 4 and $num_productos_ofertas >= 4) {
    $medida_div1 = 12;
    $medida_div2 = 3;
    $num_productos_mostrar = 4;
} elseif ($num_productos_nuevos == 0 or $num_productos_ofertas == 0) {
    $medida_div1 = 12;
    $medida_div2 = 3;
    $num_productos_mostrar = 4;
} elseif ($num_productos_nuevos < 4 or $num_productos_ofertas < 4) {
    $medida_div1 = 6;
    $medida_div2 = 6;
    $num_productos_mostrar = 2;
}


if ($num_productos_nuevos < $num_productos_mostrar) {
    $num_productos_nuevos_mostrar = $num_productos_nuevos;
} else {
    $num_productos_nuevos_mostrar = $num_productos_mostrar;
}

if ($num_productos_ofertas < $num_productos_mostrar) {
    $num_productos_ofertas_mostrar = $num_productos_ofertas;
} else {
    $num_productos_ofertas_mostrar = $num_productos_mostrar;
}



if ($productos_ofertas) {
?>

    <div class="product-collection-area pt-30 pb-30">
        <div class="container">

        </div>

    </div>

<?php } ?>

<?php
if ($productos_destacados) {
?>

    <div class="product-collection-area pt-10 pb-30">
        <div class="container">
            <div class="section-title text-center mb-55">
                <h2>Productos Destacados</h2>
            </div>
            <div class="row">

                <?php
                foreach ($productos_destacados as $prod_nuevos) { ?>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="./producto_detallado.php?c=<?= $prod_nuevos['cod_categoria']; ?>&sc=<?= $prod_nuevos['cod_subcategoria']; ?>&p=<?= $prod_nuevos['cod_producto']; ?>">
                                    <img src="./Productos/thumbs/<?= $prod_nuevos['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="">
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="product-title-price">
                                    <div class="product-title">
                                        <h4>
                                            <a href="./producto_detallado.php?c=<?= $prod_nuevos['cod_categoria']; ?>&sc=<?= $prod_nuevos['cod_subcategoria']; ?>&p=<?= $prod_nuevos['cod_producto']; ?>">
                                                <?= $prod_nuevos['nombre']; ?>
                                            </a>
                                        </h4>
                                    </div>

                                </div>

                                <div class="product-title-price">
                                    <div class="product-title">
                                        <?/*
                                        <?= $prod_nuevos['descripcion_marca']; ?>
                                        */?>
                                    </div>
                                    <div class="product-price">
                                        <span><?= $simbolo . " " . number_format($prod_nuevos['precio1_full'] * $ValorCalculo, 2, ",", "."); ?></span>
                                    </div>
                                </div>

                                <div class="product-cart-categori">
                                    <div class="product-cart">
                                        <span></span>
                                    </div>
                                    <div class="product-categori pt-2">
                                        <a href="./carrito_o.php?c=<?= $prod_nuevos['cod_categoria']; ?>&sc=<?= $prod_nuevos['cod_subcategoria']; ?>&p=<?= $prod_nuevos['cod_producto']; ?>&op=add" class="add_cart"><i class="ion-bag"></i> Agregar al Carrito</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                } ?>

            </div>
           
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <form action="./mas_productos.php?accion=destacados" method="post">
                        <input type="hidden" name="accion" value="destacados">
                        <input type="submit" value="Ver Más" class="button" />
                    </form>
                </div>
            </div>
    
        </div>
    </div>
<?php
}
?>

<?/*
<div class="banner-area pt-20 pb-10">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 col-6">
                <div class="single-banner mb-2">
                    <a href="#"><img src="assets/img/banner/a1.jpg" alt="" class="img-fluid"></a>
                    <div class="banner-content banner-content-position1">

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-6">
                <div class="single-banner mb-2">
                    <a href="#"><img src="assets/img/banner/a2.jpg" alt="" class="img-fluid"></a>
                    <div class="banner-content banner-content-position2">

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-6">
                <div class="single-banner mb-2">
                    <a href="#"><img src="assets/img/banner/a3.jpg" alt=" class=" img-fluid""></a>
                    <div class="banner-content banner-content-position2">

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-6">
                <div class="single-banner mb-2">
                    <a href="#"><img src="assets/img/banner/a4.jpg" alt="" class="img-fluid"></a>
                    <div class="banner-content banner-content-position3">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
*/?>



<?
if ($productos_nuevos or $productos_ofertas) {
?>

    <div class="product-collection-area pt-10 pb-10">
        <div class="container">

            <div class="row">
                <?php
                if ($productos_nuevos) {
                    if ($num_productos_nuevos_mostrar > 1) {
                        $productos_nuevos_aleatorios = array_rand($productos_nuevos, $num_productos_nuevos_mostrar);
                    } else {
                        $productos_nuevos_aleatorios[0] = 0;
                    }
                ?>

                    <div class="col-lg-<?= $medida_div1; ?> col-md-12 ">

                        <div class="section-title text-center mb-55">
                            <h2>Productos Nuevos</h2>
                        </div>
                        <div class="row">

                            <?php
                            for ($i = 0; $i < sizeof($productos_nuevos_aleatorios); $i++) {
                                $pos = $productos_nuevos_aleatorios[$i];
                            ?>

                                <div class="col-lg-<?= $medida_div2; ?> col-md-6  col-sm-6 col-6">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="./producto_detallado.php?c=<?= $productos_nuevos[$pos]['cod_categoria']; ?>&sc=<?= $productos_nuevos[$pos]['cod_subcategoria']; ?>&p=<?= $productos_nuevos[$pos]['cod_producto']; ?>">
                                                <img src="./Productos/thumbs/<?= $productos_nuevos[$pos]['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="">
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title-price">
                                                <div class="product-title">
                                                    <h4>
                                                        <a href="./producto_detallado.php?c=<?= $productos_nuevos[$pos]['cod_categoria']; ?>&sc=<?= $productos_nuevos[$pos]['cod_subcategoria']; ?>&p=<?= $productos_nuevos[$pos]['cod_producto']; ?>">
                                                            <?= $productos_nuevos[$pos]['nombre']; ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>

                                            <div class="product-title-price">
                                                <div class="product-title">
                                                    <?/*
                                                    <?= $productos_nuevos[$pos]['descripcion_marca']; ?>
                                                    */?>
                                                </div>
                                                <div class="product-price">
                                                    <span><?= $simbolo . " " .  number_format($productos_nuevos[$pos]['precio1_full'] * $ValorCalculo, 2, ",", "."); ?></span>
                                                </div>
                                            </div>

                                            <div class="product-cart-categori">
                                                <div class="product-cart">
                                                    <span></span>
                                                </div>
                                                <div class="product-categori pt-2">
                                                    <a href="./carrito_o.php?c=<?= $productos_nuevos[$pos]['cod_categoria']; ?>&sc=<?= $productos_nuevos[$pos]['cod_subcategoria']; ?>&p=<?= $productos_nuevos[$pos]['cod_producto']; ?>&op=add" class="add_cart"><i class="ion-bag"></i> Agregar al Carrito</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php
                            } ?>

                        </div>
                       
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                            <form action="./mas_productos.php?accion=nuevos" method="post">
                                    <input type="hidden" name="accion" value="nuevos">
                                    <input type="submit" value="Ver Más" class="button" />
                                </form>
                            </div>
                        </div>
                    </div>
                <?php
                } ?>


            </div>
        </div>
    </div>


    <?/*
    <div class="product-collection-area pt-10 pb-10">
        <div class="container">
            <div class="row">
                <img src="./assets/img/banner/banner2.jpg" class="img-fluid">
            </div>
        </div>
    </div>
    */?>

    <div class="product-collection-area pt-10 pb-10">
        <div class="container">

            <div class="row">


                <?php
                if ($productos_ofertas) {
                    if ($num_productos_ofertas_mostrar > 1) {
                        $productos_ofertas_aleatorios = array_rand($productos_ofertas, $num_productos_ofertas_mostrar);
                    } else {
                        $productos_ofertas_aleatorios[0] = 0;
                    } ?>

                    <div class="col-lg-<?= $medida_div1; ?> col-md-12 ">

                        <div class="section-title text-center mb-55">
                            <h2>Productos en Oferta</h2>
                        </div>
                        <div class="row">
                            <?php
                            for ($i = 0; $i < sizeof($productos_ofertas_aleatorios); $i++) {
                                $pos = $productos_ofertas_aleatorios[$i];
                            ?>
                                <div class="col-lg-<?= $medida_div2; ?> col-md-6  col-sm-6 col-6">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="./producto_detallado.php?c=<?= $productos_ofertas[$pos]['cod_categoria']; ?>&sc=<?= $productos_ofertas[$pos]['cod_subcategoria']; ?>&p=<?= $productos_ofertas[$pos]['cod_producto']; ?>">
                                                <img src="./Productos/thumbs/<?= $productos_ofertas[$pos]['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="">
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title-price">
                                                <div class="product-title">
                                                    <h4>
                                                        <a href="./producto_detallado.php?c=<?= $productos_ofertas[$pos]['cod_categoria']; ?>&sc=<?= $productos_ofertas[$pos]['cod_subcategoria']; ?>&p=<?= $productos_ofertas[$pos]['cod_producto']; ?>">
                                                            <?= $productos_ofertas[$pos]['nombre']; ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>

                                            <div class="product-title-price">
                                                <div class="product-title">
                                                    <?= $productos_ofertas[$pos]['descripcion_marca']; ?>
                                                </div>
                                                <div class="product-price">
                                                    <span><?= $simbolo . " " .  number_format($productos_ofertas[$pos]['precio1_full'] * $ValorCalculo, 2, ",", "."); ?></span>
                                                </div>
                                            </div>

                                            <div class="product-cart-categori">
                                                <div class="product-cart">
                                                    <span></span>
                                                </div>
                                                <div class="product-categori pt-2">
                                                    <a href="./carrito_o.php?c=<?= $productos_ofertas[$pos]['cod_categoria']; ?>&sc=<?= $productos_ofertas[$pos]['cod_subcategoria']; ?>&p=<?= $productos_ofertas[$pos]['cod_producto']; ?>&op=add" class="add_cart"><i class="ion-bag"></i> Agregar al Carrito</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php

                            } ?>

                        </div>
                        
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <form action="./mas_productos.php?accion=recomendados" method="post">
                                    <input type="hidden" name="accion" value="recomendados">
                                    <input type="submit" value="Ver Más" class="button" />
                                </form>
                            </div>
                        </div>
                    </div>
                <?php
                } ?>


            </div>
        </div>
    </div>
    </div>
<?php
} ?>






<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>
    $(document).ready(function() {
        // var url = removeParam ('moneda');
    });

    function removeParam(parameter) {
        var url = document.location.href;
        var urlparts = url.split('?');

        if (urlparts.length >= 2) {
            var urlBase = urlparts.shift();
            var queryString = urlparts.join("?");

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = queryString.split(/[&;]/g);
            for (var i = pars.length; i-- > 0;)
                if (pars[i].lastIndexOf(prefix, 0) !== -1)
                    pars.splice(i, 1);
            url = urlBase + '?' + pars.join('&');
            window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

        }
        return url;
    }
</script>