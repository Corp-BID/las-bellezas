<?php
include_once(realpath(dirname(__FILE__)) . "/include/conexion.php");

$presupuesto = (isset($_SESSION['presupuesto'])) ? $_SESSION['presupuesto'] : array() ;

//unset($_SESSION['presupuesto']);

$cat = (isset($_POST['c']) && $_POST['c']) ? strval($_POST['c']) : $_GET['c'];
$subcat = (isset($_POST['sc']) && $_POST['sc']) ? strval($_POST['sc']) : $_GET['sc'];
$cod_prod = (isset($_POST['p']) && $_POST['p']) ? strval($_POST['p']) : $_GET['p'];

$cant_prod = (isset($_POST['cant_prod']) && $_POST['cant_prod']) ? intval($_POST['cant_prod']) : 1;
$posicion = (isset($_POST['pos']) && intval($_POST['pos'])) ? intval($_POST['pos']) : '';
$operacion = (isset($_POST['op']) && $_POST['op']) ? substr(strval($_POST['op']), 0, 6) : $_GET['op'];

if ($operacion == "") {
    $operacion = (isset($_GET['op']) && $_GET['op']) ? substr(strval($_GET['op']), 0, 6) : "";
}

if ($posicion == "") {
    $posicion = (isset($_GET['pos'])) ? $_GET['pos'] : "";
}

if ($operacion == "add") {
    $j=0;
    $existe = false;
    $productos = array();

    //revisa si existe un producto en el carrito
    if (isset($presupuesto)) {
        $productos = $presupuesto['productos'];
        foreach($productos as $key => $value){
            if ($value['codigo'] == $cod_prod) {
                $productos[$key]['cantidad'] += $cant_prod;
                $productos[$key]['total'] = $productos[$key]['total']*$productos[$key]['cantidad'];
                $existe = true;
                break;
            }
        }
        $j = sizeof($productos);
    }

    if ( ! $existe ) {
        $cantidad = $cant_prod;

        if ($cantidad and $cod_prod != '') {
            $productos_obj = new Productos();
            $producto = $productos_obj->Consultar($_DB_, $cod_prod);

            $productos[$j]['codigo'] = $producto[0]['cod_producto'];
            $productos[$j]['descripcion'] = $producto[0]['descripcion'];
            $productos[$j]['cod_categoria'] = $producto[0]['cod_categoria'];
            $productos[$j]['cod_subcategoria'] = $producto[0]['cod_subcategoria'];
            $productos[$j]['nombre'] = $producto[0]['nombre'];
            $productos[$j]['marca'] = $producto[0]['marca'];
            $productos[$j]['descripcion_marca'] = $producto[0]['descripcion_marca'];
            $productos[$j]['presentacion'] = $producto[0]['presentacion'];
            $productos[$j]['foto'] = $producto[0]['foto1'];
            $productos[$j]['cant_min'] = $producto[0]['cant_min'];
            $productos[$j]['cantidad'] = $cantidad;
            $productos[$j]['precio'] = $producto[0]['precio'];
            $productos[$j]['total'] = $cantidad * $producto[0]['precio'];

            $j++;
        }
    }


//--------------------------------------------
    $total = 0;
    foreach ($productos as $prod) {
        $total += $prod['total'];
    }
    $presupuesto['productos'] = $productos;
    $presupuesto['total'] = $total;

} 
elseif ($posicion >= 0 and $operacion == "mod") 
{
    $productos = $presupuesto['productos'];
    $total = $presupuesto['total'];
    $total_eliminar = $productos[$posicion]['total'];

    $cantidad = $_POST['cantidad'];

    $productos[$posicion]['cantidad'] = $cantidad;

    $total_nuevo = $cantidad * $productos[$posicion]['precio'];

    $productos[$posicion]['total'] = $total_nuevo;


    //--------------------------------------------
    $total = 0;
    $cant_prod = sizeof($productos);
    foreach($productos as $prod) 
    {
        $total += $prod['total'];
    }

    $presupuesto['productos'] = $productos;
    $presupuesto['total'] = $total;
} 
elseif ($posicion >= 0 and $operacion == "del") 
{
    $productos = $presupuesto['productos'];
    $total = $presupuesto['total'];
    $total_eliminar = $productos[$posicion]['total'];

    unset($productos[$posicion]);

    //--------------------------------------------
    $total = 0;
    foreach($productos as $prod) 
    {
        $total += $prod['total'];
    }

    $presupuesto['productos'] = $productos;
    $presupuesto['total'] = $total;
}

elseif ($posicion >= 0 and $operacion == "modlis") {
    
    $prod = $_POST['posicion'];
    $cantidades = $_POST['cantidades'];

    $productos = $presupuesto['productos'];
    
    foreach($prod as $clave => $posicion)
    {
        if($cantidades[$clave] >0)
        {
            $productos[$posicion]['cantidad'] = $cantidades[$clave];
            $productos[$posicion]['total'] = $productos[$posicion]['precio'] * $cantidades[$clave];
        }
        else
        {
            unset($productos[$posicion]);
        }
    }

    //--------------------------------------------
    $total = 0;

    foreach($productos as $prod) 
    {
        $total += $prod['total'];
    }

    $presupuesto['productos'] = $productos;
    $presupuesto['total'] = $total;
   
}

$_SESSION['presupuesto'] = $presupuesto;

header ("Location: ./carrito.php?c=$cat&sc=$subcat");