<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");


$id = (isset($_GET['p']) && $_GET['p']) ? strval($_GET['p']) : '';

$pedido_obj = new Pedidos();
$pago_obj = new Pago();
$pedido = $pedido_obj->ConsultarPedido($_DB_, $id);

$id_pago = ($pedido['data'][0]['id_pago'] != '') ? $pedido['data'][0]['id_pago'] : 0 ;

$pago =[];
$validado = 0;
if ($id_pago > 0) {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id_pago);
    $validado =  $pago[0]['validado'];
}

?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Resumen del Pedido</h2>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li>Resumen del Pedido </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Pedido y Pago</h1>
            <div class="col-sm-12">
                <li><a href="./historico_pedido.php"> <i class="ion-android-arrow-back"></i> Volver</a></li>
            </div>
        </div>
        <div class="col-lg-12 mt-3">
            <div class="row">
                <div class="col-6">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <h3>Pedidos</h3>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Código</td>
                                        <td>
                                            <?=$pedido['data'][0]['id_pedido']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cliente</td>
                                        <td>
                                            <?=$pedido['data'][0]['cliente'];?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Fecha del Pedido</td>
                                        <td>
                                            <?php
                                            $formato = 'Y-m-d H:i:s';
                                            $fecha = DateTime::createFromFormat($formato, $pedido['data'][0]['fecha']);
                                            echo $fecha->format('d/m/Y h:i:s');
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Tasa del Pedido</td>
                                        <td><code><?=number_format($pedido['data'][0]['valor_calculo'], 2, ",", ".");?></code></td>
                                    </tr>

                                    <tr>
                                        <td>Monto $</td>
                                        <td>
                                            <?=number_format($pedido['data'][0]['monto_total'], 2, ",", ".");
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Monto Bs</td>
                                        <td>
                                            <?php
                                            $tasacompra = ($pedido['data'][0]['valor_calculo'] > 0 ) ?
                                                $pedido['data'][0]['valor_calculo'] : 1;
                                            $valorBs = number_format($pedido['data'][0]['monto_total'] * $tasacompra, 2, ",", ".") ;
                                            echo $valorBs;?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Estatus</td>
                                        <td>
                                           <?php
                                                echo "<span class='label label-info'>".$pedido['data'][0]['condicion']."</span>";
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><code>Observación del Pedido</code></td>
                                        <td><?=$pedido['data'][0]['observacion'];?></td>
                                    </tr>
                                    </tbody>
                                </table>

                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <h3>Pagos</h3>
                            <?php if ($id_pago > 0 ){ ?>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Código</td>
                                            <td>
                                                <?= $pago[0]['id'];?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Forma de Pago</td>
                                            <td>
                                                <?=$pago[0]['tipo_pago'];?>
                                            </td>
                                        </tr>

                                        <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                            <tr>
                                                <td>Tipo de Transferencia</td>
                                                <td>
                                                    <?=$pago[0]['tipo_transferencia'];?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                            <tr>
                                                <td>Banco</td>
                                                <td>
                                                    <?=$pago[0]['banco'];?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                            <tr>
                                                <td>Referencia</td>
                                                <td>
                                                    <?=$pago[0]['referencia'];?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <tr>
                                            <?php
                                            $tasa = '';
                                            if (strtolower ($pago[0]['simbolos']) =='bs')
                                            {
                                                $tasaCalculo = ($pago[0]['monto_pago'] / $pago[0]['monto_total']);
                                                $tasa = 'A tasa: '.number_format($tasaCalculo, 0, ",", ".");?>
                                            <?php }
                                            ?>
                                            <td width="15%">Monto Pagado <code><?=$tasa;?></code></td>
                                            <td>
                                                <?=$pago[0]['simbolos']." ".number_format($pago[0]['monto_pago'], 2, ",", "."); ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Fecha</td>
                                            <td>
                                                <?php
                                                $objeto_DateTime = DateTime::createFromFormat('Y-m-d', $pago[0]['fecha']);
                                                $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                                                echo $cadena_nuevo_formato;?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Estatus</td>
                                            <td>
                                                <?php
                                                if ($pago[0]['validado']) {
                                                    echo "<span class='label label-info'>Validado</span>";
                                                }else{
                                                    echo "<span class='label label-info'>Por validar pago</span>";
                                                }?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><code>Observacion del pago</code></td>
                                            <td><?=$pago[0]['observacion'];?></td>
                                        </tr>
                                        </tbody>
                                    </table>

                            <?php }else {
                                ?><h3 class="alert alert-danger">No hay pago relacionado con este pedido</h3>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <table class="table table-bordered">
                        <thead>
                        <th class="text-center">ID</th>
                        <th class="text-center">Código</th>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Cantidad</th>
                        <th class="text-center">Precio Unitario</th>
                        <th class="text-center">Total</th>
                        </thead>
                        <tbody>
                        <?php

                        if (isset($pedido['dataItems'])) {
                            $total = 0;
                            foreach ($pedido['dataItems'] as $item) { ?>
                                <tr>
                                    <td><?= $item['id_item']; ?></td>
                                    <td><?= $item['codigo_producto']; ?></td>
                                    <td><?= $item['nombre']; ?></td>
                                    <td align="right"><?= $item['cantidad']; ?></td>
                                    <td align="right"><?= number_format($item['precio_unitario'], 2, ",", "."); ?></td>
                                    <td align="right"><?= number_format($item['precio_total'], 2, ",", ".") ; ?></td>
                                </tr>
                                <?php
                                $total = $total + $item['precio_total'];
                            } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><b>Total</b></td>
                                <td align="right"><?= number_format($total, 2, ",", ".") ; ?></td>
                            </tr>

                        <?php } else { ?>
                            <tr>
                                <td class="text-center">No hay producto asociado al Pedido</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php
if ($pedido['data'][0]['metodo_entrega'] != 'retirar'){ ?>
    <div class="row">
        <div class="col-md-6 vertical">
            <div class="form-group">
                <h2><b>Dirección de entrega</b></h2>
                <p><b>Dirección Principal :</b></p><label><?=$pedido['data'][0]['direccion'];?></label>
                <hr>
                <p><b>Dirección Secundaria :</b></p><label><?=$pedido['data'][0]['direccion2'];?></label>
                <hr>
                <p><b>Observación :</b></p> <label><?=$pedido['data'][0]['observaciones'];?></label>
            </div>
        </div>
    </div>
<?php }else{ ?>
    <h3 class="alert alert-success">Retirar en tienda</h3>
<?php } ?>
</div>
<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");
