<?php

include_once(realpath(dirname(__FILE__)) . "/include/header.php");
//include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");
?>
<style>
    .product-cart{
        height:50px !important;
    }
</style>
<?php
$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : 0;
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? strval($_GET['sc']) : 0;

$nuevo = (isset($_GET['n']) && $_GET['n']) ? intval($_GET['n']) : '';
$oferta = (isset($_GET['of']) && $_GET['of']) ? intval($_GET['of']) : '';

$limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : PER_PAGE;
$page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
$links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;


$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$params = [
    'param1' =>'c',
    'value1' =>$cat,
    'param2' =>'sc',
    'value2' =>$subcat
];

$producto_obj = new Productos();

$lista_categoria = $categorias;

if($cat != '')
{
    $categoria = $categoria_obj->Obtener($_DB_, $cat);
    $lista_categoria = $categoria_obj->listado($_DB_, $cat);
}

if($subcat != '')
{$subcategoria = $categoria_obj->Obtener($_DB_, $cat, $subcat );}

if ($cat != '' and $subcat  == '') {
    $productos = $producto_obj->Listado($_DB_, $cat, '', 0, 0, $limit, $page);
} elseif ($cat  != '' and $subcat  != '') {
    $productos = $producto_obj->Listado($_DB_, $cat, $subcat, 0, 0, $limit, $page);
} elseif ($nuevo != '') {
    $productos = $producto_obj->Listado($_DB_, '', '', 1, 0, $limit, $page);
} elseif ($oferta != '') {
    $productos = $producto_obj->Listado($_DB_, '', '', 0, 1, $limit, $page);
}else{
    $productos = $producto_obj->Listado($_DB_, '', '', 0, 0, $limit, $page);
}


/*if(!isset($productos))
{
    $productos1 = $productos_obj->Listado($_DB_, 0, 0, 1, 0, $limit, $page);
    $productos2 = $productos_obj->Listado($_DB_, 0, 0, 0, 1, $limit, $page);

    $productos = array_merge($productos1, $productos2);
}*/

$Paginator  = new Paginator( $productos['total'], $limit, $page );

?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/banner_sub3.jpg)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Productos</h2>
            <ul>
                <li><a href="./index.php">Inicio</a></li>

                <?php
                if($cat != '') { ?>
                <li><a style="color: white" href="./listado.php">Categorías</a></li><?php
                }else{ ?>
                    <li>Categorías</li><?php
                }


                if($cat != '' and $subcat != '') {?>
                    <li><a  href="./listado.php?c=<?php echo $categoria[0]['categoria'];?>"><?php echo $categoria[0]['nombre'];?></a></li><?php
                }elseif($cat != ''){?>
                    <li><?php echo $categoria[0]['nombre'];?></li><?php
                }

                if($subcat != ''){?>
                    <li><?php echo $subcategoria[0]['nombre'];?></li><?php
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<div class="shop-page-wrapper shop-wrapper-pd">
    <div class="container-fluid">
        <?php
        if ($_SESSION['categorias'])
        { ?>
        <div class="shop-sidebar">
            <div class="sidebar-widget mb-45">
                
                <?php
                if($cat != '') { ?>
                    <h3 class="sidebar-title">Subcategorias</h3>
                <?php
                }else{ ?>
                    <h3 class="sidebar-title">Categorias</h3>
                <?php
                }
                ?>

                
                <div class="sidebar-categories">
                    <ul>
                        <?php

                        if($cat == 0)
                        {
                            $lista_categorias = $_SESSION['categorias'];

                            foreach ($lista_categorias as $categoria)
                            {
                                if($categoria[$cat]['num_productos'] >0)
                                {?>
                                <li><a href="./listado.php?c=<?php echo $categoria[$cat]['categoria'];?>&sc=<?php echo $categoria[$cat]['subcategoria'];?>"><?php echo $categoria[$cat]['nombre']; ?> <span><?php echo $categoria[$cat]['num_productos']; ?></span></a></li>
                                <?php
                                }
                            }
                            ?>
                            <li><a href="./index.php"> <i class="ion-android-arrow-up"></i> Volver</a></li><
                            <?
                        }
                        else
                        {
                            $lista_categorias = $_SESSION['categorias'][$cat];

                            foreach ($lista_categorias as $categoria)
                            {
                                if($categoria['num_productos'] >0 and $categoria['subcategoria']>0)
                                {?>
                                <li><a href="./listado.php?c=<?php echo $categoria['categoria'];?>&sc=<?php echo $categoria['subcategoria'];?>"><?php echo $categoria['nombre']; ?> <span><?php echo $categoria['num_productos']; ?></span></a></li>
                                <?php
                                }
                            }
                            ?>
                            <li><a href="./listado.php"> <i class="ion-android-arrow-back"></i> Volver</a></li>
                            <?
                        }
                        ?>
                    </ul>

                </div>
            </div>
        </div>
        <?php } ?>


        <div class="shop-product-wrapper">
            <div class="shop-bar-area">
                <div class="shop-product-content tab-content">
                    <div id="grid-sidebar1" class="tab-pane fade active show">
                        <div class="row custom-row">
                        <?php
                            if ($productos["total"]>0) {
                                foreach ($productos['data'] as $producto) {
                                    ?>

                                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                        <div class="single-product mb-35">
                                            <div class="product-img">
                                                <a href="./producto_detallado.php?c=<?php echo $producto['cod_categoria']; ?>&sc=<?php echo $producto['cod_subcategoria']; ?>&p=<?php echo $producto['cod_producto']; ?>">
                                                    <img src="./Productos/thumbs/<?php echo $producto['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="">
                                                </a>

                                                <?php
                                                if($producto['nuevo']) { ?>
                                                    <span>Nuevo</span><?php
                                                }elseif($producto['oferta']){?>
                                                    <span>Recomendado</span><?php
                                                }
                                                ?>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title-price">
                                                    <div class="product-title">
                                                        <h4>
                                                            <a href="./producto_detallado.php?c=<?php echo $producto['cod_categoria']; ?>&sc=<?php echo $producto['cod_subcategoria']; ?>&p=<?php echo $producto['cod_producto']; ?>">
                                                                <?php echo $producto['nombre']; ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>

                                                <div class="product-title-price">
                                                    <div class="product-title">
                                                        <?/*
                                                        <?= $producto['descripcion_marca']; ?>
                                                        */?>
                                                    </div>
                                                    <div class="product-price">
                                                        <span><?= $simbolo." ".  number_format($producto['precio1_full'] * $ValorCalculo, 2, ",", "."); ?></span>
                                                    </div>
                                                </div>

                                                <div class="product-cart-categori">
                                                    <div class="product-cart">
                                                        <span></span>
                                                    </div>
                                                    <div class="product-categori pt-2">
                                                        <a href="./carrito_o.php?c=<?php echo $producto['cod_categoria']; ?>&sc=<?php echo $producto['cod_subcategoria']; ?>&p=<?php echo $producto['cod_producto']; ?>&op=add"  class="add_cart"><i class="ion-bag"></i> Agregar al Carrito</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            <?php
                                }
                            } ?>

                        </div>
                        <?php echo $Paginator->createLinks( $links, 'pagination pg-amber justify-content-center', $params); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");