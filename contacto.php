<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb2.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>contacto</h2>
            <ul>
                <li><a href="#">Inicio</a></li>
                <li> Contáctanos </li>
            </ul>
        </div>
    </div>
</div>
<!-- contact-area start -->
<div class="contact-area ptb-100">
    <div class="container">
        <div class="contact-info">
            <h2 class="contact-title">Información</h2>
            <div class="row">
                <div class="col-lg-8 col-md-12 col-12">
                    <div class="single-contact-info mb-40">
                        <div class="contact-info-icon">
                            <i class="ion-ios-location-outline"></i>
                        </div>
                        <div class="contact-info-content">
                            <p>
                                <b>Santa Mónica:</b><br> Calles las ciencias,  entre calles Aranda y Sanoja. Quinta San José, Los Chaguaramos. 
                            </p>
                            <p>
                                <b>El Paraíso:</b><br> Av. José Antonio Páez. Quinta Ana Teresa, urbanización El Paraíso. 
                            </p>
                            <p>
                                <b>Bello Monte:</b><br> Av Miguel Ángel, edificio humage, piso PB, Colinas de Bello Monte
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-contact-info mb-40">
                        <div class="contact-info-icon">
                            <i class="ion-ios-telephone-outline"></i>
                        </div>
                        <div class="contact-info-content">
                            <p>
                                Teléfono : <br> +58 424-155.19.92
                            </p>
                        </div>
                    </div>
                </div>
                <?/*
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-contact-info mb-40">
                        <div class="contact-info-icon">
                            <i class="ion-ios-email-outline"></i>
                        </div>
                        <div class="contact-info-content">
                            <p>Email : <br> <a href="#">info@example.com</a> </p>
                        </div>
                    </div>
                </div>
                */?>
            </div>
        </div>
        <div class="contact-form-wrap">
            <h2 class="contact-title">Escríbenos</h2>
            <div class="contact-message">
                <form id="contact-form" action="assets/mail.php" method="post">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="contact-form-style contact-font-one">
                                <input name="name" placeholder="Nombre" type="text">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-form-style contact-font-two">
                                <input name="email" placeholder="Email" type="email">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-form-style contact-font-three">
                                <input name="subject" placeholder="Titulo" type="text">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-form-style contact-font-four">
                                <textarea name="message" placeholder="Mensaje"></textarea>
                                <button class="submit btn-hover" type="submit"> Enviar</button>
                            </div>
                        </div>
                    </div>
                </form>
                <p class="form-messege"></p>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");


