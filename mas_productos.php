<?php

include_once(realpath(dirname(__FILE__)) . "/include/header.php");
//include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");

$search = '';
$accion = (isset($_POST['accion']) && $_POST['accion']) ? strval($_POST['accion']) : $_GET['accion'];
$search = (isset($_POST['search']) && $_POST['search']) ? strval($_POST['search']) : isset($_GET['search']);

$limit      = (isset($_GET['limit'])) ? $_GET['limit'] : PER_PAGE;
$page       = (isset($_GET['page'])) ? $_GET['page'] : 1;
$links      = (isset($_GET['links'])) ? $_GET['links'] : 7;

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$productos = array();

switch ($accion) {
    case 'ofertas':
        $productos = $productos_obj->ListadoVerMas($_DB_, 0, 1, 0, $limit, $page);
        break;
    case 'destacados':
        $productos = $productos_obj->ListadoVerMas($_DB_, 0, 0, 1, $limit, $page);
        break;
    case 'nuevos':
        $productos = $productos_obj->ListadoVerMas($_DB_, 1, 0, 0, $limit, $page);
        break;
    case 'busqueda':
        if (!empty($search))
            $productos = $productos_obj->Buscador($_DB_, $search, $limit, $page);
}


$total = (isset($productos['total'])) ? $productos['total'] : 0;
$params = [
    'param1' => 'accion',
    'value1' => $accion,
    'param2' => 'search',
    'value2' => $search
];

$Paginator  = new Paginator($total, $limit, $page);
?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Productos <?= $accion;?></h2>
            <ul>
                <li><a href="./index.php">Inicio</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="checkout-area ptb-100">
    <div class="container">
        <div class="shop-filters-right">
            <div class="shop-product-content tab-content">

                <div id="grid-5-col1" class="tab-pane fade active show">
                    <div class="row custom-row">
                        <?php
                        if (isset($productos['data'])) {
                            foreach ($productos['data'] as $prod) {
                        ?>
                                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                    <div class="single-product mb-35">
                                        <div class="product-img">
                                            <a href="./producto_detallado.php?c=<?php echo $prod['cod_categoria']; ?>&sc=<?php echo $prod['cod_subcategoria']; ?>&p=<?php echo $prod['cod_producto']; ?>">
                                                <img src="./Productos/thumbs/<?php echo $prod['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="">
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title-price">
                                                <div class="product-title">
                                                    <h4><a href="./producto_detallado.php?c=<?php echo $prod['cod_categoria']; ?>&sc=<?php echo $prod['cod_subcategoria']; ?>&p=<?php echo $prod['cod_producto']; ?>"><?php echo $prod['nombre']; ?></a></h4>
                                                </div>
                                            </div>

                                            <div class="product-title-price">
                                                <span><?php echo  $prod['presentacion']; ?></span>
                                                <div class="product-price">
                                                    <span><?= $simbolo . " " .  number_format($prod['precio'] * $ValorCalculo, 2, ",", "."); ?></span>
                                                </div>
                                            </div>


                                            <div class="product-cart-categori">
                                                <span><br></span>
                                                <div class="product-categori">
                                                    <a href="./carrito_o.php?c=<?php echo $prod['cod_categoria']; ?>&sc=<?php echo $prod['cod_subcategoria']; ?>&p=<?php echo $prod['cod_producto']; ?>&op=add" class="add_cart"><i class="ion-bag"></i> Agregar al Carrito</a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php }
                        } ?>
                    </div>
                    <?php echo $Paginator->createLinks($links, 'pagination pg-amber justify-content-center', $params); ?>
                </div>

                <!--grid-->

                <div id="grid-5-col2" class="tab-pane fade">
                    <?php echo $Paginator->createLinks($links, 'pagination pg-amber justify-content-center', $params); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
