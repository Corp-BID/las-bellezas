<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$ped = new Pedidos();
$cliente = new Clientes();
$pago = new Pago();

$posicion = ($_GET['pos']>=0)?intval($_GET['pos']):NULL;

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];

$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$pedidos = $ped -> ListadoPedidoClientes($_DB_, $_SESSION['cliente']['correo']);

$pedido = $pedidos["$posicion"];

$datos_pedido = $ped -> ConsultarPedido($_DB_, $pedido['id_pedido']);
$productos = $ped -> ConsultarItemsPedido($_DB_, $pedido['id_pedido']);
$datos_facturacion = $cliente -> obtenerDatosFacturacion($_DB_, $pedido['datos_facturacion']);
$datos_pago = $pago -> ConsultarPagoPedido($_DB_, $pedido['id_pedido']);

?>
    <div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
        <div class="container">
            <div class="breadcrumb-content">
                <h2>Resumen del Pedido</h2>
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li>Resumen del Pedido </li>
                </ul>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row">
        <div class="col-sm-12"> 
            <div class="table-responsive">
                    
                <h2>Resumen del Pedido</h2>
                <div class="col-sm-12">
                    <li><a href="./historico_pedido.php"> <i class="ion-android-arrow-back"></i> Volver</a></li>
                </div>
                
                    <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td class="text-center"></td>
                        <td class="text-left">Código</td>
                        <td class="text-left">Producto</td>
                        <td class="text-right">Cantidad</td>
                        <td class="text-right">Precio Unitario</td>
                        <td class="text-right">Total</td>
                    </tr>
                    </thead>
                       
                    <tbody>
                    <?php
                    $total_piezas = 0;
                    $total_bolivares = 0;     
                
                    $produc = new Productos();
                        
                    foreach($productos as $prod)
                    {
                        $producto = $produc -> Consultar($_DB_, $prod['codigo_producto']);
                        ?>
                    <tr>
                        
                        <td class="product-thumbnail text-center">
                           <!-- <img src="./Productos/<?php /*echo $producto[0]['foto'];*/?>"  onerror="this.src='./Productos/no_disponible.jpg';" class="img-responsive" alt="<?php /*echo $prod['descripcion'];*/?>" title="<?php /*echo $prod['descripcion'];*/?> width="100">-->
                            <a href="#"><img src="./Productos/thumbs/<?php echo $producto[0]['foto1']; ?>" onerror="this.src='./Productos/no_disponible.jpg';" width="100" alt="<?php echo $prod['descripcion'];?> title="<?php echo $prod['descripcion'];?>"></a>
                        </td>
                        <td class="text-left"><?php echo $prod['codigo_producto'];?></td>
                        <td class="text-left"><?php echo $prod['nombre'];?><br><? echo $prod['marca'];?><br></td>
                        <td class="text-right"><?php echo $prod['cantidad']; $total_piezas += $prod['cantidad'];?></td>
                        <td class="text-right"><?= $DatosMoneda[0]['simbolos']." ". number_format($prod['precio_unitario'] ,2,",",".");?></td>
                        <td class="text-right"><?= $DatosMoneda[0]['simbolos']." ". number_format($prod['precio_total'] ,2,",","."); $total_bolivares += $prod['precio_total'];?></td>
                    </tr>
                    <?php
                    }?>    
                    </tbody>
                    </table>
                </div>
   
                <br>
                
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td class="text-right"><strong>Total a pagar:</strong></td>
                            <td class="text-right"><?= $DatosMoneda[0]['simbolos']." ". number_format($total_bolivares ,2,",",".");?></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
        </div>

        <div  class="col-sm-6">
            <h2>Datos de Facturación</h2>
            <table class="table table-bordered">
            <tr>
                <td><b>Nombre o Razon Social:</b> <?php echo $datos_facturacion[0]['nombre'];?></td>
            </tr>
            <tr>
                <td><b>Cedula o Rif:</b> <?php echo $datos_facturacion[0]['cedula'];?></td>
            </tr>
            <tr>
                <td><b>Teléfono:</b> <?php echo $datos_facturacion[0]['telefonos'];?></td>
            </tr>

            <tr>
                <td><b>Dirección:</b> <?php echo $datos_facturacion[0]['direccion'];?></td>
            </tr>
            <tr>
                <td><b>Dirección de Entrega:</b> <?php echo $datos_facturacion[0]['direccion2'];?></td>
            </tr>
            <tr>
                <td><b>Observaciones:</b> <?php echo $datos_facturacion[0]['observaciones'];?></td>
            </tr>
            </table>
            <br>
        </div>

        <div class="col-sm-6">
            <h2>Reportar Pago</h2>
            <table class="table table-bordered">
            <tr>
                <td><b>Banco que Utilizo:</b> <?= isset($datos_pago['0']['banco']);?></td>
            </tr>
            <tr>
                <td><b>Número de Referencia o Recibo:</b> <?= isset($datos_pago['0']['referencia']);?></td>
            </tr>
            <tr>
                <?php if (isset($datos_pago['0']['monto_pago'])){ ?>
                    <td><b>Monto:</b> <?= $datos_pago['0']['simbolos']." ". number_format($datos_pago['0']['monto_pago'] ,2,",",".");?></td>
                <?php } else {
                    ?>
                    <td><b>Monto:</b></td>
                <?php } ?>
            </tr>
            <tr>

                    <?php
                    if (isset($datos_pago['0']['fecha'])) { ?>
                        <td><b>Fecha:</b> <?php
                        $objeto_DateTime = DateTime::createFromFormat('Y-m-d', $datos_pago['0']['fecha']);
                        $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                        echo $cadena_nuevo_formato;
                        ?> </td>
                        <?php }
                    ?>

            </tr>
            </table>
        </div>
    </div>

    <div class="col-sm-12">
        <li><a href="./historico_pedido.php"> <i class="ion-android-arrow-back"></i> Volver</a></li>
    </div>
    <br>
</div>


<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");
