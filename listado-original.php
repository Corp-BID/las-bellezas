<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : '';
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? strval($_GET['sc']) : '';

$nuevo = (isset($_GET['n']) && $_GET['n']) ? intval($_GET['n']) : '';
$oferta = (isset($_GET['of']) && $_GET['of']) ? intval($_GET['of']) : '';

if($cat != '')
{$categoria = $categoria_obj->Obtener($_DB_, $cat);}

if($subcat != '')
{$subcategoria = $categoria_obj->Obtener($_DB_, $cat, $subcat );}

$categorias = $categoria_obj->listado($_DB_, $cat , $subcat );

if ($cat != '' and $subcat  == '') {
    $productos = $productos_obj->Listado($_DB_, $cat, $subcat);
} elseif ($cat  != '' and $subcat  != '') {
    $productos = $productos_obj->Listado($_DB_, $cat, $subcat);
} elseif ($nuevo != '') {
    $productos = $productos_obj->Listado($_DB_, 0, 0, 1);
} elseif ($oferta != '') {
    $productos = $productos_obj->Listado($_DB_, 0, 0, 0, 1);
}

if(!$productos)
{
    $productos1 = $productos_obj->Listado($_DB_, 0, 0, 1, 0);
    $productos2 = $productos_obj->Listado($_DB_, 0, 0, 0, 1);

    $productos = array_merge($productos1, $productos2);
}

?>
<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Productos</h2>
            <ul>
                <li><a href="./index.php">Inicio</a></li>

                <?php
                if($cat != '')
                {?><li><a href="./listado.php">Categorías</a></li><?php}
                else
                {?><li>Categorías</li><?php}


                if($cat != '' and $subcat != '')
                {?><li><a href="./listado.php?c=<?php echo $categoria[0]['categoria'];?>"><?php echo $categoria[0]['nombre'];?></a></li><?php}
                elseif($cat != '')
                {?><li><?php echo $categoria[0]['nombre'];?></li><?php}


                if($subcat != '')
                {?><li><?php echo $subcategoria[0]['nombre'];?></li><?php}
                ?>
            </ul>
        </div>
    </div>
</div>

<div class="shop-page-wrapper shop-wrapper-pd">
    <div class="container-fluid">
        <div class="shop-sidebar">
            <div class="sidebar-widget mb-45">
                <h3 class="sidebar-title">Categorias</h3>
                <div class="sidebar-categories">
                    <ul>
                        <?php
                        if ($categorias)
                        {
                            foreach ($categorias as $categoria)
                            { ?>
                                <li><a href="./listado.php?c=<?php echo $categoria['categoria'];?>&sc=<?php echo $categoria['subcategoria'];?>"><?php echo $categoria['nombre']; ?> <span><?php echo $categoria['num_productos']; ?></span></a></li>
                            <?php
                            }
                        }

                        if($cat != '')
                        {?><li><a href="./listado.php"> <i class="ion-android-arrow-back"></i> Volver</a></li><?php}
                        else
                        {?><li><a href="./index.php"> <i class="ion-android-arrow-up"></i> Volver</a></li><?php}
                        ?>


                    </ul>

                </div>
            </div>

        </div>
        <div class="shop-product-wrapper">
            <div class="shop-bar-area">
                <!--
                <div class="shop-bar pb-60">
                    <div class="shop-found-selector">
                        <div class="shop-found">
                            <p><span>23</span> Product Found of <span>50</span></p>
                        </div>
                        <div class="shop-selector">
                            <label>Sort By : </label>
                            <select name="select">
                                <option value="" selected="selected">Default</option>
                                <option value="">A to Z</option>
                                <option value=""> Z to A</option>
                                <option value="">In stock</option>
                            </select>
                        </div>
                    </div>
                    <div class="shop-filter-tab">
                        <div class="shop-tab nav" role="tablist">
                            <a class="active" href="#grid-sidebar1" data-toggle="tab" role="tab" aria-selected="false">
                                <i class="ion-android-apps"></i>
                            </a>
                            <a href="#grid-sidebar2" data-toggle="tab" role="tab" aria-selected="true">
                                <i class="ion-android-menu"></i>
                            </a>
                        </div>
                    </div>
                </div>
                -->
                <div class="shop-product-content tab-content">
                    <div id="grid-sidebar1" class="tab-pane fade active show">
                        <div class="row custom-row">


                            <?php
                            if ($productos) {
                                $j = 1;
                                foreach ($productos as $producto) {
                                    ?>

                                    <div class="custom-col-4 custom-col-style">
                                        <div class="single-product mb-35">
                                            <div class="product-img">
                                                <a href="./producto_detallado.php?c=<?php echo $producto['cod_categoria']; ?>&sc=<?php echo $producto['cod_subcategoria']; ?>&p=<?php echo $producto['cod_producto']; ?>">
                                                    <img src="./Productos/<?php echo $producto['foto1']; ?>" alt="">
                                                </a>

                                                <?php
                                                if($producto['nuevo'])
                                                {?><span>Nuevo</span><?php}
                                                elseif($producto['oferta'])
                                                {?><span>Oferta</span><?php}
                                                ?>


                                                <!--
                                                        <span>sale</span>
                                                        <div class="product-action">
                                                            <a title="Wishlist" class="animate-left" href="#"><i class="ion-ios-heart-outline"></i></a>
                                                            <a title="Quick View" data-toggle="modal" data-target="#exampleModal" class="animate-right" href="#"><i class="ion-ios-eye-outline"></i></a>
                                                        </div>
                                                    -->
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title-price">
                                                    <div class="product-title">
                                                        <h4>
                                                            <a href="./producto_detallado.php?c=<?php echo $producto['cod_categoria']; ?>&sc=<?php echo $producto['cod_subcategoria']; ?>&p=<?php echo $producto['cod_producto']; ?>">
                                                                <?php echo $producto['nombre']; ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div class="product-price">
                                                        <span>Bs. <?php echo  number_format($producto['precio'], 0, ",", "."); ?></span>
                                                    </div>
                                                </div>
                                                <div class="product-cart-categori">
                                                    <div class="product-cart">
                                                        <span></span>
                                                    </div>
                                                    <div class="product-categori">
                                                        <a href="#"><i class="ion-bag"></i> Agregar al Carrito</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            <?php
                                    $j++;
                                }
                            } ?>


                            <!--
                            <div class="custom-col-4 custom-col-style">
                                <div class="single-product mb-35">
                                    <div class="product-img">
                                        <a href="#"><img src="./img/product/3.jpg" alt=""></a>
                                        <span>sale</span>
                                        <div class="product-action">
                                            <a title="Wishlist" class="animate-left" href="#"><i class="ion-ios-heart-outline"></i></a>
                                            <a title="Quick View" data-toggle="modal" data-target="#exampleModal" class="animate-right" href="#"><i class="ion-ios-eye-outline"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <div class="product-title-price">
                                            <div class="product-title">
                                                <h4><a href="http://preview.freethemescloud.com/neha/product-details-6.html">WOODEN
                                                        FURNITURE</a></h4>
                                            </div>
                                            <div class="product-price">
                                                <span>$110.00</span>
                                            </div>
                                        </div>
                                        <div class="product-cart-categori">
                                            <div class="product-cart">
                                                <span>Electronic</span>
                                            </div>
                                            <div class="product-categori">
                                                <a href="#"><i class="ion-bag"></i> Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
