<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?$_POST['op'] :"";
$ve = (isset($_POST['ve']) && $_POST['ve'])?$_POST['ve'] :"";

if($operacion == "")
{$operacion = (isset($_GET['op']) && $_GET['op'])?substr(strval($_GET['op']), 0, 6) :NULL; }

if($operacion == "new")
{
    if(!$_SESSION['cliente'])
    {
        $observacion = (isset($_POST['observacion']) ) ? $_POST['observacion'] :"";
        $_SESSION['presupuesto']['observacion'] = $observacion;
        header("Location: ./login.php?pag=pago");
    }
    else
    {
        if($_SESSION['presupuesto'])
        {
            $monedas_obj = new Monedas();
            $tasa_obj = new TasaDolar();

            $valorMoneda = $monedas_obj->MonedaPorDefectoConf($_DB_);
            $isMonedaDefault = $valorMoneda['id'];
            $tasaDefault = $tasa_obj->TasaConfDefecto($_DB_, $isMonedaDefault);
            $tasaIsDefault =  number_format($tasaDefault['valor_calculo'], 2, ",", ".");
            $idTasaIsActiva =  $tasaDefault['id'];

            if($_SESSION['presupuesto']['observacion'])
            {$observacion = $_SESSION['presupuesto']['observacion'];}
            else
            {$observacion = (isset($_POST['observacion']) ) ? $_POST['observacion'] :"";}
            
            $pedido = new Pedidos();

            $id_pedido = $pedido -> AgregarPedido($_DB_, $_SESSION['cliente']['correo'], $_SESSION['presupuesto']['total'],
                $observacion, $idTasaIsActiva );

            $productos = $_SESSION['presupuesto']['productos'];

            foreach($productos as $prod)
            {
                $pedido -> AgregarItemsPedido($_DB_, $id_pedido, $prod['codigo'], $prod['descripcion'], $prod['marca'], '', $prod['cantidad'], $prod['precio'], $prod['total']);
            }

            $_SESSION['id_pedido'] = $id_pedido;

            // datos de la facturacion //
            $correo = $_SESSION['cliente']['correo'];
            $nombre = (isset($_POST['nombre']) && $_POST['nombre'])?substr(strval($_POST['nombre']), 0, 300) :NULL;
            $cedula = (isset($_POST['cedula']) && $_POST['cedula'])?substr(strval($_POST['cedula']), 0, 300) :NULL;
            $telefonos = (isset($_POST['telefonos']) && $_POST['telefonos'])?substr(strval($_POST['telefonos']), 0, 300) :NULL;


            $cliente = new Clientes();
            $datos_facturacion = $cliente -> AgregarDatosFacturacion($_DB_, $correo, $nombre, $cedula, $telefonos);
            $_SESSION['cliente_id'] = $datos_facturacion;

            if($_SESSION['id_pedido'])
            {
                $pedido = new Pedidos();
                $pedido -> AgregarDatosFacturacion($_DB_, $_SESSION['id_pedido'], $datos_facturacion);
            }
            if ($ve=='login')
                header("Location: ./pago2.php");
            else
                header("Location: ./pago.php?op=fac");
        }
    }
}
elseif($operacion == "fac")
{

    $nombre = (isset($_POST['nombre']) && $_POST['nombre'])?$_POST['nombre'] :NULL;
    $cedula = (isset($_POST['cedula']) && $_POST['cedula'])?$_POST['cedula'] :NULL;
    $telefonos = (isset($_POST['telefonos']) && $_POST['telefonos'])?$_POST['telefonos']:NULL;

    $cliente = new Clientes();

    $cliente -> ModificarClienteEntrega($_DB_, $_SESSION['cliente_id'], $cedula, $nombre, $telefonos);

    header("Location: ./pago2.php");
}

elseif($operacion == "pago")
{

    $pago = new Pago();
    $pedido = new Pedidos();
    $cliente = new Clientes();

    $conf_tienda_obj = new Config_Tienda();

    $monedas_obj = new Monedas();
    $categorias = $categoria_obj->listado($_DB_);

    $DatosMoneda = $_SESSION['DatosMonedaDefecto'];
    $DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
    $TasaDestino = $_SESSION['TasaDestino'];


    $moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
    $simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

    $idMoneda = '';
    $idTasa='';

    $tasa_pedido = $pedido -> ConsultarPedido ($_DB_, $_SESSION['id_pedido']);


    $idTasa = $tasa_pedido['data'][0]['id_tasa'];

    $idMoneda = $DatosMoneda[0]['id'];
    $ValorCalculo = $_SESSION['ValorCalculo'];

    if (isset($DatosMonedaDestino[0]['id'])){
        if ($DatosMoneda[0]['id'] != $DatosMonedaDestino[0]['id']){
            $idMoneda = $DatosMonedaDestino[0]['id'];
        }
    }

    $observacion_efectivo = (isset($_POST['observacion_efectivo']) && $_POST['observacion_efectivo'])?$_POST['observacion_efectivo'] :NULL;

    $banco = (isset($_POST['banco']) && $_POST['banco'])?$_POST['banco'] :NULL;
    $referencia = (isset($_POST['referencia']) && $_POST['referencia'])?$_POST['referencia'] :NULL;
    $monto = (isset($_POST['monto']) && $_POST['monto'])?$_POST['monto'] :NULL;

    $fecha = (isset($_POST['fecha']) && $_POST['fecha'])?$_POST['fecha'] :NULL;
    $forma_pago = (isset($_POST['forma_pago']) && $_POST['forma_pago'])?$_POST['forma_pago'] :NULL;
    $opcion_pago = (isset($_POST['opcion_pago']) && $_POST['opcion_pago'])?$_POST['opcion_pago'] :NULL;
    $opcion_entrega = (isset($_POST['opcion_entrega']) && $_POST['opcion_entrega'])?$_POST['opcion_entrega'] :NULL;

    if (!empty($_POST['bancoNacional']))
        $banco = $_POST['bancoNacional'];
    else
        $banco = $_POST['bancoInternacional'];

    if (!empty($_POST['referenciaNacional']))
        $referencia = $_POST['referenciaNacional'];
    else
        $referencia = $_POST['referenciaInternacional'];

    if (!empty($_POST['fechaNacional']))
        $fecha = $_POST['fechaNacional'];
    else
        $fecha = $_POST['fechaInternacional'];

    $newFecha = date('Y-m-d', strtotime($fecha));
    $monto_pago = ($monto * $ValorCalculo);

    if($_SESSION['id_pedido'])
    {


        $pago -> AgregarPago($_DB_, $_SESSION['id_pedido'], $banco, $referencia, $monto_pago, $newFecha, $forma_pago, $opcion_pago, $idTasa, $idMoneda, $monto, $observacion_efectivo );
        $pedido -> AgregarPago($_DB_, $_SESSION['id_pedido'], $forma_pago);
        $pedido -> ModificarCondicionEntrega($_DB_, $_SESSION['id_pedido'],$opcion_entrega);
    }

    /******************************/
    //actualizar cliente facturacion

    $direccion = (isset($_POST['direccion']) && $_POST['direccion'])?$_POST['direccion'] :NULL;
    $direccion2 = (isset($_POST['direccion2']) && $_POST['direccion2'])?$_POST['direccion2'] :NULL;
    $observaciones = (isset($_POST['observaciones']) && $_POST['observaciones'])?$_POST['observaciones'] :NULL;

    $cliente -> ModificarDireccionEntrega($_DB_, $_SESSION['cliente_id'], $direccion, $direccion2,$observaciones);

    
    $_SESSION['id_pedido'] = NULL;
    $_SESSION['presupuesto'] = NULL;
    $_SESSION['cliente_id'] = NULL;
    
    header("Location: ./historico_pedido.php");
}