<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
?>
<style>

    #owl-demo .item{
        margin: 3px;
    }
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }

</style>

<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Conócenos</h2>
            <ul>
                <li><a href="#">Inicio</a></li>
                <li> Conócenos </li>
            </ul>
        </div>
    </div>
</div>
<div class="about-story ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="about-story">
                    <h3 class="story-title">Sobre Express Charcuteria</h3>
                    <p class="story-subtitle">
                        Nacimos hace 3 años con un solo objetivo llevar a su mesa los mejores productos en charcutería, y es que a quien no le gusta un buen sándwich, un warp o nuestra tradicional arepa rellena con jamón, queso, salchichón mortadela y para usted de contar.
                    </p>
                    <p class="story-subtitle">
                        ¿Se te hizo agua la boca?... En Charcutería Express encontrarás de todo para engalanar tu mesa, productos frescos de altísima calidad que solo tu paladar podrá probar.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="about-story-img">
                    <div class="about-story-img1">
                        <img src="assets/img/team/1.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="brand-logo ptb-100">
    <div id="demo">
        <div class="container">
            <div class="section-title-others text-center mb-55">
                <h2>Nuestras Marcas</h2>
            </div>
            <div class="row">
                <div class="col-12">

                    <div id="owl-demo" class="owl-carousel">
                        <div class="item"><img src="./Marca/1.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/2.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/3.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/4.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/5.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/6.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/7.png" alt="Owl Image"></div>
                        <div class="item"><img src="./Marca/8.png" alt="Owl Image"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>

<script>
    $(document).ready(function() {

        $("#owl-demo").owlCarousel({
            autoplay: true,
            dots: true,
            loop: true,
            items: 4
        });

    });
</script>

<!--<script>
$(document).ready(function() {
    $("#owl-demo").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        items: 4
    });
    });
</script>-->