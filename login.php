<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$login = (isset($_POST['user-name'])) ? $_POST['user-name'] : NULL;
$password = (isset($_POST['user-password'])) ? $_POST['user-password'] :NULL;
$pag = (isset($_GET['pag']) && $_GET['pag'])?substr(strval($_GET['pag']), 0, 100) :$_POST['pag'];

if(trim($login) and trim($password))
{
	$cliente = new Clientes();
	$cliente = $cliente -> ValidarCliente($_DB_, $login, $password);
    
    $log = new Logs();

    if($cliente['correo'])
    {
        $_SESSION['cliente'] = $cliente;    
        
        $log->login($_DB_, $login, 1, "login");

        if($pag == 'pago')
        {header("Location: ./pago.php?op=new&ve=login");}
        else
        {header("Location: ./index_login2.php");}
        
    }
    else
    {
        $log->login($_DB_, $login, 0, "login");
        header("Location: ./index_login.php?w=1&pag=".$pag);
    }
}
else
{
    if(!trim($login)){
        if($pag == 'pago') {
            header("Location: ./index_login.php?pag=" . $pag);
        }else{
            header("Location: ./index_login.php");}
    }else
        header("Location: ./index_login.php?w=1");
}