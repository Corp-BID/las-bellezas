<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");


if (isset($_GET['vis'])){
    $visible = ($_GET['vis'] == 1) ? 1 : 0;
}else{
    $visible = 1;
}

$tasa_cambio_obj = new TasaDolar();

date_default_timezone_set('America/Caracas');
$fecha_consulta = date('Y-m-d');

$tasa_cambio = $tasa_cambio_obj->ListadoDia($_DB_, $visible);
$fecha = date('d/m/Y');
?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">TASA DE CAMBIO - <b><?=$fecha;?></b></h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <form action="tasa_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Tasa de Cambio">
            </form>
            <div class="consultar">
                <?php
                if (!$visible) { ?>
                    <a href="?vis=1" class="btn btn-warning">Historico</a>
                    <?php
                } else { ?>
                    <a href="?vis=0" class="btn btn-info">Tasa del Día</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">

                <table class="table table-bordered table-hover" style="width: 80%;" align="center">
                    <thead>
                        <th width="15%" class="text-center">Id</th>
                        <th width="15%" class="text-center">Fecha - Hora</th>
                        <th width="10%" class="text-center">Moneda</th>
                        <th width="10%" class="text-center">Valor</th>
                        <th width="15%" class="text-center">Por defecto</th>
                        <th width="10%" class="text-center"></th>
                    </thead>
                    <tbody>
                        <?php
                        $moneda_anterior = '';
                        if ($tasa_cambio) {
                            foreach ($tasa_cambio as $tasa) {
                                $por_defecto = ($tasa['activo'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                $formato = 'Y-m-d H:i:s';
                                $fecha = DateTime::createFromFormat($formato, $tasa['fecha']);
                                if ($moneda_anterior != $tasa['moneda_id'] ){
                                ?>
                                    <tr>
                                        <td class="text-left"><span class="badge badge-secondary" style="font-size: 15px;"><?php echo $tasa['descripcion']."<b>(".$tasa['simbolos'].")</b>"; ?></span></td>
                                    </tr>
                                    <?php } ?>
                                <tr>
                                    <td class="text-center"><?php echo $tasa['id']; ?></td>
                                    <td class="text-center"><?php echo $fecha->format('d/m/Y h:i:s'); ?></td>
                                    <td class="text-center"><?php echo $tasa['moneda_destino']; ?></td>
                                    <td class="text-right"><?php echo number_format($tasa['valor_calculo'], 2, ",", "."); ?></td>
                                    <td class="text-center"><?php echo $por_defecto; ?></td>
                                    <td class="align-middle text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-outline btn-success dim" href="tasa_gestion.php?o=mod&id=<?php echo $tasa['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-outline btn-danger dim" href="tasa_gestion.php?o=del&id=<?php echo $tasa['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                        <?php
                                $moneda_anterior = $tasa['moneda_id'];
                            }
                        }else { ?>
                            <h1>No hay Tasa de Cambio</h1>
                            <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
