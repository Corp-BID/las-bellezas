<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['id']) && $_GET['id']) ? strval($_GET['id']) : '';

$vigencia_obj = new VigenciaPedido();


$usa_form = true;
$boton = "Crear";
$operacion = "add";

if ($id and $op == 'mod') {
    $vigencia = $vigencia_obj->Consultar($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Horas";
    $boton = "Modificar";
    $usa_form = true;
} elseif ($id and $op == 'del') {
    $vigencia = $vigencia_obj->Consultar($_DB_, $id);
    $operacion = "del";
    $titulo = "Eliminar Horas";
    $boton = "Eliminar";
    $usa_form = false;
}
?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <form action="vigencia_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="5%">ID</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                            <?php
                                            if ($usa_form) {
                                                if ($id) { ?>
                                                    <input type="text" class="form-control" name="id"
                                                           value="<?php echo $vigencia[0]['id']; ?>" readonly><?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="id" value="" readonly><?php
                                                }
                                            } else {
                                                echo $vigencia[0]['id'];
                                            } ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>Número de Horas</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="horas"
                                                   value="<?php echo $vigencia[0]['numero_horas']; ?>" onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="horas" value="" onKeyPress="return soloNumeros(event)"><?php
                                        }
                                    } else {
                                        echo $vigencia[0]['numero_horas'];
                                    } ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td width="15%">Por Defecto</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                            <select name="por_defecto" class="form-control">
                                                <option value="">:: Seleccione ::</option>
                                                <option value="1" <?php if (isset($vigencia[0]['por_defecto'])) {
                                                    echo "selected";
                                                } ?>>Si</option>
                                                <option value="0" <?php if (isset($vigencia[0]['por_defecto']) && !$vigencia[0]['por_defecto']) {
                                                    echo "selected";
                                                } ?>>No</option>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./vigencia_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?=$vigencia[0]['id'];?>">
                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");

?>
<script>
    $(document).ready(function () {
        //datatime

        $('#fecha').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            useCurrent: true,
            format: 'DD-MM-YYYY hh:mm',
            showTodayButton: true,
            showClear: true,
            showClose: true,
            //autoclose: true,
            //maxDate: new Date(),
            //        toolbarPlacement: 'top'
        });
    });
</script>
