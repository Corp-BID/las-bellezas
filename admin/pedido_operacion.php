<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 5) :NULL;

$id = (isset($_POST['m']) && $_POST['m'])?substr(strval($_POST['m']), 0, 50) :'';
$condicion = (isset($_POST['id_condicion']) && $_POST['id_condicion'])?intval($_POST['id_condicion']):0;
$condicion_anterior = (isset($_POST['id_condicion_anterior']) && $_POST['id_condicion_anterior'])?intval($_POST['id_condicion_anterior']):0;


if ($_SESSION['error'])
    unset($_SESSION['error']);

if ($condicion <= ENTREGADO) {
    if ($condicion > ($condicion_anterior + 1)) {
        $_SESSION['error'] = 'Error: Debe seguir un orden lógico al seleccionar el estatus';
        header("Location: ./pedido_gestion.php?o=".$operacion."&p=".$id);
    }else{
        $pedido_obj = new Pedidos();
        if($operacion =="mod" && $id)
        {
            $pedido_obj->ModificarCondicion($_DB_, $id, $condicion);
            header("Location: ./pedidos_consultar.php");
        }
    }
}



