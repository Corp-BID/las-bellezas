<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?strval($_POST['op']) :NULL;
$id = (isset($_POST['id']) && $_POST['id'])?strval($_POST['id']) :'';
$email_anterior = (isset($_POST['email_anterior']) && $_POST['email_anterior'])?strval($_POST['email_anterior']) :'';
$email= (isset($_POST['email']) && $_POST['email'])?$_POST['email'] : NULL;
$password = (isset($_POST['password']) && $_POST['password'])?strval($_POST['password']) :NULL;
$cedula= (isset($_POST['cedula']) && $_POST['cedula'])?$_POST['cedula'] : NULL;
$nombres= (isset($_POST['nombres']) && $_POST['nombres'])?$_POST['nombres'] : NULL;
$apellidos= (isset($_POST['apellidos']) && $_POST['apellidos'])?$_POST['apellidos'] : NULL;
$telefono= (isset($_POST['telefono']) && $_POST['telefono'])?$_POST['telefono'] : NULL;
$condicion= (isset($_POST['condicion']) && $_POST['condicion'])?$_POST['condicion'] : NULL;

$cliente_obj = new Clientes();

if($operacion =="add")
{
    $cliente = $cliente_obj->obtenerDatosClienteEmail($_DB_, $email);
    $errors = [];
    if (count($cliente) > 0) {
        $errors = [
            'message' => 'Correo ya existe!, verifique!'
        ];
        $_SESSION['errors'] = $errors;
        header("Location: ./clientes_gestion.php");
        die;
    }else

        $cliente_obj->AgregarClienteForm($_DB_, $email, $password, $cedula, $nombres, $apellidos, $telefono,$condicion);
        unset($_SESSION["errors"]);
}
elseif($operacion =="mod" and $id)
{
    if ($email <> $email_anterior)
    {
        $cliente = $cliente_obj->obtenerDatosClienteEmail($_DB_, $email);
        $errors = [];
        if (count($cliente) > 0) {
            $errors = [
                'message' => 'Correo ya existe!, verifique!',
                'email' => $email
            ];
            $_SESSION['errors'] = $errors;
            header("Location: ./clientes_gestion.php?o=mod&id=".$id);
            die;
        }else{
            $cliente_obj->ActualizarClientes($_DB_, $id, $email, $password, $cedula, $nombres, $apellidos, $telefono, $condicion);
            unset($_SESSION["errors"]);
            }
    }else {
        $cliente_obj->ActualizarClientes($_DB_, $id, $email, $password, $cedula, $nombres, $apellidos, $telefono, $condicion);
        unset($_SESSION["errors"]);
    }
}
elseif($operacion =="del")
{
    $cliente_obj->EliminarClientes($_DB_, $id);
    unset($_SESSION["errors"]);
}

header("Location: ./clientes_consultar.php");