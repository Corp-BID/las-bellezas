<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$op = (isset($_GET['o']) && $_GET['o'])?strval($_GET['o']):'add';
$id = (isset($_GET['m']) && $_GET['m'])?substr(strval($_GET['m']), 0, 200) :NULL;

if($id and $op == 'mod')
{
    $marca_obj = new Marcas();
    $marca = $marca_obj -> Obtener($_DB_, $id);

    $operacion = "fotos";
    $titulo = "Modificar Logo de la Marca";
    $boton = "Modificar";
    $usa_form = true;
}
?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">

                <form action="marca_operacion.php" method="POST"  enctype="multipart/form-data">
                <input type="hidden" name="op" value="fotos">
                <input type="hidden" name="m" value="<?php echo $id;?>">

                <?php if (isset($_SESSION['errors'])){ ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $_SESSION['errors']['message'];?>
                    </div>
                <?php } ?>

                <table id="my_table_to_excel" class="table table-bordered">
                    <tbody>
                    <tr>
                        <td height="50">Marca</td>
                        <td><?php echo $marca[0]['descripcion'];?></td>
                    </tr>

                    <tr>
                        <td>Logo</td>
                                
                        <?php
                        if($marca[0]["logo"] != "")
                        {?>
                            <td>
                                <center><img src="<?php echo DIR_MEDIA_WEB_PROD_LOGO.$marca[0]["logo"];?>"  onerror="this.src='../Marca/logo/no_disponible.jpg';" width="90"></center>
                            </td>
                        <?php
                        }else{ ?>
                            <td>
                                <img src="<?php echo DIR_MEDIA_WEB_PROD_LOGO.'no_disponible.jpg';?>"  width="90">
                            </td>
                        <?php } ?>
                    </tr>
                    <?php
                    ?>

                    </table>

                    <?php if($usa_form) {?>
                    <td colspan="2">
                        <input type="file" size="32" name="logo" value=""/>
                    </td>
                    <?php } ?>
                    <div class="buttons clearfix pt-3">
                        <div class="pull-left">
                            <?php
                            if($boton)
                            {?><input type="submit" value="<?php echo $boton;?>" class="btn btn-primary"><?php }?>
                            <a href="./marcas_consultar.php?m=<?php echo $id;?>" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                   
                </form>
                 
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");