<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");


$vigencia_obj = new VigenciaPedido();

$vigencia = $vigencia_obj->Listado($_DB_);

?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">HORAS PROGRAMADAS</h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <form action="vigencia_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Vigencia del Pedido">
            </form>

            <div class="ibox-content">

                <table class="table table-bordered table-hover" style="width: 80%;" align="center">
                    <thead>
                        <th width="15%" class="text-center">Id</th>
                        <th width="15%" class="text-center">Horas</th>
                        <th width="10%" class="text-center">Por Defecto</th>
                        <th width="10%" class="text-center"></th>
                    </thead>
                    <tbody>
                        <?php
                        if ($vigencia) {
                            foreach ($vigencia as $horas) {
                                $por_defecto = ($horas['por_defecto'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $horas['id']; ?></td>
                                    <td class="text-right"><?php echo number_format($horas['numero_horas'], 0, ",", "."); ?></td>
                                    <td class="text-center"><?php echo $por_defecto; ?></td>
                                    <td class="align-middle text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-outline btn-success dim" href="vigencia_gestion.php?o=mod&id=<?php echo $horas['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-outline btn-danger dim" href="vigencia_gestion.php?o=del&id=<?php echo $horas['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                        <?php

                            }
                        }else { ?>
                            <h1>No hay Horas programadas</h1>
                            <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
