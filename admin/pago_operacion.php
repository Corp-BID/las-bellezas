<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? $_POST['op'] :NULL;

$id = (isset($_POST['p']) && $_POST['p']) ? $_POST['p'] :'';
$id_pedido = (isset($_POST['pedido']) && $_POST['pedido']) ? $_POST['pedido'] :'';
$condicion = (isset($_POST['validado']) && $_POST['validado'])?intval($_POST['validado']):0;

$pago_obj = new Pago();

if($operacion =="mod" && $id)
{
    $pago_obj->ModificarCondicion($_DB_, $id, $condicion);
}

header("Location: ./pedido_gestion.php?o=".$operacion."&p=".$id_pedido);