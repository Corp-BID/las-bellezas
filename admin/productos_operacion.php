<?php

ini_set('max_execution_time', 120);
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? substr(strval($_POST['op']), 0, 10) : NULL;


$cat = (isset($_POST['c']) && $_POST['c']) ? $_POST['c'] : NULL;
$subcat = (isset($_POST['sc']) && $_POST['sc']) ? $_POST['sc'] : NULL;

$id_old = (isset($_POST['id_old']) && $_POST['id_old']) ? substr(strval(trim($_POST['id_old'])), 0, 200) : NULL;
$id = (isset($_POST['id']) && $_POST['id']) ? $_POST['id'] : NULL;
$nombre = (isset($_POST['nombre']) && $_POST['nombre']) ? mb_strtoupper(substr(strval(trim($_POST['nombre'])), 0, 200)) : NULL;
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion']) ? substr(strval(mb_strtoupper($_POST['descripcion'])), 0, 1000) : NULL;
$marca = (isset($_POST['marca']) && $_POST['marca']) ? substr(strval(mb_strtoupper(trim($_POST['marca']))), 0, 200) : 0;
$presentacion = (isset($_POST['presentacion']) && $_POST['presentacion']) ? substr(strval(mb_strtoupper(trim($_POST['presentacion']))), 0, 200) : NULL;
$orden = (isset($_POST['orden']) && $_POST['orden']) ? intval($_POST['orden']) : 0;
$precio = (isset($_POST['precio']) && $_POST['precio']) ? $_POST['precio'] : 0;

$miles = str_replace(".", "", $precio);
$precio = str_replace(",", ".", $miles);

$cant_min = (isset($_POST['cant_min']) && $_POST['cant_min']) ? floatval($_POST['cant_min']) : 0;

$nuevo = (isset($_POST['nuevo']) && $_POST['nuevo']) ? intval($_POST['nuevo']) : 0;
$oferta = (isset($_POST['oferta']) && $_POST['oferta']) ? intval($_POST['oferta']) : 0;
$visible = (isset($_POST['visible']) && $_POST['visible']) ? intval($_POST['visible']) : 0;
$destacado = (isset($_POST['destacado']) && $_POST['destacado']) ? intval($_POST['destacado']) : 0;

$categoria_obj = new Categorias();
$producto_obj = new Productos();

$prefijo = date('Ymd_His_');

if ($cat and $subcat) {
    if ($operacion == "add") {
        $prod_exis = $producto_obj->Consultar($_DB_, $id);
        if (!$prod_exis) {
            $producto_obj->Agregar($_DB_, $cat, $subcat, $id, 0, $nombre, $presentacion, $marca, $descripcion, $precio, $cant_min, $nuevo, $oferta, '', $visible, $destacado);
            unset($_SESSION["errors"]);
            header("Location: ./productos_consultar3.php?c=$cat&sc=$subcat&dis=$visible");
        } else {
            $errors = [];
            $errors = [
                'message' => 'Código de producto ya se encuentra registrado, verifique!'
            ];
            $_SESSION['errors'] = $errors;
            header("Location: ./productos_gestion.php?op=mod&c=" . $cat . "&sc=" . $subcat . "&id=" . $id . "&dis=" . $visible);
            die;
        }
    } elseif ($operacion == "mod" and $id) {
        $producto_obj->Modificar($_DB_, $cat, $subcat, $id, $id_old, 0, $nombre, $presentacion, $marca, $descripcion, $precio, $cant_min, $nuevo, $oferta, $visible, $destacado);
    } elseif ($operacion == "del") {
        $producto_obj->Eliminar($_DB_, $id);
    } elseif ($operacion == "fotos") {

        $fotos = NULL;
        for ($i = 1; $i <= 5; $i++) {
            $nombre_archivo_rand = '';
            $nombre_archivo_rand = date("Ymd") . "-" . rand(100, 999);
            $nombre_archivo = $nombre_archivo_rand . ".jpg";
            $foto = "foto" . $i;
            $f = $_FILES["$foto"];
            if (!empty($f['name'])) {
                $upload = upload($f, $nombre_archivo_rand, $id, $cat, $subcat, $visible, $foto);

                if ($upload == "") {
                    $nombre_foto = $prefijo . $nombre_archivo . '.jpg';
                    $producto_obj->ModificarFotos($_DB_, $id, "foto" . $i, $nombre_archivo);
                    unset($_SESSION["errors"]);
                } else {

                    header("Location: ./productos_gestion_fotos.php?o=mod&c=" . $cat . "&sc=" . $subcat . "&id=" . $id . "&dis=" . $visible);
                    die;
                }

            }
        }
    }
    header("Location: ./productos_consultar3.php?c=$cat&sc=$subcat&dis=$visible");
}


function upload($imagen, $nombre_archivo, $id , $cat , $subcat , $visible, $foto)
{
    $banner = $imagen;
    $filename = $banner['tmp_name'];
    list($width, $height) = getimagesize($filename);

    if ($width < 300 || $height < 300){
        $errors = [
            'message' => 'Tamaño de la imagen incorrecta, verifique! '."<code>".$width."x".$height."</code>",
            'foto' =>$foto
        ];
        $_SESSION['errors'] = $errors;

        header("Location: ./productos_gestion_fotos.php?o=mod&c=".$cat."&sc=".$subcat."&id=".$id."&dis=".$visible);
        die;
    }

    if($banner['type'] == "image/jpeg"){
        $foo = new Upload($banner);

        if ($foo->uploaded)
        {
            $foo->file_new_name_body = $nombre_archivo;
            $foo->file_overwrite = true;
            $foo->image_resize = true;
            $foo->image_ratio_y = true;
            $foo->image_x = 640;
            $foo->image_y = 640;
            $foo->image_convert = 'jpg';
            $foo->Process(DIR_MEDIA_WEB_PROD_M);

            $foo->file_new_name_body = $nombre_archivo;
            $foo->file_overwrite = true;
            $foo->image_resize = true;
            $foo->image_ratio_y = true;
            $foo->image_x = 180;
            $foo->image_convert = 'jpg';
            $foo->Process(DIR_MEDIA_WEB_PROD_P);
            if ($foo->processed){$error="";}
            else{$error = $foo->error;}
        }
    }
    else
    {
        $error = "Error en la Imagen, Formato invalido (Debe ser JPG)";
    }

    $errors = [
        'message' => $error,
        'foto'=>$foto
    ];

    $_SESSION['errors'] = $errors;
    return $error;

}


