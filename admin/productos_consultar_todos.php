<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$produc = new Productos();

$productos = $produc -> ListarProductosAdmin($_DB_,'', '');

if($_GET['dis'] == 2){$disponible=0;}
else{$disponible=1;}

?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Consultar Todos los Productos - <?php if ($disponible)
            {
                echo "Visibles";
            }else{
                echo "No Visibles";
            } ?>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
            
    <div class="row">
                
        <div id="box_resultados">
             
            <form action="cambiar_precio.php" method="POST">
            
            <div class="col-md-3 text-left">
                <input type="submit" class="btn btn-danger" value="Cambiar Precios a Todos los Productos">			
            </div>
            </form>
            
            <div class="col-md-6 text-right"></div>
            
            
            <div class="col-md-3 text-right">
                
                <div class="pull-right">
                    <?php
                    if($disponible)
                    { ?>
                        <a href="?dis=2" class="btn btn-warning">Ver Productos No Visitbles</a>
                    <?php }else{ ?>
                        <a href="?dis=1" class="btn btn-warning">Ver Productos Visitbles</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        
        <br>
                
        <div class="col-lg-12">
                    
            <div class="panel-body">
                    
                <table id="my_table_to_excel" class="table table-striped table-bordered table-hover">
                <thead>
                    <th><center>Codigo</center></th>
                    <th><center>Descripcion</center></th>
                    <th><center>Marca</center></th>
                    <th><center>Unidad</center></th>
                    <th><center>Precios</center></th>
                    <th><center>Fotos</center></th>
                    <th><center>Nue</center></th>
                    <th><center>Ofe</center></th>
                    <th><center>Vis</center></th>
                    <th></th>
                </thead>
                          
                <tbody>
                              
                <?php
                if($productos)
                {
                    foreach($productos as $prod)
                    {
                        if ($prod['visible'] == $disponible)
                        { ?>
                        <tr>
                            <td><?php echo $prod['cod_producto'];?></td>
                            <td><?php echo $prod['descripcion'];?></td>
                            <td><center><?php echo $prod['marca'];?></center></td>
                            <td><center><?php echo $prod['unidad'];?></center></td>
                            <td>
                                <?php echo number_format($prod['precio1'], 2, ',', '.');?>
                            </td>
                            <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$prod['foto'];?>"  onerror="this.src='../Productos/no_disponible.jpg';" width="90"></td>

                            <td>
                                <center>
                                <?php if ($prod['nuevo']) {
                                    echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";
                                } else {
                                    echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";
                                }
                                ?>
                                </center>
                            </td>
                            <td>
                                <center>
                                <?php
                                if ($prod['oferta']) {
                                    echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";
                                } else {
                                    echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";
                                }
                                ?>
                                </center>
                            </td>
                            <td>
                                <center>
                                <?php
                                if ($prod['visible']) {
                                    echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";
                                } else {
                                    echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";
                                }
                                ?>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a class="btn btn-primary" href="productos_gestion.php?o=con&cat=<?php echo $prod['cod_categoria'];?>&subcat=<?php echo $prod['cod_subcategoria'];?>&id=<?php echo $prod['cod_producto'];?>"><i class="fa fa-search"></i></a>
                                        <a class="btn btn-success" href="productos_gestion.php?o=mod&cat=<?php echo $prod['cod_categoria'];?>&subcat=<?php echo $prod['cod_subcategoria'];?>&id=<?php echo $prod['cod_producto'];?>"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger" href="productos_gestion.php?o=del&cat=<?php echo $prod['cod_categoria'];?>&subcat=<?php echo $prod['cod_subcategoria'];?>&id=<?php echo $prod['cod_producto'];?>"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php
                        }
                    }
                }
                ?>
                </tbody>
                </table>
                
                <div class="buttons clearfix">
                    <div class="pull-right">
                        <a onclick="history.back()" class="btn btn-default">Volver</a>
                    </div>
                </div>
                 
            </div>
        </div>
                
    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");
