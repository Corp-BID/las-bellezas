<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['p']) && $_GET['p']) ? strval($_GET['p']) : '';

$pedido_obj = new Pedidos();


$condiciones = $pedido_obj->GetCondiciones($_DB_);

if ($id and $op == 'mod') {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Pedido";
    $boton = "Modificar";
    $usa_form = true;
} elseif ($id and $op == 'del') {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);

    $operacion = "del";
    $titulo = "Eliminar Pedido";
    $boton = "Eliminar";
    $usa_form = false;
} else {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);
    $titulo = "Consultar Pedido";
    $boton = "";
    $usa_form = false;
}

?>
<style>
    .vertical
    { border-left: 1px solid black; }
</style>


<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <form action="pedido_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <?php if (!$usa_form){ ?>
                    <input type="hidden" name="m" value="<?php echo $id; ?>">
                    <?php } ?>
                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $_SESSION['errors']['message'];?>
                        </div>
                    <?php } ?>

                    <table id="my_table_to_excel" class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Código</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="m" value="<?php echo $pedido['data'][0]['id_pedido']; ?>" readonly><?php
                                        }
                                    } else {
                                            echo $pedido['data'][0]['id_pedido'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Cliente</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="cliente"
                                                   value="<?php echo $pedido['data'][0]['cliente']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="cliente" value=""><?php
                                        }

                                    } else {

                                        echo $pedido['data'][0]['cliente'];
                                    }?>
                                </td>
                            </tr>

                            <tr>
                                <td>Fecha del Pedido</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) {
                                            ?>
                                            <input type="text" class="form-control" name="fecha"
                                                   value="<?=$pedido['data'][0]['fecha']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="fecha" value="">
                                            <?php
                                        }

                                    } else {
                                        $formato = 'Y-m-d H:i:s';
                                        $fecha = DateTime::createFromFormat($formato, $pedido['data'][0]['fecha']);
                                        echo $fecha->format('d/m/Y h:i:s');
                                    }?>
                                </td>
                            </tr>

                            <tr>
                                <td>Monto</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="monto"
                                                   value="<?php echo $pedido['data'][0]['monto_total']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="monto" value=""><?php
                                        }

                                    } else {
                                        echo number_format($pedido['data'][0]['monto_total'], 2, ",", ".");
                                    }?>
                                </td>
                            </tr>

                            <tr>
                                <td>Condición</td>
                                <td>
                                    <?php
                                    if ($usa_form) { ?>
                                        <select name="id_condicion">
                                        <option value="0">:: Seleccione ::</option>
                                        <?php foreach ($condiciones as $condicion) {
                                        ?>
                                            <option value="<?=$condicion['id'];?>" class="form-control" <?php
                                             if ($condicion['id'] == $pedido['data'][0]['id_condicion']) { echo "selected";} ?>>
                                                <?=$condicion['condicion'];?>
                                            </option>
                                    <?php }
                                    ?>  </select>
                                    <?php } else {
                                        $condicion = ($pedido['data'][0]['condicion']) ? "<span class='label label-info'>".$pedido['data'][0]['condicion']."</span>" : '<span class="label label-danger">Sin Estatus</span>';
                                        echo $condicion;
                                    }
                                    ?>
                                </td>
                            </tr>
                    </table>

                    <table class="table table-bordered">
                        <thead>
                            <th class="text-center">ID</th>
                            <th class="text-center">Código</th>
                            <th class="text-center">Producto</th>
                            <th class="text-center">Cantidad</th>
                            <th class="text-center">Precio Unitario</th>
                            <th class="text-center">Total</th>
                         </thead>
                        <tbody>
                        <?php

                        if (isset($pedido['dataItems'])) {
                            $total = 0;
                            foreach ($pedido['dataItems'] as $item) { ?>
                                <tr>
                                    <td><?= $item['id_item']; ?></td>
                                    <td><?= $item['codigo_producto']; ?></td>
                                    <td><?= $item['nombre']; ?></td>
                                    <td align="right"><?= $item['cantidad']; ?></td>
                                    <td align="right"><?= number_format($item['precio_unitario'], 2, ",", "."); ?></td>
                                    <td align="right"><?= number_format($item['precio_total'], 2, ",", ".") ; ?></td>
                                </tr>
                                <?php
                                $total = $total + $item['precio_total'];
                            } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="right"><?= number_format($total, 2, ",", ".") ; ?></td>
                            </tr>

                        <?php } else { ?>
                            <tr>
                                <td class="text-center">No hay producto asociado al Pedido</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h2><b>Observación del pedido</b></h2>
                                <label><?=$pedido['data'][0]['observacion'];?></label>
                            </div>
                        </div>

                        <div class="col-md-6 vertical">
                            <div class="form-group">
                                <h2><b>Dirección de entrega</b></h2>

                                    <p><b>Dirección Principal :</b></p><label><?=$pedido['data'][0]['direccion'];?></label>

                                <hr>

                                    <p><b>Dirección Secundaria :</b></p><label><?=$pedido['data'][0]['direccion2'];?></label>

                                <hr>

                                    <p><b>Observación :</b></p> <label><?=$pedido['data'][0]['observaciones'];?></label>

                            </div>
                        </div>
                    </div>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./pedidos_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
