<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$del = (isset($_GET['del']) && $_GET['del'])?intval($_GET['del']):0;

$testimonios_obj = new Testimonios();
$testimonios = $testimonios_obj->Listado($_DB_, $del);

?>

<div class="row">
    <div class="col-md-4">
        <h1 class="page-header">Consultar Testimonios
            <?php if (!$del) { echo "Visibles"; } else { echo "Eliminados"; } ?>
        </h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <a href="testimonios_gestion.php?o=add" class="btn btn-primary">Crear Testimonio</a>
            <div class="consultar">
                <?php
                if (!$del) { ?>
                    <a href="?del=1" class="btn btn-warning">Ver Eliminados</a>
                    <?php
                } else { ?>
                    <a href="?del=0" class="btn btn-info">Ver Testimonios</a>
                <?php }
                ?>
            </div>

            <div class="ibox-content">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Id</th>
                            <th class="text-center">Imagen</th>
                            <th class="text-center">Descripci&oacute;n</th>
                            <th class="text-center">Beneficiario</th>
                            <th class="text-center">Opcional 1</th>
                            <th class="text-center">Opcional 2</th>
                            <th class="text-center">Opcional 3</th>
                            <th class="text-center">Publicado</th>
                            <th class="text-center">Validado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>


                        <?php
                        if ($testimonios) {
                            foreach ($testimonios as $testimonio) { ?>
                                <tr>
                                    <td class="text-center align-middle"><?= $testimonio['id']; ?></td>
                                    <td class="text-center align-middle"><img src='img/testimonios/<?= $testimonio['imagen']; ?>' onerror="this.src='img/testimonios/no_disponible.jpg';" width="150"></td>
                                    <td class="text-center align-middle"><?= $testimonio['descripcion']; ?></td>
                                    <td class="text-center align-middle"><?= $testimonio['beneficiario']; ?></td>
                                    <td class="text-center align-middle"><?= $testimonio['opcional1']; ?></td>
                                    <td class="text-center align-middle"><?= $testimonio['opcional2']; ?></td>
                                    <td class="text-center align-middle"><?= $testimonio['opcional3']; ?></td>

                                    <td class="text-center align-middle">
                                        <?php if ($testimonio['publicado']) { ?><i class="fa fa-check-square" class="check-link"></i><?php } else { ?><i class="fa fa-square-o" class="check-link"></i><?php } ?>
                                    </td>
                                    <td class="text-center align-middle">
                                        <?php if ($testimonio['validado']) { ?><i class="fa fa-check-square" class="check-link"></i><?php } else { ?><i class="fa fa-square-o" class="check-link"></i><?php } ?>
                                    </td>

                                    <td align="center" class="align-middle">
                                        <div class="btn-group">
                                            <a class="btn btn-outline btn-success dim" href="testimonios_gestion.php?op=mod&id=<?php echo $testimonio['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            <?php if (!$del){ ?>
                                                <a class="btn btn-outline btn-danger dim" href="testimonios_gestion.php?op=del&id=<?php echo $testimonio['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                            <?php }else{ ?>
                                                <a class="btn btn-outline btn-success dim" href="testimonios_gestion.php?op=rev&id=<?php echo $testimonio['id']; ?>" type="button"><i class="fa fa-undo"></i></a>
                                            <?php } ?>
                                        </div>

                                    </td>
                                </tr>
                        <?php
                            }
                        } ?>
                    </tbody>
                </table>
                </form>

            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
