<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : PER_PAGE;
$page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
$links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;
$status = (!empty($_GET['dis']) == 1) ? 1 : 0;

$condicion = (isset($_GET['status'])) ? $_GET['status'] : 'all';

$pedidos_obj = new Pedidos();


$params = [
    'param1' =>'status',
    'value1' =>$condicion
];

$descripcionCondicion = ' (TODOS)';

if ($condicion != 'all') {
    if ($condicion != 'del') {
        $status = 0;
        $descripcion = $pedidos_obj->getCondicionId($_DB_, $condicion);
        $descripcionCondicion = "(" . strtoupper($descripcion[0]['condicion']) . ")";
    } else {
        $status = 1;
        $descripcionCondicion = "(ELIMINADOS)";
    }
}

$pedidos = $pedidos_obj->ListadoPedido($_DB_,$condicion, $limit, $page, $status);

$Paginator  = new Paginator( $pedidos['total'], $limit, $page );

?>
<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">PEDIDOS <?=$descripcionCondicion;?></h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <?php if (isset($_GET['status'])){ ?>
            <div class="buttons clearfix">
                <div class="pull-left">
                    <a onclick="history.back()" class="btn btn-default">Volver</a>
                </div>
            </div>
            <?php } ?>
           <div class="consultar">
                <?php
                if ($status) { ?>
                    <a href="?dis=0" class="btn btn-success">Pedidos</a>
                    <?php
                } else { ?>
                    <a href="?dis=1" class="btn btn-warning">Pedidos Eliminadas</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">

                <table id="my_table_to_excel" class="table table-bordered">
                    <thead>
                        <th class="text-center">ID</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Fecha</th>
                        <th class="text-center">Cantidad Items</th>
                        <th class="text-center">Monto $</th>
                        <th class="text-center">Monto Bs</th>
                        <th class="text-center">Método de Entrega</th>
                        <th class="text-center">Estatus</th>
                        <th class="text-center"></th>
                    </thead>

                    <tbody>

                        <?php
                        if ($pedidos) {
                            foreach ($pedidos['data'] as $pedido) {
                                $formato = 'Y-m-d H:i:s';
                                $fecha = DateTime::createFromFormat($formato, $pedido['fecha']);
                             //   $condicion = ($pedido['activo']) ? '<span class="label label-info">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
                                $status = ($pedido['condicion']) ? "<span class='label label-info'>".$pedido['condicion']."</span>" : '<span class="label label-danger">Sin Estatus</span>';

                                switch ($pedido['metodo_entrega']) {
                                    case 'por_definir':
                                        $condicion = "<code style='color: blue'>Por Definir</code>";
                                        break;
                                    case 'enviar':
                                        $condicion = "<code style='color: darkred'>Para Enviar</code>";
                                        break;
                                    case 'retirar':
                                        $condicion = "<code style='color: green'>Retirar en la Tienda</code>";
                                        break;
                                }

                                $tasaCompra = ($pedido['valor_calculo'] >0 ) ? $pedido['valor_calculo'] : 1;
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $pedido['id_pedido']; ?></td>
                                    <td class="text-center"><?php echo $pedido['cliente']; ?></td>
                                    <td class="text-center"><?php echo $fecha->format('d/m/Y h:i:s'); ?></td>
                                    <td class="text-center"><?php echo $pedido['total_items']; ?></td>
                                    <td class="text-center"><?php echo number_format($pedido['monto_total'], 2, ",", ".");?></td>
                                    <td class="text-center"><?php echo number_format($pedido['monto_total'] * $tasaCompra , 2, ",", ".");?></td>
                                    <td class="text-center"><?php echo $condicion; ?></td>
                                    <td class="text-center"><?php echo $status; ?></td>
                                    <td width="150">
                                        <center>
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-primary dim" href="pedido_gestion.php?o=mod&p=<?php echo $pedido['id_pedido']; ?>" type="button"><i class="fa fa-credit-card"></i></a>
                                                <!--<a class="btn btn-outline btn-warning dim" href="pedido_gestion.php?o=mod&p=<?php /*echo $pedido['id_pedido']; */?>" type="button"><i class="fa fa-pencil"></i></a>-->
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else { ?>
                            <h1>No hay pedidos</h1>
                        <?php } ?>

                    </tbody>
                </table>
                <?php echo $Paginator->createLinks( $links, 'pagination pg-blue', $params ); ?>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
