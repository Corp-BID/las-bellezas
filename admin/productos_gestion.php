<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? $_GET['o'] : 'add';
$cat = (isset($_GET['c']) && $_GET['c']) ? $_GET['c'] : NULL;
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? $_GET['sc'] : NULL;
$id = (isset($_GET['id']) && $_GET['id']) ? $_GET['id'] : NULL;
$visible = ($_GET['dis'] == 1) ? 1 : 0;

$categoria_obj = new Categorias();
$producto_obj = new Productos();
$marcas_obj = new Marcas();

$categoria = $categoria_obj->Obtener($_DB_, $cat);
$subcategoria = $categoria_obj->Obtener($_DB_, $cat, $subcat);

$marcas = $marcas_obj->Listado($_DB_);

if ($op == 'add') {
    $producto =[];
    $operacion = "add";
    $titulo = "Crear Producto";
    $boton = "Crear";
    $usa_form = true;
} else {
    $produc = new Productos();
    $producto = $producto_obj->Consultar($_DB_, $id);
    if ($id and $op == 'mod') {
        $operacion = "mod";
        $titulo = "Modificar Producto";
        $boton = "Modificar";
        $usa_form = true;
    } elseif ($id and $op == 'del') {
        $operacion = "del";
        $titulo = "Eliminar Producto";
        $boton = "Eliminar";
        $usa_form = false;
    } else {
        $titulo = "Consultar Producto";
        $boton = "";
        $usa_form = false;
    }
}
?>

<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">

                <?php if (isset($_SESSION['errors'])){ ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $_SESSION['errors']['message'];?>
                    </div>
                <?php } ?>

                <form action="productos_operacion.php" method="POST" id="formProduct">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">

                    <input type="hidden" name="c" value="<?php echo $cat; ?>">
                    <input type="hidden" name="sc" value="<?php echo $subcat; ?>">
                    <input type="hidden" name="id_old" value="<?php echo $id; ?>">
                    <input type="hidden" name="dis" value="<?php echo $visible;?>">

                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="12%">Categoria</td>
                                <td><?php echo $categoria[0]['nombre']; ?></td>
                                <td width="15%">Subcategoria</td>
                                <td><?php echo $subcategoria[0]['nombre']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Código</td>
                                <td width="30%">
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) { ?>
                                            <input type="text" class="form-control" name="id"
                                                   value="<?php echo $producto[0]['cod_producto']; ?>" readonly><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="id" value=""><?php
                                        }
                                    } else { ?>
                                        <input type="hidden" name="id" value="<?php echo $id; ?>"> <?php echo $producto[0]['cod_producto']; ?><?php }
                                    ?>
                                </td>
                                <td colspan=2></td>
                            </tr>
                            <tr>
                                <td>Nombre</td>
                                <td colspan="3">
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) { ?>
                                            <input type="text" class="form-control" name="nombre"
                                                   value="<?php echo $producto[0]['nombre']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="nombre" value=""><?php
                                        }
                                    } else { ?>
                                        <?php echo $producto[0]['nombre']; ?><?php }
                                                                                                                                                                                                                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Presentación</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) { ?>
                                            <input type="text" class="form-control" name="presentacion"
                                                   value="<?php echo $producto[0]['presentacion']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="presentacion" value=""><?php
                                        }
                                    } else { ?><?php echo $producto[0]['presentacion']; ?><?php }
                                                                                                                                                                                                                ?>
                                </td>

                                <td>
                                    <?php
                                    if ($usa_form) {
                                        $marca_producto = (isset($producto[0]['marca'])) ? $producto[0]['marca']:0;
                                        ?>
                                        <select name="marca" class="form-control">
                                            <option value="0">:: Seleccione una Marca ::</option>
                                            <?php
                                            foreach ($marcas as $marca) {
                                                ?>
                                                    <option value="<?php echo $marca['id']; ?>" <?php
                                                    if ($marca_producto == $marca['id']) {
                                                        echo "selected";
                                                    } ?> ><?php echo $marca['descripcion']; ?>
                                                    </option>
                                            <?php

                                            } ?>
                                        </select>
                                    <?php
                                    } else {
                                        echo $producto[0]['descripcion_marca'];
                                    } ?>

                                </td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td colspan="3">
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) { ?>
                                            <textarea id="descripcion" name="descripcion" rows="2" cols="195" class="form-control"><?=$producto[0]['descripcion'];?></textarea>
                                            <?php
                                        }else{ ?>
                                            <textarea id="descripcion" name="descripcion" rows="2" cols="195" class="form-control" wrap="hard"></textarea>
                                           <?php
                                        }
                                    } else { echo $producto[0]['descripcion']; } ?>
                                </td>
                            </tr>
                            <tr>
                                <?php
                                if (isset($DatosMoneda))
                                {
                                    ?>
                                    <td width="10%">Escriba el precio en <code><?=$DatosMoneda[0]['simbolos'];?></code></td>
                              <?php }else{ ?>
                                    <td><code>Debe configurar la moneda por defecto</code></td>
                               <?php }
                                ?>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) {
                                            $precio = str_replace(".", ",", $producto[0]['precio']);
                                            ?>
                                            <input type="text" name="precio" id="precio" value="<?php echo $precio; ?>" class="form-control" onkeyup="AmountFormatter(this.id);" onKeyPress="return soloNumeros(event)" placeholder="PRECIO"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="precio" id="precio" value="" onkeyup="AmountFormatter(this.id);" onKeyPress="return soloNumeros(event)" placeholder="PRECIO"><?php
                                        }
                                    } else { ?>
                                        <?php echo number_format($producto[0]['precio'], 2, ",", "."); ?><?php } ?>
                                </td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($producto) { ?>
                                            <input type="number" name="cant_min" value="<?php echo $producto[0]['cant_min']; ?>" class="form-control"  onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="cant_min" value="" placeholder="Cantidad" onKeyPress="return soloNumeros(event)"><?php
                                        }
                                    } else { ?><?php echo $producto[0]['cant_min']; ?><?php } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="50%" class="table table-bordered">
                            <tr>
                                <td colspan="4" class="align-middle text-center"><b>Opciones del Producto</b></td>
                            </tr>
                            <tr>
                                <td>
                                        <tr>
                                            <td width="25%">Nuevo
                                                <select name="nuevo" class="form-control">
                                                    <option value="">:: Seleccione ::</option>
                                                    <option value="1" <?php if (isset($producto[0]['nuevo'])) {
                                                        echo "selected";
                                                    } ?>>Si</option>
                                                    <option value="0" <?php if (isset($producto[0]['nuevo']) && !$producto[0]['nuevo']) {
                                                        echo "selected";
                                                    } ?>>No</option>
                                                </select>
                                            </td>

                                            <td width="25%">Destacado
                                                <select name="destacado" class="form-control">
                                                    <option value="">:: Seleccione ::</option>
                                                    <option value="1" <?php if (isset($producto[0]['destacado'])) {
                                                        echo "selected";
                                                    } ?>>Si</option>
                                                    <option value="0" <?php if (isset($producto[0]['destacado']) && !$producto[0]['destacado']) {
                                                        echo "selected";
                                                    } ?>>No</option>
                                                </select>
                                            </td>

                                            <td width="25%">Oferta
                                                <select name="oferta" class="form-control">
                                                    <option value="">:: Seleccione ::</option>
                                                    <option value="1" <?php if (isset($producto[0]['oferta'])) {
                                                                            echo "selected";
                                                                        } ?>>Si</option>
                                                    <option value="0" <?php if (isset($producto[0]['oferta'])) {
                                                                            echo "selected";
                                                                        } ?>>No</option>
                                                </select>
                                            </td>

                                            <td width="25%">Visible
                                                <?php
                                                if ($usa_form) { ?>
                                                    <select name="visible" class="form-control">
                                                        <option value="">:: Seleccione ::</option>
                                                        <option value="1" <?php if (isset($producto[0]['visible'])) {
                                                                                echo "selected";
                                                                            } ?>>Si</option>
                                                        <option value="0" <?php if (isset($producto[0]['visible']) && !$producto[0]['visible']) {
                                                                                echo "selected";
                                                                            } ?>>No</option>
                                                    </select>
                                                <?php
                                                } else {
                                                    $visible = ($producto[0]['visible'] == 1) ? '<i class="fa fa-eye" aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa fa-eye-slash  " aria-hidden="true" style="color: darkred;"></i>';
                                                    echo $visible;
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./productos_consultar3.php?c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&dis=<?=$_GET['dis'];?>" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>


<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>
$(document).ready(function () {
    $("#formProduct").validate({
        normalizer: function( value ) {
            return $.trim( value );
        },
        rules: {
            id: {
                required: true
            },
            nombre:{
                required: true,
                minlength: 4,
                maxlength: 100
            },
            presentacion:{
                required: true,
                minlength: 4
            },
            descripcion:{
                required: true,
                minlength: 4
            },
            precio:{
                required: true
            },
            cant_min:{
                required: true
            },
            nuevo:{
                required:true
            },
            destacado:{
                required:true
            },
            oferta:{
                required:true
            },
            visible:{
                required:true
            }
        }
    });
});
</script>
