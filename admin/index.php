<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$pedidos_obj = new Pedidos();
$pedido = $pedidos_obj->ResumenPedidoAdmin($_DB_);

$total_pedidos = 0;


if (count($pedido) > 0)
    $total_pedidos = "( " . number_format($pedido['total_pedidos'], 0, ",", ".") . " )";

$condiciones = $pedidos_obj->CondicionesPedidos($_DB_);

$condicionPedido = array();
foreach ($condiciones as $key =>  $condicion) {

    $totalCondicion = $pedidos_obj->ResumenPedidoCondicionAdmin($_DB_, $condicion['id']);
    $condicionPedido[$key] = 0;
    if (isset($totalCondicion) && count($totalCondicion) > 0) {
        $condicionPedido[$key]  = number_format($totalCondicion['total_pedidos'], 0, ",", ".");
    }
}
?>

<div class="wrapper wrapper-content">
    <div class="row">

      <?php
        foreach ($condiciones as $key =>  $condicion) { ?>
            <div class="col-lg-2">
                <div class="ibox ">
                    <div class="ibox-title">
                        <span class="label label-success float-right">Total</span>
                        <h5><?= $condicion['condicion']; ?></h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><a href="pedidos_consultar.php?status=<?= $condicion['id']; ?>"><?= $condicionPedido[$key]; ?></a></h1>
                        <small>Pedidos</small>
                    </div>
                </div>
            </div>
       <?php } ?>
    </div>

        <?php
        $moneda_obj = new Monedas();
        $tasa_obj = new TasaDolar();
        $vigencia_obj = new VigenciaPedido();

        $MonedaPorDefecto = $moneda_obj->MonedaPorDefectoConf($_DB_);

        $moneda = false;
        $tasaDefecto = false;
        $horas = false;
        $valor = '';
        if (count($MonedaPorDefecto) > 0) {
            $moneda = true;
            $tasa = $tasa_obj->TasaConfDefecto($_DB_, $MonedaPorDefecto['id']);

            if (count($tasa) > 0) {
                $tasaDefecto = true;
                $valor = $tasa['valor_calculo'];
                $valor = "(" . number_format($tasa['valor_calculo'], 0, ",", ".") . ")";
            }
        }
        $horasVigente = $vigencia_obj->VigenciaConfDefecto($_DB_);
        if (count($horasVigente) > 0) {
            $horas = true;
        }

        ?>

        <div class="ibox-content forum-container">

            <div class="ibox-content m-b-sm border-bottom">
                <div class="p-xs">
                    <div class="float-left m-r-md">
                        <i class="fa fa-shopping-cart text-navy mid-icon"></i>
                    </div>
                    <h2>Bienvenido</h2>
                    <span>Por favor revise la configuración del sistema para el buen funcionamiento del carrito de compra</span>
                </div>
            </div>

            <div class="forum-item">
                <div class="row">
                    <div class="col-md-9">
                        <div class="forum-icon">
                            <i class="fa fa-money"></i>
                        </div>
                        <a href="monedas_consultar.php" class="forum-item-title">Moneda</a>
                        <div class="forum-sub-title">Moneda por defecto que se va ha usar para la venta de los productos</div>
                    </div>
                    <div class="col-md-1 forum-info">
                        <span class="views-number">
                            <?php
                            $icon = ($moneda) ? '<i class="fa fa-check-square-o" aria-hidden="true" style="color: #1ab394;"></i>'
                                : '<i class="fa fa-times" aria-hidden="true"></i>';
                            echo $icon;
                            ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="forum-item">
                <div class="row">
                    <div class="col-md-9">
                        <div class="forum-icon">
                            <i class="fa fa-podcast"></i>
                        </div>
                        <a href="tasa_consultar.php?vis=0" class="forum-item-title">Tasa de Cambio <?= $valor; ?></a>
                        <div class="forum-sub-title">Permite realizar operaciones en Bolivares a la tasa de cambio por defecto creada en el sistema</div>
                    </div>
                    <div class="col-md-1 forum-info">
                        <span class="views-number">
                            <?php
                            $icon = ($tasa) ? '<i class="fa fa-check-square-o" aria-hidden="true" style="color: #1ab394;"></i>'
                                : '<i class="fa fa-times" aria-hidden="true"></i>';
                            echo $icon;
                            ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="forum-item">
                <div class="row">
                    <div class="col-md-9">
                        <div class="forum-icon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <a href="vigencia_consultar.php?vis=0" class="forum-item-title">Vigencia del Pedido</a>
                        <div class="forum-sub-title">Permite Eliminar los pedidos que no hayan sido pagados en un lapso de tiempo</div>
                    </div>
                    <div class="col-md-1 forum-info">
                        <span class="views-number">
                            <?php
                            $icon = ($horas) ? '<i class="fa fa-check-square-o" aria-hidden="true" style="color: #1ab394;"></i>'
                                : '<i class="fa fa-times" aria-hidden="true"></i>';
                            echo $icon;
                            ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
