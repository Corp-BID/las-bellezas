<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['id']) && $_GET['id']) ? strval($_GET['id']) : '';

$tasa_cambio_obj = new TasaDolar();
$tasa_moneda_obj = new Monedas();
$monedas = $tasa_moneda_obj->Listado($_DB_, 0);

$monedaIsDefault = $_SESSION['DatosMoneda'];

$usa_form = true;
$boton = "Crear";
$operacion = "add";

if ($id and $op == 'mod') {
    $tasa_cambio = $tasa_cambio_obj->Consultar($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Tasa de Cambio";
    $boton = "Modificar";
    $usa_form = true;
} elseif ($id and $op == 'del') {
    $tasa_cambio = $tasa_cambio_obj->Consultar($_DB_, $id);
    $operacion = "del";
    $titulo = "Eliminar Tasa de Cambio";
    $boton = "Eliminar";
    $usa_form = false;
}

date_default_timezone_set('America/Caracas');
$fecha_actual = (new \DateTime())->format('Y-m-d H:i:s');

?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <form action="tasa_operacion.php" method="post" name="frm_tasa" id="frm_tasa">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="5%">ID</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        if ($id) { ?>
                                            <label>
                                            <input type="text" class="form-control" name="id" value="<?php echo $tasa_cambio[0]['id']; ?>" readonly>
                                            </label><?php
                                        }else{ ?>
                                            <label>
                                                <input type="text" class="form-control" name="id" value="">
                                            </label>
                                        <?php }
                                    }  ?>
                                </td>
                            </tr>

                            <tr>
                                <td width="5%">Fecha del Conversión</td>

                                <td width="5%">
                                    <div class="row">
                                        <div class="col-2">
                                            <div class='input-group date'>
                                            <?php
                                            if ($usa_form) {
                                                if ($id) {
                                                    $fecha = date('d-m-Y h:i:s',strtotime($tasa_cambio[0]['fecha']));

                                                    ?>
                                                    <input type="text" class="form-control" name="fecha" id="fecha"
                                                           value="<?=$fecha; ?>">
                                                    <?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="fecha" id="fecha" value="<?=$fecha_actual;?>"><?php
                                                }

                                                ?>
                                                <span class="input-group-addon">
                                                      <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                        <?php

                                    }else{
                                            $fecha = date('d-m-Y h:i:s',strtotime($tasa_cambio[0]['fecha']));
                                            echo $fecha;
                                        } ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Moneda (Origen)</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                    <?php
                               if ($usa_form) {
                                   ?>
                                        <select name="id_moneda_origen" class="form-control" disabled>
                                            <option value="0">:: Seleccione ::</option>
                                            <?php foreach ($monedas as $moneda) {
                                                ?>
                                                <option value="<?=$moneda['id'];?>"  <?php
                                                if ($moneda['id'] == $monedaIsDefault[0]['id']) { echo "selected";} ?>>
                                                    <?=$moneda['descripcion']." (".$moneda['simbolos'].")";?>
                                                </option>
                                            <?php }
                                            ?>  </select>
                                    <?php } else {
                                        $condicion = ($tasa_cambio[0]['moneda_id']) ? "<span class='label label-info'>".$tasa_cambio[0]['moneda']."</span>" : '<span class="label label-danger">Sin Moneda</span>';
                                        echo $condicion;
                                    }
                                    ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>Moneda (Destino)</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                            <?php
                                            if ($usa_form) {
                                                $tasaMonedaDestino =  (isset($tasa_cambio[0]['moneda_id_destino'])) ?  $tasa_cambio[0]['moneda_id_destino'] : 0;
                                                ?>
                                                <select name="id_moneda_destino" class="form-control">
                                                    <option value="0">:: Seleccione ::</option>
                                                    <?php foreach ($monedas as $moneda) {
                                                        if ($moneda['id'] != $monedaIsDefault[0]['id']) {
                                                        ?>
                                                        <option value="<?=$moneda['id'];?>" <?php
                                                        if ($moneda['id'] == $tasaMonedaDestino) { echo "selected";} ?>>
                                                            <?=$moneda['descripcion']." (".$moneda['simbolos'].")";?>
                                                        </option>
                                                    <?php } }
                                                    ?>  </select>
                                            <?php } else {
                                                $condicion = ($tasa_cambio[0]['moneda_id_destino']) ? "<span class='label label-info'>".$tasa_cambio[0]['moneda_destino']."</span>" : '<span class="label label-danger">Sin Moneda</span>';
                                                echo $condicion;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>Valor</td>
                                <td>
                                    <div class="row">
                                        <div class="col-2">
                                    <?php
                                    $valor = '';
                                    $valorCalculo = (isset($tasa_cambio[0]['valor_calculo'])) ? $tasa_cambio[0]['valor_calculo'] : '';
                                    if ($valorCalculo)
                                        $valor = number_format($valorCalculo, 2, ",", ".");
                                    if ($usa_form) {
                                        if ($id) {
                                            ?>
                                            <input type="text" class="form-control" name="valor" id="valor"
                                                   value="<?php echo $valor; ?>" onkeyup="AmountFormatter(this.id);" onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="valor" id="valor" value="" onkeyup="AmountFormatter(this.id);" onKeyPress="return soloNumeros(event)"><?php
                                        }

                                    }else{
                                        echo $valor;
                                    } ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>Por Defecto</td>
                                <td width="25%">
                                    <div class="col-3">
                                    <select name="activo" class="form-control">
                                        <option value="1" <?php if (isset($tasa_cambio[0]['activo'])) {
                                            echo "selected";
                                        } ?>>Si</option>
                                        <option value="0" <?php if (isset($tasa_cambio[0]['activo']) && !$tasa_cambio[0]['activo']) {
                                            echo "selected";
                                        } ?>>No</option>
                                    </select>
                                    </div>
                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./tasa_consultar.php?vis=0" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                    <input type="hidden" id="id_moneda" name="id_moneda" value="<?=$monedaIsDefault[0]['id'];?>">
                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");

?>
<script>
    $(document).ready(function () {
        //datatime

        $('#fecha').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            useCurrent: true,
            format: 'DD-MM-YYYY hh:mm',
            showTodayButton: true,
            showClear: true,
            showClose: true,
            //autoclose: true,
            //maxDate: new Date(),
            //        toolbarPlacement: 'top'
        });

        $("#frm_tasa").validate({
            normalizer: function( value ) {
                return $.trim( value );
            },
            rules: {
                fecha: {
                    required: true
                },
                id_moneda_origen:{
                    required: true
                },
                id_moneda_destino:{
                    required: true
                },
                valor:{
                    required: true
                }
            }
        });

    });
</script>
