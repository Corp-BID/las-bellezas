<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? $_POST['op'] : NULL;
$id = (isset($_POST['id']) && $_POST['id']) ? $_POST['id'] : NULL;
$horas = (isset($_POST['horas']) && $_POST['horas']) ? $_POST['horas'] : NULL;
$por_defecto = (isset($_POST['por_defecto']) && $_POST['por_defecto']) ? $_POST['por_defecto'] : NULL;

$vigencia_obj = new VigenciaPedido();

if ($operacion == "add") {
   $vigencia_obj->Agregar($_DB_, $horas, $por_defecto);
} elseif ($operacion == "mod" and $id) {
    $vigencia_obj->Modificar($_DB_, $id, $horas, $por_defecto);
} elseif ($operacion == "del") {
    $vigencia_obj->Eliminar($_DB_, $id);
}

header("Location: ./vigencia_consultar.php");



