<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['m']) && $_GET['m']) ? strval($_GET['m']) : '';

$marca_obj = new Marcas();

if ($op == 'add') {
    $operacion = "add";
    $titulo = "Crear Marca";
    $boton = "Crear";
    $usa_form = true;
    $next_id = $marca_obj->ObtenerCorrelativo($_DB_,'marcas');

} else {

    if ($id and $op == 'mod') {
        $marca = $marca_obj->Obtener($_DB_, $id);
        $operacion = "mod";
        $titulo = "Modificar Marca";
        $boton = "Modificar";
        $usa_form = true;
    } elseif ($id and $op == 'del') {
        $marca = $marca_obj->Obtener($_DB_, $id);

        $operacion = "del";
        $titulo = "Eliminar Marca";
        $boton = "Eliminar";
        $usa_form = false;
    } else {
        $marca = $marca_obj->Obtener($_DB_, $id);
        $titulo = "Consultar Marca";
        $boton = "";
        $usa_form = false;
    }
}

?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">
                <form action="marca_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <?php if (!$usa_form){ ?>
                    <input type="hidden" name="m" value="<?php echo $id; ?>">
                    <?php } ?>
                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $_SESSION['errors']['message'];?>
                        </div>
                    <?php } ?>

                    <table id="my_table_to_excel" class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Código</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="m" value="<?php echo $marca[0]['id']; ?>" readonly><?php
                                        }
                                    } else {
                                        if (isset($next_id) > 0 ){ ?>
                                            <input type="text" class="form-control" name="m" value="<?php echo $next_id; ?>" readonly><?php
                                        }else{
                                            echo $marca[0]['id'];
                                        }
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="descripcion"
                                                   value="<?php echo $marca[0]['descripcion']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="descripcion" value=""><?php
                                        }

                                    } else {

                                        echo $marca[0]['descripcion'];
                                    }?>
                                </td>
                            </tr>

                            <tr>
                                <td>Condición</td>
                                <td>
                                    <?php
                                    if ($usa_form) { ?>
                                        <select name="condicion">
                                            <option value="1" class="form-control" <?php if (isset($marca[0]['activo'])) {
                                                                    echo "selected";
                                                                } ?>>Activo</option>
                                            <option value="0" class="form-control" <?php if ($id and !$marca[0]['activo']) {
                                                                    echo "selected";
                                                                } ?>>Inactivo</option>
                                        </select>
                                    <?php
                                    } else {
                                        $condicion = ($marca[0]['activo']) ? '<span class="label label-info">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
                                       echo $condicion;
                                    }
                                    ?>

                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./marcas_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
