<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['id']) && $_GET['id']) ? strval($_GET['id']) : '';


if ($op == 'add') {
    $operacion = "add";
    $titulo = "Crear Cliente";
    $boton = "Crear";
    $usa_form = true;
} else {
    $cliente_obj = new Clientes();
    $cliente = $cliente_obj->obtenerDatosClienteId($_DB_, $id);
    if ($id and $op == 'mod') {
        $operacion = "mod";
        $titulo = "Modificar Cliente";
        $boton = "Modificar";
        $usa_form = true;
    } elseif ($id and $op == 'del') {
        //$cliente = $cliente_obj->obtenerDatosCliente($_DB_, $id);

        $operacion = "del";
        $titulo = "Eliminar Cliente";
        $boton = "Eliminar";
        $usa_form = false;
    } else {
      //  $cliente = $cliente_obj->obtenerDatosCliente($_DB_, $id);

        $titulo = "Consultar Cliente";
        $boton = "";
        $usa_form = false;
    }
}

?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">
                <form action="cliente_operacion.php" method="post" name="frmregistro" id="frmregistro">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <?php
                    if ($id) { ?><input type="hidden" name="id" value="<?php echo $cliente[0]['id']; ?>"><?php }
                    if ($id) { ?><input type="hidden" name="email_anterior" value="<?php echo $cliente[0]['correo']; ?>"><?php }
                    ?>

                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <p><b><?=isset($_SESSION['errors']['email']);?></b></p><?=isset($_SESSION['errors']['message']);?>
                        </div>
                    <?php } ?>

                   <?php if ($operacion != 'del'){ ?>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="5%">Correo</td>
                                <td width="5%">
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="email"
                                                   value="<?php echo $cliente[0]['correo']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="email" value=""><?php
                                        }
                                    } else {
                                        echo $cliente[0]['correo'];
                                    } ?>
                                </td>
                            </tr>

                            <tr>
                                <td width="5%">Clave</td>
                                <td width="5%">
                                   <input type="password" class="form-control" name="password" value="">
                                </td>
                                <?php if ($op =='mod'){ ?>
                                <td width="5%"><code>Esccriba una nueva clave en caso de que la desee cambiar</code></td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                    <?php } ?>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><b>Datos Personales</b></td>
                            </tr>
                            <tr>
                                <td>Cédula</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="cedula"
                                                   value="<?php echo $cliente[0]['cedula']; ?>" onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="cedula" value=""><?php
                                        }

                                    } else {
                                        echo $cliente[0]['cedula'];
                                    }?>
                                </td>
                            </tr>
                            <tr>
                                <td>Nombres</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="nombres"
                                                   value="<?php echo $cliente[0]['nombres']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="nombres" value=""><?php
                                        }
                                    } else {
                                        echo $cliente[0]['nombres'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Apellidos</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="apellidos"
                                                   value="<?php echo $cliente[0]['apellidos']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="apellidos" value=""><?php
                                        }
                                    } else {
                                        echo $cliente[0]['apellidos'];
                                    } ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Télefono</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($id) { ?>
                                            <input type="text" class="form-control" name="telefono"
                                                   value="<?php echo $cliente[0]['telefono']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="telefono" value=""><?php
                                        }
                                    } else {
                                        echo $cliente[0]['telefono'];
                                    } ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Condición</td>
                                <td>
                                    <?php
                                    if ($usa_form) { ?>
                                        <select name="condicion">
                                            <option value="1" class="form-control" <?php if (isset($cliente[0]['activo'])) {
                                                                    echo "selected";
                                                                } ?>>Activo</option>
                                            <option value="0" class="form-control" <?php if ($id and !$cliente[0]['activo']) {
                                                                    echo "selected";
                                                                } ?>>Inactivo</option>
                                        </select>
                                    <?php
                                    } else {
                                        $activo = ($cliente[0]['activo'] == 1) ? '<span class="label-access label label-default" style="color: #fff">Activo</span>' : '<span class="label-danger label label-default" style="color: #fff">Inactivo</span>';
                                       echo $activo;
                                    }
                                    ?>

                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./clientes_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>
    $("#frmregistro").validate({
        normalizer: function( value ) {
            return $.trim( value );
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            cedula:{
                required: true,
                minlength: 4
            },
            nombres:{
                required: true
            },
            apellidos:{
                required: true
            }
        },
        messages: {
            email: "Por favor agregue un correo válido"
        }
    })
</script>