<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$clientes_obj = new Clientes();
$clientes = $clientes_obj->Listado($_DB_);
?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">CLIENTES</h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <form action="clientes_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Cliente">
            </form>
            <div class="ibox-content">

                <table class="table table-bordered table-hover">
                    <thead>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apellidos</th>
                        <th class="text-center">Correo</th>
                        <th class="text-center">Teléfono</th>
                        <th class="text-center">Condición</th>
                        <th class="text-center"></th>
                    </thead>
                    <tbody>
                        <?php
                        if ($clientes) {
                            foreach ($clientes as $cliente) {
                                $condicion = ($cliente['activo'] == 1) ? '<span class="label-success label label-success">Activo</span>' : '<span class="label-danger label label-danger">Inactivo</span>';
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $cliente['nombres']; ?></td>
                                    <td class="text-center"><?php echo $cliente['apellidos']; ?></td>
                                    <td class="text-center"><?php echo $cliente['correo']; ?></td>
                                    <td class="text-center"><?php echo $cliente['telefono']; ?></td>
                                    <td align="center"><?php echo $condicion; ?></td>
                                    <td class="align-middle text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-outline btn-primary dim" href="clientes_gestion.php?o=con&id=<?php echo $cliente['id']; ?>" type="button"><i class="fa fa-search"></i></a>
                                            <a class="btn btn-outline btn-success dim" href="clientes_gestion.php?o=mod&id=<?php echo $cliente['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-outline btn-danger dim" href="clientes_gestion.php?o=del&id=<?php echo $cliente['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
