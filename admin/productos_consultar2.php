<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$cat = (isset($_GET['c']) && $_GET['c']) ? $_GET['c'] : '';
$limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : PER_PAGE;
$page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
$links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;

$categoria_obj = new Categorias();

$categoria = $categoria_obj->Obtener($_DB_, $cat);
$subcategorias = $categoria_obj->ListadoAdmin($_DB_, $cat, $limit, $page);

$params = [
    'param1' =>'c',
    'value1' =>$cat
];

$Paginator  = new Paginator( $subcategorias['total'], $limit, $page );
/*echo "<pre>";
print_r($subcategorias);
echo "</pre>";*/

?>
<div class="row">
    <div class="col-md-10">
        <h1 class="page-header"><?php echo $categoria[0]['nombre'] ?> - Consultar Subcatergorias</h1>
    </div>

     <div class="col-lg-12">
        <div class="ibox ">
            <a href="./subcategoria_gestion.php?o=add&c=<?php echo $cat; ?>" class="btn btn-primary">Crear Subcategoria</a>
            <div class="ibox-content">
                <table id="my_table_to_excel" class="table table-bordered table-hover">
                    <thead>
                        <th class="text-center">Código-Subcategoria
                        </th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Descripción</th>
                        <th class="text-center">Orden</th>
                        <th class="text-center">Visible</th>
                        <th width="150">
                        </th>
                    </thead>

                    <tbody>

                        <?php

                        if ($subcategorias['data']) {
                            foreach ($subcategorias['data'] as $subcat) {
                                $visible = ($subcat['visible'] == 1) ? '<i class="fa fa-eye" aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa eye-slash " aria-hidden="true" style="color: darkred;"></i>';
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $subcat['subcategoria']; ?></td>
                                    <td><?php echo $subcat['nombre']; ?></td>
                                    <td><?php echo $subcat['descripcion']; ?></td>
                                    <td class="text-center"><?php echo $subcat['orden']; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php
                                            echo $visible
                                        ?>
                                    </td>
                                    <td class="text-center">
                                             <div class="btn-group">

                                                 <a class="btn btn-outline btn-warning dim" href="productos_consultar3.php?c=<?php echo $subcat['categoria']; ?>&sc=<?php echo $subcat['subcategoria']; ?>&dis=1" type="button"><i class="fa fa-arrow-down"></i></a>
                                                 <a class="btn btn-outline btn-primary dim" href="subcategoria_gestion.php?o=vie&c=<?php echo $subcat['categoria']; ?>&sc=<?php echo $subcat['subcategoria']; ?>" type="button"><i class="fa fa-search"></i></a>
                                                 <a class="btn btn-outline btn-success dim" href="subcategoria_gestion.php?o=mod&c=<?php echo $subcat['categoria']; ?>&sc=<?php echo $subcat['subcategoria']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                                 <a class="btn btn-outline btn-danger dim" href="subcategoria_gestion.php?o=del&c=<?php echo $subcat['categoria']; ?>&sc=<?php echo $subcat['subcategoria']; ?>" type="button"><i class="fa fa-trash-o"></i></a>

                                            </div>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        }
                        ?>

                    </tbody>
                </table>
                <?php echo $Paginator->createLinks( $links, 'pagination pg-blue', $params); ?>
                <div class="buttons clearfix">
                    <div class="pull-left">
                        <a onclick="history.back()" class="btn btn-default">Volver</a>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
