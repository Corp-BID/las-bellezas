<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['p']) && $_GET['p']) ? strval($_GET['p']) : '';

$pago_obj = new Pago();

if ($id and $op == 'mod') {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Pago";
    $boton = "Modificar";
    $usa_form = true;
}
elseif ($id and $op == 'mod2') {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id);
    $operacion = "mod2";
    $titulo = "Modificar Pago";
    $boton = "Modificar";
    $usa_form = true;
}
elseif ($id and $op == 'del') {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id);

    $operacion = "del";
    $titulo = "Eliminar Pago";
    $boton = "Eliminar";
    $usa_form = false;
} else {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id);
    $titulo = "Consultar Pago";
    $boton = "";
    $usa_form = true;
}

?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <form action="pago_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <input type="hidden" name="p" value="<?php echo $id; ?>">
                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $_SESSION['errors']['message'];?>
                        </div>
                    <?php } ?>

                    <table id="my_table_to_excel" class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Código</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                            echo $pago[0]['id'];
                                    } ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Forma de Pago</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        echo $pago[0]['tipo_pago'];
                                    } ?>
                                </td>
                            </tr>

                            <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                            <tr>
                                <td>Tipo de Transferencia</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        echo $pago[0]['tipo_transferencia'];
                                    } ?>
                                </td>
                            </tr>
                            <?php } ?>

                            <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                            <tr>
                                <td>Banco</td>
                                <td>
                                    <?php
                                    if ($usa_form && $op != 'add') {
                                        echo $pago[0]['banco'];
                                    } ?>
                                </td>
                            </tr>
                            <?php } ?>

                            <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                            <tr>
                                <td>Referencia</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        echo $pago[0]['referencia'];
                                    }?>
                                </td>
                            </tr>
                            <?php } ?>

                            <tr>
                                <?php
                                $tasa = '';
                                if (strtolower ($pago[0]['simbolos']) =='bs')
                                {
                                    $tasaCalculo = ($pago[0]['monto_pago'] / $pago[0]['monto_total']);
                                    $tasa = 'A tasa: '.number_format($tasaCalculo, 0, ",", ".");?>
                                <?php }
                                ?>
                                <td width="15%">Monto Pagado <code><?=$tasa;?></code></td>
                                <td>
                                    <?php echo $pago[0]['simbolos']." ".number_format($pago[0]['monto_pago'], 2, ",", "."); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Fecha</td>
                                <td>
                                    <?php
                                        $objeto_DateTime = DateTime::createFromFormat('Y-m-d', $pago[0]['fecha']);
                                        $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                                        echo $cadena_nuevo_formato;?>
                                </td>
                            </tr>

                            <tr><td></td></tr>

                            <tr>
                                <td colspan="2" align="center" class="alert alert-success">
                                   Datos del Cliente
                                </td>
                            </tr>

                            <tr>
                                <td>Cliente</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        echo $pago[0]['cliente'];
                                    }?>
                                </td>
                            </tr>

                            <tr>
                                <td>Monto del Pedido</td>
                                <td>
                                    <?php echo number_format($pago[0]['monto_total'], 2, ",", "."); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Condición</td>
                                <td>
                                    <?php
                                        $condicion = ($pago[0]['condicion']) ? "<span class='label label-info'>".$pago[0]['condicion']."</span>" : '<span class="label label-danger">Sin Estatus</span>';
                                        echo $condicion;
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Estatus</td>
                                <td>
                                    <?php
                                    $checked = ($pago[0]['validado']) ? 'checked' : '';
                                    ?>
                                    <div class="i-checks"><label> <input type="checkbox" value="1" name="validado" <?=$checked;?>> <i></i> Validado</label></div>
                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="javascript:history.back();" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
