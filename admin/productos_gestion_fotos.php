<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$op = (isset($_GET['o']) && $_GET['o'])?strval($_GET['o']):'add';
$cat = (isset($_GET['c']) && $_GET['c'])?$_GET['c']:NULL;
$subcat = (isset($_GET['sc']) && $_GET['sc'])?$_GET['sc']:NULL;
$id = (isset($_GET['id']) && $_GET['id'])?$_GET['id'] :NULL;
$visible = $_GET['dis'];

$categoria_obj = new Categorias();
$producto_obj = new Productos();

$categoria = $categoria_obj->Obtener($_DB_, $cat);
$subcategoria = $categoria_obj->Obtener($_DB_, $cat, $subcat);

if($id and $op == 'mod')
{
    $producto_obj = new Productos();
    $producto = $producto_obj -> Consultar($_DB_, $id);

    $operacion = "fotos";
    $titulo = "Modificar Fotos de Producto";
    $boton = "Modificar";
    $usa_form = true;
}
?>


<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">

           
                <form action="productos_operacion.php" method="POST"  enctype="multipart/form-data">
                <input type="hidden" name="op" value="fotos">
                    
                <input type="hidden" name="c" value="<?php echo $cat;?>">
                <input type="hidden" name="sc" value="<?php echo $subcat;?>">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="hidden" name="visible" value="<?php echo $visible;?>">

                <?php if (isset($_SESSION['errors'])){ ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $_SESSION['errors']['message']." (".$_SESSION['errors']['foto'].")";?>
                    </div>
                <?php } ?>

                <table id="my_table_to_excel" class="table table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td height="50">Categoria</td>
                        <td><?php echo $categoria[0]['nombre'];?></td>
                        <td>Subcategoria</td>
                        <td><?php echo $subcategoria[0]['nombre'];?></td>
                    </tr>    
                    <tr>
                        <td>Código</td>
                        <td><?php echo $producto[0]['cod_producto'];?></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td>Descripción</td>
                        <td colspan="3"><?php echo $producto[0]['descripcion'];?></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                    <span class="label-warning label label-default" style="font-size: 12px">
                        <b>Nota:</b> El tamaño de las imagenes debe ser 300px X 300px
                    </span>
                <table class="table table-bordered table-hover">
                    <tbody>
                    <?php
                    for($i=1; $i<=5; $i++)
                    {
                        $foto = "foto".$i;
                        ?>   
                    <tr>
                        <td width="10%">Foto <?php echo $i;?></td>
                                
                        <?php
                        if($producto[0]["$foto"] != "")
                        {?>
                            <td width="10%">
                                <center><img src="<?php echo DIR_MEDIA_WEB_PROD_P.$producto[0]["$foto"];?>"  onerror="this.src='../Productos/thumbs/no_disponible.jpg';" width="90"></center>
                            </td>
                        <?php
                        }
                        else
                        {?>
                            <td>
                            </td>
                        <?php
                        }
                        
                        if($usa_form)
                        {?>
                            <td width="80%">
                                <input type="file" size="32" name="<?php echo $foto;?>" value=""/>
                            </td>

                        <?php
                        }?>
                    </tr>
                    <?php
                    }?>

                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if($boton)
                            {?><input type="submit" value="<?php echo $boton;?>" class="btn btn-primary"><?php }?>
                            <a href="./productos_consultar3.php?c=<?php echo $cat;?>&sc=<?php echo $subcat;?>&dis=<?=$visible;?>" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                   
                </form>
                 
            </div>
        </div>
                
    </div>
    <!-- /.row -->
</div>


<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");