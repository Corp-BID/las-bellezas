<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$cat = (isset($_GET['cat']) && $_GET['cat'])?intval($_GET['cat']):NULL; 
$subcat = (isset($_GET['subcat']) && $_GET['subcat'])?intval($_GET['subcat']):NULL; 
$id = (isset($_GET['id']) && $_GET['id'])?substr(strval($_GET['id']), 0, 200) :NULL; 

$categ = new Categorias();
$produc = new Productos();

$categoria = $categ -> ObtenerCategorias($_DB_, $cat);
$subcategoria = $categ -> ObtenerCategorias($_DB_, $subcat);

$producto = $produc -> ObtenerProductoDetallado($_DB_, $id);
?>
<!-- Section: intro -->
<section id="boxes" class="home-section paddingtop-50 paddingbot-50">
	
    <div class="container">
        <div class="container margintop-50">
            <div class="row">

                <div class="col-sm-12">
                    <div class="wow" data-wow-delay="0.1s">
                        <div class="pricing-content2 general">
                            
                            <center>
                            <h4>Categoria: <? echo $categoria[0]['descripcion']?></h4>
                            <h4>Subcategoria: <? echo $subcategoria[0]['descripcion']?></h4>
                            </center>
                            
                            <h2>Detalle del Producto</h2>
                            
                            
                            <table class="table-cloth">
                            <tbody>

                            <tr>
                                <td width="200">Codigo</td>
                                <td><?php echo $producto[0]['cod_producto'];?></td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td><?php echo $producto[0]['descripcion'];?></td>
                            </tr>
                            <tr>
                                <td>Unidad</td>
                                <td><?php echo $producto[0]['unidad'];?></td>
                            </tr>
                            <tr>
                                <td>Características</td>
                                <td><?php echo $producto[0]['caracteristicas'];?></td>
                            </tr>
                                
                            <tr>
                                <td>Precio 1</td>
                                <td><?php echo $producto[0]['precio1'];?></td>
                            </tr>
                            <?/*
                            <tr>
                                <td>Precio 2</td>
                                <td><?php echo $producto[0]['precio2'];?></td>
                            </tr>
                            <tr>
                                <td>Precio 3</td>
                                <td><?php echo $producto[0]['precio3'];?></td>
                            </tr>
                            */?>
                            <tr>
                                <td>Foto</td>
                                <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$producto[0]['foto'];?>" width="90"></td>
                            </tr>
                            <?
                            if($producto[0]['foto2'])
                            {?>
                            <tr>
                                <td>Foto 2</td>
                                <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$producto[0]['foto2'];?>" width="90"></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['foto3'])
                            {?>
                            <tr>
                                <td>Foto 3</td>
                                <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$producto[0]['foto3'];?>" width="90"></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['foto4'])
                            {?>
                            <tr>
                                <td>Foto 4</td>
                                <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$producto[0]['foto4'];?>" width="90"></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['foto5'])
                            {?>
                            <tr>
                                <td>Foto 5</td>
                                <td><img src="<?php echo DIR_MEDIA_WEB_PROD2.$producto[0]['foto5'];?>" width="90"></td>
                            </tr>
                            <?
                            }?>
                            
                            <tr>
                                <td>Cantidad Minima</td>
                                <td><?php echo $producto[0]['cant_min'];?></td>
                            </tr>
                                
                            <?
                            if($producto[0]['prod_esp_1'])
                            {?>
                            <tr>
                                <td>Producto Especifico 1</td>
                                <td><?php echo $producto[0]['prod_esp_1'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_2'])
                            {?>
                            <tr>
                                <td>Producto Especifico 2</td>
                                <td><?php echo $producto[0]['prod_esp_2'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_3'])
                            {?>
                            <tr>
                                <td>Producto Especifico 3</td>
                                <td><?php echo $producto[0]['prod_esp_3'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_4'])
                            {?>
                            <tr>
                                <td>Producto Especifico 4</td>
                                <td><?php echo $producto[0]['prod_esp_4'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_5'])
                            {?>
                            <tr>
                                <td>Producto Especifico 5</td>
                                <td><?php echo $producto[0]['prod_esp_5'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_6'])
                            {?>
                            <tr>
                                <td>Producto Especifico 6</td>
                                <td><?php echo $producto[0]['prod_esp_6'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_7'])
                            {?>
                            <tr>
                                <td>Producto Especifico 7</td>
                                <td><?php echo $producto[0]['prod_esp_7'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_8'])
                            {?>
                            <tr>
                                <td>Producto Especifico 8</td>
                                <td><?php echo $producto[0]['prod_esp_8'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_9'])
                            {?>
                            <tr>
                                <td>Producto Especifico 9</td>
                                <td><?php echo $producto[0]['prod_esp_9'];?></td>
                            </tr>
                            <?
                            }?>
                                
                            <?
                            if($producto[0]['prod_esp_10'])
                            {?>
                            <tr>
                                <td>Producto Especifico 10</td>
                                <td><?php echo $producto[0]['prod_esp_10'];?></td>
                            </tr>    
                            <?
                            }?>    
                            
                            <tr>
                                <td>Nuevo</td>
                                <td>
                                    <?php 
                                    if($producto[0]['nuevo'])
                                    {echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";}
                                    else
                                    {echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";}
                                    ?>
                                </td>
                            </tr>
                                
                            <tr>
                                <td>Oferta</td>
                                <td>
                                    <?php 
                                    if($producto[0]['oferta'])
                                    {echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";}
                                    else
                                    {echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";}
                                    ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>Visible</td>
                                <td>
                                    <?php 
                                    if($producto[0]['visible'])
                                    {echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";}
                                    else
                                    {echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";}
                                    ?>
                                </td>
                            </tr>
                            </table>
                            
                            
                            <center>
                                <h4>Desea eliminar este Producto?</h4>
                                
                                <table>
                                <tr>
                                    <td>
                                        <form action="productos_operacion.php" method="post">
                                        <input type="hidden" name="op" value="del">
                                        <input type="hidden" name="cat" value="<?php echo $cat;?>">
                                        <input type="hidden" name="subcat" value="<?php echo $subcat;?>">
                                        <input type="hidden" name="id" value="<?php echo $producto[0]['cod_producto'];?>">
                                        <input type="submit" value="Eliminar">
                                        </form>
                                    </td>
                                    <td width="20"></td>
                                    <td>
                                        <form action="productos_consultar3.php" method="GET">
                                        <input type="hidden" name="cat" value="<?php echo $cat;?>">
                                        <input type="hidden" name="subcat" value="<?php echo $subcat;?>">
                                        <input type="submit" value="Cancelar">
                                        </form>
                                    </td>
                                </tr>
                                </table>
                                <br>
                                
                            
                            </center>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>		
</section>
<!-- /Section: intro -->
<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");