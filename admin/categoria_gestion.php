<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : '';


if ($op == 'add') {
    $operacion = "add";
    $titulo = "Crear Categoria";
    $boton = "Crear";
    $usa_form = true;
} else {
    $categoria_obj = new Categorias();

    if ($cat and $op == 'mod') {
        $categoria = $categoria_obj->Obtener($_DB_, $cat);

        $operacion = "mod";
        $titulo = "Modificar Categoria";
        $boton = "Modificar";
        $usa_form = true;
    } elseif ($cat and $op == 'del') {
        $categoria = $categoria_obj->Obtener($_DB_, $cat);

        $operacion = "del";
        $titulo = "Eliminar Categoria";
        $boton = "Eliminar";
        $usa_form = false;
    } else {
        $categoria = $categoria_obj->Obtener($_DB_, $cat);

        $titulo = "Consultar Categoria";
        $boton = "";
        $usa_form = false;
    }
}

?>
<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">
                <form action="categoria_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <?php
                    if ($cat) { ?><input type="hidden" name="c" value="<?php echo $categoria[0]['categoria']; ?>"><?php }
                    if ($cat) { ?><input type="hidden" name="cn" value="<?php echo $categoria[0]['categoria']; ?>"><?php } ?>

                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $_SESSION['errors']['message'];?>
                        </div>
                    <?php } ?>

                    <table id="my_table_to_excel" class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td>Código</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($cat) { ?>
                                            <input type="text" class="form-control" name="c"
                                                   value="<?php echo $categoria[0]['categoria']; ?>" readonly><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="c" value=""><?php
                                        }
                                    } else {
                                        echo $categoria[0]['categoria'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Nombre</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($cat) { ?>
                                            <input type="text" class="form-control" name="nombre"
                                                   value="<?php echo $categoria[0]['nombre']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="nombre" value=""><?php
                                        }

                                    } else {
                                        echo $categoria[0]['nombre'];
                                    }?>
                                </td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($cat) { ?>
                                            <input type="text" class="form-control" name="descripcion"
                                                   value="<?php echo $categoria[0]['descripcion']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="descripcion" value=""><?php
                                        }
                                    } else {
                                        echo $categoria[0]['descripcion'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Orden</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($cat) { ?>
                                            <input type="text" class="form-control" name="orden"
                                                   value="<?php echo $categoria[0]['orden']; ?>" onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="orden" value="" onKeyPress="return soloNumeros(event)"><?php
                                        }
                                    } else {
                                        echo $categoria[0]['orden'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Visible</td>
                                <td>
                                    <?php
                                    if ($usa_form) { ?>
                                        <select name="visible">
                                            <option value="1" class="form-control" <?php if (isset($categoria[0]['visible'])) {
                                                                    echo "selected";
                                                                } ?>>Si</option>
                                            <option value="0" class="form-control" <?php if ($cat and !$categoria[0]['visible']) {
                                                                    echo "selected";
                                                                } ?>>No</option>
                                        </select>
                                    <?php
                                    } else {
                                        $visible = ($categoria[0]['visible'] == 1) ? '<i class="fa fa-eye " aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa fa-eye-slash  " aria-hidden="true" style="color: darkred;"></i>';
                                       echo $visible;
                                    }
                                    ?>

                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./productos_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
