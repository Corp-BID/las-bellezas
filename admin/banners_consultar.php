<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$visible = $_GET['vis'];

$banners_obj = new Banners();
$banners = $banners_obj->Listado($_DB_,$visible);

$directorio = "../Productos/slider/";

?>
<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">
            Consultar Banners -
            <?php if (!$visible) { echo "Visibles"; } else { echo "No Visibles"; } ?></h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <a href="./banners_gestion.php?o=add" class="btn btn-primary">Crear Banner</a>
            <div class="consultar">
                <?php
                if (!$visible) { ?>
                    <a href="?vis=1" class="btn btn-warning">Ver Banner Eliminados</a>
                    <?php
                } else { ?>
                    <a href="?vis=0" class="btn btn-info">Ver Banner</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Id</th>
                            <th class="text-center">Im&aacute;gen</th>
                            <th class="text-center">Titulo</th>
                            <th class="text-center">Descripci&oacute;n</th>
                            <th class="text-center">Boton</th>
                            <th class="text-center">Link</th>
                            <th class="text-center">Publicado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if ($banners) {
                            foreach ($banners as $banner) {
                                $ancho = '';
                                $alto = '';
                                if (!empty($banner['imagen'])) {
                                    $imagen = getimagesize($directorio . $banner['imagen']);    //Sacamos la información
                                    $ancho = $imagen[0];              //Ancho
                                    $alto = $imagen[1];               //Alto
                                }
                                ?>
                                <tr>
                                    <td class="align-middle"><?php echo $banner['id']; ?></td>
                                    <td align="center">
                                        <img src="<?php echo $directorio . $banner['imagen']; ?>" width="300" class="align-middle">
                                        <br>
                                        <?php
                                        if (!empty($ancho)){
                                        echo "<code>".$ancho."x".$alto."</code>";}?>
                                    </td>
                                    <td class="align-middle"><?php echo $banner['titulo']; ?></td>
                                    <td class="align-middle"><?php echo $banner['descripcion']; ?></td>
                                    <td class="align-middle"><?php echo $banner['boton']; ?></td>
                                    <td class="align-middle"><?php echo $banner['link']; ?></td>
                                    <td width="100" align="center" class="align-middle">
                                        <?php
                                        echo $validado = ($banner['publicado'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;font-size: 20px"></i>' : '<i class="fa fa-ban" class="check-link" style="color: darkred;font-size: 20px"></i>';
                                        ?>
                                    </td>
                                    <td align="center" class="align-middle">
                                        <div class="btn-group">
                                            <a class="btn btn-outline btn-success dim" href="banners_gestion.php?o=mod&id=<?php echo $banner['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            <?php if (!$visible){ ?>
                                                <a class="btn btn-outline btn-danger dim" href="banners_gestion.php?o=del&id=<?php echo $banner['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                            <?php }else{ ?>
                                                <a class="btn btn-outline btn-success dim" href="banners_gestion.php?o=rev&id=<?php echo $banner['id']; ?>" type="button"><i class="fa fa-undo"></i></a>
                                            <?php } ?>
                                        </div>

                                    </td>
                                </tr>
                        <?php
                            }
                        } ?>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
