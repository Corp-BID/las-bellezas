<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$directorio = "../img/slides/";
//$directorio2 = "http://www.invcga.com/img/slides/";
//$directorio2 = "http://127.0.0.1/~pignoto/nacaridportal/nacarid/img/slides/";
?>

<section id="boxes" class="home-section paddingtop-50 paddingbot-50">
	
    <div class="container">
        <div class="container margintop-50">
            <div class="row">

                <div class="col-sm-12">
                    <div class="wow" data-wow-delay="0.1s">
                        <div class="pricing-content2 general">
                            <h2>Agregar Imágen para el Banner</h2>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div></div>
                                </div>
                                <div class="panel-body">
                                    <br>
                                    <center><b>Nota:</b> el Banner tiene que ser una imágen de 1400px x 400px</center>

                                    <table class="table table-striped table-advance table-hover">
                                    <?php
                                    $banner = $_FILES['banner'];
                                    $descripcion = $_POST['descripcion'];
                                    $link = $_POST['link'];

                                    if($banner['name'])
                                    {
                                        $nombre_archivo = date("Ymd")."-".rand(100,999);

                                        if($banner['type'] == "image/jpeg")
                                        {
                                            $foo = new Upload($banner);

                                            if ($foo->uploaded) 
                                            {
                                                // Guardo la imagen normal
                                                $foo->file_new_name_body = $nombre_archivo;
                                                $foo->file_overwrite = true;
                                                $foo->image_resize = true;
                                                $foo->image_ratio_y = true;
                                                $foo->image_x = 1400;
                                                $foo->image_convert = jpg;
                                                $foo->Process($directorio);

                                                if ($foo->processed){$error="";}
                                                else{$error = $foo->error;}
                                            } 
                                        }
                                        else
                                        {
                                            $error = "La imagen <b>NO</b> se cargo debido a que la mismo debe ser un JPG";
                                        }


                                        if($error=="")
                                        {
                                            $nombre_archivo = $nombre_archivo.".jpg";

                                            $slides = new Slides();
                                            $slides -> AgregarSlides($_DB_, $nombre_archivo, $descripcion, $link);

                                        ?> 
                                            <tr>
                                                <td width="100">Banner:</td>
                                                <td><img src="<?php echo $directorio2 . $nombre_archivo;?>"> </td>
                                            </tr>
                                            <tr>
                                                <td width="100">Descripcion:</td>
                                                <td><?php echo $descripcion;?> </td>
                                            </tr>
                                            <tr>
                                                <td width="100">Link:</td>
                                                <td><?php echo $link;?> </td>
                                            </tr>
                                            <?php
                                            }
                                            else
                                            {?>
                                                <tr>
                                                    <td width="100">Error:</td>
                                                    <td colspan="2"><?php echo $error;?></td>
                                                </tr>

                                            <?php
                                            }
                                            $k++;
                                        }
                                    ?>
                                    </table>    
                                        
                                    <center>
                                    <input type="submit" value="Siguiente" class="btn btn-primary">
                                    </center>
                                   
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
            </div>
        </div>        
    </div>
</div>
</section>    
            
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");