<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");


$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 5) :NULL; 

$id = (isset($_POST['id']) && $_POST['id'])?intval($_POST['id']):NULL;

$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion'])?substr(strval($_POST['descripcion']), 0, 200) :NULL;
$beneficiario = (isset($_POST['beneficiario']) && $_POST['beneficiario'])?strval($_POST['beneficiario']):NULL;
$opcional1 = (isset($_POST['opcional1']) && $_POST['opcional1'])?$_POST['opcional1'] :NULL;
$opcional2 = (isset($_POST['opcional2']) && $_POST['opcional2'])?$_POST['opcional2'] :NULL;
$opcional3 = (isset($_POST['opcional3']) && $_POST['opcional3'])?$_POST['opcional3'] :NULL;

$publicado = (isset($_POST['publicado']) && $_POST['publicado'])?$_POST['publicado'] :0;
$validado = (isset($_POST['validado']) && $_POST['validado'])?$_POST['validado'] :0;

$visible = (isset($_POST['visible']) && $_POST['visible'])?intval($_POST['visible']):0; 

$testimonios = new Testimonios();
;
if($operacion =="add")
{
    $filename = (isset($_POST['filename']) && $_POST['filename'])?$_POST['filename'] :NULL;
    $testimonios->Agregar($_DB_, $descripcion, $beneficiario, $opcional1,$opcional2,$opcional3,$filename, $publicado, $validado);
}
elseif($operacion =="mod" and $id)
{
    $filename = (!empty($_POST['filename']))?$_POST['filename'] :$_POST['image_destacada'];
    $testimonios->Modificar($_DB_, $id, $descripcion, $beneficiario, $opcional1,$opcional2,$opcional3,$filename, $publicado, $validado);
}
elseif($operacion =="del")
{
    $testimonios->Eliminar($_DB_, $id);
}
elseif($operacion =="rev")
{
    $testimonios->Revertir($_DB_, $id);
}


header("Location: ./testimonios_consultar.php?del=0");
