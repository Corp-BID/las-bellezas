<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? $_POST['op'] : NULL;
$id = (isset($_POST['id']) && $_POST['id']) ? $_POST['id'] : NULL;
$id_moneda = (isset($_POST['id_moneda']) && $_POST['id_moneda']) ? $_POST['id_moneda'] : NULL;
$id_moneda_destino = (isset($_POST['id_moneda_destino']) && $_POST['id_moneda_destino']) ? $_POST['id_moneda_destino'] : NULL;
$fecha = (isset($_POST['fecha']) && $_POST['fecha']) ? $_POST['fecha'] : NULL;
$valor = (isset($_POST['valor']) && $_POST['valor']) ? $_POST['valor'] : NULL;
$activo = (isset($_POST['activo']) && $_POST['activo']) ? $_POST['activo'] : NULL;
$miles = str_replace(".", "", $valor);
$valor = str_replace(",", ".", $miles);

$tasa_cambio_obj = new TasaDolar();

if ($operacion == "add") {
   $tasa_cambio_obj->Agregar($_DB_, $valor, $fecha, $id_moneda,$id_moneda_destino,$activo);
} elseif ($operacion == "mod" and $id) {
    $tasa_cambio_obj->Modificar($_DB_, $id, $valor, $fecha, $activo,$id_moneda, $id_moneda_destino);
} elseif ($operacion == "del") {
    $tasa_cambio_obj->Eliminar($_DB_, $id);
}

header("Location: ./tasa_consultar.php?vis=0");



