<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : PER_PAGE;
$page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
$links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;


$categoria_obj = new Categorias();
$categorias = $categoria_obj->ListadoAdmin($_DB_, '', $limit, $page);

$params = [
        'param1' =>'',
        'value1' =>''
];

$Paginator  = new Paginator( $categorias['total'], $limit, $page );
?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">CATEGORIAS</h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <form action="categoria_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Categoria">
            </form>

            <div class="ibox-content">

                <table id="my_table_to_excel" class="table table-bordered table-hover">
                    <thead>
                        <th class="text-center">Categoria</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Descripción</th>
                        <th class="text-center">Orden</th>
                        <th class="text-center">Visible</th>
                        <th class="text-center"></th>
                    </thead>
                    <tbody>
                        <?php
                        if ($categorias) {
                            foreach ($categorias['data'] as $cat) {
                                 $visible = ($cat['visible'] == 1) ? '<i class="fa fa-eye" aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa fa-eye-slash  " aria-hidden="true" style="color: darkred;"></i>';
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $cat['categoria']; ?></td>
                                    <td><?php echo $cat['nombre']; ?></td>
                                    <td><?php echo $cat['descripcion']; ?></td>
                                    <td>
                                        <center><?php echo $cat['orden']; ?></center>
                                    </td>
                                    <td align="center">
                                        <?=$visible;?>
                                    </td>
                                    <td width="150">
                                        <center>
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-warning dim" href="productos_consultar2.php?c=<?php echo $cat['categoria']; ?>" type="button"><i class="fa fa-arrow-down"></i></a>
                                                <a class="btn btn-outline btn-primary dim" href="categoria_gestion.php?o=vie&c=<?php echo $cat['categoria']; ?>" type="button"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-outline btn-success dim" href="categoria_gestion.php?o=mod&c=<?php echo $cat['categoria']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-outline btn-danger dim" href="categoria_gestion.php?o=del&c=<?php echo $cat['categoria']; ?>" type="button"><i class="fa fa-trash-o"></i></a>

                                            </div>
                                        </center>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        } ?>

                    </tbody>
                </table>
                <?php echo $Paginator->createLinks( $links, 'pagination pg-blue', $params ); ?>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
