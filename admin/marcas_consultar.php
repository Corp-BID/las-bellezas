<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$status = (!empty($_GET['dis']) == 1) ? 1 : 0;
$marcas_obj = new Marcas();
$marcas = $marcas_obj->Listado($_DB_,$status);

?>
<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">MARCAS</h1>
    </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <form action="marca_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Marca">
            </form>
            <div class="consultar">
                <?php
                if ($status) { ?>
                    <a href="?dis=0" class="btn btn-warning">Marcas</a>
                    <?php
                } else { ?>
                    <a href="?dis=1" class="btn btn-warning">Marcas Eliminadas</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">

                <table id="my_table_to_excel" class="table table-bordered">
                    <thead>
                        <th class="text-center">ID</th>
                        <th class="text-center">Marca</th>
                        <th class="text-center">Logo</th>
                        <th class="text-center">Condición</th>
                        <th class="text-center"></th>
                    </thead>

                    <tbody>

                        <?php
                        if ($marcas) {
                            foreach ($marcas as $marca) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $marca['id']; ?></td>
                                    <td><?php echo $marca['descripcion']; ?></td>
                                    <td class="text-center">
                                        <?php  if ($marca['logo']!='') { ?>
                                            <img src="<?php echo DIR_MEDIA_WEB_PROD_LOGO . $marca['logo']; ?>" width="90">
                                        <?php }else{ ?>
                                        <img src="<?php echo DIR_MEDIA_WEB_PROD_LOGO;?>/no_disponible.jpg" width="90">
                                        <?php } ?>
                                    </td>
                                    <td><?php
                                        $condicion = ($marca['activo']) ? '<span class="label label-info">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
                                        echo $condicion; ?></td>
                                     <td width="150">
                                        <center>
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-primary dim" href="marca_gestion.php?o=vie&m=<?php echo $marca['id']; ?>" type="button"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-outline btn-warning dim" href="marcas_gestion_fotos.php?o=mod&m=<?php echo $marca['id']; ?>" type="button"><i class="fa fa-camera"></i></a>
                                                <a class="btn btn-outline btn-success dim" href="marca_gestion.php?o=mod&m=<?php echo $marca['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-outline btn-danger dim" href="marca_gestion.php?o=del&m=<?php echo $marca['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
