<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$visible = ($_GET['dis'] == 1) ? 1 : 0;
$slider_obj = new Slides();
$sliders = $slider_obj->ObtenerSlides($_DB_, $visible);


?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">
            <?php echo $categoria[0]['nombre'] ?> - <?php echo $subcategoria[0]['nombre'] ?> - Consultar Productos -
            <?php if ($visible) { echo "Visibles"; } else { echo "No Visibles"; } ?></h1>
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <a href="./productos_gestion.php?o=add&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&dis=<?=$visible;?>" class="btn btn-primary">Crear Producto</a>
            <div class="consultar">
                <?php
                if ($visible) { ?>
                    <a href="?dis=2&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>" class="btn btn-warning">Ver Productos No Visitbles</a>
                    <?php
                } else { ?>
                    <a href="?dis=1&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>" class="btn btn-warning">Ver Productos Visitbles</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">
                <table id="my_table_to_excel" class="table table-striped table-bordered table-hover">
                    <thead>
                        <th class="text-center">Codigo</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Marca</th>
                        <th class="text-center">Presentación</th>
                        <th class="text-center">Precios</th>
                        <th class="text-center">Fotos</th>
                        <th class="text-center">Nue</th>
                        <th class="text-center">Ofe</th>
                        <th class="text-center">Vis</th>
                        <th width="200"></th>
                    </thead>

                    <tbody>
                        <?php
                        if ($productos['data']) {
                            foreach ($productos['data'] as $prod) {
                                ?>
                                    <tr>
                                        <td><?php echo $prod['cod_producto']; ?></td>
                                        <td><?php echo $prod['nombre']; ?></td>
                                        <td class="text-center"><?php echo $prod['descripcion_marca']; ?> </td>
                                        <td class="text-center"><?php echo $prod['presentacion']; ?> </td>
                                        <td>
                                            <?php echo number_format($prod['precio'], 2, ',', '.'); ?>
                                        </td>
                                        <td class="text-center"><img src="<?php echo DIR_MEDIA_WEB_PROD_P . $prod['foto1']; ?>" onerror="this.src='../Productos/no_disponible.jpg';" width="90"> </td>

                                        <td class="text-center">
                                            <?php
                                            $nuevo = ($prod['nuevo'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                            echo $nuevo
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                            $oferta = ($prod['oferta'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                            echo $oferta
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                            $view = ($prod['visible'] == 1) ? '<i class="fa fa-eye" aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa eye-slash " aria-hidden="true" style="color: darkred;"></i>';
                                            echo $view;
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-primary dim" href="productos_gestion.php?o=con&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&id=<?php echo $prod['cod_producto']; ?>&dis=<?=$visible;?>" type="button"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-outline btn-warning dim" href="productos_gestion_fotos.php?o=mod&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&id=<?php echo $prod['cod_producto']; ?>&dis=<?=$visible;?>" type="button"><i class="fa fa-camera"></i></a>
                                                <a class="btn btn-outline btn-success dim" href="productos_gestion.php?o=mod&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&id=<?php echo $prod['cod_producto']; ?>&dis=<?=$visible;?>" type="button"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-outline btn-danger dim" href="productos_gestion.php?o=del&c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>&id=<?php echo $prod['cod_producto']; ?>&dis=<?=$visible;?>" type="button"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                        <?php
                            }
                        } ?>
                    </tbody>
                </table>
                <?php echo $Paginator->createLinks( $links, 'pagination pg-blue', $params); ?>
                <div class="buttons clearfix">
                    <div class="pull-left">
                        <a onclick="history.back()" class="btn btn-default">Volver</a>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
