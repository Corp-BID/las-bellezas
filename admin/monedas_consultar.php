<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$status = (!empty($_GET['dis']) == 1) ? 1 : 0;
$monedas_obj = new Monedas();
$monedas = $monedas_obj->Listado($_DB_,$status);

?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">MONEDAS</h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <form action="moneda_gestion.php" class="registrar">
                <input type="submit" class="btn btn-primary" value="Crear Moneda">
            </form>
            <div class="consultar">
                <?php
                if ($status) { ?>
                    <a href="?dis=0" class="btn btn-warning">Monedas</a>
                    <?php
                } else { ?>
                    <a href="?dis=1" class="btn btn-warning">Monedas Eliminadas</a>
                <?php }
                ?>
            </div>
            <div class="ibox-content">

                <table id="my_table_to_excel" class="table table-bordered table-hover">
                    <thead>
                        <th class="text-center">ID</th>
                        <th class="text-center">Moneda</th>
                        <th class="text-center">Simbolo</th>
                        <th class="text-center">Por Defecto</th>
                        <th class="text-center"></th>
                    </thead>

                    <tbody>

                        <?php
                        if ($monedas) {
                            foreach ($monedas as $moneda) {
                                $por_defecto = ($moneda['por_defecto'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $moneda['id']; ?></td>
                                    <td><?php echo $moneda['descripcion']; ?></td>
                                    <td><?php echo $moneda['simbolos']; ?></td>
                                    <td align="center" style="font-size: large"><?php echo $por_defecto; ?></td>
                                     <td width="150">
                                        <center>
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-primary dim" href="moneda_gestion.php?o=vie&m=<?php echo $moneda['id']; ?>" type="button"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-outline btn-success dim" href="moneda_gestion.php?o=mod&m=<?php echo $moneda['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-outline btn-danger dim" href="moneda_gestion.php?o=del&m=<?php echo $moneda['id']; ?>" type="button"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
