<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 5) :NULL; 
$id = (isset($_POST['m']) && $_POST['m'])?substr(strval($_POST['m']), 0, 50) :'';
$marca_new = (isset($_POST['mn']) && $_POST['mn'])?substr(strval($_POST['mn']), 0, 50) :'';
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion'])?substr(strval(mb_strtoupper ($_POST['descripcion'])), 0, 800) :NULL;
$condicion = (isset($_POST['condicion']) && $_POST['condicion'])?intval($_POST['condicion']):0;

$marca_obj = new Marcas();

if($operacion =="add")
{
    $marca = $marca_obj->Obtener($_DB_, $id);
    $errors = [];
    if (count($marca) > 0) {
        $errors = [
            'message' => 'Marca ya existe!, verifique!'
        ];
        $_SESSION['errors'] = $errors;
        header("Location: ./marca_gestion.php");
        die;
    }else
        $marca_obj->Agregar($_DB_, $descripcion, $condicion, 0);
        unset($_SESSION["errors"]);
}
elseif($operacion =="mod" && $id)
{
    $marca_obj->Modificar($_DB_, $id, $descripcion, $condicion, 0);
}
elseif($operacion =="del")
{
    $marca_obj->Eliminar($_DB_, $id);
}
elseif ($operacion =='fotos'){

        $f = $_FILES["logo"];

        if ($f['name']) {
            $prefijo = date('Ymd_His_');
            $nombre_archivo = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);

            if ($f['type'] == "image/jpeg"){
                $foo = new Upload($f);

                if ($foo->uploaded) {
                    // Guardo la imagen normal
                    $foo->file_new_name_body = $prefijo . $nombre_archivo;
                    $foo->file_overwrite = true;
                    $foo->image_resize = true;
                    $foo->image_ratio_y = true;
                    $foo->image_x = 400;
                    //$foo->image_convert = jpg;
                    $foo->Process(DIR_MEDIA_WEB_PROD_LOGO);

                    if ($foo->processed) {
                        $error = "";
                    } else {
                        $error = $foo->error;
                    }

                    $logo = $prefijo . $nombre_archivo . ".jpg";

                    $marca_obj->ModificarLogo($_DB_, $id, $logo);
                    unset($_SESSION["errors"]);
                    header("Location: ./marcas_consultar.php?m=$id");
                }

            }else{
                $errors = [];
                $errors = [
                    'message' => 'Error en el tipo de Formato, Solo se permiten archivos JPG, verifique!'
                ];
                $_SESSION['errors'] = $errors;
                header("Location: ./marcas_gestion_fotos.php?o=mod&m=$id");
                die;
            }
        }
    }

header("Location: ./marcas_consultar.php");