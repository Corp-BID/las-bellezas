<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$pagos_obj = new Pago();
$pagos = $pagos_obj->Listado($_DB_);
?>

<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">PAGOS</h1>
    </div>

    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">

                <table id="my_table_to_excel" class="table table-bordered table-hover">
                    <thead>
                        <th class="text-center">ID</th>
                        <th class="text-center"># Pedido</th>
                        <th class="text-center">Forma de Pago</th>
                        <th class="text-center">Tipo de Transferencia</th>
                        <th class="text-center">Banco</th>
                        <th class="text-center">Referencia</th>
                        <th class="text-center">Monto Pago</th>
                        <th class="text-center">Tasa</th>
                        <th class="text-center">Fecha</th>
                        <th class="text-center">Observacion</th>
                        <th class="text-center">Validado</th>
                        <th class="text-center"></th>
                    </thead>

                    <tbody>

                        <?php
                        if ($pagos) {
                            foreach ($pagos as $pago) {
                                $objeto_DateTime = DateTime::createFromFormat('Y-m-d', $pago['fecha']);
                                $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                                $validado = ($pago['validado'] == 1) ? '<i class="fa fa-check-circle-o" aria-hidden="true" style="color: #1ab394;"></i>' : '';
                                ?>
                                <tr>
                                    <td><?php echo $pago['id']; ?></td>
                                    <td><?php echo $pago['id_pedido']; ?></td>
                                    <td><?php echo $pago['tipo_pago']; ?></td>
                                    <td><?php echo $pago['tipo_transferencia']; ?></td>
                                    <td><?php echo $pago['banco']; ?></td>
                                    <td align="right"><?php echo $pago['referencia']; ?></td>
                                    <td align="right"><?php echo $pago['simbolos']." ". number_format($pago['monto_pago'], 2, ",", ".");?></td>
                                    <td class="text-right"><?php
                                        $tasa = '';
                                        if (strtolower ($pago['simbolos']) == 'bs'){
                                            $tasa = $pago['simbolos']." ".number_format(($pago['monto_pago'] / $pago['monto']), 2, ",", ".");
                                        }
                                        echo $tasa; ?>
                                    </td>

                                    <td align="center"><?php echo $cadena_nuevo_formato; ?></td>
                                    <td><b><?php echo $pago['observacion']; ?></b></td>
                                      <td align="center">
                                        <?=$validado;?>
                                    </td>

                                    <td width="150">
                                            <div class="btn-group">
                                                <a class="btn btn-outline btn-primary dim" href="pago_gestion.php?o=vie&p=<?php echo $pago['id']; ?>" type="button"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-outline btn-success dim" href="pago_gestion.php?o=mod&p=<?php echo $pago['id']; ?>" type="button"><i class="fa fa-pencil"></i></a>
                                            </div>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
