<?php

ini_set('max_execution_time', 120);
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? $_POST['op'] : NULL;

$id = (isset($_POST['id']) && $_POST['id']) ? $_POST['id'] : '';


$titulo = (isset($_POST['titulo']) && $_POST['titulo']) ? $_POST['titulo'] : NULL;
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion']) ? $_POST['descripcion'] : NULL;
$link = (isset($_POST['link']) && $_POST['link']) ? $_POST['link'] : NULL;
$boton = (isset($_POST['boton']) && $_POST['boton']) ? $_POST['boton'] : NULL;
$publicado = (isset($_POST['publicado']) && $_POST['publicado']) ? $_POST['publicado'] : 0;
$directorio = "../Productos/slider/";

$upload='';
$prefijo = date('Ymd_His_');
$nombre_archivo_rand = date("Ymd")."-".rand(100,999);
$nombre_archivo = $nombre_archivo_rand.".jpg";
$errors = [];
unset($_SESSION["errors"]);
$banner_obj = new Banners();

if ($operacion == "add") {
    if(!empty($_FILES['banner']['name']) )
    {
        $upload = upload($_FILES['banner'],$directorio,$nombre_archivo_rand,$operacion);

        if($upload==""){

            $banner_obj -> Agregar($_DB_, $nombre_archivo, $titulo, $descripcion, $boton, $link, $publicado);
        }else{

            header("Location: ./banners_gestion.php?op=add");
            die;
        }

    }else{
        $errors = [
            'message' => 'Debe seleccionar una Imagen, verifique!'
        ];
        $_SESSION['errors'] = $errors;
        header("Location: ./banners_gestion.php?op=add");
        die;
    }

} elseif ($operacion == "mod" and $id) {

    if(!empty($_FILES['banner']['name'])){
        $upload = upload($_FILES['banner'],$directorio,$nombre_archivo_rand,$operacion,$id);
        if ($upload!=''){
            header("Location: ./banners_gestion.php?op=mod&id=".$id);
            die;
        }
    }

    if (!empty($_FILES['banner']['name']))
        $banner_obj->Modificar($_DB_, $id, $nombre_archivo, $titulo, $descripcion, $boton, $link, $publicado);
    else
        $banner_obj->Modificar($_DB_, $id, '', $titulo, $descripcion, $boton, $link, $publicado);


    } elseif ($operacion == "del") {
        $banner_obj->Eliminar($_DB_, $id);
    } elseif ($operacion == "rev") {
        $banner_obj->Revertir($_DB_, $id);
}

header("Location: ./banners_consultar.php?vis=0");

function upload($imagen,$directorio,$nombre_archivo,$operacion,$id=null)
{
    $banner = $imagen;
    $filename = $banner['tmp_name'];
    list($width, $height) = getimagesize($filename);

    if ($width < 1600){
        $errors = [
            'message' => 'Tamaño de la imagen incorrecta, verifique! '."<code>".$width."x".$height."</code>"
        ];
        $_SESSION['errors'] = $errors;
        if ($operacion ='add')
            header("Location: ./banners_gestion.php?op=add");
        else
            header("Location: ./banners_gestion.php?op=mod&id".$id);
        die;
    }

    if($banner['type'] == "image/jpeg"){
        $foo = new Upload($banner);

        if ($foo->uploaded)
        {
            $foo->file_new_name_body = $nombre_archivo;
            $foo->file_overwrite = true;
            $foo->image_resize = true;
            $foo->image_ratio_y = true;
            $foo->image_x = 1600;
            $foo->image_y = 900;
            $foo->image_convert = 'jpg';
            $foo->Process($directorio);

            if ($foo->processed){$error="";}
            else{$error = $foo->error;}
        }
    }
    else
    {
        $error = "Error en la Imagen, Formato invalido (Debe ser JPG)";
    }

    $errors = [
        'message' => $error
    ];

    $_SESSION['errors'] = $errors;
    return $error;

}


