<?php
include_once(realpath(dirname(__FILE__))."/include/header.php");

$op = (isset($_GET['o']) && $_GET['o'])?strval($_GET['o']):'add'; 
$id = (isset($_GET['id']) && $_GET['id'])?intval($_GET['id']):NULL; 


if($op == 'add')
{
    $operacion = "add";
    $titulo = "Agregar Testimonio";
    $boton = "Agregar";
    $usa_form = true;
}
else
{
    if($id)
    {
        $testimonios = new Testimonios();
        $testimonios = $testimonios -> Obtener($_DB_, $id);

        if($op == 'con')
        {
            $operacion = "con";
            $titulo = "Consultar Testimonio";
            $boton = "";
            $usa_form = false;
        }
        elseif($op == 'mod')
        {
            $operacion = "mod";
            $titulo = "Modificar Testimonio";
            $boton = "Modificar";
            $usa_form = true;
        }
        elseif($op == 'del')
        {
            $operacion = "del";
            $titulo = "Eliminar Testimonio";
            $boton = "Eliminar";
            $usa_form = false;
        }
    }
}
?>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= $titulo;?></h1>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <form action="testimonios_operacion.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="op" value="<?= $operacion;?>">
                <?php
                if($id){ ?>
                    <input type="hidden" name="id" value="<?= $testimonios[0]['id'];?>">
                <?php } ?>

                    <table class="table table-bordered table-hover">
                    <tbody>
                     
                    <?php if($id){ ?>
                    <tr>
                        <td>Id</td>
                        <td>
                            <?= $testimonios[0]['id']; ?>
                        </td>
                    </tr>    
                    <?php } ?>
                    <tr>
                        <td>Foto</td>
                        <td>
                            <?php
                            if($usa_form)
                            {
                                if(isset($testimonios[0]['foto'])){ ?>
                                    <input type="hidden" name="nom_img" value="<?= $testimonios[0]['foto'];?>">
                                    <img src="../images/testimonios/thumbs/<?= $testimonios[0]['foto'];?>" onerror="this.src='../images/no_disponible.jpg';">
                                    <br><br>
                            <?php } ?>
                                <input type="file" size="32" name="foto" value=""/>
                            <?php }else{ ?>
                                <img src="../images/testimonios/thumbs/<?= $eventos[0]['foto'];?>" onerror="this.src='../images/no_disponible.jpg';">
                            <?php
                            } ?>
                            
                        </td>
                    </tr>
                        
                   
                    <tr>
                        <td>Testimonio</td>
                        <td>
                            <div class="col-md-12">
                            <?php
                            if($usa_form)
                            {?>
                                <input type="text" name="testi" id="testi" placeholder="Testimonio" class="form-control "  value="<?= $testimonios[0]['testimonio'];?>">
                            <?php
                            }
                            else
                            {   
                                echo $testimonios[0]['testimonio'];
                            }
                            ?>
                            </div>
                        </td>
                    </tr>
                        
                        
                    <tr>
                        <td>Nombre</td>
                        <td>
                            <div class="col-md-4">
                            <?php
                            if($usa_form)
                            {?>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="form-control "  value="<?= $testimonios[0]['nombre'];?>">
                            <?php
                            }
                            else
                            {   
                                echo $testimonios[0]['nombre'];
                            }
                            ?>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Descripción</td>
                        <td>
                            <div class="col-md-4">
                            <?php
                            if($usa_form)
                            {?>
                                <input type="text" name="descripcion" id="descripcion" placeholder="Descripcion" class="form-control "  value="<?= $testimonios[0]['descripcion'];?>">
                            <?php
                            }
                            else
                            {   
                                echo $testimonios[0]['descripcion'];
                            }
                            ?>
                            </div>
                        </td>
                    </tr>
                        
                    <tr>
                        <td>Publicado</td>
                        <td>
                            <?php
                            if($usa_form)
                            {?>
                                <div class="col-md-3">
                                <select name="visible" class="form-control">
                                    <option value="1" <?php if($testimonios[0]['publicado']){echo "selected";}?> >Si</option>
                                    <option value="0" <?php if(!$testimonios[0]['publicado']){echo "selected";}?> >No</option>
                                </select>
                                </div>
                            <?php
                            }
                            else
                            {   
                                if($testimonios[0]['publicado']) {echo "<i class='fa fa-check-circle icon-success fa-2x'></i>";}
                                else {echo "<i class='fa fa-times-circle icon-danger fa-2x'></i>";}
                            }
                            ?>
                           
                        </td>
                    </tr>
                    </table>
                        
                    
                    <div class="buttons clearfix">
                        <div class="pull-right">
                            <?php
                            if($boton)
                            {?><input type="submit" value="<?= $boton;?>" class="btn btn-primary"><?php } ?>
                            <a href="./testimonios_consultar.php" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                    
                </form>
                 
            </div>
        </div>
                
    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__))."/include/footer.php");