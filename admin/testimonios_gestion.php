<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
?>
<style type="text/css">

    .img-container {
        width: 100%;
        max-width: 100%;
    }

    .img-container img {
        width: 100%;
        max-width: 100%;
        display: block;
    }

    .preview {

        overflow: hidden;
        width: 260px;
        height: 260px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg{
        max-width: 1000px !important;
    }

    .estilo_div
    {
        border:solid 3px #ccc;
        text-align: center;
        box-shadow: 0px 20px 15px -15px #818181;
    }

</style>

<?php


$op = (isset($_GET['op']) && $_GET['op']) ? $_GET['op'] : 'add';
$id = (isset($_GET['id']) && $_GET['id']) ? strval($_GET['id']) : '';

$testiomonio_obj = new Testimonios();
$directorio = "img/testimonios/";

$titulo = "Agregar un Testimonio";
$boton = "Crear";
$usa_form = true;
$operacion = "add";

if ($id and $op == 'mod') {
    $testimonio = $testiomonio_obj->Obtener($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Testimonio";
    $boton = "Modificar";
    $usa_form = true;
} elseif ($id and $op == 'del') {
    $testimonio = $testiomonio_obj->Obtener($_DB_, $id);
    $operacion = "del";
    $titulo = "Eliminar Testimonio";
    $boton = "Eliminar";
    $usa_form = false;
}elseif ($id and $op == 'rev') {
    $testimonio = $testiomonio_obj->Obtener($_DB_, $id);

    $operacion = "rev";
    $titulo = "Revertir";
    $boton = "Revertir";
    $usa_form = false;
}

?>
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header"><? echo $titulo; ?></h1>
        </div>

        <div class="col-md-8 text-right">
        </div>

        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">

                    <form action="testimonios_operacion.php" method="post" enctype="multipart/form-data" name="frm_testimonio" id="frm_testimonio">
                        <input type="hidden" name="op" value="<?= $operacion; ?>">
                        <input type="hidden" name="id" value="<?= $id; ?>">
                        <?php
                        if (!empty($_SESSION['errors']['message'])){ ?>
                            <div class="alert alert-danger" role="alert">
                                <?= $_SESSION['errors']['message'];?>
                            </div>
                        <?php } ?>

                        <table class="table table-bordered" style="width: 50% !important;">
                            <tbody>
                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>
                                            <h4>Imagen destacada</h4>
                                            <p>Se mostrará como imagen principal</p>
                                            <?php
                                            $imagenInput = (isset($testimonio[0]["imagen"])) ? $testimonio[0]["imagen"] : '';
                                            $imagen = (isset($testimonio[0]["imagen"])) ? $testimonio[0]["imagen"] : 'no_disponible.jpg';?>
                                            <input type="file" name="image" class="image">
                                            <h4>Vista previa</h4><br>

                                            <div class="mt-2">
                                                <img src="<?= $directorio.$imagen;?>" id='image_principal' name ='image_principal' style="width: 386px; height: 274px;" class="estilo_div">
                                            </div>
                                            <br>
                                            <div class="alert alert-danger" id="info" role="alert" style="display: none">
                                                <label id="errors"></label>
                                            </div>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>

                                        <?php

                                        if ($usa_form) {
                                            if ($id) { ?>
                                                <textarea cols="5" rows="2" class="form-control" name="descripcion" placeholder="Descripción"><?=$testimonio[0]['descripcion']; ?></textarea><?php
                                           }else{ ?>
                                                <textarea cols="5" rows="2" class="form-control" name="descripcion" placeholder="Descripción"></textarea><?php
                                           }

                                         }else {
                                         echo $testimonio[0]['descripcion'];
                                      }
                                       ?>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>

                                            <?php

                                            if ($usa_form) {
                                                if ($id) { ?>
                                                    <input type="text" class="form-control" name="beneficiario" placeholder="Beneficiario" value="<?=$testimonio[0]['beneficiario']; ?>">
                                                    <?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="beneficiario" placeholder="Beneficiario" value="">
                                               <?php }

                                            }else {
                                                echo $testimonio[0]['descripcion'];
                                            }
                                            ?>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>

                                            <?php

                                            if ($usa_form) {
                                                if ($id) { ?>
                                                    <input type="text" class="form-control" name="opcional1" placeholder="Opcional 1" value="<?=$testimonio[0]['opcional1']; ?>">
                                                    <?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="opcional1" placeholder="Opcional 1" value="">
                                                <?php }

                                            }else {
                                                echo $testimonio[0]['opcional1'];
                                            }
                                            ?>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>

                                            <?php

                                            if ($usa_form) {
                                                if ($id) { ?>
                                                    <input type="text" class="form-control" name="opcional2" placeholder="Opcional 2" value="<?=$testimonio[0]['opcional2']; ?>">
                                                    <?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="opcional2" placeholder="Opcional 2" value="">
                                                <?php }

                                            }else {
                                                echo $testimonio[0]['opcional2'];
                                            }
                                            ?>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <td>

                                            <?php

                                            if ($usa_form) {
                                                if ($id) { ?>
                                                    <input type="text" class="form-control" name="opcional3" placeholder="Opcional 3" value="<?=$testimonio[0]['opcional3']; ?>">
                                                    <?php
                                                }else{ ?>
                                                    <input type="text" class="form-control" name="opcional3" placeholder="Opcional 3" value="">
                                                <?php }

                                            }else {
                                                echo $testimonio[0]['opcional3'];
                                            }
                                            ?>
                                        </td>
                                    </div>
                                </div>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                    $checked = (!empty($testimonio[0]['publicado']) == 1) ? 'checked' : '';
                                    ?>
                                    <div class="i-checks"><label> <input type="checkbox" value="1" name="publicado" <?=$checked;?>> <i></i> Publicar</label></div>

                                    <?php
                                    $checkValidado = (!empty($testimonio[0]['validado']) == 1) ? 'checked' : '';
                                    ?>
                                    <div class="i-checks"><label> <input type="checkbox" value="1" name="validado" <?=$checkValidado;?>> <i></i> Validar</label></div>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="./testimonios_consultar.php?del=0" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                        <input type="hidden" id="filename" name="filename">
                        <input type="hidden" name="image_destacada" class="image_destacada" value="<?=$imagenInput;?>">
                </form>

                </div>
            </div>

        </div>
        <!-- /.row -->
    </div>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Ajuste la imagen según su preferencia</h5>
                </div>

                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="cropper-container cropper-bg" touch-action="none" style="width: 825px; height: 497px;">
                                <div class="cropper-wrap-box">
                                    <div class="cropper-canvas" style="width: 825px; height: 464.063px; transform: translateY(16.4688px);">
                                        <img id="image" class="cropper-hide" style="width: 825px; height: 464.063px; transform: none;">
                                    </div>
                                </div>
                            </div>

                            <div class="docs-preview clearfix">
                                <div class="img-preview preview-lg preview" style="width: 256px; height: 144px;"></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Cancel</button>
                    <button class="ladda-button btn btn-primary ladda-button"  id="crop" data-style="expand-left">Recortar</button>
                </div>
            </div>
        </div>
    </div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
