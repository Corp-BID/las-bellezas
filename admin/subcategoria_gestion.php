<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o'])) ? strval($_GET['o']) : 'add';
$cat = (isset($_GET['c'])) ? $_GET['c'] : NULL;
$subcat = (isset($_GET['sc'])) ? $_GET['sc'] : NULL;

$categoria_obj = new Categorias();

$categoria = $categoria_obj->Obtener($_DB_, $cat);

if ($op == 'add') {
    $operacion = "add";
    $titulo = "Crear Subcategoria";
    $boton = "Crear";
    $usa_form = true;
} else {
    if ($subcat and $op == 'mod') {
        $subcategorias = $categoria_obj->Obtener($_DB_, $cat, $subcat);

        $operacion = "mod";
        $titulo = "Modificar Subcategoria";
        $boton = "Modificar";
        $usa_form = true;
    } elseif ($subcat and $op == 'del') {
        $subcategorias = $categoria_obj->Obtener($_DB_, $cat, $subcat);

        $operacion = "del";
        $titulo = "Eliminar subcategoria";
        $boton = "Eliminar";
        $usa_form = false;
    } else {
        $subcategorias = $categoria_obj->Obtener($_DB_, $cat, $subcat);

        $titulo = "Consultar Subcategoria";
        $boton = "";
        $usa_form = false;
    }
}

?>

<div class="row">
    <div class="col-md-4">
        <h1 class="page-header"><? echo $titulo; ?></h1>
    </div>

    <div class="col-md-8 text-right">
    </div>

    <div class="col-lg-12">

        <div class="ibox ">
            <div class="ibox-content">
                <form action="subcategoria_operacion.php" method="post">
                    <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                    <input type="hidden" name="c" value="<?php echo $cat; ?>">
                    <input type="hidden" name="sc" value="<?php echo $subcat; ?>">

                    <input type="hidden" name="cn" value="<?php echo $cat; ?>">

                    <?php if (isset($_SESSION['errors'])){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $_SESSION['errors']['message'];?>
                        </div>
                    <?php } ?>

                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td width="20%">Categoria</td>
                                    <td>
                                        <?php
                                            echo $categoria[0]['categoria'];
                                        ?>
                                    </td>
                              </tr>

                            <tr>
                                <td>Código-Subcategoria</td>
                                    <td>
                                        <?php
                                        if ($usa_form) {
                                            if ($subcat) { ?>
                                                <input type="text" class="form-control" name="scn"
                                                       value="<?php echo $subcategorias[0]['subcategoria']; ?>" readonly><?php
                                            }else{ ?>
                                                <input type="text" class="form-control" name="scn" value=""><?php
                                            }
                                        } else {
                                            echo $subcategorias[0]['subcategoria'];
                                        } ?>
                                    </td>
                            </tr>

                            <tr>
                                <td>Nombre</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($subcat) { ?>
                                            <input type="text" class="form-control" name="nombre"
                                                   value="<?php echo $subcategorias[0]['nombre']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="nombre" value=""><?php
                                        }
                                    } else {
                                        echo $subcategorias[0]['nombre'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($subcat) { ?>
                                            <input type="text" class="form-control" name="descripcion"
                                                   value="<?php echo $subcategorias[0]['descripcion']; ?>"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="descripcion" value=""><?php
                                        }
                                    } else {
                                        echo $subcategorias[0]['descripcion'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Orden</td>
                                <td>
                                    <?php
                                    if ($usa_form) {
                                        if ($subcat) { ?>
                                            <input type="text" class="form-control" name="orden"
                                                   value="<?php echo $subcategorias[0]['orden']; ?>" onKeyPress="return soloNumeros(event)"><?php
                                        }else{ ?>
                                            <input type="text" class="form-control" name="orden" value="" onKeyPress="return soloNumeros(event)"><?php
                                        }
                                    } else {
                                        echo $categoria[0]['orden'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Visible</td>
                                <td>
                                    <?php
                                    if ($usa_form) { ?>
                                        <select name="visible">
                                            <option value="1" <?php if (isset($subcategorias[0]['visible'])) {
                                                                    echo "selected";
                                                                } ?>>Si</option>
                                            <option value="0" <?php if ($subcat and !$subcategorias[0]['visible']) {
                                                                    echo "selected";
                                                                } ?>>No</option>
                                        </select>
                                    <?php
                                    } else {
                                        $visible = ($subcategorias[0]['visible'] == 1) ? '<i class="fa fa-eye " aria-hidden="true" style="color: #1ab394;"></i>' : '<i class="fa fa-eye-slash  " aria-hidden="true" style="color: darkred;"></i>';
                                        echo $visible;
                                    }
                                    ?>
                                </td>
                            </tr>
                    </table>

                    <div class="buttons clearfix">
                        <div class="pull-left">
                            <?php
                            if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary"><?php } ?>
                            <a href="productos_consultar2.php?c=<?php echo $cat; ?>" class="btn btn-default">Volver</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <!-- /.row -->
</div>

<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
