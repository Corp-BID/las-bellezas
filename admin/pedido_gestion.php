<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$op = (isset($_GET['o']) && $_GET['o']) ? strval($_GET['o']) : 'add';
$id = (isset($_GET['p']) && $_GET['p']) ? strval($_GET['p']) : '';

$pedido_obj = new Pedidos();

$condiciones = $pedido_obj->GetCondiciones($_DB_);

if ($id and $op == 'mod') {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);
    $operacion = "mod";
    $titulo = "Modificar Pedido";
    $boton = "Modificar";
    $usa_form = true;
} elseif ($id and $op == 'del') {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);

    $operacion = "del";
    $titulo = "Eliminar Pedido";
    $boton = "Eliminar";
    $usa_form = false;
} else {
    $pedido = $pedido_obj->ConsultarPedido($_DB_, $id);
    $titulo = "Consultar Pedido";
    $boton = "";
    $usa_form = false;
}

$id_pago = ($pedido['data'][0]['id_pago'] != '') ? $pedido['data'][0]['id_pago'] : 0 ;

$pago_obj = new Pago();

$pago =[];
$validado = 0;
if ($id_pago > 0) {
    $pago = $pago_obj->ConsultarPagoId($_DB_, $id_pago);
    $validado =  $pago[0]['validado'];
}

?>
<style>
    .vertical
    { border-left: 1px solid black; }
</style>

<div class="row">
    <div class="col-md-4">
        <h1 class="page-header">Gestión de Pedido y Pagos</h1>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-6">
                <div class="ibox ">
                    <div class="ibox-content">
                        <h3>Pedidos</h3>
                        <form name="frmPedido" id="frmPedido" action="pedido_operacion.php" method="post">
                            <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                            <?php if (!$usa_form){ ?>
                                <input type="hidden" name="m" value="<?php echo $id; ?>">
                            <?php } ?>

                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Código</td>
                                    <td>
                                        <?php
                                        if ($usa_form && $op != 'add') {
                                            if ($id) { ?>
                                                <input type="text" class="form-control" name="m" value="<?php echo $pedido['data'][0]['id_pedido']; ?>" readonly><?php
                                            }
                                        } else {
                                            echo $pedido['data'][0]['id_pedido'];
                                        } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cliente</td>
                                    <td>
                                        <?php
                                        if ($usa_form) {
                                            if ($id) { ?>
                                                <input type="text" class="form-control" name="cliente"
                                                       value="<?php echo $pedido['data'][0]['cliente']; ?>" readonly><?php
                                            }else{ ?>
                                                <input type="text" class="form-control" name="cliente" value=""><?php
                                            }

                                        } else {
                                            echo $pedido['data'][0]['cliente'];
                                        }?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Fecha del Pedido</td>
                                    <td>
                                        <?php
                                        $formato = 'Y-m-d H:i:s';
                                        $fecha = DateTime::createFromFormat($formato, $pedido['data'][0]['fecha']);
                                        if ($usa_form) {
                                            if ($id) {
                                                ?>
                                                <input type="text" class="form-control" name="fecha"
                                                       value="<?=$fecha->format('d/m/Y h:i:s'); ?>" readonly><?php
                                            }else{ ?>
                                                <input type="text" class="form-control" name="fecha" value="">
                                                <?php
                                            }
                                        } else {
                                            echo $fecha->format('d/m/Y h:i:s');
                                        }?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Tasa del Pedido</td>
                                    <td><code><?=number_format($pedido['data'][0]['valor_calculo'], 2, ",", ".");?></code></td>
                                </tr>

                                <tr>
                                    <td>Monto $</td>
                                    <td>
                                        <?php
                                        if ($usa_form) {
                                            if ($id) { ?>
                                                <input type="text" class="form-control" name="monto"
                                                       value="<?php echo $pedido['data'][0]['monto_total']; ?>" readonly><?php
                                            }else{ ?>
                                                <input type="text" class="form-control" name="monto" value=""><?php
                                            }

                                        } else {
                                            echo number_format($pedido['data'][0]['monto_total'], 2, ",", ".");
                                        }?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Monto Bs</td>
                                    <td>
                                        <?php
                                        $tasacompra = ($pedido['data'][0]['valor_calculo'] > 0 ) ?
                                            $pedido['data'][0]['valor_calculo'] : 1;
                                        $valorBs = number_format($pedido['data'][0]['monto_total'] * $tasacompra, 2, ",", ".") ;
                                        if ($usa_form) {
                                            if ($id) { ?>
                                                <input type="text" class="form-control" name="monto_bs"
                                                       value="<?php echo $valorBs; ?>" readonly><?php
                                            }else{ ?>
                                                <input type="text" class="form-control" name="monto_bs" value=""><?php
                                            }

                                        } else {
                                            echo $valorBs;
                                        }?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Estatus</td>
                                    <td>
                                        <?php
                                        if ($usa_form && $validado) { ?>
                                            <select name="id_condicion">
                                                <option value="">:: Seleccione ::</option>
                                                <?php foreach ($condiciones as $condicion) {
                                                    ?>
                                                    <option value="<?=$condicion['id'];?>" <?php if ( $condicion['id'] <= $pedido['data'][0]['id_condicion'] ) { ?> disabled <?php } ?> class="form-control" <?php
                                                    if ($condicion['id'] == $pedido['data'][0]['id_condicion']) { echo "selected";} ?>>
                                                        <?=$condicion['condicion'];?>
                                                    </option>
                                                <?php }
                                                ?>  </select>
                                        <?php } else {
                                            echo "<span class='label label-info'>Por validar pago</span>";
                                        }

                                        if (isset($_SESSION['error']))
                                        { ?>

                                        <div class="alert alert-danger mt-3" role="alert">
                                           <?=$_SESSION['error'];?>
                                        </div>
                                         <?php } ?>
                                    </td>
                                    <tr>
                                        <td><code>Observación del Pedido</code></td>
                                        <td><?=$pedido['data'][0]['observacion'];?></td>
                                    </tr>
                                </tr>
                                </tbody>
                            </table>
                            <div class="buttons clearfix">
                                <div class="pull-left">
                                    <?php
                                    $disabled = 'disabled';
                                    if (isset($pago[0]['validado'])) {
                                        $disabled = ( ! $pago[0]['validado'] || $pedido['data'][0]['id_condicion'] == 6 ) ? 'disabled' : '';
                                    }
                                    if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>" class="btn btn-primary" <?=$disabled;?>><?php } ?>
                                    <a href="./pedidos_consultar.php" class="btn btn-default">Volver</a>
                                </div>
                            </div>
                            <input type="hidden" name="id_condicion_anterior" id="id_condicion_anterior" value="<?=$pedido['data'][0]['id_condicion'];?>">
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="ibox ">
                    <div class="ibox-content">
                        <h3>Pagos</h3>
                        <?php if ($id_pago > 0 ){ ?>
                            <form action="pago_operacion.php" method="post">
                                <input type="hidden" name="op" value="<?php echo $operacion; ?>">
                                <input type="hidden" name="p" value="<?php echo $id_pago; ?>">
                                <input type="hidden" name="pedido" value="<?php echo $id; ?>">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Código</td>
                                        <td>
                                            <?php
                                            if ($usa_form && $op != 'add') {
                                                echo $pago[0]['id'];
                                            } ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Forma de Pago</td>
                                        <td>
                                            <?php
                                            if ($usa_form && $op != 'add') {
                                                echo $pago[0]['tipo_pago'];
                                            } ?>
                                        </td>
                                    </tr>

                                    <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                        <tr>
                                            <td>Tipo de Transferencia</td>
                                            <td>
                                                <?php
                                                if ($usa_form && $op != 'add') {
                                                    echo $pago[0]['tipo_transferencia'];
                                                } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                        <tr>
                                            <td>Banco</td>
                                            <td>
                                                <?php
                                                if ($usa_form && $op != 'add') {
                                                    echo $pago[0]['banco'];
                                                } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    <?php if ($pago[0]['tipo_pago'] == 'transferencia'){ ?>
                                        <tr>
                                            <td>Referencia</td>
                                            <td>
                                                <?php
                                                if ($usa_form) {
                                                    echo $pago[0]['referencia'];
                                                }?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    <tr>
                                        <?php
                                        $tasa = '';
                                        if (strtolower ($pago[0]['simbolos']) =='bs')
                                        {
                                            $tasaCalculo = ($pago[0]['monto_pago'] / $pago[0]['monto_total']);
                                            $tasa = 'A tasa: '.number_format($tasaCalculo, 0, ",", ".");?>
                                        <?php }
                                        ?>
                                        <td width="15%">Monto Pagado <code><?=$tasa;?></code></td>
                                        <td>
                                            <?php echo $pago[0]['simbolos']." ".number_format($pago[0]['monto_pago'], 2, ",", "."); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Fecha</td>
                                        <td>
                                            <?php
                                            $objeto_DateTime = DateTime::createFromFormat('Y-m-d', $pago[0]['fecha']);
                                            $cadena_nuevo_formato = date_format($objeto_DateTime, "d/m/Y");
                                            echo $cadena_nuevo_formato;?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Estatus</td>
                                        <td>
                                            <?php
                                            $checked = ($pago[0]['validado']) ? 'checked' : '';
                                            $disabledPago = ($pago[0]['validado']) ? 'disabled' : '';
                                            ?>
                                            <div class="i-checks"><label> <input type="checkbox" value="1" name="validado" <?=$checked;?>> <i></i> Validado</label></div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><code>Observacion del pago</code></td>
                                        <td><?=$pago[0]['observacion'];?></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="buttons clearfix">
                                    <div class="pull-left">
                                        <?php
                                        if ($boton) { ?><input type="submit" value="<?php echo $boton; ?>"
                                                               class="btn btn-primary" <?=$disabledPago;?>><?php } ?>
                                    </div>
                                </div>
                            </form>
                        <?php }else {
                        ?><h3 class="alert alert-danger">No hay pago relacionado con este pedido</h3>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <table class="table table-bordered">
                    <thead>
                    <th class="text-center">ID</th>
                    <th class="text-center">Código</th>
                    <th class="text-center">Producto</th>
                    <th class="text-center">Cantidad</th>
                    <th class="text-center">Precio Unitario</th>
                    <th class="text-center">Total</th>
                    </thead>
                    <tbody>
                    <?php

                    if (isset($pedido['dataItems'])) {
                        $total = 0;
                        foreach ($pedido['dataItems'] as $item) { ?>
                            <tr>
                                <td><?= $item['id_item']; ?></td>
                                <td><?= $item['codigo_producto']; ?></td>
                                <td><?= $item['nombre']; ?></td>
                                <td align="right"><?= $item['cantidad']; ?></td>
                                <td align="right"><?= number_format($item['precio_unitario'], 2, ",", "."); ?></td>
                                <td align="right"><?= number_format($item['precio_total'], 2, ",", ".") ; ?></td>
                            </tr>
                            <?php
                            $total = $total + $item['precio_total'];
                        } ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-right"><b>Total</b></td>
                            <td align="right"><?= number_format($total, 2, ",", ".") ; ?></td>
                        </tr>

                    <?php } else { ?>
                        <tr>
                            <td class="text-center">No hay producto asociado al Pedido</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
if ($pedido['data'][0]['metodo_entrega'] != 'retirar'){ ?>
<div class="row">
    <div class="col-md-6 vertical">
        <div class="form-group">
            <h2><b>Dirección de entrega</b></h2>
                <p><b>Dirección Principal :</b></p><label><?=$pedido['data'][0]['direccion'];?></label>
            <hr>
                <p><b>Dirección Secundaria :</b></p><label><?=$pedido['data'][0]['direccion2'];?></label>
            <hr>
                <p><b>Observación :</b></p> <label><?=$pedido['data'][0]['observaciones'];?></label>
        </div>
    </div>
</div>
<?php } else { ?>
    <h3 class="alert alert-success">Retirar en tienda</h3>
<?php }

include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>



$("#frmPedido").validate({
    normalizer: function( value ) {
        return $.trim( value );
    },
    rules: {
        id_condicion: {
            required: true
        }
    },
    messages: {
        email: "Dato requerido, Verifique!"
    }
})
</script>