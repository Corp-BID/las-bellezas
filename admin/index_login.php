<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");
?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo NOMBRE_SITE;?> | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <img src="./img/logo.png" alt="">
                <br><br>
            </div>

            <h3>Bienvenidos</h3>
            <p>Coloque sus credenciales para el ingreso al Sistema de Administración.</p>
            <p>Ingreso</p>
            <form class="m-t" role="form" action="login.php" method="POST">
                <div class="form-group">
                    <input type="text" name="login" class="form-control" placeholder="Usuario" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Contraseña" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <!--
                <a href="#"><small>Olvido su Clave?</small></a>

                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
                -->
            </form>
            <p class="m-t"> <small>Charcuteria Expess &copy; 2020</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>

</body>
</html>