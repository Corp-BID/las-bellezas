<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 5) :NULL; 

$cat = (isset($_POST['c']) && $_POST['c'])?substr(strval($_POST['c']), 0, 50) :''; 
$subcat = (isset($_POST['sc']) && $_POST['sc'])?substr(strval($_POST['sc']), 0, 50) :''; 

$cat_new = (isset($_POST['cn']) && $_POST['cn'])?substr(strval($_POST['cn']), 0, 50) :''; 
$subcat_new = (isset($_POST['scn']) && $_POST['scn'])?substr(strval($_POST['scn']), 0, 50) :''; 

$nombre= (isset($_POST['nombre']) && $_POST['nombre'])?substr(strval(mb_strtoupper ($_POST['nombre'])), 0, 200) : NULL; 
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion'])?substr(strval(mb_strtoupper ($_POST['descripcion'])), 0, 800) :NULL; 
$orden = (isset($_POST['orden']) && $_POST['orden'])?intval($_POST['orden']):0; 
$visible = (isset($_POST['visible']) && $_POST['visible'])?intval($_POST['visible']):0; 

$categoria_obj = new Categorias();


if($operacion =="add")
{
    $categoria = $categoria_obj->Obtener($_DB_, $cat, $subcat);
    $errors = [];
    if (count($categoria) > 0) {
        $errors = [
            'message' => 'Categoria ya existe!, verifique!'
        ];
        $_SESSION['errors'] = $errors;
        header("Location: ./categoria_gestion.php");
        die;
    }else
        $categoria_obj->Agregar($_DB_, $cat, $subcat, $nombre, $descripcion, $orden, $visible);
        unset($_SESSION["errors"]);
}
elseif($operacion =="mod" and $cat_new and $cat)
{
    $categoria_obj->Modificar($_DB_, $cat, $subcat, $cat_new, $subcat_new, $nombre, $descripcion, $orden, $visible);
}
elseif($operacion =="del")
{
    $categoria_obj->Eliminar($_DB_, $cat, $subcat);
}

header("Location: ./productos_consultar.php");