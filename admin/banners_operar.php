<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 10) :NULL;
$id = (isset($_POST['id']) && $_POST['id'])?intval($_POST['id']) :NULL;
$activo = (isset($_POST['action']) && $_POST['action'])?intval($_POST['action']) :0;

if($operacion == "cmb_st")
{
    if($id)
    {
        $slides = new Slides();
        $slides -> CambiarEstatus($_DB_, $id, $activo);
    }
}

header("Location: ./banners_consultar.php");