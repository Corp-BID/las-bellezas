<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op'])?substr(strval($_POST['op']), 0, 5) :NULL;
$id = (isset($_POST['m']) && $_POST['m'])?substr(strval($_POST['m']), 0, 50) :'';
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion'])?substr(strval($_POST['descripcion']), 0, 800) :NULL;

$simbolo = (isset($_POST['simbolo']) && $_POST['simbolo'])? $_POST['simbolo']:null;


//$por_defecto = (isset($_POST['por_defecto']) && $_POST['por_defecto'])?intval($_POST['por_defecto']):0;
$condicion = (isset($_POST['condicion']) && $_POST['condicion'])?intval($_POST['condicion']):0;

$moneda_obj = new Monedas();

if($operacion =="add")
{
    $moneda = $moneda_obj->Obtener($_DB_, $id);
    $errors = [];
    if (count($marca) > 0) {
        $errors = [
            'message' => 'Moneda ya existe!, verifique!'
        ];
        $_SESSION['errors'] = $errors;
        header("Location: ./moneda_gestion.php");
        die;
    }else
        $moneda_obj->Agregar($_DB_, $descripcion, $simbolo, $condicion, 0);
        unset($_SESSION["errors"]);
}
elseif($operacion =="mod" && $id)
{
    $moneda_obj->Modificar($_DB_, $id, $descripcion, $simbolo, $condicion);
}
elseif($operacion =="del")
{
    $moneda_obj->Eliminar($_DB_, $id);
}

header("Location: ./monedas_consultar.php");