<?php
include_once(realpath(dirname(__FILE__)) . "/include/conexion.php");

$operacion = (isset($_POST['op']) && $_POST['op']) ? substr(strval($_POST['op']), 0, 5) : NULL;

$cat = (isset($_POST['c']) && $_POST['c']) ? substr(strval($_POST['c']), 0, 50) : '';
$subcat = (isset($_POST['sc']) && $_POST['sc']) ? substr(strval($_POST['sc']), 0, 50) : '';

$cat_new = (isset($_POST['cn']) && $_POST['cn']) ? substr(strval($_POST['cn']), 0, 50) : '';
$subcat_new = (isset($_POST['scn']) && $_POST['scn']) ? substr(strval($_POST['scn']), 0, 50) : '';

$nombre = (isset($_POST['nombre']) && $_POST['nombre']) ? substr(strval(mb_strtoupper($_POST['nombre'])), 0, 200) : NULL;
$descripcion = (isset($_POST['descripcion']) && $_POST['descripcion']) ? substr(strval(mb_strtoupper($_POST['descripcion'])), 0, 800) : NULL;
$orden = (isset($_POST['orden']) && $_POST['orden']) ? intval($_POST['orden']) : 0;
$visible = (isset($_POST['visible']) && $_POST['visible']) ? intval($_POST['visible']) : 0;

/*
echo "<pre>";
print_r($_POST);
echo "</pre>";
die;
*/

$categoria_obj = new Categorias();

if ($cat) 
{
    if ($operacion == "add") {
        $subcategorias = $categoria_obj->Obtener($_DB_, $cat, $subcat_new);
        $errors = [];
        if (count($subcategorias) > 0) {
            $errors = [
                'message' => 'SubCategoria ya existe!, verifique!'
            ];
            $_SESSION['errors'] = $errors;
            header("Location: ./subcategoria_gestion.php?o=add&c=$cat");
            die;
        }else
            $categoria_obj->Agregar($_DB_, $cat, $subcat_new, $nombre, $descripcion, $orden, $visible);
            unset($_SESSION["errors"]);
    } elseif ($operacion == "mod" and $cat_new and $cat) {
        $categoria_obj->Modificar($_DB_, $cat, $subcat, $cat_new, $subcat_new, $nombre, $descripcion, $orden, $visible);
    } elseif ($operacion == "del") {
        $categoria_obj->Eliminar($_DB_, $cat, $subcat);
    }


    header("Location: ./productos_consultar2.php?c=$cat&sc=$subcat");
    die;
}
header("Location: ./productos_consultar.php");