<?php
include_once(realpath(dirname(__FILE__))."/include/conexion.php");

$login = (isset($_POST['login']) && $_POST['login'])?substr(strval($_POST['login']), 0, 100) :NULL;
$password = (isset($_POST['password']) && $_POST['password'])?substr(strval($_POST['password']), 0, 100) :NULL;

if(trim($login) and trim($password))
{
	$usuario_obj = new Usuario();
	$usuario = $usuario_obj -> ValidarUsuario($_DB_, $login, $password);

	if($usuario[login] and $usuario['activo'])
	{
        $_SESSION['usuario'] = $usuario;        
		header("Location: ./index.php");
	}
	else
	{header("Location: ./index_login.php?p=1");}
}
else
{
	header("Location: ./index_login.php?p=1");
}