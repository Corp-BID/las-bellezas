<?php

include_once(realpath(dirname(__FILE__)) . "/include/header.php");
//include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");

$cat = (isset($_GET['c']) && $_GET['c']) ? strval($_GET['c']) : '';
$subcat = (isset($_GET['sc']) && $_GET['sc']) ? strval($_GET['sc']) : '0';
$cod_prod = (isset($_GET['p']) && $_GET['p']) ? strval($_GET['p']) : '';

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$producto = $productos_obj->Consultar($_DB_, $cod_prod);

if ($cat != '') {
    $categoria = $categoria_obj->Obtener($_DB_, $cat);
}

if ($subcat != '') {
    $subcategoria = $categoria_obj->Obtener($_DB_, $cat, $subcat);
}

//$categorias = $categoria_obj->listado($_DB_, $cat, $subcat);
?>

<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb2.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Producto</h2>
            <ul>
            <li><a href="./index.php">Inicio</a></li>

                    <li><a href="./listado.php">Categorías</a></li>

                    <li><a href="./listado.php?c=<?php echo $categoria[0]['categoria']; ?>"><?php echo $categoria[0]['nombre']; ?></a></li>

                    <li><a href="./listado.php?c=<?php echo $subcategoria[0]['categoria']; ?>&sc=<?php echo $subcategoria[0]['subcategoria']; ?>"><?php echo $subcategoria[0]['nombre']; ?></a></li>
            </ul>
        </div>
    </div>
</div>

<!-- contact-area start -->
<div class="contact-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-details-btn">
                    <a class="btn btn-outline-danger" href="./listado.php?c=<?php echo $cat; ?>&sc=<?php echo $subcat; ?>">Volver</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-7 col-12">
                <div class="product-details-img-content">
                    <div class="product-details-tab mr-70">
                        <div class="product-details-large tab-content">
                            <div class="tab-pane active show fade" id="pro-details1" role="tabpanel">
                                    <a href="./Productos/thumbs/<?php echo $producto[0]['foto1'] ?>">
                                    <img src="./Productos/thumbs/<?php echo $producto[0]['foto1'] ?>" onerror="this.src='./Productos/no_disponible.jpg';" alt="" width="90%">
                                    </a>
                            </div>

                            <?php
                            if ($producto[0]['foto2']) { ?>

                                <div class="tab-pane fade" id="pro-details2" role="tabpanel">
                                    <img src="./Productos/thumbs/<?php echo $producto[0]['foto2']; ?>" alt="">
                                </div>
                            <?php
                            } ?>

                            <?php
                            if ($producto[0]['foto3'] != 'NULL') { ?>
                                <div class="tab-pane fade" id="pro-details3" role="tabpanel">
                                    <img src="./Productos/thumbs/<?php echo $producto[0]['foto3']; ?>" alt="">
                                </div>
                            <?php
                            } ?>

                            <?php
                            if ($producto[0]['foto4'] != NULL and $producto[0]['foto4'] != '') { ?>
                                <div class="tab-pane fade" id="pro-details4" role="tabpanel">
                                    <img src="./Productos/thumbs/<?php echo $producto[0]['foto4']; ?>" alt="">
                                </div>
                            <?php
                            } ?>

                        </div>


                        <?php
                        if (($producto[0]['foto2'] != 'NULL') and ($producto[0]['foto2'] != '')) { ?>


                            <div class="product-details-small nav mt-12 main-product-details" role=tablist>

                                <?php
                                if ($producto[0]['foto1']) { ?>
                                    <a class="active mr-12" href="#pro-details1" data-toggle="tab" role="tab" aria-selected="true">
                                        <img src="./Productos/thumbs/<?php echo $producto[0]['foto1']; ?>" alt="" >
                                    </a>
                                <?php
                                } ?>

                                <?php
                                if (($producto[0]['foto2'] != 'NULL') and ($producto[0]['foto2'] != '')) { ?>
                                    <a class="mr-12" href="#pro-details2" data-toggle="tab" role="tab" aria-selected="true">
                                        <img src="./Productos/thumbs/<?php echo $producto[0]['foto2']; ?>" alt="">
                                    </a>
                                <?php
                                } ?>

                                <?php
                                if (($producto[0]['foto3'] != 'NULL') and ($producto[0]['foto3'] != '')) { ?>
                                    <a class="mr-12" href="#pro-details3" data-toggle="tab" role="tab" aria-selected="true">
                                        <img src="./Productos/thumbs/<?php echo $producto[0]['foto3']; ?>" alt="">
                                    </a>
                                <?php
                                } ?>

                                <?php
                                if (($producto[0]['foto4'] != 'NULL') and ($producto[0]['foto4'] != '')) { ?>
                                    <a class="mr-12" href="#pro-details4" data-toggle="tab" role="tab" aria-selected="true">
                                        <img src="./Productos/thumbs/<?php echo $producto[0]['foto4']; ?>" alt="">
                                    </a>
                                <?php
                                } ?>
                            </div>

                        <?php
                        } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-5 col-12">
                <div class="product-details-content">
                    <h3><?php echo $producto[0]['nombre'];
                        if ($producto[0]['nuevo']) {
                            echo " - Nuevo!";
                        } elseif ($producto[0]['oferta']) {
                            echo " - Oferta!";
                        } ?></h3>

                    
                    <p>
                        <?php echo $producto[0]['presentacion']; ?>

                        <?
                        if($producto[0]['marca'])
                        {?>
                            <br><?php echo $producto[0]['descripcion_marca']; ?>
                        <?
                        }?>
                        

                    </br>

                    <div class="details-price">
                        <span><?= $simbolo." ".  number_format($producto[0]['precio'] * $ValorCalculo, 2, ",", "."); ?></span>
                    </div>


                    <form action="./carrito_o.php" method="POST">
                    <input type="hidden" name="c" value="<?php echo $cat; ?>">
                    <input type="hidden" name="sc" value="<?php echo $subcat; ?>">
                    <input type="hidden" name="p" value="<?php echo $cod_prod; ?>">
                    <input type="hidden" name="op" value="add">

                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus">
                                <input type="text" value="<?php echo $producto[0]['cant_min'] ?>" name="cant_prod" class="cart-plus-minus-box">
                            </div>
                            <div class="quickview-btn-cart">
                                <input type="submit" class="btn-hover-black boton" value="Agregar al Carrito" style="cursor:pointer;">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
$productos_relacionados = array();
$productos_relacionados = $productos_obj->ProductosRelacionados($_DB_, $producto[0]['nombre']);

if (isset($productos_relacionados) > 0 ){

?>
<div class="product-collection-area pb-60">
    <div class="container">
        <div class="section-title text-center mb-55">
            <h2>Productos Relacionados</h2>
        </div>
        <div class="row">
            <div class="">
                <?php
                foreach ($productos_relacionados as $relacionados ) {
                    ?>
                    <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                        <div class="single-product mb-35">
                            <div class="product-img">
                                <a href="./producto_detallado.php?c=<?php echo $relacionados['cod_categoria']; ?>&sc=<?php echo $relacionados['cod_subcategoria']; ?>&p=<?php echo $relacionados['cod_producto']; ?>">
                                    <img src="./Productos/thumbs/<?php echo $relacionados['foto1']; ?>" alt="">
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="product-title-price">
                                    <div class="product-title">
                                        <h4><a href="./producto_detallado.php?c=<?php echo $relacionados['cod_categoria']; ?>&sc=<?php echo $relacionados['cod_subcategoria']; ?>&p=<?php echo $relacionados['cod_producto']; ?>"><?php echo $relacionados['nombre']; ?>1</a></h4>
                                    </div>

                                </div>
                            </div>
                            <div class="product-price">
                                <span><?= $simbolo." ".  number_format($relacionados['precio'] * $ValorCalculo, 2, ",", "."); ?></span>
                            </div>
                        </div>
                    </div>

                <?php
            } ?>
            </div>
        </div>
    </div>
</div>
 <?php }
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");

