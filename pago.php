<?php
include_once(realpath(dirname(__FILE__)) . "/include/header.php");
include_once(realpath(dirname(__FILE__)) . "/config/Config_Tasa_Moneda.php");
?>
<style>
    .error {
        color: red !important;
    }

    .esquinas_redondeadas1 {
        border: 1px solid darkgreen;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        height: 22%;
    }

    .esquinas_redondeadas2 {
        border: 1px solid darkred;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        padding: 10px;
        margin-left: 18%;
    }
</style>
<?php
$operacion = (isset($_POST['op']) && $_POST['op']) ? $_POST['op'] : $_GET['op'];
$ve = (isset($_GET['ve']) && $_GET['ve']) ? $_GET['ve'] : "";

$cliente = new Clientes();
$datos_facturacion = $cliente->obtenerDatosCliente($_DB_, $_SESSION['cliente']['correo']);

$DatosMoneda = $_SESSION['DatosMonedaDefecto'];
$DatosMonedaDestino = $_SESSION['DatosMonedaDestino'];
$ValorCalculo = $_SESSION['ValorCalculo'];

$moneda = (!empty($DatosMonedaDestino[0]['descripcion'])) ? $DatosMonedaDestino[0]['descripcion'] : $DatosMoneda[0]['descripcion'];
$simbolo = (!empty($DatosMonedaDestino[0]['simbolos'])) ? $DatosMonedaDestino[0]['simbolos'] : $DatosMoneda[0]['simbolos'];

$cedula = '';
$nombre = '';
$correo = '';
$direccion = '';
$direccion2 = '';
$telefonos = '';

if (count($datos_facturacion) > 0) {
    $cedula = $datos_facturacion[0]['cedula'];
    $nombre = $datos_facturacion[0]['nombre'];
    $correo = $datos_facturacion[0]['correo'];
    $direccion = $datos_facturacion[0]['direccion'];
    $direccion2 = $datos_facturacion[0]['direccion2'];
    $telefonos = $datos_facturacion[0]['telefonos'];
}
?>

<div class="breadcrumb-area pt-50 pb-50 bg-img" style="background-image: url(assets/img/bg/breadcrumb.png)">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>checkout</h2>
            <ul>
                <li><a href="./index.php">Inicio</a></li>
                <li> Reportar Pago </li>
            </ul>
        </div>
    </div>
</div>
<!-- checkout-area start -->
<div class="checkout-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="coupon-accordion">
                    <?php
                    if (!$_SESSION['cliente']['correo']) { ?>
                        <!-- ACCORDION START -->
                        <h3>Eres Cliente? <span id="showlogin">Haz Clic aquí para Iniciar Sesión</span></h3>
                        <div id="checkout-login" class="coupon-content">
                            <div class="coupon-info">
                                <form action="./login.php">
                                    <input type="hidden" name="pag" value="pago">
                                    <p class="form-row-first">
                                        <label>Correo Electrónico <span class="required">*</span></label>
                                        <input type="text" name="user-name" placeholder="Correo Electrónico">
                                    </p>
                                    <p class="form-row-last">
                                        <label>Clave <span class="required">*</span></label>
                                        <input type="password" name="user-password" placeholder="Clave">
                                    </p>
                                    <p class="form-row">
                                        <input type="submit" value="Ingresar" />
                                    </p>
                                    <p class="lost-password">
                                        <a href="#">¿Olvido su Contraseña?</a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    <?php
                    } ?>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-6 col-md-12">
                <h3>Pagos en Divisas</h3>
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                    Zelle
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                jla_dasilva294@hotmail.com<br> A nombre de José Luis da Silva
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                    Efectivo
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <h3>Pagos en Bolívares</h3>
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="heading11">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne11" aria-expanded="true" aria-controls="collapseOne11">
                                    Transferencia Banesco
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne11" class="collapse show" aria-labelledby="heading11" data-parent="#accordionExample">
                            <div class="card-body">
                                Cuenta: <b>01341010150001005306</b><br>
                                A nombre de <b>Corporación Express 2017, CA</b><br>
                                Rif: <b>J409770087</b>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading12">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne12" aria-expanded="false" aria-controls="collapseOne12">
                                    Transferencia Banplus
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne12" class="collapse" aria-labelledby="heading12" data-parent="#accordionExample">
                            <div class="card-body">
                                Cuenta: <b>01740145991454116268</b><br>
                                A nombre de <b>Corporación Express 2017, CA</b><br>
                                Rif: <b>J409770087</b>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading13">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne13" aria-expanded="false" aria-controls="collapseOne13">
                                    Transferencia BNC
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne13" class="collapse" aria-labelledby="heading13" data-parent="#accordionExample">
                            <div class="card-body">
                                Cuenta: <b>01910001472101135206</b><br>
                                A nombre de <b>Corporación Express 2017, CA</b><br>
                                Rif: <b>J409770087</b>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading14">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne14" aria-expanded="false" aria-controls="collapseOne14">
                                    Pago Movil Banesco
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne14" class="collapse" aria-labelledby="heading14" data-parent="#accordionExample">
                            <div class="card-body">
                                Teléfono: <b>04141096283</b><br>
                                Rif: <b>J409770087</>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading15">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne15" aria-expanded="false" aria-controls="collapseOne15">
                                    Punto de Venta
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne15" class="collapse" aria-labelledby="heading15" data-parent="#accordionExample">
                            <div class="card-body">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <form action="./pago_o.php" method="POST" name="frm_pago" id="frm_pago">
                    <div class="checkbox-form">
                        <h3>Detalles de la Factura</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Nombre o Razón Social <span>*</span></label>
                                    <input type="text" placeholder="" name="nombre" value="<?php echo $nombre; ?>" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Cédula o Rif <span>*</span></label>
                                    <input type="text" placeholder="" name="cedula" value="<?php echo $cedula; ?>" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Telefono</label>
                                    <input type="text" placeholder="" name="telefonos" value="<?php echo $telefonos; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="col-lg-6 col-md-12 col-12">
                <div class="your-order">
                    <h3>Tu Orden</h3>
                    <div class="your-order-table table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-name">Productos</th>
                                    <th class="product-total">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($productos_carrito)) {
                                    $total_order = 0;
                                    foreach ($productos_carrito as $clave => $prod) { ?>
                                        <tr class="cart_item">
                                            <td class="product-name">
                                                <?php echo $prod['nombre'] ?> <strong class="product-quantity">x<?php echo $prod['cantidad']; ?></strong>
                                            </td>
                                            <td class="product-total">
                                                <span class="amount"><?= $simbolo . " " . number_format($prod['total'] * $ValorCalculo, 2, ",", "."); ?></span>
                                            </td>
                                        </tr>
                                    <?php
                                        $total_order = $total_order + $prod['total'];
                                    }
                                } else { ?>
                                    <h1>Carrito Vacio</h1>
                                <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr class="order-total">
                                    <th>Total Orden</th>
                                    <td><strong><span class="amount"><?= $simbolo . " " . number_format($total_order * $ValorCalculo, 2, ",", "."); ?></span></strong>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="order-button-payment">
                            <input type="hidden" name="op" value="<?= $operacion; ?>">
                            <input type="hidden" name="ve" value="<?= $ve; ?>">
                            <input type="submit" value="Guardar y Reporta el Pago" />
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once(realpath(dirname(__FILE__)) . "/include/footer.php");
?>
<script>
    $("#frm_pago").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            nombre: {
                required: true,
                minlength: 3,
                maxlength: 60
            },
            cedula: {
                required: true
            },
            telefonos: {
                required: true
            }
        }
    })
</script>