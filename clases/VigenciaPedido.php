<?php
class VigenciaPedido
{

    function Agregar($sql, $horas, $por_defecto)
    {
        if ($por_defecto > 0 ) {
            $query = "UPDATE vigencia_pedido SET por_defecto = 0";
            $sql->ExecQuery($query);
        }

        $query = "INSERT INTO vigencia_pedido (numero_horas, por_defecto, eliminado) VALUES ('$horas', '$por_defecto',0)";
        $sql->ExecQuery($query);
    }

    function Modificar($sql, $id, $horas, $por_defecto)
    {
        if ($por_defecto > 0 ) {
            $query = "UPDATE vigencia_pedido SET por_defecto = 0";
            $sql->ExecQuery($query);
        }

        $query="UPDATE vigencia_pedido SET numero_horas = '$horas', por_defecto = '$por_defecto' where id = ".$id;
        $sql->ExecQuery($query);
    }

    function Eliminar($sql, $id)
    {
        $query = "UPDATE vigencia_pedido SET eliminado=1 WHERE id=".$id;

        $sql->ExecQuery($query);
    }

    function Consultar($sql, $id)
    {
        $result=array();
        
        $query="select * from vigencia_pedido where id=".$id;

		$resul=$sql->ExecQuery($query);
		
        $i=0;
		while($row=$sql->FetchArray($resul))
		{
            $result[$i]['id'] = $row['id'];
            $result[$i]['numero_horas'] = $row['numero_horas'];
            $result[$i]['por_defecto'] = $row['por_defecto'];
            $i++;
		}

		return $result;
    }

    function ConsultarPorDefecto($sql)
    {
        $result=array();

        $query="select numero_horas from vigencia_pedido where por_defecto=1";

        $resul=$sql->ExecQuery($query);

        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $result[$i]['numero_horas'] = $row['numero_horas'];
            $i++;
        }

        return $result;
    }


    function Listado($sql)
    {
        $result = array();
        $query = "SELECT * FROM vigencia_pedido where eliminado = 0";
        $resul = $sql->ExecQuery($query);

        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $result[$i]['id'] = $row['id'];
            $result[$i]['numero_horas'] = $row['numero_horas'];
            $result[$i]['por_defecto'] = $row['por_defecto'];
            $i++;
        }

        return $result;
    }

    function VigenciaConfDefecto($sql)
    {
        $result = array();
        $query = "SELECT id FROM vigencia_pedido where por_defecto = 1";
        $resul = $sql->ExecQuery($query);

        return $row = $sql->FetchArray($resul);

    }

}