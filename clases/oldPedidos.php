<?php
class Pedidos extends ConsultaComun
{
    private $_total;
    private $_limit;
    private $_page;
    private $_query;

    function AgregarPedido($sql, $cliente, $monto_total, $observacion)
	{
        $query="INSERT INTO pedido(cliente, monto_total, condicion, activo, eliminada, observacion) 
                VALUES ('$cliente', '$monto_total', 0, 1, 0,'$observacion')";
        
        $sql->ExecQuery($query);
        
        $query="SELECT LAST_INSERT_ID() as id;";
       
        $resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
            $res = $row['id'];
        }
		return $res;
	}

    function ModificarCondicion($sql, $id_pedido, $id_condicion)
    {
        $query="UPDATE pedido SET condicion=".$id_condicion." WHERE id_pedido=$id_pedido";

        $sql->ExecQuery($query);
    }

    function ModificarCondicionEntrega($sql, $id_pedido, $condicion)
    {
        $query="UPDATE pedido SET metodo_entrega='$condicion' WHERE id_pedido=$id_pedido";
        $sql->ExecQuery($query);
    }


    function GetCondiciones($sql)
    {
        $res = array();
        $query = "SELECT * FROM condicion WHERE eliminado = 0";

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['id'] = $row['id'];
            $res[$i]['condicion'] = $row['condicion'];
            $i++;
        }
        return $res;
    }
    function getCondicionId($sql, $condicion)
    {
        $res = array();
        $query = "SELECT * FROM condicion WHERE id = ".$condicion;

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['id'] = $row['id'];
            $res[$i]['condicion'] = $row['condicion'];
            $i++;
        }
        return $res;
    }


    function ConsultarPedido($sql, $id_pedido)
	{
        $res = array();
		$query="SELECT pedido.id_pedido, pedido.cliente, pedido.datos_facturacion, pedido.fecha, pedido.monto_total, 
                pedido.condicion as id_condicion, condicion.condicion, pedido.activo, pedido.eliminada, pedido.observacion
                FROM pedido left join condicion on pedido.condicion = condicion.id
                WHERE pedido.id_pedido = " . $id_pedido;
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['cliente'] = $row['cliente'];
            $res[$i]['datos_facturacion'] = $row['datos_facturacion'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['id_condicion'] = $row['id_condicion'];
            $res[$i]['condicion'] = $row['condicion'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['eliminada'] = $row['eliminada'];
            $res[$i]['observacion'] = $row['observacion'];
            $i++;
		}

        $resItem = array();
        $query="SELECT *,marcas.descripcion as descripcion_marca, pedido_item.descripcion as descripcion_producto FROM pedido_item inner join productos on pedido_item.codigo_producto = productos.cod_producto LEFT JOIN marcas on pedido_item.marca = marcas.id WHERE pedido_item.id_pedido = " .$id_pedido;
        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $resItem[$i]['id_item'] = $row['id_item'];
            $resItem[$i]['codigo_producto'] = $row['codigo_producto'];
            $resItem[$i]['descripcion'] = $row['descripcion_producto'];
            $resItem[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $resItem[$i]['foto'] = $row['foto1'];
            $resItem[$i]['nombre'] = $row['nombre'];
            $resItem[$i]['cantidad'] = $row['cantidad'];
            $resItem[$i]['precio_unitario'] = $row['precio_unitario'];
            $resItem[$i]['precio_total'] = $row['precio_total'];
            $i++;
        }

        $result         = new stdClass();
        $result->data   = $res;
        $result->dataItems   = $resItem;
        return (array) $result;
	}
    
    function ListadoPedido($sql, $condicion = "", $limit = 15, $page = 1, $status)
	{
        $this->_limit   = $limit;
        $this->_page    = $page;

        $and = ($condicion == 'all' || $condicion == 'del') ? '' : ' and pedido.condicion = '.$condicion ;

        $res = array();
		$query="SELECT pedido.metodo_entrega, pagos.id, pedido.id_pedido,pedido.cliente,pedido.datos_facturacion,pedido.fecha,pedido.monto_total,pedido.condicion as id_condicion ,condicion.condicion, pedido.activo,pedido.eliminada,Count(pedido_item.id_item) AS total_items
        FROM pedido
        LEFT JOIN pedido_item ON pedido.id_pedido = pedido_item.id_pedido
        left join condicion on pedido.condicion = condicion.id
        left join pagos on pagos.id_pedido = pedido.id_pedido
        WHERE 
        pedido.eliminada = ".$status.$and." 
        GROUP BY 
        pedido.id_pedido,pedido.cliente,pedido.datos_facturacion,pedido.fecha,pedido.monto_total,pedido.condicion,pedido.activo,pedido.eliminada
        ORDER BY pedido.id_pedido DESC";

        $this->_total = $this::TotalRegistros($sql,$query);

        $this->_query = " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";

        //consulta con la paginacion //

        $query="SELECT pedido.metodo_entrega,pagos.id, pedido.id_pedido,pedido.cliente,pedido.datos_facturacion,pedido.fecha,pedido.monto_total,pedido.condicion as id_condicion,condicion.condicion,pedido.activo,pedido.eliminada,Count(pedido_item.id_item) AS total_items
        FROM pedido
        LEFT JOIN pedido_item ON pedido.id_pedido = pedido_item.id_pedido
        left join condicion on pedido.condicion = condicion.id
        left join pagos on pagos.id_pedido = pedido.id_pedido
        WHERE 
        pedido.eliminada = ".$status.$and." 
        GROUP BY 
        pedido.id_pedido,pedido.cliente,pedido.datos_facturacion,pedido.fecha,pedido.monto_total,pedido.condicion,pedido.activo,pedido.eliminada
        ORDER BY pedido.id_pedido DESC".$this->_query;

     	$resul=$sql->ExecQuery($query);

		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
			$res[$i]['id_pedido'] = $row['id_pedido'];
            $res[$i]['metodo_entrega'] = $row['metodo_entrega'];
			$res[$i]['cliente'] = $row['cliente'];
            $res[$i]['datos_facturacion'] = $row['datos_facturacion'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['id_condicion'] = $row['id_condicion'];
            $res[$i]['condicion'] = $row['condicion'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['eliminada'] = $row['eliminada'];
            $res[$i]['total_items'] = $row['total_items'];
            $i++;
		}

        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $res;
        return (array) $result;

	}

    function ListadoPedidoClientes($sql, $cliente)
	{
        $res = array();
		$query="SELECT pedido.id_pedido, cliente, datos_facturacion, pedido.fecha, monto_total, pedido.condicion as id_condicion, condicion.condicion,activo, eliminada, pagos.validado
                FROM pedido 
                left join condicion on pedido.condicion = condicion.id
                left join pagos on pagos.id_pedido = pedido.id_pedido
                WHERE cliente = '$cliente' AND eliminada=0
                ORDER BY id_pedido DESC";

      	$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['cliente'] = $row['cliente'];
            $res[$i]['datos_facturacion'] = $row['datos_facturacion'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['id_condicion'] = $row['id_condicion'];
            $res[$i]['condicion'] = $row['condicion'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['eliminada'] = $row['eliminada'];
            $res[$i]['validado'] = $row['validado'];
            
            $i++;
		}
		return $res;
	}
    
    
    function ResumenPedidoAdmin($sql)
	{
        $res = array();
		$query="SELECT count(id_pedido) as total_pedidos FROM pedido";
		
      	$resul = $sql->ExecQuery($query);
		return $sql->FetchArray($resul);
	}

    function ResumenPedidoCondicionAdmin($sql, $condicion)
    {
        $res = array();
        $query="SELECT condicion, count(id_pedido) as total_pedidos FROM pedido
                WHERE condicion = '$condicion' and eliminada = 0
                group by condicion";

        $resul = $sql->ExecQuery($query);
        return $sql->FetchArray($resul);
    }

    function ResumenPedidoEliminadosAdmin($sql)
    {
        $res = array();
        $query="SELECT count(id_pedido) as total_pedidos FROM pedido WHERE eliminada = 1";

        $resul = $sql->ExecQuery($query);
        return $sql->FetchArray($resul);
    }


    function AgregarDatosFacturacion($sql, $id_pedido, $id_datos_facturacion)
	{
      $query="UPDATE pedido 
                SET datos_facturacion='$id_datos_facturacion'
                WHERE id_pedido=$id_pedido";


        $sql->ExecQuery($query);
	}
    
    function AgregarPago($sql, $id_pedido)
	{
      $query="UPDATE pedido 
                SET condicion=1
                WHERE id_pedido=$id_pedido";
        
        $sql->ExecQuery($query);
	}
    
    function AgregarEnviado($sql, $id_pedido)
	{
      $query="UPDATE pedido 
                SET condicion=4
                WHERE id_pedido=$id_pedido";
        
        $sql->ExecQuery($query);
	}
    
    function AgregarEnsamblada($sql, $id_pedido)
	{
        $query="UPDATE pedido 
                SET condicion=3
                WHERE id_pedido=$id_pedido";
        $sql->ExecQuery($query);
	}
    
    function AgregarConfirmado($sql, $id_pedido)
	{
        $query="UPDATE pedido 
                SET condicion=2
                WHERE id_pedido=$id_pedido";
        $sql->ExecQuery($query);
	}

    function EliminarPedido($sql, $id_pedido)
	{
        $query="UPDATE pedido 
                SET eliminada='1'
                WHERE id_pedido=$id_pedido";
        $sql->ExecQuery($query);
	}

    function EliminarPedidoVigencia($sql, $id_pedido)
    {
        $query="UPDATE pedido 
                SET eliminada='1'
                WHERE id_pedido=$id_pedido and condicion = 0";

        $sql->ExecQuery($query);
    }

    
    function ModificarMontoTotal($sql, $id_pedido, $nuevo_monto)
	{
      $query="UPDATE pedido 
                SET monto_total='$nuevo_monto'
                WHERE id_pedido=$id_pedido";
        
        $sql->ExecQuery($query);
	}

    function ListadoFechasCobradoCorpDID($sql)
	{
        
        $res = array();
		$query="SELECT DISTINCT(fecha_cobro), count(id_pedido) AS cantidad, sum(monto_total) as monto_total
                FROM pedido 
                GROUP BY fecha_cobro 
                ORDER BY fecha_cobro DESC ";
		
     	$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['fecha_cobro'] = $row['fecha_cobro'];
            $res[$i]['cantidad'] = $row['cantidad'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $i++;
		}
		return $res;
	}
    

    function AgregarItemsPedido($sql, $id_pedido, $codigo_producto, $descripcion, $marca, $unidad, $cantidad, $precio_unitario, $precio_total)
    {
        $query="INSERT INTO pedido_item(id_pedido, codigo_producto, descripcion, marca, unidad, cantidad, precio_unitario, precio_total) 
                VALUES ('$id_pedido', '$codigo_producto', '$descripcion', '$marca', '$unidad', '$cantidad', '$precio_unitario', '$precio_total')";
        
        $sql->ExecQuery($query);
    }
    
    function ConsultarItemsPedido($sql, $id_pedido)
    {
        $res = array();
		$query="SELECT productos.nombre, pedido_item.id_pedido, pedido_item.codigo_producto, pedido_item.descripcion, pedido_item.marca, pedido_item.unidad, pedido_item.cantidad, pedido_item.precio_unitario, pedido_item.precio_total
                FROM pedido_item inner join productos on productos.cod_producto = pedido_item.codigo_producto
                WHERE pedido_item.id_pedido = '$id_pedido'
                ORDER BY pedido_item.id_pedido ASC";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['codigo_producto'] = $row['codigo_producto'];
			$res[$i]['descripcion'] = $row['descripcion'];
			$res[$i]['marca'] = $row['marca'];
			$res[$i]['unidad'] = $row['unidad'];
			$res[$i]['cantidad'] = $row['cantidad'];
			$res[$i]['precio_unitario'] = $row['precio_unitario'];
			$res[$i]['precio_total'] = $row['precio_total'];
            $res[$i]['nombre'] = $row['nombre'];

            $i++;
		}
		return $res;
    }
    
    function ConsultarMontoItemPedido($sql, $id_pedido)
    {
        $res = 0;
		$query="SELECT sum(precio_total) AS total
                FROM pedido_item 
                WHERE id_pedido = '$id_pedido'";

		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res = $row['total'];
		}
		return $res;
    }

    function ListadoPedidos($sql)
    {

        $res = array();
        $query="SELECT id_pedido, fecha FROM pedido 
                where condicion = 0 and eliminada = 0 order by id_pedido";

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id_pedido'] = $row['id_pedido'];
            $res[$i]['fecha'] = $row['fecha'];
            $i++;
        }
        return $res;
    }

    function CondicionesPedidos($sql)
    {

        $res = array();
        $query="SELECT id, condicion FROM condicion";

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id'] = $row['id'];
            $res[$i]['condicion'] = $row['condicion'];
            $i++;
        }
        return $res;
    }


    
}
