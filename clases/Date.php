<?php
class basica_Date
{
	var $year;
	var $month;
	var $day;
	var $hour; 
	var $mint;
	var $seg;
	
	function __construct()
	{
		$this->year=0000;
		$this->month=00;
		$this->day=00;
		$this->hour=00;
		$this->mint=00;
		$this->seg=00;
	}
	
	function Date()
	{
		$this->year=date('Y');
		$this->month=date('j');
		$this->day=date('d');
		$this->hour=date('H');
		$this->mint=date('i');
		$this->seg=date('s');
	}
	
	function SetDateMysql($str)
	{
		$this->year=strtok($str,"-");
		$this->month=strtok("-");
		$temp=strtok("-");
		if(substr_count($temp, ':')!=0)
		{
			$this->day=strtok($temp," ");
			$temp2=strtok(" ");
			$this->hour=strtok($temp2,":");
			$this->mint=strtok(":");
			$this->seg=strtok(":");
		}
		else
		{$this->day=(int)$temp;}
	}
	
	function SetDate($seg,$minutes,$hour,$day,$month,$year)
	{
		$this->year=$year;
		$this->month=$month;
		$this->day=$day;
		$this->hour=$hour;
		$this->mint=$minutes;
		$this->seg=$seg;
	}
	
	function ObtenerDia()
	{return $this->day;}
	
	function ObtenerMes()
	{return $this->month;}
	
	function ObtenerAno()
	{return $this->year;}

	function obtenerFecha()
	{return $this->year."-".$this->month."-".$this->day;}
	
	function obtenerFechaCalendario()
	{return $this->day."/".$this->month."/".$this->year;}
	
	function obtenerFechaEspanol()
	{
		$texto = $this->day." ";
		
		if($this->month == "01"){$mes = "Enero";}
		elseif($this->month == "02"){$mes = "Febrero";}
		elseif($this->month == "03"){$mes = "Marzo";}
		elseif($this->month == "04"){$mes = "Abril";}
		elseif($this->month == "05"){$mes = "Mayo";}
		elseif($this->month == "06"){$mes = "Junio";}
		elseif($this->month == "07"){$mes = "Julio";}
		elseif($this->month == "08"){$mes = "Agosto";}
		elseif($this->month == "09"){$mes = "Septiembre";}
		elseif($this->month == "10"){$mes = "Octubre";}
		elseif($this->month == "11"){$mes = "Noviembre";}
		elseif($this->month == "12"){$mes = "Diciembre";}
		
		$texto .= "de ".$mes." de ".$this->year;
		return $texto;
	}
}
?>