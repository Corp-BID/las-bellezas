<?php
class Monedas extends ConsultaComun
{
        function Agregar($sql, $descripcion, $simbolo, $condicion, $status)
        {
            $query="INSERT INTO monedas (descripcion, simbolos, activo, eliminado) 
                    VALUES ('$descripcion', '$simbolo', $condicion, $status)";
            $sql->ExecQuery($query);
        }

        function PorDefectoUpdate($sql)
        {
            $query="UPDATE monedas SET por_defecto = 0";
            $sql->ExecQuery($query);
        }

        function Modificar($sql, $id, $descripcion, $simbolo, $condicion)
        {
            $query="UPDATE monedas SET descripcion = '$descripcion', simbolos = '$simbolo', activo = $condicion WHERE id='$id'";

            $sql->ExecQuery($query);
        }

        function Eliminar($sql, $id)
        {
            $query="UPDATE monedas SET eliminado='1' WHERE id='$id'";
            $sql->ExecQuery($query);
        }

        function Obtener($sql, $id)
        {
                $result = array();

                $query="SELECT id, descripcion, simbolos, por_defecto, activo, eliminado 
                        FROM monedas 
                        WHERE id=".$id;

                $resul = $sql->ExecQuery($query);

                $i = 0;
                while ($row = $sql->FetchArray($resul)) {
                        $result[$i]['id'] = $row['id'];

                        $result[$i]['id'] = $row['id'];
                        $result[$i]['descripcion'] = $row['descripcion'];
                        $result[$i]['simbolos'] = $row['simbolos'];
                        $result[$i]['por_defecto'] = $row['por_defecto'];
                        $result[$i]['activo'] = $row['activo'];
                        $i++;
                }

                return $result;
        }

        function Listado($sql, $status)
        {
                $result = array();

                $query="SELECT id, descripcion, simbolos, por_defecto, activo, eliminado 
                        FROM monedas 
                        WHERE eliminado=".$status;

                $resul = $sql->ExecQuery($query);

                $i = 0;
                while ($row = $sql->FetchArray($resul)) {
                        $result[$i]['id'] = $row['id'];
                        $result[$i]['descripcion'] = $row['descripcion'];
                        $result[$i]['simbolos'] = $row['simbolos'];
                        $result[$i]['por_defecto'] = $row['por_defecto'];
                        $result[$i]['activo'] = $row['activo'];

                        $i++;
                }

                return $result;
        }


    function MonedaPorDefecto($sql, $id_moneda)
    {
        $result = array();

        $query="select monedas.id, monedas.descripcion,monedas.simbolos, tasa_dolar.id as id_tasa, tasa_dolar.valor_calculo from monedas 
        LEFT JOIN tasa_dolar on tasa_dolar.moneda_id = monedas.id and tasa_dolar.activo = 1 
        where monedas.id=".$id_moneda;

        $resul = $sql->ExecQuery($query);

        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $result[$i]['id'] = $row['id'];
            $result[$i]['id_tasa'] = $row['id_tasa'];
            $result[$i]['descripcion'] = $row['descripcion'];
            $result[$i]['simbolos'] = $row['simbolos'];
            $result[$i]['valor_calculo'] = $row['valor_calculo'];
            $i++;
        }
        return $result;
    }

    function IdMonedaBs($sql)
    {
        $result=array();

        $query="SELECT id FROM monedas WHERE simbolos in ('Bs','bs')";
        $resul=$sql->ExecQuery($query);

        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $result[$i]['id']=$row['id'];
            $i++;
        }

        return $result;
    }

    function MonedaDestino($sql, $id_moneda)
    {
        $result = array();

         $query="SELECT monedas.id, tasa_dolar.moneda_id, monedas.descripcion,monedas.simbolos, tasa_dolar.id as id_tasa, tasa_dolar.valor_calculo
        FROM
            tasa_dolar
            INNER JOIN monedas ON tasa_dolar.moneda_id_destino = monedas.id 
        WHERE tasa_dolar.activo = 1 and tasa_dolar.moneda_id = ".$id_moneda;

        $resul = $sql->ExecQuery($query);

        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $result[$i]['id'] = $row['id'];
            $result[$i]['moneda_id'] = $row['moneda_id'];
            $result[$i]['id_tasa'] = $row['id_tasa'];
            $result[$i]['descripcion'] = $row['descripcion'];
            $result[$i]['simbolos'] = $row['simbolos'];
            $result[$i]['valor_calculo'] = $row['valor_calculo'];
            $i++;
        }
        return $result;
    }

    function MonedaDestinoCambio($sql, $id_moneda)
    {
        $result = array();

        $query="SELECT id, descripcion, simbolos, por_defecto, activo
        FROM monedas WHERE id = ".$id_moneda;

        $resul = $sql->ExecQuery($query);

        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $result[$i]['id'] = $row['id'];
            $result[$i]['descripcion'] = $row['descripcion'];
            $result[$i]['simbolos'] = $row['simbolos'];
            $result[$i]['descripcion'] = $row['descripcion'];
            $result[$i]['por_defecto'] = $row['por_defecto'];
            $result[$i]['activo'] = $row['activo'];
            $i++;
        }
        return $result;
    }

    function MonedaPorDefectoConf($sql)
    {
        $row = array();
        $query="SELECT id FROM monedas WHERE por_defecto = 1";
        $resul = $sql->ExecQuery($query);
        return $row = $sql->FetchArray($resul);

    }

}