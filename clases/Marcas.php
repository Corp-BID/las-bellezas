<?php
class Marcas extends ConsultaComun
{
        function Agregar($sql, $descripcion, $condicion,$status)
        {
                $query="INSERT INTO marcas (descripcion, activo, eliminado) 
                        VALUES ('$descripcion', '$condicion','$status')";

                $sql->ExecQuery($query);
        }


        function Modificar($sql, $id, $descripcion, $condicion, $status)
        {
                $query="UPDATE marcas 
                        SET descripcion='$descripcion', activo='$condicion', eliminado='$status' WHERE id='$id'";
                $sql->ExecQuery($query);
        }

        function ModificarLogo($sql, $id, $logo)
        {
            $query="UPDATE marcas SET logo='$logo' WHERE id=$id";
            $sql->ExecQuery($query);
        }

        function Eliminar($sql, $id)
        {
                $query="UPDATE marcas 
                        SET eliminado='1'
                        WHERE id='$id'";

                $sql->ExecQuery($query);
        }

        function Obtener($sql, $id)
        {
                $result=array();

                $query="SELECT * 
                        FROM marcas 
                        WHERE id=$id AND eliminado=0";
                $resul=$sql->ExecQuery($query);

                $i=0;
                while ($row=$sql->FetchArray($resul)) {
                        $result[$i]['id']=$row['id'];
                        $result[$i]['descripcion']=$row['descripcion'];
                        $result[$i]['logo']=$row['logo'];
                        $result[$i]['activo']=$row['activo'];
                        $result[$i]['eliminado']=$row['eliminado'];
                        $i++;
                }

                return $result;
        }

        function Listado($sql, $status=0)
        {
                $result=array();

                $query="SELECT * FROM marcas WHERE eliminado=$status and id > 0";
                $resul=$sql->ExecQuery($query);

                $i=0;
                while ($row=$sql->FetchArray($resul)) {
                        $result[$i]['id']=$row['id'];
                        $result[$i]['descripcion']=$row['descripcion'];
                        $result[$i]['logo']=$row['logo'];
                        $result[$i]['activo']=$row['activo'];
                        $result[$i]['eliminado']=$row['eliminado'];
                        $i++;
                }

                return $result;
        }


        function ListadoCantidad($sql)
        {
                $result=array();

                $query="SELECT M.id, M.descripcion, M.logo, (SELECT COUNT(P.cod_producto) FROM productos P WHERE P.marca=M.id) AS cantidad 
                        FROM marcas M
                        WHERE M.activo=1 AND M.eliminado=0
                        ORDER BY cantidad DESC";

                $resul=$sql->ExecQuery($query);

                $i=0;
                while ($row=$sql->FetchArray($resul)) {
                        $result[$i]['id']=$row['id'];
                        $result[$i]['descripcion']=$row['descripcion'];
                        $result[$i]['logo']=$row['logo'];
                        $result[$i]['cantidad']=$row['cantidad'];
                        $i++;
                }

                return $result;
        }
}