<?php
class Pago
{
	function AgregarPago($sql, $id_pedido, $banco, $referencia, $monto, $fecha, $forma_pago, $opcion_pago, $id_tasa, $id_moneda, $monto_pago, $observacion_efectivo)
	{

        $query="INSERT INTO pagos(id_pedido, tipo_pago, tipo_transferencia, id_tasa, id_moneda, monto_pago, banco, 
        referencia, monto, fecha, validado, observacion) 
                VALUES('$id_pedido','$forma_pago', '$opcion_pago', '$id_tasa', '$id_moneda', '$monto',  '$banco', 
                '$referencia', '$monto_pago', '$fecha', 0, '$observacion_efectivo')";

        $sql->ExecQuery($query);

        $query="SELECT LAST_INSERT_ID() as id;";

        $resul=$sql->ExecQuery($query);

        if($row=$sql->FetchArray($resul))
        {
            $res = $row['id'];
        }
        return $res;

	}

    function ModificarCondicion($sql, $id_pago, $id_condicion)
    {
        $query="UPDATE pagos SET validado=".$id_condicion." WHERE id=$id_pago";
        $sql->ExecQuery($query);

        // confirmar el pedido una vez validado el pago //

        $query="select id_pedido from pagos where id=".$id_pago;

        $resul=$sql->ExecQuery($query);

        if($row=$sql->FetchArray($resul))
        {
            $id_pedido = $row['id_pedido'];
        }

        $condicion = CONFIRMADO;

        $query="UPDATE pedido SET condicion=".$condicion." WHERE id_pedido=".$id_pedido;
        $sql->ExecQuery($query);

    }

    function ConsultarPagoId($sql, $id)
	{
        $res = array();
		$query="SELECT tasa_dolar.valor_calculo, pagos.id, pagos.monto_pago, monedas.simbolos, 
pagos.id_pedido, pagos.banco, pagos.referencia, pagos.monto, 
pagos.fecha, pagos.validado, 
pedido.cliente,pedido.monto_total, condicion.condicion ,
pagos.tipo_transferencia,pagos.tipo_pago,pagos.observacion
                FROM pagos 
                left join pedido on pedido.id_pedido = pagos.id_pedido left join condicion on pedido.condicion = condicion.id
                left join monedas on monedas.id = pagos.id_moneda
                left join tasa_dolar on pagos.id_tasa = tasa_dolar.id
                WHERE pagos.id = '$id'";

		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
			$res[$i]['id_pedido'] = $row['id_pedido'];
            $res[$i]['banco'] = $row['banco'];
            $res[$i]['referencia'] = $row['referencia'];
            $res[$i]['monto'] = $row['monto'];
            $res[$i]['monto_pago'] = $row['monto_pago'];
            $res[$i]['valor_calculo'] = $row['valor_calculo'];
            $res[$i]['simbolos'] = $row['simbolos'];
            $res[$i]['monto_pago'] = $row['monto_pago'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['validado'] = $row['validado'];
            $res[$i]['cliente'] = $row['cliente'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['condicion'] = $row['condicion'];
            $res[$i]['tipo_transferencia'] = $row['tipo_transferencia'];
            $res[$i]['tipo_pago'] = $row['tipo_pago'];
            $res[$i]['observacion'] = $row['observacion'];
            
            $i++;
		}
		return $res;
	}
    
    function ConsultarPagoPedido($sql, $id_pedido)
	{
        $res = array();
		$query="SELECT pagos.id, id_pedido, banco, referencia, monto, fecha, validado , monto_pago, monedas.simbolos
                FROM pagos inner join monedas on monedas.id = pagos.id_moneda
                WHERE pagos.id_pedido = '$id_pedido'";

		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
			$res[$i]['id_pedido'] = $row['id_pedido'];
            $res[$i]['banco'] = $row['banco'];
            $res[$i]['referencia'] = $row['referencia'];
            $res[$i]['monto'] = $row['monto'];
            $res[$i]['monto_pago'] = $row['monto_pago'];
            $res[$i]['simbolos'] = $row['simbolos'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['validado'] = $row['validado'];
            
            $i++;
		}
		return $res;
	}
    
    function ListadoPedido($sql)
	{
        $res = array();
		$query="SELECT id_pedido, cliente, datos_facturacion, fecha, monto_total, pagada,  confirmada, activo, eliminada
                FROM pedido 
                ORDER BY id_pedido DESC";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['cliente'] = $row['cliente'];
            $res[$i]['datos_facturacion'] = $row['datos_facturacion'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['pagada'] = $row['pagada'];
            $res[$i]['confirmada'] = $row['confirmada'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['eliminada'] = $row['eliminada'];
            
            $i++;
		}
		return $res;
	}
    
    function ListadoPedidoClientes($sql, $cliente)
	{
        $res = array();
		$query="SELECT id_pedido, cliente, datos_facturacion, fecha, monto_total, pagada,  confirmada, activo, eliminada
                FROM pedido 
                WHERE cliente = $cliente;
                ORDER BY id_pedido DESC";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['cliente'] = $row['cliente'];
            $res[$i]['datos_facturacion'] = $row['datos_facturacion'];
            $res[$i]['fecha'] = $row['fecha'];
            $res[$i]['monto_total'] = $row['monto_total'];
            $res[$i]['pagada'] = $row['pagada'];
            $res[$i]['confirmada'] = $row['confirmada'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['eliminada'] = $row['eliminada'];
            
            $i++;
		}
		return $res;
	}
    
    function AgregarDatosFacturacion($sql, $id_pedido, $id_datos_facturacion)
	{
      $query="UPDATE pedido 
                SET datos_facturacion='$id_datos_facturacion'
                WHERE id_pedido=$id_pedido";
        
        $sql->ExecQuery($query);
	}
    
    function ValidarPago($sql, $id_pedido, $id_pago)
	{
      $query="UPDATE pagos 
                SET validado='1'
                WHERE id='$id_pago' AND id_pedido='$id_pedido'";
       
        $sql->ExecQuery($query);
	}
    
    ///-------------------------------------------------------------------------------------
    
    function AgregarItemsPedido($sql, $id_pedido, $codigo_producto, $descripcion, $marca, $unidad, $cantidad, $precio_unitario, $precio_total)
    {
        $query="INSERT INTO pedido_item(id_pedido, codigo_producto, descripcion, marca, unidad, cantidad, precio_unitario, precio_total) 
                VALUES ('$id_pedido', '$codigo_producto', '$descripcion', '$marca', '$unidad', '$cantidad', '$precio_unitario', '$precio_total')";
        
        $sql->ExecQuery($query);
    }
    
    function ConsultarItemsPedido($sql, $id_pedido)
    {
        $res = array();
		$query="SELECT id_pedido, codigo_producto, descripcion, marca, unidad, cantidad, precio_unitario, precio_total
                FROM pedido_item 
                WHERE id_pedido = '$id_pedido'
                ORDER BY id_pedido ASC";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id_pedido'] = $row['id_pedido'];
			$res[$i]['codigo_producto'] = $row['codigo_producto'];
			$res[$i]['descripcion'] = $row['descripcion'];
			$res[$i]['marca'] = $row['marca'];
			$res[$i]['unidad'] = $row['unidad'];
			$res[$i]['cantidad'] = $row['cantidad'];
			$res[$i]['precio_unitario'] = $row['precio_unitario'];
			$res[$i]['precio_total'] = $row['precio_total'];
            
            $i++;
		}
		return $res;
    }

    function Listado($sql)
    {
        $result=array();

        $query="SELECT pagos.id, id_pedido, banco, referencia, monto, monto_pago, pagos.fecha, validado, tipo_pago, 
                tipo_transferencia, tasa_dolar.valor_calculo, monedas.simbolos,observacion
                FROM pagos left join tasa_dolar on pagos.id_tasa = tasa_dolar.id 
                left join monedas on monedas.id = pagos.id_moneda order by pagos.id desc ";

        $resul=$sql->ExecQuery($query);

        $i=0;
        while ($row=$sql->FetchArray($resul)) {
            $result[$i]['id']=$row['id'];
            $result[$i]['id_pedido']=$row['id_pedido'];
            $result[$i]['tipo_pago']=$row['tipo_pago'];
            $result[$i]['tipo_transferencia']=$row['tipo_transferencia'];
            $result[$i]['banco']=$row['banco'];
            $result[$i]['referencia']=$row['referencia'];
            $result[$i]['monto']=$row['monto'];
            $result[$i]['monto_pago']=$row['monto_pago'];
            $result[$i]['simbolos']=$row['simbolos'];
            $result[$i]['valor_calculo']=$row['valor_calculo'];
            $result[$i]['fecha']=$row['fecha'];
            $result[$i]['validado']=$row['validado'];
            $result[$i]['observacion']=$row['observacion'];
            $i++;
        }

        return $result;
    }
    
}