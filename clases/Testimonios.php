<?php
class Testimonios
{
    //tamaño minimo de la imagen
    const WIDTH_MIN = 263;
    const HEIGHT_MIN =  180;

    function Agregar($sql, $descripcion, $beneficiario, $opcional1,$opcional2,$opcional3,$filename,$publicado, $validado)
	{
		$query="INSERT INTO testimonios(imagen, descripcion, beneficiario, opcional1,opcional2,opcional3,publicado,eliminado,validado,id_usuario) VALUES 
                ('$filename', '$descripcion', '$beneficiario','$opcional1', '$opcional2', '$opcional3', '$publicado', 0, '$validado', 0 )";
        
        $sql->ExecQuery($query);
	}
    
    function Modificar($sql, $id, $descripcion, $beneficiario, $opcional1,$opcional2,$opcional3,$filename,$publicado, $validado)
    {
        $query="UPDATE testimonios 
                SET imagen='$filename',
                    descripcion='$descripcion',
                    beneficiario='$beneficiario',
                    opcional1='$opcional1',
                    opcional2='$opcional2',
                    opcional3='$opcional3',
                    publicado='$publicado',
                    validado='$validado' WHERE id='$id'";

        $sql->ExecQuery($query);
    }

    function Eliminar($sql, $id)
    {
        $query="UPDATE testimonios 
                SET eliminado = 1
                WHERE id=".$id;
        $sql->ExecQuery($query);
    }

    function Obtener($sql, $id)
	{
		$res = NULL;
        
        $query="SELECT * FROM testimonios WHERE id=".$id;
        
        $resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['beneficiario'] = $row['beneficiario'];
            $res[$i]['opcional1'] = $row['opcional1'];
            $res[$i]['opcional2'] = $row['opcional2'];
            $res[$i]['opcional3'] = $row['opcional3'];
            $res[$i]['validado'] = $row['validado'];
            $res[$i]['publicado'] = $row['publicado'];
            $res[$i]['fecha_sistema'] = $row['fecha_sistema'];
           
            $i++;
		}
		return $res;
	}
    
    function Listado($sql, $del)
	{
		$res = NULL;
        $query="SELECT id, imagen, descripcion, beneficiario, opcional1, opcional2, opcional3, validado, publicado, eliminado, fecha_sistema 
                FROM testimonios WHERE eliminado='$del' ORDER BY id DESC";

        $resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['beneficiario'] = $row['beneficiario'];
            $res[$i]['opcional1'] = $row['opcional1'];
            $res[$i]['opcional2'] = $row['opcional2'];
            $res[$i]['opcional3'] = $row['opcional3'];
            $res[$i]['validado'] = $row['validado'];
            $res[$i]['eliminado'] = $row['eliminado'];
            $res[$i]['publicado'] = $row['publicado'];
            $res[$i]['fecha_sistema'] = $row['fecha_sistema'];
           
            $i++;
		}
		return $res;
	}

    public function upload($inputImage, $folderPath)
    {

            $pattern = '/data:image\/(.+);base64,(.*)/';
            preg_match($pattern, $inputImage, $matches);
            if (!empty($matches)) {
                $imageExtension = $matches[1];
                $encodedImageData = $matches[2];
                $decodedImageData = base64_decode($encodedImageData);
                $filename = uniqid() . '.' . $imageExtension;
                $filepath = $folderPath . $filename;

                $data = getimagesizefromstring($decodedImageData);

                list($width, $height) = explode(' ', $data[3]);
                $match = explode('"', $width);
                $width = $match[1];
                $match = explode('"', $height);
                $height = $match[1];

                if ($width >= $this::WIDTH_MIN && $height >= $this::HEIGHT_MIN) {

                    if (file_put_contents($filepath, $decodedImageData)) {
                        $info['success'] = true;
                        $info['filepath'] = $filename;
                    } else {
                        $info['success'] = false;
                        $info['errors'] = 'Error al subir la imagen';
                    }

                } else {
                    $info['success'] = false;
                    $info['errors'] = 'El tamaño de la imagen es muy pequeña';
                }

            }else
            {
                $info['success'] = false;
                $info['errors'] = 'Error en la imagen';
            }

            echo json_encode($info);
    }

    function resizeImage( $filename , $folderPath)
    {

        $nombre_archivo_rand = date("Ymd")."-".rand(100,999);
        $nombre_archivo = $nombre_archivo_rand.".jpg";

        $foo = new Upload($filename);

        if ($foo->uploaded)
        {
            $foo->file_new_name_body = $nombre_archivo;
            $foo->file_overwrite = true;
            $foo->image_resize = true;
            $foo->image_ratio_y = true;
            $foo->image_x = 263;
            $foo->image_y = 180;
            $foo->image_convert = 'jpg';
            $foo->Process($folderPath);

            if ($foo->processed)
                $error="";
            else
                $error = $foo->error;
        }else{
            $error = 'Error al subir la imagen';
        }

        return $error;

    }

    function Revertir($sql, $id)
    {
        $query="UPDATE testimonios 
                SET eliminado = 0
                WHERE id=".$id;

        $sql->ExecQuery($query);
    }
}