<?php

/**
 * Clase: Base_Exception 
 * Descripcion: Deficicion estandar de la Excepcion manejada en el sistema
 *            
 *
 * @package Base
*/

class Base_Exception extends Exception
{
 protected $detalles; // Detalles adicionales
 const DESCONOCIDO = 0;
 
 const INVALIDO = 1; // Conex invalida

// Desl codigo 2 al 10 esta RESERVADOS para errores basicos de Conexcion
 
 const METODO_PROHIBIDO = 11;
 const METODO_INEXISTENTE = 12;
 const CLASE_INEXISTENTE = 13;
 const INF_INCO = 14;
 const NO_ENCONTRADO = 15;
 const EXISTENTE = 16;
 const NO_EXISTENTE = 17;
 const DATO_INCORRECTO = 18;
 const NO_GUARDADA = 19;
 const RESULTADO_VACIO = 20;
 const ERROR_METODO_ALTERNO = 21;
 const SIN_AUTORIZACION = 22;
 const NO_SERIALIZE = 23;
 const PARAMETRO_INVALIDO = 24;
 


  	    
 public function __construct($message='Excepcion NO especificada', $code = 0, Exception $previous = null)
 { parent::__construct($message, $code,$previous);}

//  public function __toString() 
//  {return __CLASS__ . ": [{$this->code}]: {$this->message}\n"; }

 public function asignarDetalles($detalles)
 {$this->detalles=$detalles;}

 public function obtenerDetalles()
 {return $this->detalles;}
}