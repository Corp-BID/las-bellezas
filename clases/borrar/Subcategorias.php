<?php
class Subcategorias
{
    function AgregarSubcategoria($sql, $cod_categoria, $cod_subcategoria, $descripcion, $num_productos)
	{
		$cod_categoria = (isset($cod_categoria) && intval($cod_categoria))?intval($cod_categoria):0;
        $cod_subcategoria = (isset($cod_subcategoria) && intval($cod_subcategoria))?intval($cod_subcategoria):0;
		$descripcion = (isset($descripcion) && $descripcion)?substr(strval($descripcion), 0, 140) :NULL;
		$num_productos = (isset($num_productos) && intval($num_productos))?intval($num_productos):0;
        
        $fecha_generado = date("Y-m-d H:i:s");
        $activo = 1;
        
		$query="INSERT INTO subcategorias (cod_categoria, cod_subcategoria, descripcion, num_productos, fecha_generado, activo) 
                VALUES ($cod_categoria, $cod_subcategoria, '$descripcion', $num_productos, '$fecha_generado', '$activo')";
        
        $sql->ExecQuery($query);
	}
    
    function ModificarSubcategoria($sql, $cod_categoria, $cod_subcategoria, $descripcion, $num_productos)
	{
		$cod_categoria = (isset($cod_categoria) && intval($cod_categoria))?intval($cod_categoria):0;
        $cod_subcategoria = (isset($cod_subcategoria) && intval($cod_subcategoria))?intval($cod_subcategoria):0;
		$descripcion = (isset($descripcion) && $descripcion)?substr(strval($descripcion), 0, 140) :NULL;
		$num_productos = (isset($num_productos) && intval($num_productos))?intval($num_productos):0;
		
        $fecha_generado = date("Y-m-d H:i:s");
        $activo = 1;
        
		$query="UPDATE subcategorias 
                SET descripcion='$descripcion', num_productos='$num_productos', fecha_generado='$fecha_generado', activo='$activo'  
                WHERE cod_categoria=$cod_categoria and cod_subcategoria=$cod_subcategoria";
        
        $sql->ExecQuery($query);
	}
    
    function ActualizarSubcategoria($sql, $cod_categoria, $cod_subcategoria, $descripcion, $num_productos)
	{
		$cod_categoria = (isset($cod_categoria) && intval($cod_categoria))?intval($cod_categoria):0;
        $cod_subcategoria = (isset($cod_subcategoria) && intval($cod_subcategoria))?intval($cod_subcategoria):0;
		$descripcion = (isset($descripcion) && $descripcion)?substr(strval($descripcion), 0, 140) :NULL;
		$num_productos = (isset($num_productos) && intval($num_productos))?intval($num_productos):0;
        
        $fecha_generado = date("Y-m-d H:i:s");
        $activo = 1;
        
		$query="INSERT INTO subcategorias (cod_categoria, cod_subcategoria, descripcion, num_productos, fecha_generado, activo) 
                VALUES ($cod_categoria, $cod_subcategoria, '$descripcion', $num_productos, '$fecha_generado', '$activo')
                ON  DUPLICATE KEY UPDATE  
                    descripcion='$descripcion', 
                    num_productos='$num_productos', 
                    fecha_generado='$fecha_generado', 
                    activo='$activo'";
        
        $sql->ExecQuery($query);
	}
    
    
	function ObtenerSubcategoria($sql, $cod_categoria, $cod_subcategoria=NULL)
	{
		$res = NULL;
        
        if($cod_categoria and $cod_subcategoria)
        {
            $query="SELECT cod_categoria, cod_subcategoria, descripcion, num_productos, fecha_generado, activo 
                    FROM subcategorias 
                    WHERE cod_categoria = $cod_categoria and cod_subcategoria=$cod_subcategoria";
        }
        else
        {
            $query="SELECT cod_categoria, cod_subcategoria, descripcion, num_productos, fecha_generado, activo 
                    FROM subcategorias 
                    WHERE cod_categoria = $cod_categoria
                    ORDER BY descripcion ASC";
        }

		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
			$res[$i]['descripcion'] = $row['descripcion'];
			$res[$i]['num_productos'] = $row['num_productos'];
            $res[$i]['fecha_generado'] = $row['fecha_generado'];
			$res[$i]['activo'] = $row['activo'];
			$i++;
		}
		return $res;
	}
}
