<?php
class TasaDolar
{

    function Agregar($sql, $valor_calculo, $fecha, $id_moneda, $id_moneda_destino,$activo)
    {
        if ($activo) {
            $query = "UPDATE tasa_dolar SET activo =0 where moneda_id = $id_moneda and moneda_id_destino =  $id_moneda_destino";
            $sql->ExecQuery($query);
        }

        $fecha =$this::convertString($fecha);

        $query = "INSERT INTO tasa_dolar (fecha, activo,moneda_id, moneda_id_destino, valor_calculo)
                VALUES ('$fecha', $activo, $id_moneda, $id_moneda_destino, $valor_calculo)";

        $sql->ExecQuery($query);
    }

    function Modificar($sql, $id, $valor_calculo, $fecha, $activo, $id_moneda, $id_moneda_destino)
    {

        $fecha =$this::convertString($fecha);

        //buscar id antes de blanquear el status

        $query="SELECT id FROM tasa_dolar WHERE moneda_id = ".$id_moneda." and moneda_id_destino = " .$id_moneda_destino." and activo = 1";
        $resul=$sql->ExecQuery($query);
        $row=$sql->FetchArray($resul);
        $id = $row['id'];

        if ($activo){
            $query="UPDATE tasa_dolar SET activo =0 where moneda_id = $id_moneda and moneda_id_destino =  $id_moneda_destino";
            $sql->ExecQuery($query);
        }

        $query="UPDATE tasa_dolar 
                SET valor_calculo ='$valor_calculo',
                fecha = '$fecha',
                activo ='$activo'
                WHERE id= ".$id;


		$sql->ExecQuery($query);
    }

    function Eliminar($sql, $id)
    {
        $query = "UPDATE tasa_dolar 
                SET activo=0
                WHERE id=".$id;

        $sql->ExecQuery($query);
    }

    function convertString ($date)
    {
        $sec = strtotime($date);
        $date = date("Y-m-d H:i", $sec);
        $date = $date . ":00";
        return $date;
    }

    function Consultar($sql, $id)
    {
        $result=array();
        
        $query="SELECT tasa_dolar.id, tasa_dolar.fecha, tasa_dolar.valor_calculo , tasa_dolar.activo, tasa_dolar.moneda_id, monedas.descripcion as moneda, monedas.simbolos,
        tasa_dolar.moneda_id_destino, moneda_destino.descripcion as moneda_destino,moneda_destino.simbolos as simbolo_destino
                FROM tasa_dolar 
                inner join monedas on monedas.id = tasa_dolar.moneda_id
                inner join monedas as moneda_destino on moneda_destino.id = tasa_dolar.moneda_id_destino
                WHERE tasa_dolar.id=$id";

		$resul=$sql->ExecQuery($query);
		
        $i=0;
		while($row=$sql->FetchArray($resul))
		{
            $result[$i]['id']=$row['id'];
            $result[$i]['fecha']=$row['fecha'];
            $result[$i]['valor_calculo']=$row['valor_calculo'];
            $result[$i]['activo']=$row['activo'];
            $result[$i]['moneda_id']=$row['moneda_id'];
            $result[$i]['moneda_id_destino']=$row['moneda_id_destino'];
            $result[$i]['moneda']=$row['moneda'];
            $result[$i]['moneda_destino']=$row['moneda_destino'];
            $result[$i]['simbolos']=$row['simbolos'];
            $i++;
		}

		return $result;
    }



    function ListadoDia($sql, $visible)
    {
        $result=array();
        
        if (!$visible){
            $query="SELECT tasa_dolar.moneda_id_destino,
	moneda_destino.descripcion AS moneda_destino,tasa_dolar.id, tasa_dolar.fecha, tasa_dolar.moneda_id, tasa_dolar.valor_calculo, tasa_dolar.activo, monedas.descripcion, monedas.simbolos, monedas.por_defecto 
                FROM tasa_dolar 
                INNER JOIN monedas AS moneda_destino ON moneda_destino.id = tasa_dolar.moneda_id_destino 
                inner join monedas on monedas.id = tasa_dolar.moneda_id 
                
                where tasa_dolar.activo = 1 order by tasa_dolar.id desc";
        }else{
            $query="SELECT tasa_dolar.moneda_id_destino,
	moneda_destino.descripcion AS moneda_destino,tasa_dolar.id, tasa_dolar.fecha, tasa_dolar.moneda_id, tasa_dolar.valor_calculo, tasa_dolar.activo, monedas.descripcion, monedas.simbolos, monedas.por_defecto 
                FROM tasa_dolar 
                INNER JOIN monedas AS moneda_destino ON moneda_destino.id = tasa_dolar.moneda_id_destino 
                inner join monedas on monedas.id = tasa_dolar.moneda_id
                order by tasa_dolar.id desc";
        }

		$resul=$sql->ExecQuery($query);
		
        $i=0;
		while($row=$sql->FetchArray($resul))
		{
            $result[$i]['id']=$row['id'];
            $result[$i]['fecha']=$row['fecha'];
            $result[$i]['valor_calculo']=$row['valor_calculo'];
            $result[$i]['activo']=$row['activo'];
            $result[$i]['descripcion']=$row['descripcion'];
            $result[$i]['simbolos']=$row['simbolos'];
            $result[$i]['por_defecto']=$row['por_defecto'];
            $result[$i]['moneda_id']=$row['moneda_id'];
            $result[$i]['moneda_destino']=$row['moneda_destino'];
            $result[$i]['moneda_id_destino']=$row['moneda_id_destino'];
            $i++;
		}

		return $result;
    }
    
    
    function ListadoHistorico($sql)
    {
        $result=array();
        
        $query="SELECT id, fecha, valor1, valor2, valor3, login 
                FROM tasa_dolar_historico 
                ORDER BY id DESC";
    	
		$resul=$sql->ExecQuery($query);
		
        $i=0;
		while($row=$sql->FetchArray($resul))
		{
            $result[$i]['id']=$row['id'];
            $result[$i]['fecha']=$row['fecha'];
            $result[$i]['valor']=$row['valor1'];
            $result[$i]['valor2']=$row['valor2'];
            $result[$i]['valor3']=$row['valor3'];
            $result[$i]['login']=$row['login'];
            $i++;
		}

		return $result;
    }

    function TasaConfDefecto($sql, $moneda)
    {
        $row =array();
        $query="SELECT id FROM monedas WHERE simbolos in ('Bs','bs')";
        $resul=$sql->ExecQuery($query);
        $row=$sql->FetchArray($resul);
        $idBs = $row['id'];

        $row = array();
        $query="SELECT id, valor_calculo FROM tasa_dolar where moneda_id=".$moneda." and moneda_id_destino = '$idBs' and activo =1";

        $result=$sql->ExecQuery($query);
        return $row=$sql->FetchArray($result);

    }

}