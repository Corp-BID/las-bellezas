<?php
/**
 * Clase que define los metodos basicos para guardar la sessiones de los usuarios
 * en el sistema
 *
 * @author Asdrubal A Dominguez S
 * @package Base
 */

abstract class Base_SessionHandler
{
 abstract public function open($savePath, $sessionName);

 public function close()
 {return true;}

 abstract public function read($id);

 abstract public function write($id, $session_data);

 abstract public function destroy($id);

 abstract public function gc($maxlifetime = 300);
}