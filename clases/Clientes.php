<?php
class Clientes
{
	
/*--- Funciones de los usuarios ---*/
	function ValidarCliente($sql, $login, $password)
	{
		$result=array();
		$query="SELECT correo, cedula, nombres, apellidos, telefono, activo, eliminado 
			     FROM clientes 
                 WHERE correo= '$login' AND clave = MD5('$password') AND activo =1 AND eliminado=0";

		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['correo']=$row['correo'];
            $result['cedula']=$row['cedula'];
			$result['nombres']= $row['nombres'];
			$result['apellidos']=$row['apellidos'];
			$result['telefono']=$row['telefono'];
		}
		
		if($result){return $result;}
		else return false;
	}
    
    function ValidarCorreo($sql, $login)
	{
		$result=array();
		$query="SELECT id, correo
			FROM clientes 
			WHERE correo= '$login'";

        
        
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['correo']=$row['correo'];
            $result['id']=$row['id'];
		}
		
		if($result){return $result;}
		else return false;
	}



    function setCodigoValidacion($sql, $id )
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $input_length = strlen($permitted_chars);
        $random_string = '';
        for($i = 0; $i < 8; $i++) {
            $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        $query = "UPDATE clientes SET 
        codigo_validacion = '$random_string'
        WHERE id = ".$id;

        return $sql->ExecQuery($query);

    }

    function getCodigoValidacion($sql, $codigo )
    {
          $res = array();
          $query="SELECT id FROM clientes WHERE codigo_validacion = '$codigo'";

          $resul=$sql->ExecQuery($query);
          $i=0;
          while($row=$sql->FetchArray($resul)) {
              $res[$i]['id'] = $row['id'];
          }

          $rs = array();
          $rs[0] = false;
        if($res){return $res;}
        else return $rs;
    }

    function getCodigoValidacionId($sql, $id )
    {
        $res = array();
        $query="SELECT codigo_validacion, nombres, apellidos FROM clientes WHERE id = ". $id;

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul)) {
            $res[$i]['codigo_validacion'] = $row['codigo_validacion'];
            $res[$i]['nombres'] = $row['nombres'];
            $res[$i]['apellidos'] = $row['apellidos'];
        }

        return $res;
    }

    function setResetPassword($sql, $id , $password)
    {

        $query = "UPDATE clientes SET 
        clave = MD5('$password')
        WHERE id = ".$id;

        return $sql->ExecQuery($query);

    }


/*--- Funciones para el coordinador ---*/
	function obtenerClientes($sql)
	{
		$res = array();
		$query="SELECT id, correo, cedula, nombres, apellidos, telefono, fecha_creacion, activo, eliminado 
			FROM clientes 
			ORDER BY fecha_creacion DESC";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
			$res[$i]['correo'] = $row['correo'];
			$res[$i]['cedula'] = $row['cedula'];
            $res[$i]['nombre'] = $row['nombres'];
			$res[$i]['apellido'] = $row['apellidos'];
			$res[$i]['telefono'] = $row['telefono'];
            $res[$i]['fecha_creacion'] = $row['fecha_creacion'];
			$res[$i]['activo'] = $row['activo'];
			$i++;
		}
		return $res;
	}
    
    
    function AgregarCliente($sql, $correo, $password, $cedula, $nombres, $apellidos, $telefono)
	{
        $query="INSERT INTO clientes(correo, clave, cedula, nombres, apellidos, telefono, activo, eliminado ) 
                VALUES ('$correo', MD5('$password'), '$cedula', '$nombres', '$apellidos', '$telefono', '1', '0')";
        
        $sql->ExecQuery($query);
	}

    function AgregarClienteForm($sql, $correo, $clave, $cedula, $nombres, $apellidos, $telefono, $condicion)
    {
            $query="INSERT INTO clientes(correo, clave, cedula, nombres, apellidos, telefono, activo, eliminado ) 
                VALUES ('$correo', MD5('$clave'), '$cedula', '$nombres', '$apellidos', '$telefono', $condicion, '0')";


        $sql->ExecQuery($query);
    }

    
    
    function NumClientes($sql)
	{
		$res = array();
		$query="SELECT count(correo) as num
			FROM clientes 
			ORDER BY nombres";
		
		$resul=$sql->ExecQuery($query);

        if($row=$sql->FetchArray($resul))
		{
			$res = $row['num'];
		}
		return $res;
	}
    
    
	function ActualizarClientes($sql, $id, $correo, $clave, $cedula, $nombres, $apellidos, $telefono, $condicion)
	{
        if (!empty($clave)) {
             $query = "UPDATE clientes SET 
				correo='$correo', 
				clave='$clave', 
				cedula='$cedula', 
				nombres='$nombres',
				apellidos='$apellidos', 
				telefono='$telefono', 
				activo='$condicion' 
			WHERE id = ".$id;
        }else{

            $query = "UPDATE clientes SET 
				correo='$correo', 
				cedula='$cedula', 
				nombres='$nombres',
				apellidos='$apellidos', 
				telefono='$telefono', 
				activo='$condicion' 
			WHERE id = ".$id;
        }

		$sql->ExecQuery($query);
	}

    function EliminarClientes($sql, $id)
    {
            $query = "UPDATE clientes SET 
            eliminado = 1
			WHERE id = ".$id;

        $sql->ExecQuery($query);
    }

	

/*--- Funciones Para la Facturacion ---*/
	function obtenerDatosFacturacion($sql, $id)
	{
        $res = array();
		$query="SELECT id, correo, nombre, cedula, estado, direccion, direccion2, telefonos, observaciones 
                FROM cliente_facturacion 
                WHERE id=".$id;
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['correo'] = $row['correo'];
			$res[$i]['nombre'] = $row['nombre'];
			$res[$i]['cedula'] = $row['cedula'];
            $res[$i]['estado'] = $row['estado'];
            $res[$i]['direccion'] = $row['direccion'];
            $res[$i]['direccion2'] = $row['direccion2'];
            $res[$i]['telefonos'] = $row['telefonos'];
            $res[$i]['observaciones'] = $row['observaciones'];
			$i++;
		}
		return $res;
	}

    function obtenerDatosCliente($sql, $email)
    {
        $res = array();
        $query="SELECT id, correo, nombre, cedula, estado, direccion, direccion2, telefonos, observaciones 
                FROM cliente_facturacion 
                WHERE correo='$email'";

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id'] = $row['id'];
            $res[$i]['correo'] = $row['correo'];
            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['cedula'] = $row['cedula'];
            $res[$i]['estado'] = $row['estado'];
            $res[$i]['direccion'] = $row['direccion'];
            $res[$i]['direccion2'] = $row['direccion2'];
            $res[$i]['telefonos'] = $row['telefonos'];
            $res[$i]['observaciones'] = $row['observaciones'];
            $i++;
        }
        return $res;
    }

    function obtenerDatosClienteId($sql, $id)
    {
        $res = array();
        $query="SELECT * 
                FROM clientes 
                WHERE id=".$id;

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id'] = $row['id'];
            $res[$i]['correo'] = $row['correo'];
            $res[$i]['nombres'] = $row['nombres'];
            $res[$i]['apellidos'] = $row['apellidos'];
            $res[$i]['cedula'] = $row['cedula'];
            $res[$i]['activo'] = $row['activo'];
            $res[$i]['telefono'] = $row['telefono'];
            $i++;
        }
        return $res;
    }

    function obtenerDatosClienteEmail($sql, $email)
    {
        $res = array();
        $query="SELECT *  
                FROM clientes 
                WHERE correo='$email'";

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id'] = $row['id'];
            $i++;
        }
        return $res;
    }

    function ModificarDireccionEntrega($sql, $id_cliente, $direccion, $direccion2, $observaciones)
    {
        $query="UPDATE cliente_facturacion 
                SET direccion = '$direccion',
                direccion2= '$direccion2',
                observaciones = '$observaciones'
                WHERE id= ".$id_cliente;

        $sql->ExecQuery($query);
    }

    function ModificarClienteEntrega($sql, $id_cliente, $cedula, $nombre, $telefonos)
    {
        $query="UPDATE cliente_facturacion 
                SET cedula='$cedula',
                nombre = '$nombre',
                telefonos= '$telefonos'
                WHERE id= ".$id_cliente;

        $sql->ExecQuery($query);
    }
    
    function AgregarDatosFacturacion($sql, $correo, $nombres, $cedula, $telefonos)
	{

        $query="INSERT INTO cliente_facturacion(correo, nombre, cedula, telefonos) 
                VALUES ('$correo', '$nombres', '$cedula', '$telefonos')";

        $sql->ExecQuery($query);
        
        $query="SELECT LAST_INSERT_ID() as id;";
       
        $resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
            $res = $row['id'];
        }
		return $res;
	}

    function Listado($sql)
    {
        $result=array();

        $query="SELECT id, correo, nombres, apellidos, telefono, activo, eliminado 
                FROM clientes 
                WHERE eliminado=0";

        $resul=$sql->ExecQuery($query);

        $i=0;
        while ($row=$sql->FetchArray($resul)) {
            $result[$i]['id']=$row['id'];
            $result[$i]['correo']=$row['correo'];
            $result[$i]['nombres']=$row['nombres'];
            $result[$i]['apellidos']=$row['apellidos'];
            $result[$i]['telefono']=$row['telefono'];
            $result[$i]['activo']=$row['activo'];
            $result[$i]['eliminado']=$row['eliminado'];
            $i++;
        }

        return $result;
    }
	
}