<?php
class Noticias
{
/*--Funciones Variadas--*/
	public $num_noticias = 32;
	
	function fecha_actual()
	{
		$fecha=date("Y-m-d H:i:s");
		return $fecha;
	}
	
	public static function transformar_fecha_bd_js($fecha_bd)
	{
		$fecha_dia = substr($fecha_bd,8,2);
		$fecha_mes = substr($fecha_bd,5,2);
		$fecha_ano = substr($fecha_bd,0,4);
		$fecha_hr = substr($fecha_bd,11,2);
		$fecha_min = substr($fecha_bd,14,2);
		
		if(($fecha_hr == "00"))
		{$fecha_hr = "12"; $fecha_zona = "AM";}
		elseif($fecha_hr ==12)
		{$fecha_hr = 12; $fecha_zona = "PM";}
		elseif($fecha_hr >12)
		{$fecha_hr = $fecha_hr-12; $fecha_zona = "PM";}
		else
		{$fecha_zona = "AM";}
		
		$fecha_js = $fecha_dia."/".$fecha_mes."/".$fecha_ano." [".sprintf("%02d",$fecha_hr).":".$fecha_min." ".$fecha_zona."]";
		return $fecha_js;
	}
	
	public static function transformar_fecha_bd_persona($fecha_bd)
	{
		$fecha_dia = substr($fecha_bd,8,2);
		$fecha_mes = substr($fecha_bd,5,2);
		$fecha_ano = substr($fecha_bd,0,4);
		$fecha_hr = substr($fecha_bd,11,2);
		$fecha_min = substr($fecha_bd,14,2);
		
		if($fecha_mes == "01"){$fecha_mes_desc = "Enero";}
		elseif($fecha_mes == "02"){$fecha_mes_desc = "Febrero";}
		elseif($fecha_mes == "03"){$fecha_mes_desc = "Marzo";}
		elseif($fecha_mes == "04"){$fecha_mes_desc = "Abril";}
		elseif($fecha_mes == "05"){$fecha_mes_desc = "Mayo";}
		elseif($fecha_mes == "06"){$fecha_mes_desc = "Junio";}
		elseif($fecha_mes == "07"){$fecha_mes_desc = "Julio";}
		elseif($fecha_mes == "08"){$fecha_mes_desc = "Agosto";}
		elseif($fecha_mes == "09"){$fecha_mes_desc = "Septiembre";}
		elseif($fecha_mes == "10"){$fecha_mes_desc = "Octubre";}
		elseif($fecha_mes == "11"){$fecha_mes_desc = "Noviembre";}
		elseif($fecha_mes == "11"){$fecha_mes_desc = "Diciembre";}
		
		if(($fecha_hr == "00"))
		{$fecha_hr = "12"; $fecha_zona = "AM";}
		elseif($fecha_hr ==12)
		{$fecha_zona = "PM";}
		elseif($fecha_hr >12)
		{$fecha_hr = $fecha_hr-12; $fecha_zona = "PM";}
		else
		{$fecha_zona = "AM";}
		
		$fecha_persona = $fecha_mes_desc." ".$fecha_dia.", ".$fecha_ano." ".$fecha_hr.":".$fecha_min." ".$fecha_zona." ";
		return $fecha_persona;
	}
	
	public static function transformar_fecha_persona_bd($dia, $mes, $ano, $hora, $min, $zona)
	{
		if(($hora == 12) and ($zona =="AM"))
		{$hora = "00";}
		elseif(($hora == 12) and ($zona =="PM"))
		{}
		elseif($zona =="PM")
		{$hora = $hora+12;}
		
		$fecha_bd = $ano."-".$mes."-".$dia." ".$hora.":".$min.":00";
		
		return $fecha_bd;
	}
	
/*--Funciones de Administrador--*/
	function Agregar($sql, $titulo, $tw1, $tw2, $tw3, $sumario, $fecha, $lugar, $contenido, $autor, $link, $comentarios, $foto, $usuario_creacion)
	{
		$result=array();
		$fecha_creacion = $this->fecha_actual();
		
		$fecha_dia = substr($fecha,0,2);
		$fecha_mes = substr($fecha,3,2);
		$fecha_ano = substr($fecha,6,4);
		$fecha_hr = substr($fecha,12,2);
		$fecha_min = substr($fecha,15,2);
		$fecha_zona = substr($fecha,18,2);
		
		$fecha_bd = $this->transformar_fecha_persona_bd($fecha_dia, $fecha_mes, $fecha_ano, $fecha_hr, $fecha_min, $fecha_zona);
		
		$query="INSERT INTO noticias (titulo, tw1, tw2, tw3,  sumario, fecha, lugar, contenido, autor, link, comentarios, foto, destacado, publicado, eliminado, usuario_creacion, fecha_creacion, fecha_publicacion, num_visita) 
			VALUES ('$titulo', '$tw1','$tw2','$tw3', '$sumario', '$fecha_bd', '$lugar', '$contenido', '$autor', '$link', '$comentarios', '$foto', 0, 0, 0, '$usuario_creacion', '$fecha_creacion', '$fecha_creacion', 0)";
		
		$sql->ExecQuery($query);
	}
	
	function Modificar($sql, $codigo, $titulo, $tw1, $tw2, $tw3, $sumario, $fecha, $lugar, $contenido, $autor, $link, $comentarios, $foto=0)
	{
		$fecha_dia = substr($fecha,0,2);
		$fecha_mes = substr($fecha,3,2);
		$fecha_ano = substr($fecha,6,4);
		$fecha_hr = substr($fecha,12,2);
		$fecha_min = substr($fecha,15,2);
		$fecha_zona = substr($fecha,18,2);
		
		$fecha_bd = $this->transformar_fecha_persona_bd($fecha_dia, $fecha_mes, $fecha_ano, $fecha_hr, $fecha_min, $fecha_zona);
		
		$query="UPDATE noticias 
			SET titulo = '".$titulo."', 
				tw1 = '".$tw1."', 
				tw2 = '".$tw2."', 
				tw3 = '".$tw3."', 
				sumario = '".$sumario."', 
				fecha = '".$fecha_bd ."', 
				lugar = '".$lugar."', 
				contenido = '".$contenido."', 
				autor = '".$autor."', 
				link = '".$link."', ";
		if($foto)
		{$query.="foto = '".$foto."', ";}
		
		$query.=" comentarios = ".$comentarios."
			WHERE codigo=$codigo";
		
		$sql->ExecQuery($query);
	}
	
	function Publicar($sql, $codigo, $operacion)
	{
		$fecha_publicacion = $this->fecha_actual();
		$query="UPDATE noticias SET publicado=$operacion, fecha_publicacion='$fecha_publicacion' WHERE codigo=$codigo";
		$sql->ExecQuery($query);
	}
	
	function Eliminar($sql, $codigo)
	{
		$query="UPDATE noticias SET eliminado=1 WHERE codigo=$codigo";
		$sql->ExecQuery($query);
	}
	
	function ConsultarAdmin($sql, $codigo)
	{	
		$result=array();
		$query="SELECT codigo, titulo, titulo_link, tw1, tw2, tw3, sumario, contenido, lugar, link, num_tw, fecha, comentarios, foto, autor, destacado, publicado, num_visita 
			FROM noticias 
			WHERE eliminado=0 AND codigo=$codigo";
		
		$resul=$sql->ExecQuery($query);
		
		while($row=$sql->FetchArray($resul))
		{
			$result['codigo']=$row['codigo'];
			$result['titulo']=$row['titulo'];
			$result['titulo_link']=$row['titulo_link'];
			$result['tw1']=$row['tw1'];
			$result['tw2']=$row['tw2'];
			$result['tw3']=$row['tw3'];
			$result['sumario']=$row['sumario'];
			$result['contenido']=$row['contenido'];
			$result['lugar']=$row['lugar'];
			$result['link']=$row['link'];
			$result['num_tw']=$row['num_tw'];
			$result['fecha']=$this-> transformar_fecha_bd_persona($row['fecha']);
			$result['fecha_bd']=$this-> transformar_fecha_bd_js($row['fecha']);
			$result['comentarios']=$row['comentarios'];
			$result['foto']=$row['foto'];
			$result['autor']=$row['autor'];
			$result['destacado']=$row['destacado'];
			$result['publicado']=$row['publicado'];
			$result['num_visita']=$row['num_visita'];
		}
		return $result;
	}
	
	function ListadoAdmin($sql, $pagina=0, $tipo=1, $pub_futuro=0)
	{
		$num_articulo = $this -> num_noticias;
		$articulo_inic = $num_articulo*$pagina;
		
		$result=array();
		$query="SELECT N.codigo, N.titulo, N.titulo_link, N.sumario, N.fecha, N.link, N.num_tw, N.comentarios, N.foto, N.destacado, N.publicado, N.eliminado, N.num_visita, U.nombre, U.apellido 
			FROM noticias N
			INNER JOIN usuarios U ON U.id_usuario = N.usuario_creacion
			WHERE N.eliminado=0 ";
		if($tipo == 2)
		{$query.=" AND publicado=0 ";}
		
		if($pub_futuro==1)
		{
			$query .= " AND fecha>='". $this->fecha_actual()."'";
			$query.=" ORDER BY N.fecha DESC LIMIT $articulo_inic, $num_articulo";
		}
		else
		{
			$query .= " AND fecha<='". $this->fecha_actual()."'";
			$query.=" ORDER BY N.fecha_publicacion DESC LIMIT $articulo_inic, $num_articulo";
		}
        
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']=$row['titulo'];
			$result[$i]['titulo_link']=$row['titulo_link'];
			$result[$i]['sumario']=$row['sumario'];
			$result[$i]['fecha']=$this-> transformar_fecha_bd_persona($row['fecha']);
			$result[$i]['link']=$row['link'];
			$result[$i]['num_tw']=$row['num_tw'];
			$result[$i]['comentarios']=$row['comentarios'];
			$result[$i]['foto']=$row['foto'];
			$result[$i]['destacado']=$row['destacado'];
			$result[$i]['publicado']=$row['publicado'];
			$result[$i]['eliminado']=$row['eliminado'];
			$result[$i]['num_visita']=$row['num_visita'];
			$result[$i]['usuario']=$row['nombre']." ".$row['apellido'];
			$i++;
		}
		return $result;
	}
	
	function ListadoNumNoticiasAdmin($sql, $categoria=0, $pagina=0, $tipo)
	{
		$fecha = $this->fecha_actual();
		
		$result=array();
		$query="SELECT count(codigo) AS num_noticias
			FROM noticias 
			WHERE eliminado=0";
		
		if($categoria>0)
		{$query .= " AND codigo_categoria=$categoria";}
		
		if($tipo==2)
		{$query .= " AND publicado=0 ";}
		elseif($tipo==3)
		{$query .= " AND fecha>='$fecha' ";}
		
		$query .= " ORDER BY codigo DESC";
		
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['num_noticias']=$row['num_noticias'];
			$result['num_paginas'] = ceil($row['num_noticias']/$this -> num_noticias);
		}
		return $result;
	}
	
	function crearTituloLink($sql, $codigo, $titulo, $fecha)
	{
		if($codigo AND $titulo AND $fecha)
		{
			$result=array();
			$fecha_creacion = $this->fecha_actual();
			
			$fecha_dia = substr($fecha,0,2);
			$fecha_mes = substr($fecha,3,2);
			$fecha_ano = substr($fecha,8,2);
			
			$tt = sanear_string($titulo);
			
			$titulo_link = "/h/".$fecha_ano."/".$fecha_mes."/".$fecha_dia."/".$codigo."/".$tt."/";
			
			$query="UPDATE noticias SET titulo_link='$titulo_link' WHERE codigo=$codigo";
			$sql->ExecQuery($query);
		}
	}
	
/*--Funciones Publicas--*/
	function Listado($sql, $categoria=0, $pagina=0)
	{
		$fecha = $this->fecha_actual();
		
		$num_articulo = $this -> num_noticias;
		$articulo_inic = $num_articulo*$pagina;
		
		$result=array();
		/*
        $query="SELECT N.codigo, C.descripcion, N.codigo_categoria, N.codigo_subcategoria, N.antetitulo, N.titulo, N.titulo_link, N.lugar, N.sumario, N.fecha, N.link, N.num_tw, N.comentarios, N.foto, N.destacado, N.publicado, N.eliminado, N.num_visita, N.fecha_publicacion 
			FROM noticias N 
			INNER JOIN categorias C ON N.codigo_categoria = C.codigo AND C.activo=1 AND C.eliminado=0
			WHERE N.publicado=1 AND N.destacado=0 AND N.eliminado=0 ";
		*/
        
        $query="SELECT N.codigo, N.titulo, N.titulo_link, N.lugar, N.sumario, N.fecha, N.link, N.num_tw, N.comentarios, N.foto, N.destacado, N.publicado, N.eliminado, N.num_visita, N.fecha_publicacion 
			FROM noticias N 
			WHERE N.publicado=1 AND N.destacado=0 AND N.eliminado=0 ";
		
        
		if($categoria>0)
		{$query .= " AND codigo_categoria=$categoria";}
		
		$query .= " AND fecha<='$fecha' ";
		$query .= " ORDER BY N.fecha_publicacion DESC LIMIT $articulo_inic, $num_articulo";
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			//$result[$i]['descripcion']=$row['descripcion'];
			//$result[$i]['codigo_categoria']=$row['codigo_categoria'];
			//$result[$i]['codigo_subcategoria']=$row['codigo_subcategoria'];
			//$result[$i]['antetitulo']= $row['antetitulo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['titulo_link']= $row['titulo_link'];
			$result[$i]['lugar']= $row['lugar'];
			$result[$i]['sumario']= $row['sumario'];
			$result[$i]['fecha']= $this-> transformar_fecha_bd_persona($row['fecha']);
			$result[$i]['link']= $row['link'];
			$result[$i]['num_tw']= $row['num_tw'];
			$result[$i]['comentarios']= $row['comentarios'];
			$result[$i]['foto']= $row['foto'];
			$i++;
		}
		return $result;
	}
	
	function LasMasLeidas($sql, $dias=1, $num_articulo=10, $categoria=0)
	{
		$articulo_inic = $num_articulo*$pagina;
		
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-$dias day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo, titulo, titulo_link, foto, num_visita 
			FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 ";

		if($categoria>0)
		{$query .= " AND codigo_categoria=$categoria";}
		
		$query .= " ORDER BY num_visita DESC 
				LIMIT 0 , $num_articulo";
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['titulo_link']= $row['titulo_link'];
			$result[$i]['foto']= $row['foto'];
			$result[$i]['num_visita']= $row['num_visita'];
			$i++;
		}
		return $result;
	}
	
	function ListadoNumNoticias($sql, $categoria=0, $pagina)
	{
		$fecha = $this->fecha_actual();
	
		$result=array();
		$query="SELECT count(codigo) AS num_noticias
			FROM noticias 
			WHERE publicado=1 AND destacado=0 AND eliminado=0 AND fecha<='$fecha' ";
		
		if($categoria>0)
		{$query .= " AND codigo_categoria=$categoria";}
		
		
		$query .= " ORDER BY codigo DESC";
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['num_noticias']=$row['num_noticias'];
			$result['num_paginas'] = ceil($row['num_noticias']/$this -> num_noticias);
		}
		
		return $result;
	}
	
	function Consultar($sql, $codigo)
	{
		$fecha = $this->fecha_actual();
		
		$result=array();
		/*
        $query="SELECT codigo, codigo_categoria, codigo_subcategoria, antetitulo, titulo, titulo_link, tw1, tw2, tw3,  sumario,  contenido, lugar, fecha, comentarios, foto, link, num_tw, autor, destacado, publicado, num_visita 
			FROM noticias 
			WHERE publicado=1 AND eliminado=0 AND codigo=$codigo AND fecha<='$fecha'";
		*/
        
        $query="SELECT codigo, titulo, titulo_link, tw1, tw2, tw3,  sumario,  contenido, lugar, fecha, comentarios, foto, link, num_tw, autor, destacado, publicado, num_visita 
			FROM noticias 
			WHERE publicado=1 AND eliminado=0 AND codigo=$codigo AND fecha<='$fecha'";
		
		$resul=$sql->ExecQuery($query);
		
		while($row=$sql->FetchArray($resul))
		{
			$result['codigo']=$row['codigo'];
			//$result['codigo_categoria']= $row['codigo_categoria'];
			//$result['codigo_subcategoria']= $row['codigo_subcategoria'];
			//$result['antetitulo']=$row['antetitulo'];
			$result['titulo']=$row['titulo'];
			$result['titulo_link']=$row['titulo_link'];
			$result['tw1']=$row['tw1'];
			$result['tw2']=$row['tw2'];
			$result['tw3']=$row['tw3'];
			$result['sumario']=$row['sumario'];
			$result['contenido']=$row['contenido'];
			$result['lugar']=$row['lugar'];
			$result['fecha']=$this-> transformar_fecha_bd_persona($row['fecha']);
			$result['fecha_bd']=$this-> transformar_fecha_bd_js($row['fecha']);
			$result['comentarios']=$row['comentarios'];
			$result['foto']=$row['foto'];
			$result['link']=$row['link'];
			$result['num_tw']=$row['num_tw'];
			$result['autor']=$row['autor'];
			$result['destacado']=$row['destacado'];
			$result['publicado']=$row['publicado'];
			$result['num_visita']=$row['num_visita'];
		}
		return $result;
	}


	function Destacado($sql)
	{
		$result=array();
		$query="SELECT codigo, codigo_categoria, codigo_subcategoria, antetitulo, titulo, titulo_link, sumario, fecha, comentarios, foto, destacado, publicado, eliminado, num_visita 
				FROM noticias 
				WHERE publicado=1 AND destacado=1 AND eliminado=0
				ORDER BY codigo DESC";

		$resul=$sql->ExecQuery($query);
		
		while($row=$sql->FetchArray($resul))
		{
			$result['codigo']=$row['codigo'];
			$result['codigo_subcategoria']= $row['codigo_subcategoria'];
			$result['antetitulo']= $row['antetitulo'];
			$result['titulo']=$row['titulo'];
			$result['titulo_link']=$row['titulo_link'];
			$result['sumario']=$row['sumario'];
			$result['fecha'] = $this-> transformar_fecha_bd_persona($row['fecha']);
			$result['comentarios']=$row['comentarios'];
			$result['foto']=$row['foto'];
		}
		return $result;
	}
	
	function ContadorVisitas($sql, $codigo)
	{
		$query="UPDATE noticias SET num_visita = (num_visita + 1) WHERE codigo = $codigo";
		$sql->ExecQuery($query);
	}
	
/*--Redes Sociales Twitter--*/
	
	function PublicarTwittear($sql, $codigo, $titulo)
	{
        /*
		require_once('TWAPI/TwitterAPIExchange.php');
		
		if($codigo AND $titulo)
		{
			/*paoloignoto2* /
			$settings = array(
				'oauth_access_token' => "140486375-ZxdflJjxqDpO8ZtfRHbdCnF1bFVdkYfWs6sISPPA",
				'oauth_access_token_secret' => "3VnDvR5izIHnv0MMdsjxYhLiqVaVsxHELAWp0CTmWQpwQ",
				'consumer_key' => "phSKGyrOPbl3fe9fyPYOiA",
				'consumer_secret' => "g3Myb7MBT0lrRONv2vELoCHt5B7H8Ung3zVXtUyo");
			/** /
			
			/*Actualidad 360* /
			$settings = array(
				'oauth_access_token' => "1534584325-N5zdLTcqkAcYgMBtZeSvaxR3q8IHk3sKX4MKFGZ",
				'oauth_access_token_secret' => "Y793mJ7Io4bJsAFGVH6i02BJo1ihhpFOBfjVY7HhZfYvW",
				'consumer_key' => "UNOZncx2SoKPlRqoIIFRUhd4d",
				'consumer_secret' => "wsnW2q7IjiF4WzKHEPTrqZlHSXYZSsL8iOpNkhc7U8iV6CBp07");
			/** /
			
			$url = 'https://api.twitter.com/1.1/statuses/update.json';
			$requestMethod = 'POST';
			
			$tipo = rand(1,3);
			
			if($tipo==1)	{$postfields = array('status' => "$titulo Via actualidad360.com/?n=$codigo");}
			elseif($tipo==2){$postfields = array('status' => "$titulo - actualidad360.com/?n=$codigo");}
			elseif($tipo==3){$postfields = array('status' => "$titulo -- actualidad360.com/?n=$codigo");}
			else			{$postfields = array('status' => "$titulo. actualidad360.com/?n=$codigo");}
			
			$this -> ContadorTweetPublicado($sql, $codigo);
			
			/** Perform a POST request and echo the response ** /
			$twitter = new TwitterAPIExchange($settings);
			
			$response = $twitter->buildOauth($url, $requestMethod)
				->setPostfields($postfields)
				->performRequest();
		}
        */
	}
	
	
	function ContadorTweetPublicado($sql, $codigo)
	{
		$query="UPDATE noticias SET num_tw = (num_tw + 1) WHERE codigo = $codigo";
		$sql->ExecQuery($query);
	}
	
	function ListadoParaTwittear($sql, $dias=1, $num_articulo=15, $categoria=0)
	{
		$articulo_inic = $num_articulo*$pagina;
		
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-$dias day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo, titulo, foto, num_tw 
			FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 ";

		if($categoria>0)
		{$query .= " AND codigo_categoria=$categoria";}
		
		$query .= " ORDER BY num_tw DESC 
				LIMIT 0 , $num_articulo";
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['num_tw']= $row['num_tw'];
			$i++;
		}
		return $result;
	}
	
	//1.-Nacionales / 3.-Internacionales
	function Obterner_Tweet_Nac_Int($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-18 hours"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
				FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
				AND (codigo_categoria=1 or codigo_categoria=3) 
				ORDER BY RAND() LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	//4.-Economia / 5.-Deporte
	function Obterner_Tweet_Eco_Dep($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-1 day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
				FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
				AND (codigo_categoria=4 or codigo_categoria=5) 
				ORDER BY RAND() LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	
	//7.-Vida / 8.-Entretenimiento / 10.-Ciencia y Tecnologia
	function Obterner_Tweet_Vid_Ent_Tec($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-3 day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
				FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
				AND (codigo_categoria=7 or codigo_categoria=8 or codigo_categoria=10) 
				ORDER BY RAND() LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		while($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	//21.-Animaleak / 22.-Mujermamam
	function Obterner_Tweet_Ani_MM($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-29 day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
				FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
				AND (codigo_categoria=21 or codigo_categoria=22) 
				ORDER BY RAND() LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		while($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	//21.-Animaleaks
	function Obterner_Tweet_AL($sql)
	{
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
			FROM noticias 
			WHERE publicado=1 AND eliminado=0 AND codigo_categoria=21 
			ORDER BY RAND()	LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	//22.-Mujermamam
	function Obterner_Tweet_MM($sql)
	{
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
			FROM noticias 
			WHERE publicado=1 AND eliminado=0 AND codigo_categoria=22 
			ORDER BY RAND() LIMIT 1";
		
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['codigo_categoria']=$row['codigo_categoria'];
			$result['codigo']=$row['codigo'];
			$result['titulo']= $row['titulo'];
			$result['tw1']= $row['tw1'];
			$result['tw2']= $row['tw2'];
			$result['tw3']= $row['tw3'];
			$result['num_tw']= $row['num_tw'];
		}
		return $result;
	}
	
	
	/*
	//22.-Mujermamam
	function Obterner_Tweet_MM($sql)
	{
		$result=array();
		$query="SELECT codigo_categoria, codigo, titulo, tw1, tw2, tw3, foto, num_tw 
			FROM noticias 
			WHERE publicado=1 AND eliminado=0 AND codigo_categoria=22 
			ORDER BY RAND() 
			LIMIT 10";
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo_categoria']=$row['codigo_categoria'];
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['tw1']= $row['tw1'];
			$result[$i]['tw2']= $row['tw2'];
			$result[$i]['tw3']= $row['tw3'];
			$result[$i]['num_tw']= $row['num_tw'];
			$i++;
		}
		return $result;
	}
	
	
	/*
	//7.-Vida / 8.-Entretenimiento / 21.-Animaleak / 22.-Mujermamam / 10.-Ciencia y Tecnologia
	function ListadoParaTwittear2($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-3 day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo, titulo, foto, num_tw 
			FROM noticias 
			WHERE fecha 
			BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
			AND (codigo_categoria=7 or codigo_categoria=8 or codigo_categoria=21 or codigo_categoria=22 or codigo_categoria=10) 
			ORDER BY RAND() 
			LIMIT 10";
		
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['num_tw']= $row['num_tw'];
			$i++;
		}
		return $result;
	}
	
	//1.-Nacionales / 3.-Internacionales / 4.-Economia / 5.-Deporte
	function ListadoParaTwittear1($sql)
	{
		$fecha_i=date("Y-m-d 00:00:00",strtotime("-1 day"));
		$fecha_f=date("Y-m-d 23:59:59");
		
		$result=array();
		$query="SELECT codigo, titulo, foto, num_tw 
				FROM noticias 
				WHERE fecha 
				BETWEEN '$fecha_i' AND '$fecha_f' AND publicado=1 AND eliminado=0 
				AND (codigo_categoria=1 or codigo_categoria=3 or codigo_categoria=4 or codigo_categoria=5) 
				ORDER BY RAND() 
				LIMIT 10";
		
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['codigo']=$row['codigo'];
			$result[$i]['titulo']= $row['titulo'];
			$result[$i]['num_tw']= $row['num_tw'];
			$i++;
		}
		return $result;
	}
	
	*/
/*--Redes Sociales Facebook--*/
	
}