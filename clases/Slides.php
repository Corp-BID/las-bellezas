<?php
class Slides
{
	function AgregarSlides($sql, $imagen, $titulo, $descripcion, $boton, $link)
	{
		$query="INSERT INTO slides(imagen, titulo, descripcion, boton, link) VALUES 
                                ('$imagen', '$titulo', '$descripcion', '$boton', '$link', 1, 0)";
        
        $sql->ExecQuery($query);
	}
    
    function ModificarSlides($sql, $imagen, $titulo, $descripcion, $boton, $link, $activo)
	{
		$query="UPDATE slides 
                SET imagen = $imagen, 
                    titulo = $titulo, 
                    descripcion = $descripcion, 
                    boton = $boton, 
                    link = $link, 
                    activo='$activo'
                WHERE id='$id";
        
        $sql->ExecQuery($query);
	}

    function ObtenerSlidesPublic($sql)
    {
        $res = NULL;

        $query="SELECT id, imagen, titulo, descripcion, boton, link, activo, eliminado
                FROM slides 
                WHERE activo='1' AND eliminado='0'
                ORDER BY id DESC";

        $resul=$sql->ExecQuery($query);
        $i=0;
        while($row=$sql->FetchArray($resul))
        {
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['titulo'] = $row['titulo'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['boton'] = $row['boton'];
            $res[$i]['activo'] = $row['activo'];
            $i++;
        }
        return $res;
    }
    
	function ObtenerSlides($sql, $condicion)
	{
		$res = NULL;
        
        $query="SELECT id, imagen, titulo, descripcion, boton, link, activo, eliminado
                FROM slides 
                WHERE activo='1' AND eliminado='$condicion'
                ORDER BY id DESC";
        
        $resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['titulo'] = $row['titulo'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['boton'] = $row['boton'];
            $res[$i]['activo'] = $row['activo'];
            $i++;
		}
		return $res;
	}
    
    function CambiarEstatus($sql, $id, $activo)
	{
		$query="UPDATE slides 
                SET activo='$activo'
                WHERE id='$id'";
                
        $sql->ExecQuery($query);
	}
    
    function Eliminar($sql, $id)
	{
		$query="UPDATE slides 
                SET eliminado=1
                WHERE id='$id'";
                
        $sql->ExecQuery($query);
	}
}
