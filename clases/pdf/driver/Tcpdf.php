<?php
/**
 * Actualizacion de la libreria PDF usada para generar los documentos, pasando de la libreria "class.ezpdf.php" a TCPDF
 * 
 * Clase programada con requerimiento minimo de PHP 5.3
 * @package Pdf
 */ 

// Definiendo variables de configuracion basicas para la libreria tcpdf.php

define('K_TCPDF_EXTERNAL_CONFIG',true);

/**
* Installation path.
* By default it is automatically calculated but you can also set it as a fixed string to improve performances.
*/
define ('K_PATH_MAIN', realpath(dirname(__DIR__)."/tcpdf") . '/');

/**
 * URL path to tcpdf installation folder (http://localhost/tcpdf/).
 * By default it is automatically calculated but you can also set it as a fixed string to improve performances.
 */
// define ('K_PATH_URL', $k_path_url);

/**
 * cache directory for temporary files (url path)
 */
// define ('K_PATH_URL_CACHE', K_PATH_URL.'cache/');

/**
 * cache directory for temporary files (full path)
 */
define ('K_PATH_CACHE', K_PATH_MAIN.'cache/');

/**
 * path for PDF fonts
 * use K_PATH_MAIN.'fonts/old/' for old non-UTF8 fonts
 */
define ('K_PATH_FONTS', K_PATH_MAIN.'fonts/');


/**
 *images directory
 */
define ('K_PATH_IMAGES', K_PATH_MAIN.'images/');

/**
 * blank image
 */
define ('K_BLANK_IMAGE', K_PATH_IMAGES.'_blank.png');
/**
 * height of cell repect font height
 */
define('K_CELL_HEIGHT_RATIO', 1.25);

/**
 * title magnification respect main font size
 */
// define('K_TITLE_MAGNIFICATION', 1.3);

/**
 * reduction factor for small font
 */
define('K_SMALL_RATIO', 2/3);

/**
 * set to true to enable the special procedure used to avoid the overlappind of symbols on Thai language
 */
define('K_THAI_TOPCHARS', false);

/**
 * if true allows to call TCPDF methods using HTML syntax
 * IMPORTANT: For security reason, disable this feature if you are printing user HTML content.
 */
define('K_TCPDF_CALLS_IN_HTML', false);
// Fin de la definicion de variables


// require_once(realpath(dirname(__FILE__).'/tcpdf/config/tcpdf_config_cne.php'));
require_once(realpath(dirname(__DIR__).'/tcpdf/config/lang/spa.php'));
require_once(realpath(dirname(__DIR__).'/tcpdf/tcpdf.php'));

//extends base_Base
class pdf_driver_Tcpdf extends TCPDF
{
 protected $pdf_modulo;
 protected $lang='spa';
 
 public function __construct(pdf_Pdf $pdf,$orientation='P', $unit='mm', $format='LETTER', $unicode=true, $encoding='UTF-8', $diskcache=false) 
 {
  $this->pdf_modulo=$pdf;
  
  parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  
    //set some language-dependent strings
  require_once(realpath(dirname(__DIR__)."/tcpdf/config/lang/{$this->lang}.php"));
  $this->setLanguageArray($l);
 }
 public function Header()
 {
  if(!($this->pdf_modulo->Header()))
  parent::Header();
  
 }
 
 public function Footer() 
 {
  if(!($this->pdf_modulo->Footer()))
   parent::Footer($this);
 }
}