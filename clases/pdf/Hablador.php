<?php
include_once(realpath(__DIR__)."/Pdf.php");
//
class pdf_Hablador extends pdf_Pdf
{
 protected $header_xobjid=false;
 protected $margin= array('left'=>15,'right'=>15,'top'=>17,'BOTTOM'=>15);	
 protected $template_img = NULL;
 protected $paleta, $sql;
 protected $edo, $mun, $par;
 protected $plan, $numPlam;
 
 
 
 public function __construct($sql,Paleta $paleta=NULL)
 {
  $this->sql= $sql;
  if($paleta)
  {$this->setPaleta($paleta);}
  
  parent::__construct();
 }
 
 protected function setPaleta(Paleta $paleta)
 {$this->paleta= $paleta;}
 
 public function Footer() 
 {
  // Position at 15 mm from bottom
  $this->pdf->SetY(-15);
 // Set font
  $this->pdf->SetFont('helvetica', 'I', 8);
 // Page number
 $this->pdf->Cell(0, 10, "Plan {$this->numPlam}  -  Página ".$this->pdf->getAliasNumPage().'/'.$this->pdf->getAliasNbPages(),
                  0, false, 'C', 0, '', 0, false, 'T', 'M');
  
  return true;
 }
 
 public function Header()
 {
   if ($this->header_xobjid === false)
   {
	$margin= $this->pdf->getMargins();
	$this->header_xobjid = $this->pdf->startTemplate($this->obtenerAnchoMargenPx(), $margin['top']);
	$headerfont = $this->pdf->getHeaderFont();
	$headerdata = $this->pdf->getHeaderData();

	// set starting margin for text data cell

	$cw = 10;
	$cell_height=15;
	$header_x=0;
	$page_width=216;
	$this->pdf->SetTextColorArray($this->header_text_color);
	// header title
	$this->pdf->SetX($margin['left']);
	$this->pdf->SetFont('helvetica', 'B', 15);
	$this->pdf->Cell(200, $cell_height, 'Uniprint SA', 0, 1, 'L', 0, '', 0);
	$this->pdf->SetY(0);
	$this->pdf->Cell($page_width - $margin['right'], $cell_height, 'Colección Bicentenaria 2.016 - 2.017', 0, 1, 'R', 0, '', 0);
	$this->pdf->SetLineStyle(array('width' => 0.35 , 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => 'black'));
	$this->pdf->SetX($margin['left']);
	$this->pdf->Cell($page_width-$margin['right'] - $margin['left'], 1, '', 'T', 0, 'C');
	$this->pdf->endTemplate();
   }
   // print header template
   $x = 0;
   $this->pdf->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);

  return TRUE;
 }
 
 // Colored table
 public function printTable($header,$data) 
 {
  $font_size= $this->pdf->getFontSizePt();
  $magins=$this->pdf->getMargins();
	//   $this->pdf->SetX(15);
    // Colors, line width and bold font
  $this->pdf->SetFillColor(255, 0, 0);
  $this->pdf->SetTextColor(255);
  $this->pdf->SetDrawColor(0, 0, 0);
  $this->pdf->SetLineWidth(0.3);
  $this->pdf->SetFont('', 'B',11);
  // Header
  $w = array(30, 40, 75, 35);
  $num_headers = count($header);
  for($i = 0; $i < $num_headers; ++$i)
  {$this->pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);}
  
  $this->pdf->Ln();
  // Color and font restoration
  $this->pdf->SetFillColor(224, 235, 255);
  $this->pdf->SetTextColor(0);
  $this->pdf->SetFont('');
  // Data
  $fill = 0;
  $total_libro=0;
  $a=ord ('A');
  foreach($data as $k=>$row)
  {
   //verficamos si la caja cabe
   if( ($this->pdf->GetY() + $magins['bottom']+8) > 279)
   {$this->pdf->Ln();}
   $this->pdf->Cell($w[0], 6, chr($a + $k ), 'LR', 0, 'C', $fill);
   $this->pdf->Cell($w[1], 6, $row['grado'], 'LR', 0, 'C', $fill);
   $this->pdf->Cell($w[2], 6, $row['nombre_combo'], 'LR', 0, 'C', $fill);
  // $this->pdf->Cell($w[3], 6, $row['grado'], 'LR', 0, 'C', $fill);
   $this->pdf->Cell($w[3], 6, $row['cantidad_cargada'], 'LR', 0, 'C', $fill);
   $total_libro += $row['cantidad_cargada'];
   
   $this->pdf->Ln();
   $fill=!$fill;
  }
  
  $this->pdf->Cell(array_sum($w), 0, '', 'T');
  //verficamos si la caja cabe
  if( ($this->pdf->GetY() + $magins['bottom']+8) > 279)
  {$this->pdf->AddPage();}
  else
  {$this->pdf->SetX(array_sum($w)  -  $w[2]-  $w[3] +$magins['left']);}
  $this->pdf->Cell($w[2], 6, 'TOTAL', '', 0, 'C', 0);
  $this->pdf->Cell($w[3], 6, $total_libro, 'LRB', 0, 'C', 0);
  $this->pdf->Ln();
  $this->pdf->SetFontSize($font_size);
 }
 
 protected function imprimitTablaInt($paleta,$data)
 {
  $this->pdf->SetFont('helvetica', '', 14);
  $this->pdf->printTemplate($this->template_img, '', $this->pdf->GetY()-3, 0, 0, '', '', false);
  $this->pdf->writeHTMLCell(0, 0,25, '', "Paleta <b>{$paleta->id}</b>", 0, 0, false,true, 'L');
  $this->pdf->Ln();
  
  
  $header = array('Segmento', 'Grado', 'Descripcion', 'Cantidad');
  //$data=array();
  //$data[]= array('Paquete', 'Num de Empaques', 'Materia',66,22121);
  //$data[]= array('Grupo', 'Num de Empaques', 'Materia',66,22121);
  $this->printTable($header,$data);
  //$this->printTable($header,$data);
 }
 protected function imprimitPaleta($paleta, $data)
 {
     $centro = substr($paleta->centro, 0, 300);
     $responsable = substr($paleta->responsable, 0, 300);
     
     
     
     $this->pdf->SetFont('helvetica', '', 20);
   $this->pdf->writeHTML("Estado: <b>{$paleta->nombre_estado}</b>",true, false, true, false, '');
   $this->pdf->writeHTML("Municipio:  <b>{$paleta->nombre_municipio}</b>");
   $this->pdf->writeHTML("Paleta:  <b>".$paleta->id."</b>");
   
  $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'phase' => 10, 'dash' => 0, 'color' => array(0, 0, 0));
  $this->pdf->Line(10, $this->pdf->getY(), 200, $this->pdf->getY(), $style);
  
  $this->pdf->writeHTML('Centro de Acopio', true,false,false,false, 'C');
  $this->pdf->writeHTML("Estado: <b>{$paleta->nombre_estado_acopio}</b>",true, false, true, false, '');
  $this->pdf->writeHTML("Municipio:  <b>{$paleta->nombre_municipio_acopio}</b>");
  $this->pdf->Ln();
  $this->pdf->SetFont('helvetica', '', 15);
  $this->pdf->writeHTML("Centro:  <b>{$centro}</b>");
  $this->pdf->writeHTML("Responsable:  <b>{$responsable}</b>");
  $this->pdf->Ln();
  $this->pdf->Line(10, $this->pdf->getY(), 200, $this->pdf->getY(), $style);
  $this->pdf->Ln();
  
  $this->pdf->writeHTMLCell(0, 0,'', '', "Lista de Empaque", 0, 1, false,true, 'C'); 
  $this->imprimitTablaInt($paleta,$data);
  
 }
 
 protected function impPagina($paleta)
 {
     
     $centro = substr($paleta->centro, 0, 150);
     
     
  //$this->pdf->Ln();
  //$this->pdf->Ln();
  $this->pdf->SetFont('helvetica', '', 20);
  $this->pdf->writeHTML("Estado: <b>{$paleta->nombre_estado}</b>",true, false, true, false, '');
  $this->pdf->writeHTML("Municipio:  <b>{$paleta->nombre_municipio}</b>");
//  $this->pdf->writeHTML("Parroquia:  <b>{$this->par}</b>");
  
  $this->pdf->Ln();
  
  $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'phase' => 10, 'dash' => 0, 'color' => array(0, 0, 0));
  $this->pdf->Line(10, $this->pdf->getY(), 200, $this->pdf->getY(), $style);
  
  $this->pdf->writeHTML('Centro de Acopio', true,false,false,false, 'C');
  $this->pdf->writeHTML("Estado: <b>{$paleta->nombre_estado_acopio}</b>",true, false, true, false, '');
  $this->pdf->writeHTML("Municipio:  <b>{$paleta->nombre_municipio_acopio}</b>");
  $this->pdf->Ln();
  $this->pdf->SetFont('helvetica', '', 18);
  $this->pdf->writeHTML("Centro:  <b>{$centro}</b>");
  $this->pdf->Ln();
  $this->pdf->Line(10, $this->pdf->getY(), 200, $this->pdf->getY(), $style);
  $this->pdf->Ln();
  
  $this->pdf->SetFont('helvetica', '', 25);
  $this->pdf->writeHTML('PALETA', true,false,false,false, 'C');
  $this->pdf->SetFont('helvetica', 'B', 200);
  $this->pdf->writeHTML( $paleta->id , false,false,false,false, 'C');
  //$this->pdf->writeHTML( 2222, false,false,false,false, 'C');
  $this->pdf->SetFont('helvetica', 'B', 25);
  
  $this->pdf->setY(175);
  $this->pdf->Line(10, 210, 200, 210, $style);
  
  
  $this->pdf->writeHTML("Paleta {$paleta->consecutivo} de {$paleta->total_ca}", false,false,false,false, 'C');
  
 }
 
 protected function PrinterHablador($paleta,$arreglo_combo)
 {
  // agregamos la pagina si es necesario
  if($this->pdf->getY() > 27)
  {$this->pdf->AddPage();}
   

 // Primeda pagina
   $this->pdf->StartTransform();
   $this->imprimitPaleta($paleta,$arreglo_combo);
    
   $this->pdf->StopTransform();
// FIN de la Primeda pagina

	
   $this->pdf->AddPage();
//   $this->pdf->printTemplate($packList);
  //** Segunda pagina
   $this->pdf->StartTransform();
   $this->imprimitPaleta($paleta,$arreglo_combo);
    
   $this->pdf->StopTransform();
// FIN de la Segunda pagina

    
   $this->pdf->AddPage();
   $this->pdf->setY(20);
   $this->impPagina($paleta);
   $this->pdf->AddPage();
   $this->pdf->setY(20);
   $this->impPagina($paleta);

 }
 
 public function Printer ($data=NULL)
 {
  //$margin= $this->pdf->getMargins();
  //$page_width=216;
  //$page_resta=$page_width-$margin['right'] - $margin['left'];
    
  $this->pdf->AddPage();
  
  $this->template_img = $this->pdf->startTemplate(10, 10);
  $this->pdf->StartTransform();
  $this->pdf->Image(realpath(__DIR__.DIRECTORY_SEPARATOR.'palet02.png'), 0, 0, 10, 10, '', '', '', true, 72, '', false, false, 0, false, false, false);
  $this->pdf->StopTransform();
  $this->pdf->endTemplate();
  
  $arreglo_combo = $this->paleta->ObtenerArregloCombo($this->sql);
  $this->PrinterHablador($this->paleta,$arreglo_combo);
  
  $this->Output("paleta_uniprint-{$this->paleta->id}.pdf");
 }
 
}