<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2010-08-08
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+
/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */
include_once(realpath(dirname(__FILE__))."/tcpdf/config/lang/spa.php");
include_once(realpath(dirname(__FILE__))."/tcpdf/tcpdf.php");


if($datos['expediente']['fecha_evento'])
{
    $dateObj = \DateTime::createFromFormat("Y-m-d", $datos['expediente']['fecha_evento']);
    if($dateObj){$fecha_evento = $dateObj->format("d-m-Y");}
}

if($datos['personales']['fecha_nac'])
{
    $dateObj = \DateTime::createFromFormat("Y-m-d", $datos['personales']['fecha_nac']);
    if($dateObj){$fecha_nac = $dateObj->format("d-m-Y");}
}


$productos = $datos['productos'];
$datos_pedido = NULL;
          
foreach($productos as $prod)
{
    $datos_pedido .= "<tr>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". $prod['codigo_producto']. "</td>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". $prod['descripcion'] . "</td>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". $prod['marca']. "</td>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". $prod['cantidad']. "</td>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". number_format($prod['precio_unitario'],2,",",".") . "</td>";
    $datos_pedido .= "<td style=\"border:1px solid black;\">". number_format($prod['precio_total'],2,",",".") ."</td>";
    $datos_pedido .= "</tr>";
    
    $total_bolivares += $prod['precio_total'];
}


$datos_pedido .= "<tr>";
$datos_pedido .= "<td style=\"border:1px solid black;\" colspan=\"5\"><b>TOTAL A PAGAR</b></td>";
$datos_pedido .= "<td style=\"border:1px solid black;\"><b>". number_format($total_bolivares,2,",",".") ."</b></td>";
$datos_pedido .= "</tr>";



$pagos = $datos['pagos'];
$datos_pago = NULL;

if($pagos)
{
    foreach($pagos as $pago)
    {
        if($pago['validado'])
        {$pago_validado = "Si";}
        else
        {$pago_validado = "No";}
        
        $datos_pago .= "<table width='100%' style=\"border:1px solid black; border-collapse:collapse;\">";
        $datos_pago .= "<tr>";
        $datos_pago .= "<td style=\"border:1px solid black;\"><b>Banco que Utilizo:</b>". $pago['banco'] . "</td>";
        $datos_pago .= "<td style=\"border:1px solid black;\"><b>Número de Referencia o Recibo:</b>". $pago['referencia'] . "</td>";
        $datos_pago .= "<td style=\"border:1px solid black;\"><b>Validado:</b>". $pago_validado  . "</td>";
        $datos_pago .= "</tr>";
        $datos_pago .= "<tr>";
        $datos_pago .= "<td style=\"border:1px solid black;\"><b>Fecha:</b>". $pago['fecha']. "</td>";
        $datos_pago .= "<td style=\"border:1px solid black;\"><b>Monto:</b>Bs. ". number_format($pago['monto'],2,",",".") . "</td>";
        $datos_pago .= "<td style=\"border:1px solid black;\"></td>";
        $datos_pago .= "</tr>";
        $datos_pago .= "</table><br>";
    }
}





$datos_estudios = NULL;
$num_exam = 0;

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($datos['expediente']['id_fv']);
$pdf->SetTitle("Informe Social Exp ".$datos['expediente']['id_fv']);
$pdf->SetSubject("Informe Social Exp ".$datos['expediente']['id_fv']);
$pdf->SetKeywords("Informe Social Exp ".$datos['expediente']['id_fv']);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'INFORME DESCRIPTIVO - AÑO ESCOLAR 2011-2012', '');

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();



$html = <<<EOF

<html>
<head>
    <style>
        body {font-family: Century Gothic, Arial;}
        h1   {color: #0492d2;}
        
        h4 {color: #0492d2; }
        
        .version{font-size: 80%; color: #6E6E6E;}
        .pie1{font-size: 90%; color: #6E6E6E;}
        .pie2{font-size: 75%; color: #6E6E6E;}
        .pie3{font-size: 65%; color: #6E6E6E;}
        
        
        </style>

</head>

<body>


        <table width="100%">
        <tr>
            <td align="left" width="30%">Las Bellezas</td> 
            <td align="center" width="40%"><h1><br>Pedido Nº {$datos['pedido'][0]['id_pedido']} </h1></td>
            <td align="right" width="30%">
                
            </td> 
        </tr>
        
        <tr>
            <td align="right" colspan="3">
                <b>Fecha:</b> {$datos['pedido'][0]['fecha']} 
            </td>   
        </tr>
        </table>
        
        
        <table width="100%">
        <tr><td colspan="3"><h4>1.- LISTA DE PRODUCTOS </h4></td></tr>
        </table>
        
        <br>           
        <table width="100%" style="border:1px solid black; border-collapse:collapse;">
        <tr>
            <td width="12%" style="border:1px solid black;"><b>Codigo</b></td>
            <td width="26%" style="border:1px solid black;"><b>Descripcion</b></td>
            <td style="border:1px solid black;"><b>Marca</b></td>
            <td width="12%" style="border:1px solid black;"><b>Cantidad</b></td>
            <td style="border:1px solid black;"><b>P. Unitario</b></td>
            <td style="border:1px solid black;"><b>P. Total</b></td>
        </tr>

        {$datos_pedido}
                
        </table>
        
        <br>
      
        <table width="100%">
        <tr><td colspan="3"><h4>2.- DATOS DE FACTURACION</h4></td></tr>
        </table>
        <br>         
        <table width="100%" style="border: 1px solid black;">
        <tr>
            <td style="border:1px solid black;"><b>Nombres y Apellidos:</b> {$datos['facturacion'][0]['nombre']}</td>
            <td style="border:1px solid black;"><b>Cedula o Rif:</b> {$datos['facturacion'][0]['cedula']}</td>
        </tr>
        <tr>
            <td style="border:1px solid black;"><b>Correo:</b> {$datos['facturacion'][0]['correo']}</td>
            <td style="border:1px solid black;"><b>Telefono:</b> {$datos['facturacion'][0]['telefonos']}</td>
        </tr>
        <tr>
            <td style="border:1px solid black;"><b>Estado:</b> {$datos['facturacion'][0]['estado']}</td>
            <td></td>
        </tr>
        <tr>
            <td style="border:1px solid black;"><b>Direccion:</b> {$datos['facturacion'][0]['direccion']}</td>
            <td style="border:1px solid black;"><b>Direccion Entrega:</b> {$datos['facturacion'][0]['direccion2']}</td>
        </tr>
        <tr>
            <td style="border:1px solid black;"><b>Tipo de Entrega:</b> {$datos['facturacion'][0]['tipo_entrega']}</td>
            <td style="border:1px solid black;"><b>Observaciones:</b> {$datos['facturacion'][0]['observaciones']}</td>
        </tr>
        </table>
        
        <br>
            
        <table width="100%">
        <tr><td colspan="3"><h4>3.- DEPORTES DE PAGO</h4></td></tr>
        </table>
        <br> 
     
        {$datos_pago}
        
</body>
</html>

EOF;

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('InformeSocial.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+