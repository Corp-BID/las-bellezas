<?php

class pdf_OrdenProduccion extends pdf_Hablador
{
 protected $idOrden= NULL;
 
 
 
 public function __construct($sql, $idOrden)
 {
 // Orden::ConsultarOrdenProduccionId($this->sql,$this->idOrden);
  $this->idOrden= $idOrden;
  $this->sql= $sql;
  parent::__construct($sql);
 }
 
 public function Footer() 
 {
 // Position at 15 mm from bottom
  $this->pdf->SetY(-15);
 // Set font
  $this->pdf->SetFont('helvetica', 'I', 8);
 // Page number
  $this->pdf->Cell(0, 10, "Plan {$this->numPlam}  -  Página ".$this->pdf->getAliasNumPage().'/'.$this->pdf->getAliasNbPages(),0, false, 'C', 0, '', 0, false, 'T', 'M');
  
  return TRUE;
 }
 

 public function Printer ($data=NULL)
 {  
  $this->pdf->AddPage();
  
  $this->template_img = $this->pdf->startTemplate(10, 10);
  $this->pdf->StartTransform();
  $this->pdf->Image(realpath(__DIR__.DIRECTORY_SEPARATOR.'palet02.png'), 0, 0, 10, 10, '', '', '', true, 72, '', false, false, 0, false, false, false);
  $this->pdf->StopTransform();
  $this->pdf->endTemplate();
  
  ////////
  $inf_plan= current(Orden::ConsultarOrdenProduccionId($this->sql,$this->idOrden));
  $fecha = new DateTime($inf_plan['fecha_generado']);
  $this->pdf->SetFont('helvetica', '', 22);
  $this->pdf->writeHTMLCell(0, 0,'', '', "Orden de producción: <b>{$this->idOrden}</b>.", 0, 1, false,true, 'C');
  $this->pdf->writeHTMLCell(0, 0,'', '', "Fecha de generación: <b>{$fecha->format('d-m-Y g:i A')}</b>", 0, false, false,true, 'C');
  $this->pdf->SetFont('helvetica', '', 14);
  $this->pdf->Ln();$this->pdf->Ln(); 
  $this->pdf->writeHTMLCell(0, 0,'', '', "Estado: <b>{$inf_plan['desc_estado']}</b>, Municipio:  <b>{$inf_plan['desc_municipio']}</b>", 0, true, false,true, 'L');
  $this->pdf->Ln();

  $paletas=Orden::ConsultarPaletaOrdenProduccion($this->sql,$this->idOrden);
  $arreglo_paletas = array();
  foreach($paletas as $num=>$ord)
  {
   $paleta = new Paleta($this->sql,$ord['id_paleta']);
    
   //$this->pdf->Ln();
   $arreglo_combo = $paleta->ObtenerArregloCombo($this->sql);
   $arreglo_paletas[]=array('paleta'=> $paleta, 'comb'=> $arreglo_combo);
   
   $this->imprimitTablaInt($paleta,$arreglo_combo);
  }
  
  ///////
  
  foreach($arreglo_paletas as $inf)
  {
   $this->pdf->AddPage();
   $this->PrinterHablador($inf['paleta'] ,$inf['comb']);
  }
  
  $this->Output("paleta_uniprint-{$this->paleta->id}.pdf");
 }
 
}