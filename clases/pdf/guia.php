<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2010-08-08
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */
$horas =4;
$tiempo = $horas * 3600;
ini_set('max_execution_time', $tiempo); 
ini_set("session.gc_maxlifetime",$tiempo);
ini_set("memory_limit","512M");

include_once(realpath(dirname(__FILE__))."/tcpdf/config/lang/spa.php");
include_once(realpath(dirname(__FILE__))."/tcpdf/tcpdf.php");

class GuiaPDF extends TCPDF 
{
 protected $guia=0;
 
 public function asginarGuia($guia)
 {$this->guia=$guia;}

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(0, 10, " Guia {$this->guia} Página {$this->getAliasNumPage()}/{$this->getAliasNbPages()}", 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new GuiaPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Guía de Despacho");
$pdf->SetTitle("Guía de Despacho");
$pdf->SetSubject("Guía de Despacho");
$pdf->SetKeywords("Guía de Despacho");

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'INFORME DESCRIPTIVO - AÑO ESCOLAR 2011-2012', '');

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 12);
$pdf->asginarGuia($datos['num_despacho']);

// add a page
$pdf->AddPage();

// create some HTML content
//$fecha = new DateTime($datos['fecha']);
$datos['fecha']=date('d-m-Y g:i A',strtotime($datos['fecha']));
$placa =strtoupper($datos['placa']);

if($datos['edo'] == "DTTO CAPITAL")
{$observ = "Entregado por instrucciones del MPPE en el Liceo Aplicación (Montalban) Esq. Autopista Francisco Fajardo";}

$html = <<<EOF

<table style="border-collapse: collapse;" border="1" bordercolor="#08528b" width="100%">
<tr>
	<td>
		<br>
		
		<table style="border-collapse: collapse;" border="0" bordercolor="#08528b" width="100%">
		<tr>
			<td>
				<table width="100%">
                <tr>
                    <td align="left" width="35%"><h1>Uniprint SA</h1></td> 
                    <td align="right" width="65%"><h1>Colección Bicentenaria</h1></td> 
                </tr>
				<tr>
                    <td align="left" width="35%"><b>J-31120546-2</b></td> 
                    <td align="right" width="65%"><b> 2.014-2.015</b></td> 
                </tr>
                </table>
                <br>
                <table width="100%">
                <tr>
                    <td align="center" colspan="2"><h1>Guía de Despacho</h1></td>    
                </tr>
                <tr>
                    <td align="left">Número de Guía: <b>{$datos['num_despacho']}</b></td> 
                    <td align="right">Fecha: <b>{$datos['fecha']}</b></td> 
                </tr>
                </table>
                
                <br>
                
                <table width="100%">
                <tr>
                    <td align="center" colspan="2" bgcolor="#ff0000"><h3>Datos del Chofer</h3></td>    
                </tr>
                <tr>
                     <td colspan="2"><b>Empresa de Transporte:</b> {$datos['empresa']}</td>            
                 </tr>
                 <tr>
                     <td><b>Nombres y Apellidos:</b> {$datos['nombre_chofer']}</td>            
                     <td><b>Cédula:</b> {$datos['ci_chofer']}</td>            
                 </tr>
                 <tr>
                     <td><b>Teléfono:</b> {$datos['tel_chofer']}</td>   
                     <td><b>Firma:</b> _________________________</td>   
                 </tr>
                 </table>
                 
                 <br>
                
                 <table width="100%">
                 <tr>
                    <td align="center" colspan="2" bgcolor="#ff0000"><h3>Datos del Vehiculo</h3></td>    
                 </tr>
                 <tr>
                    <td width=50%><b>Placa:</b> {$placa}</td>            
                    <td width=50%><b>Modelo:</b> {$datos['modelo']}</td>            
                 </tr>
                 </table>
                 
                 <br>
                
                 <table width="100%">
                 <tr>
                    <td align="center" bgcolor="#ff0000"><h3>Datos del Destino</h3></td>    
                 </tr>
                 <tr>
                     <td><b>Estado:</b> {$datos['edo']}</td>            
                 </tr>
                 <tr>
                     <td><b>Municipio:</b> {$datos['mun']}</td>            
                 </tr>
                 <tr>
                     <td><b>Parroquia:</b> {$datos['par']}</td>            
                 </tr>
                 </table>
                 
                 <br>
                
                 <table width="100%">
                 <tr>
                    <td align="center" bgcolor="#ff0000"><h3>Responsables</h3></td>    
                 </tr>
                 </table>
                 <br>
                <table border="1" width="100%">
                <tr>
                    <td width="33%" align="center"><b>Uniprint</b></td>
                    <td width="34%" align="center"><b>MPPE</b></td>
                    <td width="33%" align="center"><b>Plantel Parroquial</b></td>
                </tr>
                <tr>
                    <td align="center">
                        <br><br><br><br><br>
                        Firma y Sello<br>
                        Nombre: Joan Diaz<br>
                        CI: 16.762.603
                    </td>
                    <td align="center">
                        <br><br><br><br><br>
                        Firma y Sello<br>
                        Nombre: Miguel Paniagua <br>
                        CI: 17.970.449
                    </td>
                    <td align="center">
                        <br><br><br><br><br>
                        Firma y Sello<br>
                        Nombre: _______________<br>
                        CI: ___________________
                    </td>
                </tr>
                </table>
                
                <br>
                
                <table width="100%"> 
                <tr>
                    <td align="center" bgcolor="#ff0000"><h3>Centro Educativo de Entrega</h3></td>    
                </tr> 
                <tr>
                     <td><b>Centro:</b> {$datos['colegio']}</td>            
                </tr>
                <tr>
                     <td><b>Dirección:</b> {$datos['colegio_direccion']}</td>            
                </tr>
                </table>
                
                <br>
                
                <table width="100%"> 
                <tr>
                    <td align="center" colspan="2" bgcolor="#ff0000"><h3>Descripción del Despacho</h3></td>    
                </tr>
                <tr>
                    <td><b>Cantidad de Paletas:</b> {$datos['cantidad_paletas']}</td>            
                    <td><b>Cantidad de Libros:</b> {$datos['cantidad_libros']}</td>            
                </tr>
                <tr>
                    <td colspan="2"><b>Paletas a Transportar Números:</b> {$datos['descripcion_paletas']}</td>            
                </tr>
                </table>
                
                <br>
                
                <table border="1" width="100%">
                <tr>
                    <td align="center" bgcolor="#ff0000" width="40%" colspan=2><h3>Control de Destino</h3></td>
                    <td align="center" bgcolor="#ff0000" width="60%"><h3>Observaciones</h3></td>    
                </tr>
                <tr>
                    <td>
                        <table border="1" width="100%">
                        <tr>
                            <td width="50%" align="center">Llegada</td>
                            <td width="50%" align="center">Salida</td>
                        </tr>
                        <tr>
                            <td><br><br><br><br></td>
                            <td></td>
                        </tr>
                        </table>
                    </td>
                    <td>{$observ}</td>
                </tr>
                </table>
                </td>
        </tr>
        </table>
        </td>
</tr>
</table>
EOF;

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
//$pdf->lastPage();

//kjhj
function printTable($header,$data,$pdf,$materia) 
   {
	   //$materia = array( 1=> "Lengua y Literatura",  2=> "Matemáticas",
 	//						 3 => "Ciencias Naturales",4 => "Ciencias Sociales");
							 
	   
	   $font_size= $pdf->getFontSizePt();
	   $pdf->SetX(15);
    // Colors, line width and bold font
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(0.3);
        $pdf->SetFont('', 'B',8);
        // Header
        //$w = array(17, 30, 95, 23,20);
		$w = array(17, 25,15, 95, 15,20);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
		
        $fill = 0;
	$total_libro=0;
    $magins=$pdf->getMargins();
        foreach($data as $row) {
		$arb=$materia[$row['grado']][$row['materia']]['abr'];
		//echo "<pre>";print_r($materia[$row['grado']][$row['materia']]['abr']);die;
			 $row['materia']=substr($materia[$row['grado']][$row['materia']]['nom'],0,57);

   if($row['tipo_empaque'] == "Grupo")
   $row['num_empaques'] .= " (".$row['libros_por_empaque']." Lib)";
            
//verficamos si la caja cabe
            if(($pdf->GetY() + $magins['bottom']+8) > 279)
            {$pdf->Ln();}
            
   
            $pdf->Cell($w[0], 6, $row['tipo_empaque'], 'LR', 0, 'C', $fill);
            $pdf->Cell($w[1], 6, $row['num_empaques'], 'LR', 0, 'C', $fill);
			$pdf->Cell($w[2], 6,  $arb, 'LR', 0, 'C', $fill);
            $pdf->Cell($w[3], 6, $row['materia'], 'LR', 0, 'C', $fill);
            $pdf->Cell($w[4], 6, $row['grado'], 'LR', 0, 'C', $fill);
	    $pdf->Cell($w[5], 6, intval($row['num_empaques'])*intval($row['libros_por_empaque']), 'LR', 0, 'C', $fill);
	    $total_libro += intval($row['num_empaques'])*intval($row['libros_por_empaque']);

            $pdf->Ln();
            $fill=!$fill;
        }
        
        $pdf->Cell(array_sum($w), 0, '', 'T');
      //verficamos si la caja cabe
    if( ($pdf->GetY() + $magins['bottom']+8) > 279)
    {$pdf->AddPage();}
     else
     {$pdf->SetX(array_sum($w) - $w[4] -  $w[5] +$magins['left']);}
	$pdf->Cell($w[4], 6, 'TOTAL', '', 0, 'C', 0);
	$pdf->Cell($w[5], 6, $total_libro, 'LRB', 0, 'C', 0);
	$pdf->Ln();
		
		$pdf->SetFontSize($font_size);
    }

//$y= $pdf->getY();
//$pdf->AddPage();

if($pdf->getY()< 270)
  {$pdf->AddPage();}

$pdf->SetFont('helvetica', '', 15);
  $pdf->writeHTML("<b>Control de Entrega</b>",true, false, true, false, 'C');
  $pdf->Ln();
  $pdf->SetFont('helvetica', '', 13);
  $pdf->writeHTML("<b>Estado:  {$datos['edo']}</b>",true, false, true, false, '');
  $pdf->writeHTML("<b>Municipio:  {$datos['mun']}</b>");
  $pdf->writeHTML("<b>Parroquia:  {$datos['par']}</b>");
  
  $pdf->Ln();

$header = array('Tipo', 'Nº Empaques', 'Abrev', 'Materia', 'Grado', 'Libros');
//$plan = new Planes();
//$materia=Libros::ObtenerLibros($_DB_);



 $template_img = $pdf->startTemplate(10, 10);
  $pdf->StartTransform();
  $pdf->Image(realpath(__DIR__.DIRECTORY_SEPARATOR.'palet02.png'), 0, 0, 10, 10, '', '', '', true, 72, '', false, false, 0, false, false, false);
  $pdf->StopTransform();
  $pdf->endTemplate();

if(!is_array($datos['paleta_numero']))
{$datos['paleta_numero']=array();}

foreach($datos['paleta_numero'] as $num_paleta)
{ 
 $pdf->SetFont('helvetica', '', 14);
 $pdf->printTemplate($template_img, '', $pdf->GetY()-3, 0, 0, '', '', false);
 $pdf->writeHTMLCell(0, 0,25, '', "Paleta <b>$num_paleta</b> &nbsp;&nbsp;  Guía <b>{$datos['num_despacho']}</b>", 0, 0, false,true, 'L');
 $pdf->Ln();
 //$paleta=$plan->ObtenerPlanesDetalladosPorPaleta($_DB_,$num_paleta);

    $paleta = new Paleta($_DB_,$num_paleta);
    $paleta=$paleta->ObtenerArregloCombo($_DB_);
   
   
// echo "oooooo($num_paleta)<pre>";print_r($paleta);die;
  printTable($header,$paleta,$pdf,$materia);
  $pdf->Ln();$pdf->Ln();
}
//Close and output PDF document
$pdf->Output("guia_despacho-{$datos['num_despacho']}.pdf", 'I');

//============================================================+
// END OF FILE                                                
//============================================================+