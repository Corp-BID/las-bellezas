<?php
/**
 * Actualizacion de la libreria PDF usada para generar los documentos, pasando de la libreria "class.ezpdf.php" a TCPDF
 * 
 * Clase programada con requerimiento minimo de PHP 5.3
 * @package Pdf
 */ 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
include_once(realpath(dirname(__FILE__))."/driver/Tcpdf.php");

//
class pdf_Pdf
{
 //Letter=612X792
 protected $pdf;
 protected $tipoOutput='I'; // I descarga Inline
 protected $header_xobjid;
 const PORTRAIT = 'P';
 const LANDSCAPE ='L';
 const MEDIDA_PUNTO='pt';
 const MEDIDA_MILIMETRO='mm';
 const MEDIDA_CENTIMETRO='cm';
 const MEDIDA_PULGADA='in';
 const IMAGE_SCALE_RATIO=1.25;
 
 protected $infDocumento=array('creator'=>'uniprint','author'=>'uniprint',
			'title'=>'Documento','subject'=>'Documento','keywords'=>'Documento, PDF');
 protected $numero= array(0=>'',1=>'UNO',2=>'DOS',3=>'TRES', 4=>'CUATRO', 5=>'CINCO', 6=>'SEIS', 7=>'SIETE', 8=>'OCHO',9=>'NUEVE',
			10=>'DIEZ',11=>'ONCE', 12=>'DOCE', 13=>'TRECE', 14=>"CATORCE", 15=> "QUINCE", 
			16=>"DIECISEIS", 17=>"DIECISIETE", 18 =>"DIECIOCHO", 19=>"DIECINUEVE");
 protected $numeroSegundoDigito= array(0=>'',2=>'VEINTE',3=>'TREINTA', 4=>'CUARENTA', 5=>'CINCUENTA', 6=>'SESENTA', 7=>'SETENTA', 8=>'OCHENTA',9=>'NOVENTA');
 protected $margin= array('left'=>15,'right'=>15,'top'=>27,'BOTTOM'=>25);
 protected $font= array('main'=>'helvetica','data'=>'helvetica','monospaced'=>'helvetica');
 
 /**
  * Constructor de la Clase, crea el objeto PDF y setea los valores de tamano de pagina, orientacion y unodad de medida usada.
  * @param $size (string)Tamano de la pagina a usar (LETTER,LEGAL, A4, etc), tamano LETTER predeterminado 
  * @param $orientation Orientacion de la pagina, posibles valores pdf_Pdf::PORTRAIT y pdf_Pdf::LANDSCAPE (pdf_Pdf::PORTRAIT predeterminado)
  * @param $unidad Unidad de medida en el documento: pdf_Pdf::MEDIDA_PUNTO, pdf_Pdf::MEDIDA_MILIMETRO, pdf_Pdf::MEDIDA_CENTIMETRO, pdf_Pdf::MEDIDA_PULGADA (pdf_Pdf::MEDIDA_PUNTO predeterminado)
  */
  
 public function __construct($size='LETTER', $orientation=self::PORTRAIT, $unidad_medida=self::MEDIDA_MILIMETRO) 
 {
  // create new PDF document
  $this->pdf = new pdf_driver_Tcpdf($this,$orientation, $unidad_medida, $size, true, 'UTF-8', false);
  
  // set document information
  $this->pdf->SetCreator($this->infDocumento['creator']);
  $this->pdf->SetAuthor($this->infDocumento['author']);
  $this->pdf->SetTitle($this->infDocumento['title']);
  $this->pdf->SetSubject($this->infDocumento['subject']);
  $this->pdf->SetKeywords($this->infDocumento['keywords']);
  
  if($unidad_medida==self::MEDIDA_MILIMETRO)
  {
   $this->pdf->setImageScale(self::IMAGE_SCALE_RATIO);
   //set margins
   $this->pdf->SetMargins($this->margin['left'], $this->margin['top'], $this->margin['right']);
   
   //set auto page breaks
   $this->pdf->SetAutoPageBreak(TRUE, $this->margin['BOTTOM']);
  }
  
  // set header and footer fonts
//   $this->pdf->setHeaderFont(Array($this->font['main'], '', 10));
//   $this->pdf->setFooterFont(Array($this->font['data'], '', 8));
  
//   $this->pdf->setPrintHeader(false);
//   $this->pdf->setPrintFooter(false);
  
  // set default monospaced font
  $this->pdf->SetDefaultMonospacedFont($this->font['monospaced']);
  $this->pdf->SetFont($this->font['main'], '', 9);
  
  // Colocamos el color del texto en CMYK (negro)
  $this->pdf->SetTextColor(0, 0, 0, 100);
 }
 
 /**
  * Retorna el ancho en Pixel del margen restante de la página actual, esto es, el ancho total de la pagina 
  * menos el margen inquierdo y margen derecho
  * @public
  */
 public function obtenerAnchoMargenPx()
 {
  $margin= $this->pdf->getMargins();
  return round(($this->pdf->getPageWidth() - $margin['left'] - $margin['right'])/$this->pdf->pixelsToUnits(1));
 }
 
 public function Header()
 {return false;}
 
 public function Footer() 
 {
  // Position at 15 mm from bottom
  $this->pdf->SetY(-15);
 // Set font
  $this->pdf->SetFont('helvetica', 'I', 8);
 // Page number
 $this->pdf->Cell(0, 10, 'Página '.$this->pdf->getAliasNumPage().'/'.$this->pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
  
  return true;
 }
 
 public static function date_Formate($date)
 {
  $patterns= array("/Jan/","/Feb/","/Mar/","/Apr/","/May/","/Jun/","/Jul/","/Aug/","/Sep/","/Oct/","/Nov/","/Dec/");
  $replacements= array("01","02","03","04","05","06","07","08","09","10","11","12"); 
  $textemp= preg_replace($patterns, $replacements,$date);
  $pattern = "/(\d+) (\d+) (\d+) ([\w:\d]*)/i";
  $replacement = "\${2}/\${1}/\${3}";
  
  return preg_replace($pattern, $replacement, $textemp);
 }

 private static function numeroDigito1($int,$unidadMil=0)
 {
   if($int==1)
   {return($unidadMil==0)?"UNO":"UN";}
  
  return $this->numero[$int];
 }

 private static function numeroDigito2Alt($int)
 {
  if($int>9 && $int<20)
   return $this->numero[$int];
  
  return "";
 }

 private static function numeroDigito2($int,$unidadMil=0)
 {
  if(strlen($int)>1 && $int{0}>0)
  {
   $last=($int{1}>0)?" y ".self::numeroDigito1($int{1},$unidadMil):"";
   
   if($int{0}==1)
   {return self::numeroDigito2Alt($int);}
   else
   {return $this->numeroSegundoDigito[$int{0}].$last;}
  }
  elseif(strlen($int)>1)
  {return self::numeroDigito1($int{1},$unidadMil);}
  else
  {return self::numeroDigito1($int);}
 }

 private static function numeroDigito3($int,$unidadMil=0)
 {
  switch(strlen($int))
  {
   case 3:
    if($int{0}==0)
    {return self::numeroDigito3("".$int{1}.$int{2});}
    elseif($int{0}==1 && $int{1}==0 && $int{2}==0)
    {return "CIEN";}
    elseif($int{0}==1)
    {return "CIENTO ".self::numeroDigito2("".$int{1}.$int{2},$unidadMil);}
    elseif($int{0}==9)
    {return "NOVECIENTO ".self::numeroDigito2("".$int{1}.$int{2},$unidadMil);}
    else
    {return self::numeroDigito1($int{0})."CIENTO ".self::numeroDigito2("".$int{1}.$int{2},$unidadMil);}
    break;
   case 2:
    return self::numeroDigito2("".$int{0}.$int{1},$unidadMil);
    break;
   case 1:
    return self::numeroDigito1("".$int{0},$unidadMil);
    break;
   default:
    return "";
    break;
  }
 }

 protected static function numeroLetra($int)
 {
  if(is_numeric($int))
   $int="".$int;
  
  switch(strlen($int))
  {
   case 1:
    return self::numeroDigito1($int);
    break;
   
   case 2:
    return self::numeroDigito2($int);
    break;
   
   case 3:
    return self::numeroDigito3($int);
    break;
   
   case 4:
    if($int{0} >0)
    {
     if($int{0}==1)
     {return "MIL ".self::numeroLetra("".$int{1}.$int{2}.$int{3});}
     else
     {return self::numeroDigito1($int{0})." MIL ".self::numeroLetra("".$int{1}.$int{2}.$int{3});}
    }
    else
     return self::numeroLetra("".$int{1}.$int{2}.$int{3});
    break;
   
   case 5:
    if($int{0} >0)
    {return self::numeroDigito2("".$int{0}.$int{1},1)." MIL ".self::numeroLetra("".$int{2}.$int{3}.$int{4});}
    else
    {return self::numeroLetra("".$int{1}.$int{2}.$int{3}.$int{4});}
    break;
   
   case 6:
    if($int{0} >0)
    {return self::numeroDigito3("".$int{0}.$int{1}.$int{2},1)." MIL ".self::numeroLetra("".$int{3}.$int{4}.$int{5});}
    else
    {return self::numeroLetra("".$int{1}.$int{2}.$int{3}.$int{4}.$int{5});}
    break;
   
   case 7:
    if($int{0} >0)
    {
     if($int{0}==1)
     {return "UN MILLON ".numeroLetra("".$int{1}.$int{2}.$int{3}.$int{4}.$int{5}.$int{6});}
     else
     {return self::numeroDigito1($int{0})." MILLONES ".self::numeroLetra("".$int{1}.$int{2}.$int{3}.$int{4}.$int{5}.$int{6});}
    }
    else
     return self::numeroLetra("".$int{0}.$int{1}.$int{2}.$int{3}.$int{4}.$int{5});
    break;
   
   case 8:
    if($int{0} >0)
    {return self::numeroDigito2("".$int{0}.$int{1})." MILLONES ".self::numeroLetra("".$int{2}.$int{3}.$int{4}.$int{5}.$int{6}.$int{7});}
    else
     return self::numeroLetra("".$int{0}.$int{1}.$int{2}.$int{3}.$int{4}.$int{5}.$int{6});
    break;
   
   case 9:
    if($int{0} >0)
    {return self::numeroDigito3("".$int{0}.$int{1}.$int{2})." MILLONES ".self::numeroLetra("".$int{3}.$int{4}.$int{5}.$int{6}.$int{7}.$int{8});}
    else
     return self::numeroLetra("".$int{0}.$int{1}.$int{2}.$int{3}.$int{4}.$int{5}.$int{6}.$int{7});
    break;
   
   case 0:
   default:
    return "";
    break;
  }
 }

/**
 * I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
    * D: send to the browser and force a file download with the name given by name.
    * F: save to a local server file with the name given by name.
    * S: return the document as a string (name is ignored).
    * FI: equivalent to F + I option
    * FD: equivalent to F + D option
    * E: return the document as base64 mime multi-part email attachment (RFC 2045)
*/
 public function setTipoSalida($tipo)
 {
  $tipo=strtoupper($tipo);
  
  switch($tipo)
  {
   case 'I':
   case 'D':
   case 'F':
   case 'S':
   case 'FI':
   case 'FD':
   case 'E':
    $this->tipoOutput=$tipo;
    return true;
   default:
    return false;
  }
  return false;
 }
 
 protected function Output($filename='')
 {
  if ($filename)
  {$this->pdf->Output($filename, $this->tipoOutput);}
   else
  {$this->pdf->Output('file_uniprint.pdf', $this->tipoOutput);}
 }
  
 public function Printer ($data='') { }
}