<?php
class Faq
{
	function AgregarFaq($sql, $pregunta, $respuesta, $visible, $activo)
	{
		$pregunta = trim(mb_convert_encoding($pregunta, "ISO-8859-1", "UTF-8"));
		$respuesta = trim(mb_convert_encoding($respuesta, "ISO-8859-1", "UTF-8"));
        
        $fecha_generado = date("Y-m-d H:i:s");
        
		$query="INSERT INTO faq (pregunta, respuesta, fecha, visible, activo) 
                VALUES ('$pregunta', '$respuesta', '$fecha_generado', '$visible', '$activo')";
        
        $sql->ExecQuery($query);
	}
    
    function ModificarFaq($sql, $id, $pregunta, $respuesta)
	{
		$pregunta = trim(mb_convert_encoding($pregunta, "ISO-8859-1", "UTF-8"));
		$respuesta = trim(mb_convert_encoding($respuesta, "ISO-8859-1", "UTF-8"));
        
        $fecha_generado = date("Y-m-d H:i:s");
        
		$query="UPDATE faq 
                SET pregunta='$pregunta',
                    respuesta='$respuesta',
                    fecha='$fecha_generado'
                WHERE id=$id";
        
        $sql->ExecQuery($query);
	}
    
    function ActivarFaq($sql, $id, $activo)
	{
		$query="UPDATE faq 
                SET activo='$activo'
                WHERE id=$id";
        
        $sql->ExecQuery($query);
	}
    
    
    function VisibleFaq($sql, $id, $visible)
	{
		$query="UPDATE faq 
                SET visible='$visible'
                WHERE id=$id";
        
        $sql->ExecQuery($query);
	}
    
    
	function ObtenerFaq($sql, $id=NULL)
	{
		$res = NULL;
        
        if($id)
        {
            $query="SELECT id, pregunta, respuesta, fecha, visible, activo 
                    FROM faq 
                    WHERE id=$id";
        }
        else
        {
            $query="SELECT id, pregunta, respuesta, fecha, visible, activo 
                    FROM faq 
                    ORDER BY id ASC";
        }
	 
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['id'] = $row['id'];
			$res[$i]['pregunta'] = $row['pregunta'];
			$res[$i]['respuesta'] = $row['respuesta'];
			$res[$i]['fecha'] = $row['fecha'];
            $res[$i]['visible'] = $row['visible'];
            $res[$i]['activo'] = $row['activo'];
			$i++;
		}
		return $res;
	}
}