<?php
class Banners
{
    function Agregar($sql, $imagen, $titulo, $descripcion, $boton, $link, $publicado)
	{
        $query="INSERT INTO banners(imagen, titulo, descripcion, boton, link, publicado,eliminado) VALUES 
                                ('$imagen', '$titulo', '$descripcion', '$boton', '$link', $publicado, 0)";
        
        $sql->ExecQuery($query);
	}
    
    function Modificar($sql, $id, $imagen, $titulo, $descripcion, $boton, $link, $publicado)
    {
        $datos = array();

        if ($imagen!=''){
            $datos = [
              "imagen = '$imagen'",
              "titulo = '$titulo'",
              "descripcion = '$descripcion'",
              "boton = '$boton'",
              "link = '$link'",
              "publicado = $publicado",

            ];
        }else{
            $datos = [
                "titulo = '$titulo'",
                "descripcion = '$descripcion'",
                "boton = '$boton'",
                "link = '$link'",
                "publicado = $publicado",
            ];

        }
        $implodeArray = implode(', ', $datos);
        $query="UPDATE banners SET $implodeArray WHERE id=".$id;
        $sql->ExecQuery($query);
    }

    function Publicar($sql, $id, $publicado)
    {
        $query="UPDATE banners 
                SET publicado = $publicado
                WHERE id='$id'";

        $sql->ExecQuery($query);
    }

    function Eliminar($sql, $id)
    {
        $query="UPDATE banners 
                SET eliminado = 1
                WHERE id=".$id;
        
        $sql->ExecQuery($query);
    }

    function Revertir($sql, $id)
    {
        $query="UPDATE banners 
                SET eliminado = 0
                WHERE id=".$id;

        $sql->ExecQuery($query);
    }


    function Obtener($sql, $id)
	{
		$res = NULL;
        
        $query="SELECT id, imagen, titulo, descripcion, boton, link, publicado, eliminado, usuario, fecha_sistema
                FROM banners 
                WHERE id=".$id;
        
        $resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['titulo'] = $row['titulo'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['boton'] = $row['boton'];
         
            $res[$i]['link'] = $row['link'];
            $res[$i]['publicado'] = $row['publicado'];
           
            $res[$i]['usuario'] = $row['usuario'];
            $res[$i]['fecha_sistema'] = $row['fecha_sistema'];
           
            $i++;
		}
		return $res;
	}

    function Listado($sql, $eliminado = 0)
	{
		$res = NULL;

        $query="SELECT id, imagen, titulo, descripcion, boton, link, publicado, eliminado, usuario, fecha_sistema
                FROM banners 
                WHERE eliminado='$eliminado'
                ORDER BY id DESC";

        $resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['id'] = $row['id'];
            $res[$i]['imagen'] = $row['imagen'];
            $res[$i]['titulo'] = $row['titulo'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['boton'] = $row['boton'];
         
            $res[$i]['link'] = $row['link'];
            $res[$i]['publicado'] = $row['publicado'];
           
            $res[$i]['usuario'] = $row['usuario'];
            $res[$i]['fecha_sistema'] = $row['fecha_sistema'];
            $i++;
		}
		return $res;
	}

}