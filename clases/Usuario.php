<?php
class Usuario
{
	
/*--- Tipos de Usuario ---*/
	function tipos_usuarios($sql)
	{
		$result=array();
		$query="SELECT id,descripcion
			FROM tipo_usuarios";
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$result[$i]['id']= $row['id'];
			$result[$i]['descripcion']=$row['descripcion'];
			$i++;
		}
		return $result;
	}
	
	function desc_tipo_usuario($sql, $id_tipo)
	{
		$query="SELECT descripcion 
			FROM tipo_usuarios 
			WHERE id = $id_tipo";
		$resul2=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul2))
		{$result['descripcion']=$row['descripcion'];}
		return $result['tipo_usuario'];
	}
	
/*--- Funciones de los usuarios ---*/
	function ValidarUsuario($sql, $login, $password)
	{
		$result=array();
		$query="SELECT login, id_tipo_usuario, nombre, email, telefono, activo
			FROM usuarios 
			WHERE login= '$login' AND (clave = '$password' OR clave = MD5('$password')) AND activo =1 AND eliminado=0";
        
		$resul=$sql->ExecQuery($query);
		
		if($row=$sql->FetchArray($resul))
		{
			$result['login']= $row['login'];
			$result['id_tipo_usuario']= $row['id_tipo_usuario'];
			$result['nombres']=$row['nombre'];
			$result['apellidos']=$row['apellido'];
			$result['email']=$row['email'];
			$result['telefono']=$row['telefono'];
			$result['activo']=$row['activo'];
		}
		
		if($result){return $result;}
		else return false;
	}
	
    function CambiarPassword($sql, $login, $password)
	{
		$result=array();
		$query="UPDATE usuarios SET clave='$password' WHERE login='$login'";
		$sql->ExecQuery($query);
	}
    
	
/*--- Funciones para el coordinador ---*/
	function obtenerUsuarios($sql)
	{
		$res = array();
		$query="SELECT login, login_alternativo,clave,id_tipo_usuario,nombre,apellido,cedula,rif,profesion,sexo,email,
			tlf_contacto,tlf_celular,pregunta_secreta,respuesta_secreta,num_cupos,id_plantel,id_plan,id_status,activo
			FROM usuarios
			WHERE activo = 0 AND (id_status = 3 OR id_status = 4)
			ORDER BY login";
		
		$resul=$sql->ExecQuery($query);
		$i=0;
		while($row=$sql->FetchArray($resul))
		{
			$res[$i]['login'] = $row['login'];
			$res[$i]['tipo_usuario'] = $this->desc_tipo_usuario($row['id_tipo_usuario'],$sql);
			$res[$i]['id_tipo_usuario'] = $row['id_tipo_usuario'];
			$res[$i]['nombre'] = $row['nombre'];
			$res[$i]['apellido'] = $row['apellido'];
			$res[$i]['cedula'] = $row['cedula'];
			$res[$i]['rif'] = $row['rif'];
			$res[$i]['profesion'] = $row['profesion'];
			$res[$i]['sexo'] = $row['sexo'];
			$res[$i]['email'] = $row['email'];
			$res[$i]['tlf_contacto'] = $row['tlf_contacto'];
			$res[$i]['tlf_celular'] = $row['tlf_celular'];
			$res[$i]['preg_secreta'] = $row['pregunta_secreta'];
			$res[$i]['resp_secreta'] = $row['respuesta_secreta'];
			$res[$i]['num_cupos'] = $row['num_cupos'];
			$res[$i]['id_plantel'] = $row['id_plantel'];
			$res[$i]['id_plan'] = $row['id_plan'];
			$res[$i]['id_status'] = $row['id_status'];
			$res[$i]['desc_status'] = $this->desc_status_usuario($row['id_status'],$sql);
			$res[$i]['activo'] = $row['activo'];
			$i++;
		}
		return $res;
	}
	
	
	function ActualizarUsuario($login, $nombres, $apellidos, $nacionalidad, $cedula, $sexo, $email, $telefono,$sql)
	{
		$nombres = trim(mb_convert_encoding($nombres, "ISO-8859-1", "UTF-8"));
		$apellidos = trim(mb_convert_encoding($apellidos, "ISO-8859-1", "UTF-8"));
		$cedula = trim($cedula);
		$nacionalidad  = trim(strtoupper($nacionalidad));
	
		$query="UPDATE usuarios SET 
				nombre='$nombres', 
				apellido='$apellidos', 
				nacionalidad='$nacionalidad', 
				cedula='$cedula',
				sexo='$sexo', 
				email='$email', 
				telefono='$telefono' 
			WHERE login = '$login'";
		$sql->ExecQuery($query);
	}
	
	function ActualizarClaveUsuario($login, $clave_actual, $clave_nueva,$sql)
	{
		$query="UPDATE usuarios SET clave=MD5('$clave_nueva') 
			WHERE login = '$login' AND clave=MD5('$clave_actual')";
		$sql->ExecQuery($query);
	}
}