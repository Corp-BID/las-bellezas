<?php
class Productos extends ConsultaComun
{
    function Agregar($sql, $cod_categoria, $cod_subcategoria, $cod_producto, $cod_producto_padre, $nombre, $presentacion, $marca, $descripcion, $precio, 
                    $cant_min, $nuevo, $oferta, $fecha_modificacion, $visible,$destacado)
    {
        $fecha_modificacion = date('Y-m-d  h:i:s');

        $query = "INSERT INTO productos(cod_categoria, cod_subcategoria, cod_producto, cod_producto_padre, nombre, presentacion, marca, descripcion, precio, 
                        cant_min, nuevo, oferta, fecha_modificacion, visible, eliminado,destacado) 
                VALUES ('$cod_categoria', '$cod_subcategoria', '$cod_producto', '$cod_producto_padre', '$nombre', '$presentacion', $marca, '$descripcion', '$precio', 
                        $cant_min, $nuevo, $oferta, '$fecha_modificacion', $visible, 0,$destacado)";

        $sql->ExecQuery($query);
    }

    function Modificar($sql, $cod_categoria, $cod_subcategoria, $cod_producto, $cod_producto_old, $cod_producto_padre, $nombre, $presentacion, $marca, $descripcion, $precio, 
                $cant_min, $nuevo, $oferta, $visible, $destacado)
    {

        $fecha_modificacion = date('Y-m-d  h:i:s');

        $query = "UPDATE productos 
                SET cod_categoria = '$cod_categoria',
                    cod_subcategoria = '$cod_subcategoria',
                    cod_producto='$cod_producto',

                    nombre='$nombre',
                    presentacion='$presentacion',
                    descripcion='$descripcion',
                    marca=$marca,
                    
                    precio='$precio',
                    cant_min=$cant_min,

                    fecha_modificacion='$fecha_modificacion',
                    visible=$visible,
                    nuevo=$nuevo,
                    oferta=$oferta,
                    destacado=$destacado

                WHERE cod_producto='$cod_producto_old'";

        $sql->ExecQuery($query);
    }


    function ModificarFotos($sql, $codigo_producto, $foto, $imagen = NULL)
    {
        $query = "UPDATE productos 
                SET $foto='$imagen'
                WHERE cod_producto='$codigo_producto'";

        $sql->ExecQuery($query);
    }


    function Eliminar($sql, $codigo_producto)
    {
        $query = "UPDATE productos 
                SET visible='0',
                    eliminado='1'
                WHERE cod_producto='$codigo_producto'";

        $sql->ExecQuery($query);
    }

    function Consultar($sql, $codigo_producto)
    {
        $res = NULL;

        $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado,P.destacado,
                        P.presentacion
                FROM productos P
                LEFT JOIN marcas M ON M.id=P.marca
                WHERE P.cod_producto='$codigo_producto'";

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['cod_producto_padre'] = $row['cod_producto_padre'];

            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['presentacion'] = $row['presentacion'];
            $res[$i]['marca'] = $row['marca'];
            $res[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['precio'] = $row['precio'];

            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['visible'] = $row['visible'];
            $res[$i]['destacado'] = $row['destacado'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }


            $res[$i]['foto2'] = $row['foto2'];
            $res[$i]['foto3'] = $row['foto3'];
            $res[$i]['foto4'] = $row['foto4'];
            $res[$i]['foto5'] = $row['foto5'];

            $res[$i]['cant_min'] = $row['cant_min'];
            $i++;
        }
        return $res;
    }

    function Listado($sql, $codigo_categoria = 0, $codigo_subcategoria = 0, $nuevo = 0, $oferta = 0, $limit = 15, $page = 1, $visible = 1)
    {
        $res = NULL;
        $this->_limit   = $limit;
        $this->_page    = $page;

        if ($codigo_categoria !='' and $codigo_subcategoria == '') {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.cod_categoria='$codigo_categoria' AND P.visible=$visible AND P.eliminado=0
                    ";
        } elseif ($codigo_categoria != '' and $codigo_subcategoria != '') {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.cod_categoria='$codigo_categoria' AND P.cod_subcategoria='$codigo_subcategoria' AND P.visible=$visible AND P.eliminado=0
                    ";
        } elseif ($nuevo > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.nuevo='1' AND P.visible=$visible AND P.eliminado=0
                    ";
        } elseif ($oferta > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.oferta='1' AND P.visible=$visible AND P.eliminado=0
                    ";
        } else {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.visible=$visible AND P.eliminado=0
                    ";
        }

        $this->_total = $this::TotalRegistros($sql,$query);
        $this->_query = " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";

        if ($codigo_categoria != '' and $codigo_subcategoria == '') {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado,P.destacado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.cod_categoria='$codigo_categoria' AND P.visible=$visible AND P.eliminado=0
                    ".$this->_query;
        } elseif ($codigo_categoria !='' and $codigo_subcategoria !='') {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado ,P.destacado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.cod_categoria='$codigo_categoria' AND P.cod_subcategoria='$codigo_subcategoria' AND P.visible=$visible AND P.eliminado=0
                    ".$this->_query;
        } elseif ($nuevo > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado ,P.destacado 
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.nuevo='1' AND P.visible=$visible AND P.eliminado=0
                    ".$this->_query;
        } elseif ($oferta > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado ,P.destacado
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.oferta='1' AND P.visible=$visible AND P.eliminado=0
                    ".$this->_query;
        } else {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado ,P.destacado
                    FROM productos P
                    LEFT JOIN marcas M ON M.id=P.marca
                    WHERE P.visible=$visible AND P.eliminado=0
                    ".$this->_query;
        }


        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['cod_producto_padre'] = $row['cod_producto_padre'];

            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['presentacion'] = $row['presentacion'];
            $res[$i]['marca'] = $row['marca'];
            $res[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['precio'] = $row['precio'];

            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['visible'] = $row['visible'];
            $res[$i]['destacado'] = $row['destacado'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }
            
            $res[$i]['foto2'] = $row['foto2'];
            $res[$i]['foto3'] = $row['foto3'];
            $res[$i]['foto4'] = $row['foto4'];
            $res[$i]['foto5'] = $row['foto5'];

            $res[$i]['cant_min'] = $row['cant_min'];

            $i++;
        }

        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $res;
        return (array) $result;
    }

    function ListadoVerMas($sql, $nuevo = 0, $oferta = 0, $destacado = 0, $limit = 15, $page = 1)
    {
        $res = NULL;
        $this->_limit   = $limit;
        $this->_page    = $page;

        if ($nuevo > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.nuevo='1' AND P.visible='1' AND P.eliminado=0";
        }

        elseif ($oferta > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.oferta='1' AND P.visible='1' AND P.eliminado=0";
        }

        elseif ($destacado > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.destacado='1' AND P.visible='1' AND P.eliminado=0";
        }


        $this->_total = $this::TotalRegistros($sql,$query);
        $this->_query = " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";

        if ($nuevo > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.nuevo='1' AND P.visible='1' AND P.eliminado=0".$this->_query;
        }

        if ($oferta > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.oferta='1' AND P.visible='1' AND P.eliminado=0".$this->_query;
        }

        if ($destacado > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.destacado='1' AND P.visible='1' AND P.eliminado=0".$this->_query;
        }


        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['cod_producto_padre'] = $row['cod_producto_padre'];

            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['presentacion'] = $row['presentacion'];
            $res[$i]['marca'] = $row['marca'];
            $res[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['precio'] = $row['precio'];

            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['visible'] = $row['visible'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }

            $res[$i]['foto2'] = $row['foto2'];
            $res[$i]['foto3'] = $row['foto3'];
            $res[$i]['foto4'] = $row['foto4'];
            $res[$i]['foto5'] = $row['foto5'];

            $res[$i]['cant_min'] = $row['cant_min'];

            $i++;
        }

        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $res;
        return (array) $result;
    }

    function ProductosRelacionados($sql, $product_name)
    {
        $res = NULL;

        $query = "SELECT cod_categoria, cod_subcategoria, cod_producto, nombre,
                    nuevo, oferta, destacado, precio,foto1
                FROM productos 
                WHERE visible='1' AND eliminado=0 and nombre like '%$product_name%' and nombre <> '$product_name'
                ORDER BY RAND() LIMIT 4";

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['precio'] = $row['precio'];

            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['destacado'] = $row['destacado'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }

            $i++;
        }
        return $res;
    }

    function ListadoAleatorio($sql, $nuevo = 0, $oferta = 0, $destacado = 0, $limit = 8)
    {
        $res = NULL;

        if ($nuevo > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.nuevo='1' AND P.visible='1' AND P.eliminado=0
                    ORDER BY RAND() LIMIT $limit";
        }

        elseif ($oferta > 0 ) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.oferta='1' AND P.visible='1' AND P.eliminado=0
                    ORDER BY RAND() LIMIT $limit";
        }

        elseif ($destacado > 0) {
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.destacado='1' AND P.visible='1' AND P.eliminado=0
                    ORDER BY RAND() LIMIT $limit";
        }

        else{
            $query = "SELECT P.cod_categoria, P.cod_subcategoria, P.cod_producto, P.cod_producto_padre, P.nombre, P.presentacion, P.marca, 
                        P.descripcion, M.descripcion as descripcion_marca,
                        P.precio, P.foto1, P.foto2, P.foto3, P.foto4, P.foto5, P.cant_min, 
                        P.nuevo, P.oferta, P.fecha_modificacion, P.visible, P.eliminado 
                    FROM productos P
                    INNER JOIN marcas M ON M.id=P.marca
                    WHERE P.visible='1' AND P.eliminado=0
                    ORDER BY RAND() LIMIT $limit";
        }

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['cod_producto_padre'] = $row['cod_producto_padre'];

            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['presentacion'] = $row['presentacion'];
            $res[$i]['marca'] = $row['marca'];
            $res[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['precio'] = $row['precio'];

            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['visible'] = $row['visible'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }

            $res[$i]['foto2'] = $row['foto2'];
            $res[$i]['foto3'] = $row['foto3'];
            $res[$i]['foto4'] = $row['foto4'];
            $res[$i]['foto5'] = $row['foto5'];

            $res[$i]['cant_min'] = $row['cant_min'];

            $i++;
        }
        return $res;
    }

    function ListarProductosAdmin($sql, $categoria, $subcategoria)
    {
        $res = NULL;

        $query = "SELECT cod_categoria, cod_subcategoria, cod_producto, cod_subproducto, descripcion, unidad, marca, caracteristicas, precio1, precio2, precio3, foto1, foto2, foto3, foto4, foto5, cant_min_1, cant_min_2, cant_min_3, nuevo, oferta, fecha_modificacion, visible, eliminado 
                FROM productos 
                WHERE cod_categoria=$categoria AND cod_subcategoria=$subcategoria
                ORDER BY cod_categoria, cod_subproducto, descripcion ASC";

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['cod_subproducto'] = $row['cod_subproducto'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['unidad'] = $row['unidad'];
            $res[$i]['marca'] = $row['marca'];
            $res[$i]['caracteristicas'] = $row['caracteristicas'];
            $res[$i]['precio1'] = $row['precio1'];
            $res[$i]['precio2'] = $row['precio2'];
            $res[$i]['precio3'] = $row['precio3'];
            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];
            $res[$i]['visible'] = $row['visible'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }

            $res[$i]['foto2'] = $row['foto2'];
            $res[$i]['foto3'] = $row['foto3'];
            $res[$i]['foto4'] = $row['foto4'];
            $res[$i]['foto5'] = $row['foto5'];

            $i++;
        }
        return $res;
    }

    function Buscador($sql, $palabra, $limit = 15, $page = 1)
    {
        $res = NULL;
        $this->_limit   = $limit;
        $this->_page    = $page;

        $query = "SELECT productos.cod_categoria, productos.cod_subcategoria, productos.cod_producto,
productos.precio,productos.foto1,productos.oferta,productos.nuevo,productos.destacado,productos.nombre,marcas.descripcion as descripcion_marca, categorias.nombre as categoria FROM productos 
                inner join marcas on marcas.id = productos.marca and marcas.eliminado = 0
                inner join categorias on categorias.categoria = productos.cod_categoria and categorias.visible = 1 and categorias.eliminado = 0 
	            and categorias.subcategoria = ''
                where (productos.cod_producto LIKE '%$palabra%' OR 
                productos.nombre LIKE '%$palabra%' OR 
                marcas.descripcion LIKE '%$palabra%' OR 
                categorias.nombre LIKE '%$palabra%') AND  
                productos.visible=1 AND  productos.eliminado=0
                ORDER BY productos.cod_producto ASC";

        $this->_total = $this::TotalRegistros($sql,$query);
        $this->_query = " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";

        $query = "SELECT productos.cod_categoria, productos.cod_subcategoria, productos.cod_producto,productos.descripcion,
productos.precio,productos.foto1,productos.oferta,productos.nuevo,productos.destacado,productos.nombre,marcas.descripcion as descripcion_marca, categorias.nombre as categoria FROM productos 
                inner join marcas on marcas.id = productos.marca and marcas.eliminado = 0
                inner join categorias on categorias.categoria = productos.cod_categoria and categorias.visible = 1 and categorias.eliminado = 0 
	            and categorias.subcategoria = ''
                where (productos.cod_producto LIKE '%$palabra%' OR 
                productos.nombre LIKE '%$palabra%' OR 
                marcas.descripcion LIKE '%$palabra%' OR 
                categorias.nombre LIKE '%$palabra%') AND  
                productos.visible=1 AND  productos.eliminado=0
                ORDER BY productos.cod_producto ASC ".$this->_query;

        $resul = $sql->ExecQuery($query);
        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['cod_categoria'] = $row['cod_categoria'];
            $res[$i]['cod_subcategoria'] = $row['cod_subcategoria'];
            $res[$i]['cod_producto'] = $row['cod_producto'];
            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['descripcion_marca'] = $row['descripcion_marca'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['categoria'] = $row['categoria'];
            $res[$i]['precio'] = $row['precio'];
            $res[$i]['destacado'] = $row['destacado'];
            $res[$i]['nuevo'] = $row['nuevo'];
            $res[$i]['oferta'] = $row['oferta'];

            if ($row['foto1'] == "") {
                $res[$i]['foto1'] = "no_disponible.jpg";
            } else {
                $res[$i]['foto1'] = $row['foto1'];
            }

            $i++;
        }

        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $res;
        return (array) $result;
    }
}
