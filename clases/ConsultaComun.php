<?php


class ConsultaComun
{
    public function TotalRegistros($sql,$query){
        $resul = $sql->ExecQuery($query);
        return $resul->num_rows;
    }

    function ObtenerCorrelativo($sql, $table)
    {
        $result=array();
        $query="SELECT max(id) as next_id FROM ".$table;
        $result=$sql->ExecQuery($query);
        $row=$sql->FetchArray($result);
        return $next_id = ($row['next_id'] > 0) ? $row['next_id'] + 1  : 1;

    }
}