<?php
class ImagenProducto
{

    function AgregarImagenProducto($sql, $codigo, $foto, $pdf, $video, $foto1, $foto2, $foto3)
	{
        $codigo = (isset($codigo) && $codigo)?substr(strval($codigo), 0, 50) :NULL;
		$foto = (isset($foto) && $foto)?substr(strval($foto), 0, 50) :NULL;
        $pdf = (isset($pdf) && $pdf)?substr(strval($pdf), 0, 200) :NULL;
        $video = (isset($video) && $video)?substr(strval($video), 0, 50) :NULL;
        $foto1 = (isset($foto1) && $foto1)?substr(strval($foto1), 0, 50) :NULL;
        $foto2 = (isset($foto2) && $foto2)?substr(strval($foto2), 0, 50) :NULL;
        $foto3 = (isset($foto3) && $foto3)?substr(strval($foto3), 0, 50) :NULL;
        
        $activo = 1;
        
        $codigo =  addslashes(utf8_encode($codigo));
        $foto = addslashes(utf8_encode($foto));
        $foto1 = addslashes(utf8_encode($foto1));
        $foto2 = addslashes(utf8_encode($foto2));
        $foto3 = addslashes(utf8_encode($foto3));
        
        
		$query="INSERT INTO productos_images (codigo, foto, pdf, video, foto1, foto2, foto3, activo) 
                VALUES ('$codigo', '$foto', '$pdf', '$video', '$foto1', '$foto2', '$foto3', '$activo')";
        
        $sql->ExecQuery($query);
	}
    
    function ModificarImagenProducto($sql, $codigo, $foto, $pdf, $video, $foto1, $foto2, $foto3)
	{
		$codigo = (isset($codigo) && $codigo)?substr(strval($codigo), 0, 50) :NULL;
		$foto = (isset($foto) && $foto)?substr(strval($foto), 0, 50) :NULL;
        $pdf = (isset($pdf) && $pdf)?substr(strval($pdf), 0, 200) :NULL;
        $video = (isset($video) && $video)?substr(strval($video), 0, 50) :NULL;
        $foto1 = (isset($foto1) && $foto1)?substr(strval($foto1), 0, 50) :NULL;
        $foto2 = (isset($foto2) && $foto2)?substr(strval($foto2), 0, 50) :NULL;
        $foto3 = (isset($foto3) && $foto3)?substr(strval($foto3), 0, 50) :NULL;
        
        $activo = 1;
        
        $codigo =  addslashes(utf8_encode($codigo));
        $foto = addslashes(utf8_encode($foto));
        $foto1 = addslashes(utf8_encode($foto1));
        $foto2 = addslashes(utf8_encode($foto2));
        $foto3 = addslashes(utf8_encode($foto3));
        
        
		$query="UPDATE productos_images 
                SET foto='$foto',
                    pdf='$pdf',
                    video='$video',
                    foto1='$foto1',
                    foto2='$foto2',
                    foto3='$foto3',
                    activo = '$activo'
                WHERE codigo='$codigo'";
        
        $sql->ExecQuery($query);
	}
    
    function ActualiarImagenProducto($sql, $codigo, $foto, $pdf, $video, $foto1, $foto2, $foto3)
	{
        $codigo = (isset($codigo) && $codigo)?substr(strval($codigo), 0, 50) :NULL;
		$foto = (isset($foto) && $foto)?substr(strval($foto), 0, 50) :NULL;
        $pdf = (isset($pdf) && $pdf)?substr(strval($pdf), 0, 200) :NULL;
        $video = (isset($video) && $video)?substr(strval($video), 0, 50) :NULL;
        $foto1 = (isset($foto1) && $foto1)?substr(strval($foto1), 0, 50) :NULL;
        $foto2 = (isset($foto2) && $foto2)?substr(strval($foto2), 0, 50) :NULL;
        $foto3 = (isset($foto3) && $foto3)?substr(strval($foto3), 0, 50) :NULL;
        
        $activo = 1;
        
        $codigo =  addslashes(utf8_encode($codigo));
        $foto = addslashes(utf8_encode($foto));
        $foto1 = addslashes(utf8_encode($foto1));
        $foto2 = addslashes(utf8_encode($foto2));
        $foto3 = addslashes(utf8_encode($foto3));
        
        
		$query="INSERT INTO productos_images (codigo, foto, pdf, video, foto1, foto2, foto3, activo) 
                VALUES ('$codigo', '$foto', '$pdf', '$video', '$foto1', '$foto2', '$foto3', '$activo')
                ON  DUPLICATE KEY UPDATE   
                    foto='$foto'";
        
        $sql->ExecQuery($query);
	}
    
    
    function ModificarImagenProductoFoto($sql, $codigo, $foto)
	{
		$codigo = (isset($codigo) && $codigo)?substr(strval($codigo), 0, 50) :NULL;
		$foto = (isset($foto) && $foto)?substr(strval($foto), 0, 50) :NULL;
        
        $query="UPDATE productos_images 
                SET foto='$foto'
                WHERE codigo='$codigo'";
        
        $sql->ExecQuery($query);
	}
    
    function ModificarImagenProductoPdf($sql, $codigo, $pdf)
	{
		$codigo = (isset($codigo) && $codigo)?substr(strval($codigo), 0, 50) :NULL;
		$pdf = (isset($pdf) && $pdf)?substr(strval($pdf), 0, 50) :NULL;
        
        $query="UPDATE productos_images 
                SET pdf='$pdf'
                WHERE codigo='$codigo'";
        
        $sql->ExecQuery($query);
	}
    
    
	function ObtenerImagenProducto($sql, $codigo)
	{
		$res = NULL;
        
        $query="SELECT codigo, foto, pdf, video, foto1, foto2, foto3, activo 
                FROM productos_images
                WHERE codigo = '$codigo'";
        
		$resul=$sql->ExecQuery($query);
		$i=1;
		while($row=$sql->FetchArray($resul))
		{
            $res[$i]['codigo'] = $row['codigo'];
            $res[$i]['foto'] = $row['foto'];
            $res[$i]['pdf'] = $row['pdf'];
			$res[$i]['video'] = $row['video'];
            $res[$i]['foto1'] = $row['foto1'];
            $res[$i]['foto2'] = $row['unfoto2idad'];
            $res[$i]['foto3'] = $row['foto3'];
			$res[$i]['activo'] = $row['activo'];
            $res[$i]['activo'] = $row['activo'];
			$i++;
		}
		return $res;
	}
}
