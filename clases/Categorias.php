<?php

class Categorias extends ConsultaComun
{
	function Agregar($sql, $categoria, $subcategoria, $nombre, $descripcion, $orden, $visible)
	{
		$query = "INSERT INTO categorias (categoria, subcategoria, nombre, descripcion, orden, visible) 
                VALUES ('$categoria', '$subcategoria', '$nombre', '$descripcion', $orden, $visible)";

		$sql->ExecQuery($query);
	}

	function Obtener($sql, $categoria, $subcategoria = '')
	{
		$res = NULL;
		if ($subcategoria != '') {
			$query = "SELECT categoria, subcategoria, nombre, descripcion, orden, visible, eliminado 
					FROM categorias 
					WHERE eliminado=0 AND categoria = '$categoria' AND subcategoria = '$subcategoria'";
		} else {
			$query = "SELECT categoria, subcategoria, nombre, descripcion, orden, visible, eliminado 
					FROM categorias 
					WHERE eliminado=0 AND categoria = '$categoria'";
		}

		$resul = $sql->ExecQuery($query);

		$i = 0;
		while ($row = $sql->FetchArray($resul)) {
			$res[$i]['categoria'] = $row['categoria'];
			$res[$i]['subcategoria'] = $row['subcategoria'];
			$res[$i]['nombre'] = $row['nombre'];
			$res[$i]['descripcion'] = $row['descripcion'];
			$res[$i]['orden'] = $row['orden'];
			$res[$i]['visible'] = $row['visible'];
			$i++;
		}
		return $res;
	}

	function Modificar($sql, $categoria, $subcategoria, $categoria_new, $subcategoria_new, $nombre, $descripcion, $orden, $visible)
	{
		$query = "UPDATE categorias 
                SET categoria='$categoria_new', 
					subcategoria='$subcategoria_new', 
					nombre='$nombre',
                    descripcion='$descripcion',
                    orden='$orden',
                    visible='$visible'
                WHERE categoria='$categoria' AND subcategoria='$subcategoria'";

		$sql->ExecQuery($query);
	}

	function Eliminar($sql, $categoria, $subcategoria)
	{
		$query = "UPDATE categorias 
                SET eliminado=1
                WHERE categoria='$categoria' AND subcategoria='$subcategoria'";

		$sql->ExecQuery($query);
	}

	function Desactivar($sql, $categoria, $subcategoria)
	{
		$query = "UPDATE categorias 
                SET visible=0
                WHERE categoria='$categoria' AND subcategoria='$subcategoria'";


		$sql->ExecQuery($query);
	}



	function Listado($sql, $categoria = '')
	{
		$res = NULL;

		/*
        if ($categoria == '') {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible,
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
					FROM categorias C
					WHERE C.eliminado=0 AND C.subcategoria = '' and C.visible = 1
					ORDER BY C.orden, C.nombre ASC ";
        } else {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible, 
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.cod_subcategoria = C.subcategoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
						FROM categorias C
						WHERE C.eliminado=0 AND C.categoria = '$categoria' AND C.subcategoria != '' and C.visible = 1
						ORDER BY C.orden, C.nombre ASC ";
        }
		*/


		$query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible, C.eliminado,
					CASE WHEN C.subcategoria = 0 
					THEN  
						(	SELECT count(P.cod_producto) 
							FROM productos P 
							INNER JOIN categorias CC ON CC.categoria = P.cod_categoria AND CC.subcategoria = P.cod_subcategoria
							WHERE P.cod_categoria = C.categoria AND P.visible=1 AND P.eliminado =0 AND CC.visible=1 AND CC.eliminado =0
						)
					ELSE
						(
							SELECT count(P.cod_producto) 
							FROM productos P 
							WHERE P.cod_categoria = C.categoria AND P.cod_subcategoria = C.subcategoria AND C.visible=1 AND C.eliminado =0 AND P.visible=1 AND P.eliminado =0
						)
					END AS num_productos
					
					FROM categorias C
					WHERE C.eliminado=0 AND C.visible=1
					ORDER BY CASE WHEN C.subcategoria=0 THEN C.orden END  ASC";


		$resul = $sql->ExecQuery($query);

		while ($row = $sql->FetchArray($resul)) {

			$cat = $row['categoria'];
			$subcat = $row['subcategoria'];

			$res[$cat][$subcat]['categoria'] = $row['categoria'];
			$res[$cat][$subcat]['subcategoria'] = $row['subcategoria'];
			$res[$cat][$subcat]['nombre'] = $row['nombre'];
			$res[$cat][$subcat]['descripcion'] = $row['descripcion'];
			$res[$cat][$subcat]['orden'] = $row['orden'];
			$res[$cat][$subcat]['visible'] = $row['visible'];
			$res[$cat][$subcat]['eliminado'] = $row['eliminado'];
			$res[$cat][$subcat]['num_productos'] = $row['num_productos'];
		}

		return (array) $res;
	}




	/********************************************************************************************************************** Por Borrar Inicio * / 


	function ListadoAleatorio($sql, $categoria = '')
	{
		$res = NULL;

		if ($categoria == '') {
			$query = "SELECT categoria, subcategoria, nombre, descripcion, orden, visible, eliminado 
                FROM categorias
                WHERE eliminado=0 and subcategoria = ''
                ORDER BY RAND() LIMIT 10";
		} else {
			$query = "SELECT categoria, subcategoria, nombre, descripcion, orden, visible, eliminado 
                FROM categorias 
                WHERE eliminado=0 and categoria = '$categoria' AND subcategoria != ''
                ORDER BY RAND() LIMIT 10";
		}

		$resul = $sql->ExecQuery($query);
		$i = 0;
		while ($row = $sql->FetchArray($resul)) {
			$res[$i]['categoria'] = $row['categoria'];
			$res[$i]['subcategoria'] = $row['subcategoria'];
			$res[$i]['nombre'] = $row['nombre'];
			$res[$i]['descripcion'] = $row['descripcion'];
			$res[$i]['orden'] = $row['orden'];
			$res[$i]['visible'] = $row['visible'];
			$i++;
		}
		return $res;
	}
	 function ListadoAdmin($sql, $categoria = '', $limit = 15, $page = 1)
    {
        $res = NULL;
        $this->_limit   = $limit;
        $this->_page    = $page;

        if ($categoria == '') {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible,
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
					FROM categorias C
					WHERE C.eliminado=0 AND C.subcategoria = ''
					ORDER BY C.orden, C.nombre ASC ";
        } else {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible, 
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.cod_subcategoria = C.subcategoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
						FROM categorias C
						WHERE C.eliminado=0 AND C.categoria = '$categoria' AND C.subcategoria != ''
						ORDER BY C.orden, C.nombre ASC ";
        }

        $this->_total = $this::TotalRegistros($sql,$query);

        $desde = ( ( $this->_page - 1 ) * $this->_limit );
        $this->_query = " LIMIT " . $desde . ", $this->_limit";

        if ($categoria == '') {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible,
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
					FROM categorias C
					WHERE C.eliminado=0 AND C.subcategoria = ''
					ORDER BY C.orden, C.nombre ASC ".$this->_query;
        } else {
            $query = "SELECT C.categoria, C.subcategoria, C.nombre, C.descripcion, C.orden, C.visible, 
						( SELECT count(P.cod_producto) FROM productos P WHERE P.cod_categoria = C.categoria AND P.cod_subcategoria = C.subcategoria AND P.visible='1' AND P.eliminado ='0' ) as num_productos
						FROM categorias C
						WHERE C.eliminado=0 AND C.categoria = '$categoria' AND C.subcategoria != ''
						ORDER BY C.orden, C.nombre ASC ".$this->_query;
        }

        $resul = $sql->ExecQuery($query);

        $i = 0;
        while ($row = $sql->FetchArray($resul)) {
            $res[$i]['categoria'] = $row['categoria'];
            $res[$i]['subcategoria'] = $row['subcategoria'];
            $res[$i]['nombre'] = $row['nombre'];
            $res[$i]['descripcion'] = $row['descripcion'];
            $res[$i]['orden'] = $row['orden'];
            $res[$i]['visible'] = $row['visible'];
            $res[$i]['num_productos'] = $row['num_productos'];
            $i++;
        }
        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $res;

        return (array) $result;
	}
	
/********************************************************************************************************************** Por Borrar Fin */
}
