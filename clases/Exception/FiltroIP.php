<?php

/**
 * Clase: Base_Exception_FiltroIP 
 * Descripcion: Excepcion lanzada cuando ocurre un error en algunas de las clases 
 * Métodos: 
 *
 * @package Base_Exception
 */

class Base_Exception_FiltroIP extends Base_Exception
{
  		    
public function __construct($message='No es posible obtener infomacion del Filtro', $code = 0, Exception $previous = null)
{parent::__construct($message, $code,$previous);}
}
