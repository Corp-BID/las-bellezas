<?php
/**
 * Clase: Base_Exception_AccionDiferida
 * Descripcion: Excepcion lanzada cuando ocurre un error en algunas de las clases
 * Métodos:
 *
 * @package Base_Exception
 */

class Base_Exception_Session extends Base_Exception
{
 const IP_INVALIDO = 30;
 const NAVEGADOR_INVALIDO = 31;
 const EXPIRADO = 32;
 
 public function __construct($message='No es posible inicializar la Session', $code = 0, Exception $previous = null)
 { parent::__construct($message, $code,$previous);}
}

