<?php
/**
 * Clase: exception_Archivo 
 * Descripcion: Excepcion lanzada cuando ocurre un error en algunas de las clases 
 * Métodos: 
 *
 * @author Asdrubal A Dominguez S
 * @package Base_Exception
 */

class Base_Exception_AutoLoader extends Base_Exception
{
 public function __construct($message='No es posible obtener la clase', $code = 0, Exception $previous = null)
 {parent::__construct($message, $code,$previous);}
}