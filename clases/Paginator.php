<?php

class Paginator
{
    private $_limit;
    private $_page;
    private $_total;

    public function __construct( $total, $limit ,$page)
    {
        $this->_limit = PER_PAGE;
        $this->_total = $total;
        $this->_page  = $page;
    }

    public function createLinks( $links, $list_class,  $params = [] ) {

        $last       = ceil( $this->_total / $this->_limit );

        $link1 = (isset($params['value1']) != '') ? '&'.$params['param1'].'='.$params['value1'] : '';
        $link2 = (isset($params['value2']) != '') ? '&'.$params['param2'].'='.$params['value2'] : '';
        $link3 = (isset($params['value3']) != '') ? '&'.$params['param3'].'='.$params['value3'] : '';

        $links_join = $link1 . $link2 . $link3 ;

        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;

        $html       = '<div class="example"><nav aria-label="Page navigation example"><ul class="' . $list_class . '">';

        if ( $start > 1 ) {
            $html   .= '<li><a href="?page=1' . $links_join.'">1</a></li>';
            $html   .= '<li class="disabled page-link"><span>...</span></li>';
        }

        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "page-item active" : "";
            $html   .= '<li class="' . $class . '"><a class="page-link" href="?page=' . $i .$links_join . '">' . $i . '</a></li>';
        }

        if ( $end < $last ) {
            $html   .= '<li class="disabled page-link"><span>...</span></li>';
            $html   .= '<li><a class="page-link" href="?page=' . $last . $links_join . '">' . $last . '</a></li>';
        }

        $html       .= '</ul></div></nav>';

        return $html;
    }
}