<?php
//require_once('Mail.php');
//require_once('Mail/mime.php');

class MailInt
{
	private $puerto;
	private $servidor;
 
	function __construct($servidor, $puerto) 
	{
		$this->puerto = $puerto;
		$this->servidor =$servidor;
	}
 
	// Funcion usada para envia correo, retorna true en caso de envio exitoso del correo,
	// false en caso contrario
	public function enviarHTML($desde,$destinatario,$titulo,$mensajehtml)
	{
		$mime =& new Mail_mime("\n");
		$mime->setTXTBody(strip_tags($mensajehtml));
		$mime->setHTMLBody($mensajehtml);
		//   $mime->addAttachment('fichero_adjunto.zip', 'application/zip'); // Esto es para agregarle un attachment al correo
		$body = $mime->get();
		$hdrs = $mime->headers(array('From' => $desde, 'Subject' => $titulo, 'Date'=> date("r")));
		
		$mail=& Mail::factory('smtp',array('host'=>"{$this->servidor}",'port'=>"{$this->puerto}",'auth'=>false));
		if (!PEAR::isError($mail->send($destinatario, $hdrs, $body)))
		{return true;}
		else
		{return false;}
	}
}